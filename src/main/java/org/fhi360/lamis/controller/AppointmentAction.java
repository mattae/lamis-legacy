/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.controller;

import java.sql.PreparedStatement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;

public class AppointmentAction extends ActionSupport implements Preparable {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    
    @Override
    public void prepare() {
        request = ServletActionContext.getRequest();
        session = request.getSession();
    }
//setUploaded(0); 
    public String appointmentDate() {
        try {
            jdbcUtil = new JDBCUtil();
            if(!request.getParameter("dateNextClinic").isEmpty() && !request.getParameter("dateNextRefill").isEmpty()) {
                query = "UPDATE patient SET date_next_clinic = ?, date_next_refill = ?, send_message = ?, time_stamp = NOW() WHERE facility_id = ? AND patient_id = ?";                
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setDate(1, DateUtil.parseStringToSqlDate(request.getParameter("dateNextClinic"), "MM/dd/yyyy")); // convert java util date to java sql date format           
                preparedStatement.setDate(2, DateUtil.parseStringToSqlDate(request.getParameter("dateNextRefill"), "MM/dd/yyyy"));            
                preparedStatement.setInt(3, (request.getParameterMap().containsKey("enableSms") == true)? Integer.parseInt(request.getParameter("sendMessage")) : 0);
                preparedStatement.setLong(4, (Long) session.getAttribute("facilityId")); 
                preparedStatement.setLong(5, Long.parseLong(request.getParameter("patientId")));
            } 
            else {
                if(!request.getParameter("dateNextClinic").isEmpty()) {
                    query = "UPDATE patient SET date_next_clinic = ?, send_message = ?, time_stamp = NOW() WHERE facility_id = ? AND patient_id = ?";
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setDate(1, DateUtil.parseStringToSqlDate(request.getParameter("dateNextClinic"), "MM/dd/yyyy")); // convert java util date to java sql date format           
                    preparedStatement.setInt(2, (request.getParameterMap().containsKey("enableSms") == true)? Integer.parseInt(request.getParameter("sendMessage")) : 0);
                    preparedStatement.setLong(3, (Long) session.getAttribute("facilityId")); 
                    preparedStatement.setLong(4, Long.parseLong(request.getParameter("patientId")));
                }
                else {
                    if(!request.getParameter("dateNextRefill").isEmpty()) {
                        query = "UPDATE patient SET date_next_refill = ?, send_message = ?, time_stamp = NOW() WHERE facility_id = ? AND patient_id = ?";                                        
                        preparedStatement = jdbcUtil.getStatement(query);
                        preparedStatement.setDate(1, DateUtil.parseStringToSqlDate(request.getParameter("dateNextRefill"), "MM/dd/yyyy")); // convert java util date to java sql date format           
                        preparedStatement.setInt(2, (request.getParameterMap().containsKey("enableSms") == true)? Integer.parseInt(request.getParameter("sendMessage")) : 0);
                        preparedStatement.setLong(3, (Long) session.getAttribute("facilityId")); 
                        preparedStatement.setLong(4, Long.parseLong(request.getParameter("patientId")));                        
                    }                    
                }
            }
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return SUCCESS;
    }
    
}