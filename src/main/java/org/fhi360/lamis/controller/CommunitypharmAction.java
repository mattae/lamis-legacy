/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.controller;

import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ModelDriven;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.fhi360.lamis.dao.hibernate.CommunitypharmDAO;
import org.fhi360.lamis.model.Communitypharm;
import org.fhi360.lamis.service.sms.SmsMessageSender;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.builder.CommunitypharmListBuilder;

public class CommunitypharmAction extends ActionSupport implements ModelDriven, Preparable {
    private Long communitypharmId;
    private Communitypharm communitypharm;
    private Set<Communitypharm> communitypharmes = new HashSet<Communitypharm>(0);
    
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    private ArrayList<Map<String, String>> pharmList = new ArrayList<Map<String, String>>();
    private Map<String, String> pharmMap = new TreeMap<String, String>();
    
    @Override    
    public void prepare() {
        request = ServletActionContext.getRequest();
        session = request.getSession();
    }

    @Override
    public Object getModel() {
        communitypharm = new Communitypharm();
        return communitypharm;
    }    

    public String saveCommunitypharm() {
        CommunitypharmDAO.save(communitypharm);
        return SUCCESS;
    }
    
    public String updateCommunitypharm() {
        CommunitypharmDAO.update(communitypharm);
        return SUCCESS;
    }
    
    public String deleteCommunitypharm() {
        CommunitypharmDAO.delete(communitypharmId);
        return SUCCESS;
    }
    
    public String retrieveCommunitypharm() {
        long stateId = Long.parseLong(ServletActionContext.getRequest().getParameter("stateId"));
        long lgaId = Long.parseLong(ServletActionContext.getRequest().getParameter("lgaId"));  
        query = "SELECT * FROM communitypharm WHERE state_id  = " + stateId + " AND lga_id = " + lgaId + " ORDER BY pharmacy";        
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String communitypharmId = Long.toString(resultSet.getLong("communitypharm_id"));
                String pharmacy = resultSet.getString("pharmacy");
                pharmMap.put(communitypharmId, pharmacy);
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return SUCCESS;
    }
    
    public String retrieveCommunitypharmById() {
        query = "SELECT * FROM communitypharm WHERE communitypharm_id  = " + Long.parseLong(request.getParameter("communitypharmId"));         
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            new CommunitypharmListBuilder().buildCommunitypharmList(resultSet);
            pharmList = new CommunitypharmListBuilder().retrieveCommunitypharmList();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return SUCCESS;        
    }
    
    // Retrieve a Communitypharm in database
    public String findCommunitypharm() {
        communitypharmId = Long.parseLong(request.getParameter("communitypharmId"));        
        communitypharm = CommunitypharmDAO.find(communitypharmId);          
        return SUCCESS;
    }

    /**
     * @return the pharmMap
     */
    public Map<String, String> getPharmMap() {
        return pharmMap;
    }

    /**
     * @param pharmMap the pharmMap to set
     */
    public void setPharmMap(Map<String, String> pharmMap) {
        this.pharmMap = pharmMap;
    }

    /**
     * @return the pharmList
     */
    public ArrayList<Map<String, String>> getPharmList() {
        return pharmList;
    }

    /**
     * @param pharmList the pharmList to set
     */
    public void setPharmList(ArrayList<Map<String, String>> pharmList) {
        this.pharmList = pharmList;
    }
    
}