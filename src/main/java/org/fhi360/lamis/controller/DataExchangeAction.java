/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.controller;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import org.fhi360.lamis.service.ExportService;
import org.fhi360.lamis.service.ImportService;
import org.fhi360.lamis.service.InternetConnectionService;
import com.opensymphony.xwork2.Preparable;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.resource.ApplicationUpdatesWebserviceInvocator;
import org.fhi360.lamis.resource.SyncWebserviceInvocator;
import org.fhi360.lamis.resource.UploadWebserviceInvocator;
import org.fhi360.lamis.service.UploadFolderService;
import org.fhi360.lamis.service.barcode.QRCodeService;

public class DataExchangeAction extends ActionSupport implements Preparable{
    private HttpSession session;
    private String status;
    private String fileName;
    
    @Override    
    public void prepare() {
        session = ServletActionContext.getRequest().getSession();
    }

    public String exportData() {
        setFileName(ExportService.buildXml());
        return SUCCESS;
    }
    
    public String importData() {
        if(session.getAttribute("fileName") != null) {
            ImportService.processXml();            
            session.removeAttribute("fileName");        
        }
        return SUCCESS;
    }
    
    public String uploadData() {
        setStatus(UploadWebserviceInvocator.invokeUploadService());
        return SUCCESS;
    }

    public String syncData() {
        setStatus(SyncWebserviceInvocator.invokeSyncService());
        return SUCCESS;
    }
    
    public String generateBarcode() {
        setStatus(new QRCodeService().generate());
        return SUCCESS;
    }

    public String checkUpdates() {
        setStatus(ApplicationUpdatesWebserviceInvocator.invokeUpdatesService());
        return SUCCESS;
    }
    
    public String internetConnection() {
        setStatus(InternetConnectionService.getInstance().getConnectionStatus());
        return SUCCESS;
    }

    public String getDisableRecordsAll() {
        setStatus(UploadFolderService.getDisableRecordsAll());
        return SUCCESS;
    }
    
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    
}