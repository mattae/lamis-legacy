/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.controller;

import com.opensymphony.xwork2.ActionSupport;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.builder.BiometricReportListBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author User10
 */
public class FingerprintReportAction extends ActionSupport implements ServletRequestAware {

    Logger LOG = LoggerFactory.getLogger(FingerprintReportAction.class);
    HttpServletRequest request;
    private String location = "/reports/patient/biometric_enrollment.jasper";
    private Map<String, Object> parameters = new HashMap<>();
    private List<Map<String, Object>> dataSource;

    public String getLocation() {
        return location;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public List<Map<String, Object>> getDataSource() {
        return dataSource;
    }
    
    
    public String enrollmentReport() {
        Long facilityId = (Long) request.getSession().getAttribute("facilityId");
        try {
            JDBCUtil util = new JDBCUtil();
            PreparedStatement statement = util.getStatement(
                    "select other_names, surname, unique_id, p.gender, age, age_unit, "
                            + "p.address, p.phone, current_status from biometric b "
                            + "inner join patient p on b.patient_id = p.id_uuid where "
                            + "enrollment_date between ? and ? and facility_id = ?");
            statement.setDate(1, new java.sql.Date(getDateParameter("from").getTime()));
            statement.setDate(2, new java.sql.Date(getDateParameter("to").getTime()));
            statement.setLong(3, facilityId);
            ResultSet rs = statement.executeQuery();
            dataSource = BiometricReportListBuilder.buildEnrollmentList(rs);
            statement = util.getStatement("select f.name as facility, s.name as state, l.name as lga from "
                    + "facility f inner join lga l on f.lga_id = l.lga_id inner join state s "
                    + "on f.state_id = s.state_id where f.facility_id = ?");
            statement.setLong(1,facilityId);
            rs = statement.executeQuery();
            while(rs.next()) {
                parameters.put("facilityName", rs.getString("facility"));
                parameters.put("state", rs.getString("state"));
                parameters.put("lga", rs.getString("lga"));
            }
            util.getConnection().close();
        } catch (SQLException | IOException ex) {
            LOG.error("Error: {}", ex.getMessage());
        }
        return SUCCESS;
    }

    public String duplicateReport() {
        Long facilityId = (Long) request.getSession().getAttribute("facilityId");
        try {
            JDBCUtil util = new JDBCUtil();
            PreparedStatement statement = util.getStatement(
                    "select other_names, surname, unique_id, p.gender, age, age_unit, "
                            + "p.address, p.phone, current_status from patient p "
                            + "left join biometric b on p.id_uuid = b.patient_id where "
                            + "enrollment_date between ? and ? and facility_id = ? and b.gender is null");
            statement.setDate(1, new java.sql.Date(getDateParameter("from").getTime()));
            statement.setDate(2, new java.sql.Date(getDateParameter("to").getTime()));
            statement.setLong(3, facilityId);
            ResultSet rs = statement.executeQuery();
            dataSource = BiometricReportListBuilder.buildEnrollmentList(rs);
             statement = util.getStatement("select f.name as facility, s.name as state, l.name as lga from "
                    + "facility f inner join lga l on f.lga_id = l.lga_id inner join state s "
                    + "on f.state_id = s.state_id where f.facility_id = ?");
            statement.setLong(1,facilityId);
            rs = statement.executeQuery();
            while(rs.next()) {
                parameters.put("facilityName", rs.getString("facility"));
                parameters.put("state", rs.getString("state"));
                parameters.put("lga", rs.getString("lga"));
            }
            util.getConnection().close();
        } catch (SQLException | IOException ex) {
            LOG.error("Error: {}", ex.getMessage());
        }
        return SUCCESS;
    }

    private Date getDateParameter(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return format.parse(request.getParameter(date));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.request = hsr;
    }
}
