/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.controller;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.FileUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

/**
 * @author User10
 */
public class HTSDataConverter {
    private final HttpServletRequest request;
    private final HttpSession session;
    private static final Logger LOG = LoggerFactory.getLogger(HTSDataConverter.class);

    public HTSDataConverter() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession();
    }

    public synchronized String convertExcel() {
        String fileName = "";
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        Long userId = (Long) session.getAttribute("userId");
        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
        workbook.setCompressTempFiles(true);
        Sheet sheet = workbook.createSheet();

        try {
            JDBCUtil jdbcu = new JDBCUtil();
            Statement st = jdbcu.getConnection().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM HTS");

            ResultSetMetaData rsMetaData = rs.getMetaData();
            int numberOfColumns = rsMetaData.getColumnCount();
            int rownum = 0;
            int cellnum = 0;
            Row row = sheet.createRow(rownum++);
            for (int i = 1; i < numberOfColumns + 1; i++) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(rsMetaData.getColumnName(i));
            }
            cellnum = 0;
            while (rs.next()) {
                row = sheet.createRow(rownum++);
                for (int i = 1; i < numberOfColumns + 1; i++) {
                    Cell cell = row.createCell(cellnum++);
                    Object value = rs.getObject(rsMetaData.getColumnName(i));
                    cell.setCellValue(value != null ? value.toString() : null);
                }
                if (rownum % 100 == 0) {
                    ((SXSSFSheet) sheet).flushRows(100);
                }
            }

            String directory = contextPath + "transfer/";
            FileUtil fileUtil = new FileUtil();
            fileUtil.makeDir(directory);
            fileUtil.makeDir(request.getContextPath() + "/transfer/");

            fileName = request.getParameterMap().containsKey("facilityIds") ?
                    "hts_" + request.getParameter("state").toLowerCase() + "_" + userId + ".xlsx" :
                    "hts_" + userId + ".xlsx";
            FileOutputStream outputStream = new FileOutputStream(new File(directory + fileName));
            workbook.write(outputStream);
            outputStream.close();
            workbook.dispose();  // dispose of temporary files backing this workbook on disk

        } catch (Exception ignored) {

        }
        return "transfer/" + fileName;
    }

}
