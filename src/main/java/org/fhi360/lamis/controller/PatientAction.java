/**
 *
 * @author AALOZIE
 */
package org.fhi360.lamis.controller;

import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ModelDriven;
import java.io.IOException;
import java.sql.SQLException;
import org.fhi360.lamis.model.Patient;
import org.fhi360.lamis.dao.hibernate.PatientDAO;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.model.Facility;
import org.fhi360.lamis.service.DeleteService;
import org.fhi360.lamis.service.MonitorService;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.builder.PatientListBuilder;
import org.fhi360.lamis.model.dto.PatientObjHandler;
import org.fhi360.lamis.utility.PatientNumberNormalizer;
import org.fhi360.lamis.utility.Scrambler;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.fhi360.lamis.interceptor.updater.PatientHospitalNumberUpdater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PatientAction extends ActionSupport implements ModelDriven, Preparable {

    static final Logger LOG = LoggerFactory.getLogger(PatientAction.class);
    private Long facilityId;
    private Facility facility;
    private Long patientId;
    private Long userId;
    private Patient patient;
    private Set<Patient> patients = new HashSet<>(0);
    private List<Object> biometricStatus;
    private String status;

    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    private ArrayList<Map<String, String>> patientList = new ArrayList<Map<String, String>>();
    private ArrayList<Map<String, String>> messageList = new ArrayList<Map<String, String>>();
    private Map<String, String> patientMap = new HashMap<String, String>();

    @Override
    public void prepare() {
        request = ServletActionContext.getRequest();
        session = request.getSession();
        facilityId = (Long) session.getAttribute("facilityId");
        userId = (Long) session.getAttribute("userId");
    }

    @Override
    public Object getModel() {
        facility = new Facility();
        patient = new Patient();
        return patient;
    }

    // Save new patient to database
    public List<Object> getBiometricStatus() {
        return biometricStatus;
    }

    public String biometricStatus() {
        if (session.getAttribute("biometrics") != null) {
            biometricStatus = (List<Object>) session.getAttribute("biometrics");
        }
        session.removeAttribute("biometrics");
        return SUCCESS;
    }

    public String savePatient() {
        LOG.info("Save patient1");
        if (facilityId > 0L) {
            LOG.info("Save patient2: {}", facilityId);
            if (request.getParameter("dateBirth").isEmpty()) {
                Date date = DateUtil.parseStringToDate(request.getParameter("dateRegistration"), "MM/dd/yyyy");
                int number = Integer.parseInt(request.getParameter("age"));
                String unit = request.getParameter("ageUnit");
                date = DateUtil.addYearMonthDay(date, -number, unit);
                patient.setDateBirth(date);
            }
            patient.setSendMessage(0);
            facility.setFacilityId(facilityId);
            patient.setFacility(facility);
            patient.setUserId(userId);
            patient.setTimeStamp(new java.sql.Timestamp(new Date().getTime()));
            scrambleIdentifier();
            Long id = PatientDAO.save(patient);
            patient.setPatientId(id);
            session.setAttribute("patient", patient); //store in session for AfterUpdateInterceptor to access

            try {
                //Get biometric status
                JDBCUtil jdbcu = new JDBCUtil();
                PreparedStatement statement = jdbcu.getStatement("select count(*) as b_count from biometric b "
                        + "inner join patient p on p.id_uuid = b.patient_id where p.patient_id = ? and facility_id = ?");
                statement.setLong(1, id);
                statement.setLong(2, facilityId);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    Long count = rs.getLong("b_count");
                    LOG.info("Biometric status: {}", count);
                    List<Object> biometrics = new ArrayList<>();
                    biometrics.add(id);
                    biometrics.add(count > 0);
                    session.setAttribute("biometrics", biometrics);
                }
            } catch (SQLException | IOException ex) {
                LOG.error("{}", ex.getMessage());
            }
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    // Update patient in database
    public String updatePatient() {
        if (facilityId > 0L) {
            if (request.getParameter("dateBirth").isEmpty()) {
                Date date = DateUtil.parseStringToDate(request.getParameter("dateRegistration"), "MM/dd/yyyy");
                int number = Integer.parseInt(request.getParameter("age"));
                String unit = request.getParameter("ageUnit");
                date = DateUtil.addYearMonthDay(date, -number, unit);
                patient.setDateBirth(date);
            }
            facility.setFacilityId(facilityId);
            patient.setFacility(facility);
            patient.setPatientId(Long.parseLong(request.getParameter("patientId")));
            patient.setUserId(userId);
            patient.setTimeStamp(new java.sql.Timestamp(new Date().getTime()));
            patient = PatientObjHandler.restore(patient);
            scrambleIdentifier();
            PatientDAO.update(patient);
            session.setAttribute("patient", patient); //store in session for AfterUpdateInterceptor to access
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    static String extractPostRequestBody(HttpServletRequest request) throws IOException {
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            LOG.info("POST");
            Scanner s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
        LOG.info("Method: {}", request.getMethod());
        return "";
    }

    // Delete patient from database
    public String deletePatient() throws IOException {
        String entityId = request.getParameter("hospitalNum");
        MonitorService.logEntity(entityId, "patient", 3);
        DeleteService deleteService = new DeleteService();
        if (facilityId > 0L) {
            deleteService.deletePatient(facilityId, Long.parseLong(request.getParameter("patientId")));
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    // Retrieve a patient in database
    public String findPatient() {
        try {
            if (request.getParameter("patientId") != "" && request.getParameter("patientId") != null) {

                patientId = Long.parseLong(request.getParameter("patientId"));
                patient = PatientDAO.find(patientId);
                PatientObjHandler.store(patient);
                new PatientListBuilder().buildPatientList(patient);
                patientList = new PatientListBuilder().retrievePatientList();

                session.setAttribute("fromPrescription", "false");
                session.setAttribute("fromLabTest", "false");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return SUCCESS;
    }

    // Retrieve a patient with drug prescriptions from database
    public String findPatientWithPrescription() {
        try {
            System.out.println("Patient ID in pharm is: " + request.getParameter("patientId"));
            if (!request.getParameter("patientId").equals("") && request.getParameter("patientId") != null) {
                patientId = Long.parseLong(request.getParameter("patientId"));
                patient = PatientDAO.find(patientId);
                PatientObjHandler.store(patient);
                new PatientListBuilder().buildPatientList(patient);
                patientList = new PatientListBuilder().retrievePatientList();

                session.setAttribute("fromPrescription", "true");
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        return SUCCESS;
    }

    // Retrieve a patient with drug prescriptions from database
    public String findPatientWithLabTest() {
        try {
            if (!request.getParameter("patientId").equals("") && request.getParameter("patientId") != null) {
                patientId = Long.parseLong(request.getParameter("patientId"));
                patient = PatientDAO.find(patientId);
                PatientObjHandler.store(patient);
                new PatientListBuilder().buildPatientList(patient);
                patientList = new PatientListBuilder().retrievePatientList();
                System.out.println(patientList);

                session.setAttribute("fromLabTest", "true");
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        return SUCCESS;
    }

    public String findPatientByNumber() {
        try {
            query = "SELECT * FROM patient WHERE facility_id = " + facilityId + " AND TRIM(LEADING '0' FROM hospital_num) = '" + PatientNumberNormalizer.unpadNumber(request.getParameter("hospitalNum")) + "'";
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            new PatientListBuilder().buildPatientList(resultSet, "");
            patientList = new PatientListBuilder().retrievePatientList();
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return SUCCESS;
    }

    public String findPatientByNames() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Scrambler scrambler = new Scrambler();
        String surname = scrambler.scrambleCharacters(request.getParameter("surname"));
        String otherNames = scrambler.scrambleCharacters(request.getParameter("otherNames"));
        String dateBirth = dateFormat.format(DateUtil.parseStringToDate(request.getParameter("dateBirth"), "MM/dd/yyyy"));
        String gender = request.getParameter("gender");
        try {
            query = "SELECT * FROM patient WHERE facility_id = ? AND surname = '" + surname + "' AND other_names = '" + otherNames + "' AND gender = '" + gender + "' AND date_birth = '" + dateBirth + "'";
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, facilityId);
            resultSet = preparedStatement.executeQuery();

            Map<String, String> map = new HashMap<String, String>();
            if (resultSet.next()) {
                map.put("hospitalNum", resultSet.getString("hospital_num"));
                map.put("surname", scrambler.unscrambleCharacters(resultSet.getString("surname")));
                map.put("otherNames", scrambler.unscrambleCharacters(resultSet.getString("other_names")));
                map.put("gender", resultSet.getString("gender"));
                map.put("age", Integer.toString(resultSet.getInt("age")));
                map.put("ageUnit", resultSet.getString("age_unit"));
                map.put("address", scrambler.unscrambleCharacters(resultSet.getString("address")));
                map.put("state", resultSet.getString("state"));
                map.put("lga", resultSet.getString("lga"));
            }
            messageList.add(map);
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return SUCCESS;
    }

    public String updateHospitalNumber() {
        try {
            query = "SELECT patient_id FROM patient WHERE facility_id = ? AND hospital_num = ?";
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, facilityId);
            preparedStatement.setString(2, request.getParameter("newHospitalNum"));
            resultSet = preparedStatement.executeQuery();

            Map<String, String> map = new HashMap<String, String>();
            if (resultSet.next()) {
                map.put("message", "Not Updated");
            } else {
                new PatientHospitalNumberUpdater().changeHospitalNum(request.getParameter("hospitalNum"), request.getParameter("newHospitalNum"), facilityId);
                String entity = request.getParameter("hospitalNum") + "#" + request.getParameter("newHospitalNum");
                MonitorService.logEntity(entity, "patient", 4);
                map.put("message", "Updated");
            }
            messageList.add(map);
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return SUCCESS;
    }

    public String retrievePatientList() {
        patientList = new PatientListBuilder().retrievePatientList();
        return SUCCESS;
    }

    public void scrambleIdentifier() {
        Scrambler scrambler = new Scrambler();
        patient.setSurname(scrambler.scrambleCharacters(patient.getSurname()));
        patient.setOtherNames(scrambler.scrambleCharacters(patient.getOtherNames()));
        patient.setAddress(scrambler.scrambleCharacters(patient.getAddress()));
        patient.setPhone(scrambler.scrambleNumbers(patient.getPhone()));
        patient.setNextKin(scrambler.scrambleCharacters(patient.getNextKin()));
        patient.setAddressKin(scrambler.scrambleCharacters(patient.getAddressKin()));
        patient.setPhoneKin(scrambler.scrambleNumbers(patient.getPhoneKin()));
    }

    public String clearPatientDetail() {
        ServletActionContext.getRequest().getSession().setAttribute("patientDetail", null);
        return SUCCESS;
    }

    public String retrievePatientDetail() {
        if (ServletActionContext.getRequest().getSession().getAttribute("patientDetail") != null) {
            patientMap = (Map) ServletActionContext.getRequest().getSession().getAttribute("patientDetail");
        }
        return SUCCESS;
    }

    /**
     * @return the patientId
     */
    public Long getPatientId() {
        return patientId;
    }

    /**
     * @param patientId the patientId to set
     */
    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    /**
     * @return the patient
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * @param patient the patient to set
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     * @return the patients
     */
    public Set<Patient> getPatients() {
        return patients;
    }

    /**
     * @param patients the patients to set
     */
    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    /**
     * @return the facilityId
     */
    public Long getFacilityId() {
        return facilityId;
    }

    /**
     * @param facilityId the facilityId to set
     */
    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the facility
     */
    public Facility getFacility() {
        return facility;
    }

    /**
     * @param facility the facility to set
     */
    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    /**
     * @return the patientList
     */
    public ArrayList<Map<String, String>> getPatientList() {
        return patientList;
    }

    /**
     * @param patientList the patientList to set
     */
    public void setPatientList(ArrayList<Map<String, String>> patientList) {
        this.patientList = patientList;
    }

    /**
     * @return the messageList
     */
    public ArrayList<Map<String, String>> getMessageList() {
        return messageList;
    }

    /**
     * @param messageList the messageList to set
     */
    public void setMessageList(ArrayList<Map<String, String>> messageList) {
        this.messageList = messageList;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @param patients the patients to set
     */
    /**
     * @return the patientMap
     */
    public Map<String, String> getPatientMap() {
        return patientMap;
    }

    /**
     * @param patientMap the patientMap to set
     */
    public void setPatientMap(Map<String, String> patientMap) {
        this.patientMap = patientMap;
    }

}
