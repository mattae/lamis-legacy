/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.controller;

import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.fhi360.lamis.service.SyncUtilService;
import org.fhi360.lamis.utility.FileUtil;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author MEdor
 */
public class SyncDownloadAction extends ActionSupport implements ServletRequestAware {

    private HttpServletRequest request;
    private String status;
    private String fileName;

    private InputStream stream;

    public SyncDownloadAction() {
    }

    public InputStream getStream() {
        return stream;
    }

    public String downloadSync() {
        SyncUtilService.syncFolder();
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");

        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);  // turn off auto-flushing and accumulate all rows in memory
        Sheet sheet = workbook.createSheet();

        try {
            JDBCUtil jdbcUtil = new JDBCUtil();

            String query = "select facility_id, facility_name, sync_date from facility_sync where "
                    + "last_modified between ? and ? order by facility_id";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setDate(1, new java.sql.Date(getDateParameter("from").getTime()));
            preparedStatement.setDate(2, new java.sql.Date(getDateParameter("to").getTime()));
            int rownum = 0;
            int cellnum = 0;
            Row row = sheet.createRow(rownum++);
            Cell cell = row.createCell(cellnum++);
            cell.setCellValue("Facility Id");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Facility Name");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Upload Date");
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy");

                while (resultSet.next()) {
                    cellnum = 0;
                    row = sheet.createRow(rownum++);
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet.getLong("facility_id"));

                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet.getString("facility_name"));

                    cell = row.createCell(cellnum++);
                    Date date = resultSet.getDate("sync_date");
                    cell.setCellValue(format.format(date));
                }
            }
            String directory = contextPath + "sync/";
            FileUtil fileUtil = new FileUtil();
            fileUtil.makeDir(directory);

            fileName = "Sync.xlsx";
            try (FileOutputStream outputStream = new FileOutputStream(new File(directory + fileName))) {
                workbook.write(outputStream);
            }
            workbook.dispose();
            stream = new FileInputStream(new File(directory + fileName));
        } catch (SQLException | IOException | IllegalStateException exception) {
        }
        return SUCCESS;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private Date getDateParameter(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return format.parse(request.getParameter(date));
        } catch (ParseException ex) {
        }
        return null;
    }

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        request = hsr;
    }
}
