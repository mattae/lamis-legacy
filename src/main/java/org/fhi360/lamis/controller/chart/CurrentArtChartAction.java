/**
 *
 * @author aalozie
 */

package org.fhi360.lamis.controller.chart;
import org.fhi360.lamis.utility.ChartUtil;
import org.fhi360.lamis.utility.DateUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.ObjectSerializer;

public class CurrentArtChartAction extends ActionSupport {
    private List<String> categories;
    private List<Map<String, Object>> series;
    private String title;
    private String subtitle;
    private String titleForYAxis;
    
    private ObjectSerializer objectSerializer;
    private ArrayList<Map<String, Object>> analysisList;

    public String getChartData() {
        ChartUtil chartUtil = new ChartUtil();
        objectSerializer = ObjectSerializer.getInstance();
        
        categories = new ArrayList<String>();
        series = new ArrayList<Map<String, Object>>();
        
        List<Integer> data1 = new ArrayList<Integer>();
        List<Integer> data2 = new ArrayList<Integer>();
    
        long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
        String fileName = Long.toString(facilityId);
        
        //Deserialize performance values for this facility
        try {
            analysisList = (ArrayList<Map<String, Object>>) objectSerializer.deserialize(fileName);                       
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        
        Date startDate = DateUtil.addMonth(new Date(), -6); 
        Date endDate = DateUtil.addMonth(new Date(), -1);
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");        
            String periodLabel = (String) period.get("periodLabel");
            
            Map values = getCount(4, month, year);
            if(values != null) {
                data1.add((Integer) values.get("male"));
                data2.add((Integer) values.get("female"));    
            }            
            categories.add(periodLabel);
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", "Male");
        map.put("data", data1);
        series.add(map);

        map = new HashMap<String, Object>();
        map.put("name", "Female");
        map.put("data", data2);
        series.add(map);
        
        String periodStart = DateUtil.parseDateToString(startDate, "MMMMM yyyy");
        String periodEnd = DateUtil.parseDateToString(endDate, "MMMMM yyyy");
        title = "Current on ART Male vs Female ";
        subtitle = periodStart + " to " + periodEnd;
        setTitle(title);
        setSubtitle(subtitle);
        setCategories(categories);
        setTitleForYAxis("Number");
        setSeries(series);
             
        return SUCCESS;
    }

    private Map getCount(int indicatorId, int month, int year) {
        for(Map map : analysisList) {
            if(indicatorId == (Integer) map.get("indicatorId") && month == (Integer) map.get("month") && year == (Integer) map.get("year")) {
                return map;
            }
        }
        return null;
    }
    
    /**
     * @return the categories
     */
    public List<String> getCategories() {
        return categories;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    /**
     * @return the series
     */
    public List<Map<String, Object>> getSeries() {
        return series;
    }

    /**
     * @param series the series to set
     */
    public void setSeries(List<Map<String, Object>> series) {
        this.series = series;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the titleForYAxis
     */
    public String getTitleForYAxis() {
        return titleForYAxis;
    }

    /**
     * @param titleForYAxis the titleForYAxis to set
     */
    public void setTitleForYAxis(String titleForYAxis) {
        this.titleForYAxis = titleForYAxis;
    }
    
}
