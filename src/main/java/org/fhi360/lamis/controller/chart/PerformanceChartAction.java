/**
 *
 * @author aalozie
 */
package org.fhi360.lamis.controller.chart;

import org.fhi360.lamis.report.PerformanceIndicatorProcessor;
import org.fhi360.lamis.utility.ChartUtil;
import org.fhi360.lamis.utility.DateUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fhi360.lamis.report.indicator.PerformanceIndicators;

public class PerformanceChartAction extends ActionSupport {
    private Integer reportingMonthBegin;
    private Integer reportingYearBegin;
    private Integer reportingMonthEnd;
    private Integer reportingYearEnd;
    private Integer indicatorId;
    
    private List<String> categories;
    private List<Map<String, Object>> series;
    private String title;
    private String subtitle;
    private String titleForYAxis;
    
    private String[] indicator = new String[23];
    
    public String getChartData() {        
        ChartUtil chartUtil = new ChartUtil();
        PerformanceIndicatorProcessor indicatorProcessor = new PerformanceIndicatorProcessor();
        indicator = new PerformanceIndicators().initialize();

        //structures to hold data for the chart
        categories = new ArrayList<String>();
        series = new ArrayList<Map<String, Object>>();
        List<Double> data = new ArrayList<Double>();
        
        int id = getIndicatorId().intValue();
        Date startDate = chartUtil.getDate(getReportingMonthBegin().intValue() -1, getReportingYearBegin().intValue()); // month is zero-based
        Date endDate = chartUtil.getDate(getReportingMonthEnd().intValue() -1, getReportingYearEnd().intValue());

        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);

        for (int i = 0; i <= monthsBetween; i++) {
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            String periodLabel = (String) period.get("periodLabel");
            double percentage = indicatorProcessor.process(month, year, id-1);
            data.add(percentage);
            categories.add(periodLabel);
        }	

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", "Series 1");
        map.put("data", data);
        series.add(map);

        // get title for chart
        String periodStart = DateUtil.parseDateToString(startDate, "MMMMM yyyy");
        String periodEnd = DateUtil.parseDateToString(endDate, "MMMMM yyyy");
        title = indicator[id-1];
        subtitle = periodStart + " - " + periodEnd;
        setTitle(title);
        setSubtitle(subtitle);
        setCategories(categories);
        setTitleForYAxis("Percentage");
        setSeries(series);

        return SUCCESS;
    }
  
    /**
     * @return the reportingMonthBegin
     */
    public Integer getReportingMonthBegin() {
        return reportingMonthBegin;
    }

    /**
     * @param reportingMonthBegin the reportingMonthBegin to set
     */
    public void setReportingMonthBegin(Integer reportingMonthBegin) {
        this.reportingMonthBegin = reportingMonthBegin;
    }

    /**
     * @return the reportingYearBegin
     */
    public Integer getReportingYearBegin() {
        return reportingYearBegin;
    }

    /**
     * @param reportingYearBegin the reportingYearBegin to set
     */
    public void setReportingYearBegin(Integer reportingYearBegin) {
        this.reportingYearBegin = reportingYearBegin;
    }

    /**
     * @return the reportingMonthEnd
     */
    public Integer getReportingMonthEnd() {
        return reportingMonthEnd;
    }

    /**
     * @param reportingMonthEnd the reportingMonthEnd to set
     */
    public void setReportingMonthEnd(Integer reportingMonthEnd) {
        this.reportingMonthEnd = reportingMonthEnd;
    }

    /**
     * @return the reportingYearEnd
     */
    public Integer getReportingYearEnd() {
        return reportingYearEnd;
    }

    /**
     * @param reportingYearEnd the reportingYearEnd to set
     */
    public void setReportingYearEnd(Integer reportingYearEnd) {
        this.reportingYearEnd = reportingYearEnd;
    }

    /**
     * @return the indicatorId
     */
    public Integer getIndicatorId() {
        return indicatorId;
    }

    /**
     * @param indicatorId the indicatorId to set
     */
    public void setIndicatorId(Integer indicatorId) {
        this.indicatorId = indicatorId;
    }

    /**
     * @return the categories
     */
    public List<String> getCategories() {
        return categories;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    /**
     * @return the series
     */
    public List<Map<String, Object>> getSeries() {
        return series;
    }

    /**
     * @param series the series to set
     */
    public void setSeries(List<Map<String, Object>> series) {
        this.series = series;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the titleForYAxis
     */
    public String getTitleForYAxis() {
        return titleForYAxis;
    }

    /**
     * @param titleForYAxis the titleForYAxis to set
     */
    public void setTitleForYAxis(String titleForYAxis) {
        this.titleForYAxis = titleForYAxis;
    }


}
