/**
 *
 * @author aalozie
 */

package org.fhi360.lamis.controller.chart;

import org.fhi360.lamis.utility.ChartUtil;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.ObjectSerializer;

public class StatusSummaryChartAction extends ActionSupport{
    private List<Map<String, Object>> series;
    private String title;

    private ObjectSerializer objectSerializer;
    private ArrayList<Map<String, Object>> analysisList;
    
    public String getChartData() {
        ChartUtil chartUtil = new ChartUtil();
        objectSerializer = ObjectSerializer.getInstance();
        
        series = new ArrayList<Map<String, Object>>();
        
        // structures to hold data for the chart        
        List<Object> data = new ArrayList<Object>();
        List<Object> hivContainer = new ArrayList<Object>();
        List<Object> transferInContainer = new ArrayList<Object>();
        List<Object> transferOutContainer = new ArrayList<Object>();
        List<Object> restartContainer = new ArrayList<Object>();
        List<Object> stopContainer = new ArrayList<Object>();
        List<Object> lostContainer = new ArrayList<Object>();
        List<Object> deadContainer = new ArrayList<Object>();
        List<Object> othersContainer = new ArrayList<Object>();
        Map<String, Object> startContainer = new HashMap<String, Object>(); // to be selected by default on the interface

        long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");        
        String fileName = Long.toString(facilityId);
        
        //Deserialize performance values for this facility
        try {
            analysisList = (ArrayList<Map<String, Object>>) objectSerializer.deserialize(fileName);                       
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        
        int month = DateUtil.getMonth(new Date());
        int year = DateUtil.getYear(new Date());
        Map values = getCount(2, month, year);
        if(values != null) { 
            hivContainer.add("HIV+ non ART");
            hivContainer.add((Integer) values.get("HIV+ non ART"));

            startContainer.put("name", "ART Start");
            startContainer.put("y", (Integer) values.get("ART Start"));
            startContainer.put("sliced", true);
            startContainer.put("selected", true);

            restartContainer.add("Restart");
            restartContainer.add((Integer) values.get("Restart"));

            transferInContainer.add("Transfer In");
            transferInContainer.add((Integer) values.get("Transfer In"));

            transferOutContainer.add("Transfer Out");
            transferOutContainer.add((Integer) values.get("Transfer Out"));

            stopContainer.add("Stop Treatment");
            stopContainer.add((Integer) values.get("Stop Treatment"));

            lostContainer.add("LTFU");
            lostContainer.add((Integer) values.get("LTFU"));

            deadContainer.add("Known Death");
            deadContainer.add((Integer) values.get("Known Death"));

            othersContainer.add("Others");
            othersContainer.add((Integer) values.get("Others"));
        }
        
        data.add(hivContainer);
        data.add(startContainer);
        data.add(restartContainer);
        data.add(transferInContainer);
        data.add(transferOutContainer);
        data.add(stopContainer);
        data.add(lostContainer);
        data.add(deadContainer);
        data.add(othersContainer);
        
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("type", "pie");
        map.put("name", "Status Summary");
        map.put("data", data);
        series.add(map);
        title = "Summary of patient status";
        return SUCCESS;
    }

    private Map getCount(int indicatorId, int month, int year) {
        for(Map map : analysisList) {
            if(indicatorId == (Integer) map.get("indicatorId") && month == (Integer) map.get("month") && year == (Integer) map.get("year")) {
                return map;
            }
        }
        return null;
    }

    /**
     * @return the series
     */
    public List<Map<String, Object>> getSeries() {
        return series;
    }

    /**
     * @param series the series to set
     */
    public void setSeries(List<Map<String, Object>> series) {
        this.series = series;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
