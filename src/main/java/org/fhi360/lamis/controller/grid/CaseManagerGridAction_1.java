package org.fhi360.lamis.controller.grid;///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.fhi360.lamis.controller.grid;
//
//import static com.opensymphony.xwork2.Action.SUCCESS;
//import com.opensymphony.xwork2.ActionSupport;
//import com.opensymphony.xwork2.Preparable;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.util.ArrayList;
//import java.util.Map;
//import java.util.TreeMap;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//import org.apache.struts2.ServletActionContext;
//import org.fhi360.lamis.utility.Constants;
//import org.fhi360.lamis.utility.JDBCUtil;
//import org.fhi360.lamis.utility.builder.CaseManagerListBuilder;
//
///**
// *
// * @author user10
// */
//public class CaseManagerGridAction_1 extends ActionSupport implements Preparable {
//    private Integer page;
//    private Integer rows;
//    private String sidx;
//    private String sord;
//    private Integer totalpages;
//    private Integer currpage;
//    private Integer totalrecords;
//
//    private HttpServletRequest request;
//    private HttpSession session;
//    private String query;
//    private JDBCUtil jdbcUtil;
//    private PreparedStatement preparedStatement;
//    private ResultSet resultSet;
//    private Long facilityId;
//    
//    private ArrayList<Map<String, String>> caseManagerList = new ArrayList<Map<String, String>>();
//    private ArrayList<Map<String, String>> caseManagerClientsList = new ArrayList<Map<String, String>>();
//    private ArrayList<Map<String, String>> clientSearchList = new ArrayList<Map<String, String>>();
//    private Map<String, String> clientsStatusCountMap = new TreeMap<String, String>();
//    
//    @Override    
//    public void prepare() {
//        request = ServletActionContext.getRequest();
//        session = request.getSession();
//    }
//    
//    public String caseManagerGrid() {
//        //setTotalpages(1);
//        //setCurrpage(1);
//        //setTotalrecords(20);
//        try {
//            // obtain a JDBC connect and execute query
//            jdbcUtil = new JDBCUtil();           
//            // fetch the required records from the database
//            facilityId = (Long) session.getAttribute("facilityId");
//            query = "SELECT * FROM casemanager WHERE facility_id = ?"; 
//            preparedStatement = jdbcUtil.getStatement(query);
//            preparedStatement.setLong(1, facilityId);
//            resultSet = preparedStatement.executeQuery();
//            
//            new CaseManagerListBuilder().buildCaseManagerList(resultSet);
//            caseManagerList = new CaseManagerListBuilder().retrieveCaseManagerList();
//            resultSet = null;
//        }
//        catch (Exception exception) {
//            resultSet = null;
//            jdbcUtil.disconnectFromDatabase();  //disconnect from database
//            exception.printStackTrace();
//        }
//        return SUCCESS;
//    }
//    
//    public String caseManagerClientsGrid() {
////        setTotalpages(1);
////        setCurrpage(1);
////        setTotalrecords(20);
//        try {
//            // obtain a JDBC connect and execute query
//            jdbcUtil = new JDBCUtil();           
//            // fetch the required records from the database
//            facilityId = (Long) session.getAttribute("facilityId");
//            String casemanagerId = request.getParameter("casemanagerId");
//            if(casemanagerId != "")
//                query = "SELECT * FROM patient WHERE facility_id = "+facilityId+" AND casemanager_id = "+Integer.parseInt(casemanagerId);
//            else
//                return SUCCESS;
//            
//            preparedStatement = jdbcUtil.getStatement(query);
//            resultSet = preparedStatement.executeQuery();
//            
//            new CaseManagerListBuilder().buildCaseManagerClientsList(resultSet);
//            caseManagerClientsList = new CaseManagerListBuilder().retrieveCaseManagerClientsList();
//            resultSet = null;
//        }
//        catch (Exception exception) {
//            resultSet = null;
//            jdbcUtil.disconnectFromDatabase();  //disconnect from database
//            exception.printStackTrace();
//        }
//        return SUCCESS;
//    }
//    
//    public void initClientSearch(){
//        //Create a temporary Table to hold values.
//        try{
//            String client_drop = "DROP TABLE clients IF EXISTS";
//            jdbcUtil = new JDBCUtil();
//            preparedStatement = jdbcUtil.getStatement(client_drop);
//            preparedStatement.executeUpdate(); 
//            
//            //(
//            String client = "CREATE TEMPORARY TABLE IF NOT EXISTS clients (facility_id bigint, patient_id bigint, hospital_num varchar(25), surname varchar(45), other_names varchar(75), gender varchar(7), state varchar(75), lga varchar(150), date_birth date, age int(11), age_unit varchar(30), address varchar(100), date_started date, current_viral_load double, current_cd4 double, current_cd4p double, current_status varchar(75), status int(11), casemanager_id int(11))";           
//            //String client = "CREATE TEMPORARY TABLE IF NOT EXISTS clients (facility_id INT, patient_id INT, hospital_num TEXT, surname TEXT, other_names TEXT, gender TEXT, date_birth DATE, address TEXT, date_started DATE, current_viral_load INT, current_cd4 INT, current_status TEXT, status INT)";
//            preparedStatement = jdbcUtil.getStatement(client);
//            preparedStatement.executeUpdate();
//            
//            //ACTIVE CLIENTS                
//            String active_clients = "INSERT INTO clients (facility_id, patient_id, hospital_num, surname, other_names, gender, state, lga, date_birth, age, age_unit, address, date_started, current_viral_load, current_cd4, current_cd4p, current_status, status, casemanager_id)"
//                    + "SELECT facility_id, patient_id, hospital_num, surname, other_names, gender, state, lga, date_birth, age, age_unit, address, date_started, last_viral_load, last_cd4, last_cd4p, current_status, 0, casemanager_id FROM patient where current_status NOT IN ('Known Death', 'ART Transfer Out', 'Pre-ART Transfer Out')";
//                    
//            jdbcUtil = new JDBCUtil();
//            preparedStatement = jdbcUtil.getStatement(active_clients);
//            preparedStatement.executeUpdate();
//            
//            //GROUP VARIOUS CLIENTS BY STATUS
//            String query_client_status = "SELECT * FROM clients";
//            jdbcUtil = new JDBCUtil();
//            preparedStatement = jdbcUtil.getStatement(query_client_status);
//            resultSet = preparedStatement.executeQuery();
//            
//            //Iterate through the result set...
//            while(resultSet.next()){
//                long patientId = resultSet.getLong("patient_id");
//                String internal_query = "";
//                String currentStatus = resultSet.getString("current_status");
//                String currentViralLoad = resultSet.getObject("current_viral_load") == null ? "" :  resultSet.getDouble("current_viral_load") == 0.0 ? "0" : Double.toString(resultSet.getDouble("current_viral_load"));
//                String currentCd4 = resultSet.getObject("current_cd4") == null ? "0" :  resultSet.getDouble("current_cd4") == 0.0 ? "0" : Double.toString(resultSet.getDouble("current_cd4"));
//                String currentCd4p = resultSet.getObject("current_cd4p") == null ? "0" :  resultSet.getDouble("current_cd4p") == 0.0 ? "0" : Double.toString(resultSet.getDouble("current_cd4p"));
//
//                if(!currentStatus.equals("HIV+ non ART") && !currentStatus.equals("Pre-ART Transfer In")){
//                    
//                    if(!currentStatus.equals("Stopped Treatment") && !currentStatus.equals("Lost to Follow Up")){
//                        if(currentViralLoad != ""){
//                            if(Double.parseDouble(currentViralLoad) < 1000){ //stable = 1
//                                internal_query = "UPDATE clients SET status = "+Constants.CaseManager.STABLE+" WHERE patient_id = "+patientId;
//                            }else if(Double.parseDouble(currentViralLoad) >= 1000){    
//                                internal_query = "UPDATE clients SET status = "+Constants.CaseManager.UNSTABLE+" WHERE patient_id = "+patientId;
//                            }
//                        }else if(currentViralLoad == ""){
//                            if(currentCd4 != "0" && baselineCd4(patientId) > 0.0){
//                                if(Double.parseDouble(currentCd4) >= 250.0){ //stable
//                                    internal_query = "UPDATE clients SET status = "+Constants.CaseManager.STABLE+" WHERE patient_id = "+patientId;
//                                }else{
//                                    if(Double.parseDouble(currentCd4) < 250.0 && (Double.parseDouble(currentCd4) < baselineCd4(patientId))){//stable
//                                        internal_query = "UPDATE clients SET status = "+Constants.CaseManager.STABLE+" WHERE patient_id = "+patientId;
//                                    }else if(Double.parseDouble(currentCd4) < 250.0 && (Double.parseDouble(currentCd4) > baselineCd4(patientId))){//unstable
//                                        internal_query = "UPDATE clients SET status = "+Constants.CaseManager.UNSTABLE+" WHERE patient_id = "+patientId;
//                                    }
//                                }
//                            }else if(currentCd4 == "0" && currentCd4p != "0" && baselineCd4(patientId) > 0.0){
//                                if(Double.parseDouble(currentCd4p) >= 250.0){
//                                    internal_query = "UPDATE clients SET status = "+Constants.CaseManager.STABLE+" WHERE patient_id = "+patientId;
//                                }else{
//                                    if(Double.parseDouble(currentCd4p) < 250.0 && (Double.parseDouble(currentCd4p) < baselineCd4(patientId))){//stable
//                                        internal_query = "UPDATE clients SET status = "+Constants.CaseManager.STABLE+" WHERE patient_id = "+patientId;
//                                    }else if(Double.parseDouble(currentCd4p) < 250.0 && (Double.parseDouble(currentCd4p) > baselineCd4(patientId))){//unstable
//                                        internal_query = "UPDATE clients SET status = "+Constants.CaseManager.UNSTABLE+" WHERE patient_id = "+patientId;
//                                    }
//                                }
//                            }else if(currentCd4 == "0" && currentCd4p == "0"){
//                                if(clinicVisits(patientId) >= 5){ //Stable
//                                    internal_query = "UPDATE clients SET status = "+Constants.CaseManager.STABLE+" WHERE patient_id = "+patientId;                               
//                                }else if(clinicVisits(patientId) < 5){                               
//                                    if(isRecentTxNew(patientId)){ //Recent TX_New = 3
//                                        internal_query = "UPDATE clients SET status = "+Constants.CaseManager.RECENT_TX_NEW+" WHERE patient_id = "+patientId;
//                                    }else{ //Unstable = 2
//                                        internal_query = "UPDATE clients SET status = "+Constants.CaseManager.UNSTABLE+" WHERE patient_id = "+patientId;
//                                    }
//                                }                               
//                            }
//                        }  
//                    }else{
//                        internal_query = "UPDATE clients SET status = "+Constants.CaseManager.UNSTABLE+" WHERE patient_id = "+patientId;                                    
//                    }
//                }
//                else{//Pre-ART = 4
//                    internal_query = "UPDATE clients SET status = "+Constants.CaseManager.PRE_ART+" WHERE patient_id = "+patientId;
//                }
//                JDBCUtil internalJdbcUtil = new JDBCUtil();
//                PreparedStatement internalPreparedStatement = internalJdbcUtil.getStatement(internal_query);
//                internalPreparedStatement.executeUpdate();
//            }
//        }catch(Exception ex){
//            ex.printStackTrace();
//        }
//    }
//    
//    public String clientSearchGrid() {
//        try {
//            //Initilaize the Client Search
//            initClientSearch();
//            //Call for Grid..
//            facilityId = (Long) session.getAttribute("facilityId");
//            query = "SELECT DISTINCT clients.* FROM clients JOIN clinic ON clients.patient_id = clinic.patient_id WHERE clients.facility_id = "+facilityId;
//            
//            if(request.getParameter("hospitalNum").trim().isEmpty()){
//            if(!request.getParameter("gender").trim().isEmpty() && !request.getParameter("gender").trim().equals("--All--")) query += " AND gender = '" + request.getParameter("gender") + "'";
//            //TODO: Arrange Age Group:
//            if(!request.getParameter("ageGroup").trim().isEmpty()  && !request.getParameter("ageGroup").trim().equals("0")){
//                String ageGroup = request.getParameter("ageGroup").trim();
//                String[] ageRange = ageGroup.split("-");
//                query += " AND DATEDIFF('YEAR', date_birth, CURDATE()) >= " + Integer.parseInt(ageRange[0].toString()) + " AND DATEDIFF('YEAR', date_birth, CURDATE()) <= " + Integer.parseInt(ageRange[1].toString());
//            }
//            if(!request.getParameter("state").trim().isEmpty()) query += " AND state = '" + request.getParameter("state") + "'";
//            if(!request.getParameter("lga").trim().isEmpty() && request.getParameter("lga") != null) query += " AND lga = '" + request.getParameter("lga") + "'";
//            //Pregnancy Status
//            if(!request.getParameter("pregnancyStatus").trim().isEmpty() && !request.getParameter("pregnancyStatus").trim().equals("--All--")){
//                //System.out.println("Pregnancy Status is: "+request.getParameter("pregnancyStatus"));
//                if(Integer.parseInt(request.getParameter("pregnancyStatus")) == 1)
//                    query += " AND clinic.pregnant = 1";    
//                else if(Integer.parseInt(request.getParameter("pregnancyStatus")) == 2)
//                    query += " AND clinic.breastfeeding = 1";
//            }
//
//            if(!request.getParameter("categoryId").trim().isEmpty()) {
//                String categoryId = (request.getParameter("categoryId").trim());
//                if (categoryId.equals("0")) {
//                    query += " AND (status = "+Constants.CaseManager.STABLE+" OR status = "+Constants.CaseManager.UNSTABLE+" OR status = "+Constants.CaseManager.RECENT_TX_NEW+" OR status = "+Constants.CaseManager.PRE_ART+")"; 
//                }
//                else if (categoryId.equals("1")) {
//                    query += " AND status = "+Constants.CaseManager.STABLE; 
//                }
//                else if (categoryId.equals("2")) {
//                    query += " AND status = "+Constants.CaseManager.UNSTABLE; 
//                }
//                else if (categoryId.equals("3")) {
//                    query += " AND status = "+Constants.CaseManager.RECENT_TX_NEW; 
//                }
//                else if (categoryId.equals("4")) {
//                    query += " AND status = "+Constants.CaseManager.PRE_ART; 
//                }
//            }
//            
//            if(!request.getParameter("showAssigned").trim().isEmpty()) 
//                if(Boolean.valueOf(request.getParameter("showAssigned")) == true)
//                    query += " AND casemanager_id IS NOT NULL";
//                else
//                    query += " AND casemanager_id IS NULL ";
//            }
//            
//            else if(!request.getParameter("hospitalNum").trim().isEmpty()){
//                //System.out.println("Hospital Number is: "+request.getParameter("hospitalNum"));
//                query += " AND hospital_num LIKE '%" + request.getParameter("hospitalNum") + "%'";
//            }
//
//            query += " ORDER BY surname ASC";
//            //System.out.println(query);
//            
//            preparedStatement = jdbcUtil.getStatement(query);
//            resultSet = preparedStatement.executeQuery();
//            
//            new CaseManagerListBuilder().buildClientSearchList(resultSet);
//            clientSearchList = new CaseManagerListBuilder().retrieveClientSearchList();
//            resultSet = null;
//        }
//        catch (Exception exception) {
//            resultSet = null;
//            jdbcUtil.disconnectFromDatabase();  //disconnect from database
//            exception.printStackTrace();
//        }
//        return SUCCESS;
//    }
//    
//    public String getAssignedClientsTreatmentStatus(){
//        try {
//            //Initilaize the Client Search
//            initClientSearch();
//            //Call for Grid..
//            facilityId = (Long) session.getAttribute("facilityId");
//            Integer casemanagerId ;
//            if(request.getParameter("casemanagerId") != null && !request.getParameter("casemanagerId").trim().isEmpty()){
//                casemanagerId = Integer.valueOf(request.getParameter("casemanagerId"));
//            
//                String internalQuery = "SELECT DISTINCT status as treatment_status, COUNT(status) as treatment_status_count FROM clients WHERE facility_id = "+facilityId+" AND casemanager_id = "+casemanagerId+" GROUP BY status oRDER BY status ASC";
//
//                JDBCUtil internalJdbcUtil = new JDBCUtil();
//                PreparedStatement internalPreparedStatement = internalJdbcUtil.getStatement(internalQuery);
//                ResultSet internalResultSet = internalPreparedStatement.executeQuery();
//
//                while(internalResultSet.next()){
//                    Integer treatmentStatus = internalResultSet.getInt("treatment_status");
//                    Integer treatmentStatusCount = internalResultSet.getInt("treatment_status_count");
//                    if(treatmentStatus == 1)
//                        clientsStatusCountMap.put("stable", Integer.toString(treatmentStatusCount));
//                    else if(treatmentStatus == 2)
//                        clientsStatusCountMap.put("unstable", Integer.toString(treatmentStatusCount));
//                    else if(treatmentStatus == 3)
//                        clientsStatusCountMap.put("tx_new", Integer.toString(treatmentStatusCount));
//                    else if(treatmentStatus == 4)
//                        clientsStatusCountMap.put("preart", Integer.toString(treatmentStatusCount));
//                }
//            }
//        }
//        catch (Exception exception) {
//            resultSet = null;
//            jdbcUtil.disconnectFromDatabase();  //disconnect from database
//            exception.printStackTrace();
//            return ERROR;
//        }
//        
//        return SUCCESS;
//    }
//    
//    private double baselineCd4(long patientId){
//        double baselineCd4 = 0.0;
//        String query = "SELECT cd4 FROM clinic WHERE patient_id = "+patientId+" AND commence = 1";
//        JDBCUtil jdbcUtil = null;
//        try{
//            jdbcUtil = new JDBCUtil();
//            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                String baselineCd4Str = resultSet.getObject("cd4") == null ? "0" :  resultSet.getDouble("cd4") == 0.0 ? "0" : Double.toString(resultSet.getDouble("cd4"));
//                if(baselineCd4Str != "0"){
//                    baselineCd4= Double.parseDouble(baselineCd4Str);
//                }  
//            }
//        }catch(Exception ex){
//            ex.printStackTrace();
//            jdbcUtil.disconnectFromDatabase();
//        }
//        
//        return baselineCd4;
//    }
//    
//    private int clinicVisits(long patientId){
//        int clinicVisitCount = 0;
//        String query = "SELECT count(*) AS count FROM clinic WHERE patient_id = "+patientId+" AND date_visit >= DATEADD('MONTH', -12, CURDATE()) AND date_visit <= CURDATE()";
//        JDBCUtil jdbcUtil = null;
//        try{
//            jdbcUtil = new JDBCUtil();
//            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                clinicVisitCount = resultSet.getInt("count");
//            }
//        }catch(Exception ex){
//            jdbcUtil.disconnectFromDatabase();
//            ex.printStackTrace();
//        }
//        
//        return clinicVisitCount;
//    }
//    
//    private boolean isRecentTxNew(long patientId){
//        boolean recentTxNew = false;
//        String query = "SELECT patient_id FROM patient WHERE patient_id = "+patientId+" AND DATEDIFF('MONTH', date_started, CURDATE()) < 12";
//        JDBCUtil jdbcUtil = null;
//        try{
//            jdbcUtil = new JDBCUtil();
//            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                recentTxNew = true;
//            }
//        }catch(Exception ex){
//            jdbcUtil.disconnectFromDatabase();
//            ex.printStackTrace();
//        }
//        
//        return recentTxNew;
//    }
//    
//    /*
//     * @return the page
//     */
//    public Integer getPage() {
//        return page;
//    }
//
//    /**
//     * @param page the page to set
//     */
//    public void setPage(Integer page) {
//        this.page = page;
//    }
//
//    /**
//     * @return the limit
//     */
//    public Integer getRows() {
//        return rows;
//    }
//
//    /**
//     * @param limit the limit to set
//     */
//    public void setRows(Integer rows) {
//        this.rows = rows;
//    }
//
//    /**
//     * @return the sidx
//     */
//    public String getSidx() {
//        return sidx;
//    }
//
//    /**
//     * @param sidx the sidx to set
//     */
//    public void setSidx(String sidx) {
//        this.sidx = sidx;
//    }
//
//    /**
//     * @return the sord
//     */
//    public String getSord() {
//        return sord;
//    }
//
//    /**
//     * @param sord the sord to set
//     */
//    public void setSord(String sord) {
//        this.sord = sord;
//    }
//
//    /**
//     * @return the totalpages
//     */
//    public Integer getTotalpages() {
//        return totalpages;
//    }
//
//    /**
//     * @param totalpages the totalpages to set
//     */
//    public void setTotalpages(Integer totalpages) {
//        this.totalpages = totalpages;
//    }
//
//    /**
//     * @return the currpage
//     */
//    public Integer getCurrpage() {
//        return currpage;
//    }
//
//    /**
//     * @param currpage the currpage to set
//     */
//    public void setCurrpage(Integer currpage) {
//        this.currpage = currpage;
//    }
//
//    /**
//     * @return the totalrecords
//     */
//    public Integer getTotalrecords() {
//        return totalrecords;
//    }
//
//    /**
//     * @param totalrecords the totalrecords to set
//     */
//    public void setTotalrecords(Integer totalrecords) {
//        this.totalrecords = totalrecords;
//    }
//
//    /**
//     * @return the laboratoryList
//     */
//    public ArrayList<Map<String, String>> getCaseManagerList() {
//        return caseManagerList;
//    }
//
//    /**
//     * @param laboratoryList the laboratoryList to set
//     */
//    public void setCaseManagerList(ArrayList<Map<String, String>> caseManagerList) {
//        this.caseManagerList = caseManagerList;
//    }  
//
//    public ArrayList<Map<String, String>> getCaseManagerClientsList() {
//        return caseManagerClientsList;
//    }
//
//    public void setCaseManagerClientsList(ArrayList<Map<String, String>> caseManagerClientsList) {
//        this.caseManagerClientsList = caseManagerClientsList;
//    }
//
//    public ArrayList<Map<String, String>> getClientSearchList() {
//        return clientSearchList;
//    }
//
//    public void setClientSearchList(ArrayList<Map<String, String>> clientSearchList) {
//        this.clientSearchList = clientSearchList;
//    }
//
//    public Map<String, String> getClientsStatusCountMap() {
//        return clientsStatusCountMap;
//    }
//
//    public void setClientsStatusCountMap(Map<String, String> clientsStatusCountMap) {
//        this.clientsStatusCountMap = clientsStatusCountMap;
//    }
//
//    
//}
