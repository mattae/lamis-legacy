/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.controller.grid;

import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ActionSupport;
import org.fhi360.lamis.utility.JDBCUtil;

public class DispenserGridAction_version1 extends ActionSupport implements Preparable {
    private Integer page;
    private Integer rows;
    private String sidx;
    private String sord;   
    private Integer totalpages;
    private Integer currpage;
    private Integer totalrecords;

    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    private ArrayList<Map<String, String>> dispenserList = new ArrayList<Map<String, String>>();
    
    @Override    
    public void prepare() {
        request = ServletActionContext.getRequest();
        session = request.getSession();
    }
    
    public String dispenserGrid() {
        //setTotalpages(1);
        //setCurrpage(1);
        //setTotalrecords(20);

        String ids = request.getParameter("regimenIds"); 
        String[] idList =  ids.split(",");
        try {
           //obtain a JDBC connect and execute query
           jdbcUtil = new JDBCUtil();
           for(String id : idList) {
                // fetch the details of selected lab test
                query = "SELECT drug.name, drug.strength, drug.morning, drug.afternoon, drug.evening, regimendrug.regimendrug_id, regimendrug.regimen_id, regimendrug.drug_id, regimen.regimentype_id "
                        + " FROM drug JOIN regimendrug ON regimendrug.drug_id = drug.drug_id JOIN regimen ON regimendrug.regimen_id = regimen.regimen_id WHERE regimendrug.regimen_id = ?"; 
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setLong(1, Long.parseLong(id));
                resultSet = preparedStatement.executeQuery();

                // loop through resultSet for each row and put into Map
                while (resultSet.next()) {
                    String regimentypeId = Long.toString(resultSet.getLong("regimentype_id")); 
                    String regimendrugId = Long.toString(resultSet.getLong("regimendrug_id")); 
                    String regimenId = Long.toString(resultSet.getLong("regimen_id")); 
                    String drugId = Long.toString(resultSet.getLong("drug_id")); 
                    String description = resultSet.getString("name")+ " "+ resultSet.getString("strength");
                    String morning = Double.toString(resultSet.getDouble("morning"));                 
                    String afternoon = Double.toString(resultSet.getDouble("afternoon"));                 
                    String evening = Double.toString(resultSet.getDouble("evening"));                 
                    String duration = "0";                 
                    String quantity = "0.0";                 

                    Map<String, String> map = new HashMap<String, String>();                
                    map.put("regimentypeId", regimentypeId);
                    map.put("regimendrugId", regimendrugId);
                    map.put("regimenId", regimenId);
                    map.put("drugId", drugId);
                    map.put("description", description);
                    map.put("morning", morning);                
                    map.put("afternoon", afternoon);                
                    map.put("evening", evening);                
                    map.put("duration", duration);                
                    map.put("quantity", quantity);                
                    dispenserList.add(map);
                }                    
            }
            session.setAttribute("dispenserList", dispenserList);
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }            
        return SUCCESS;
    }

    public String updateDispenserList() {
        // retrieve pharmacy information to be saved from request parameters 
        String regimendrugId = request.getParameter("id");
        String morning = request.getParameter("morning");
        String afternoon = request.getParameter("afternoon");
        String evening = request.getParameter("evening");
        String duration = request.getParameter("duration");
        Double q = Double.parseDouble(request.getParameter("morning"))+Integer.parseInt(request.getParameter("afternoon"))+Integer.parseInt(request.getParameter("evening"));
        String quantity = Double.toString(Integer.parseInt(request.getParameter("duration"))*q);
        
        // retrieve the list stored as an attribute in session object
        if(session.getAttribute("dispenserList") != null) {
            dispenserList = (ArrayList) session.getAttribute("dispenserList");                        
        }

        // find the target drug and update with values of request parameters
        for(int i = 0; i < dispenserList.size(); i++) {
            String id = (String) dispenserList.get(i).get("regimendrugId"); // retrieve regimen id from list
            if(id.equals(regimendrugId)) {
                dispenserList.get(i).remove("morning");
                dispenserList.get(i).put("morning", morning);
                dispenserList.get(i).remove("afternoon");
                dispenserList.get(i).put("afternoon", afternoon);
                dispenserList.get(i).remove("evening");
                dispenserList.get(i).put("evening", evening);
                dispenserList.get(i).remove("duration");
                dispenserList.get(i).put("duration", duration);
                dispenserList.get(i).remove("quantity");
                dispenserList.get(i).put("quantity", quantity);
            }
        }        
        // set dispenserList as a session-scoped attribute
        session.setAttribute("dispenserList", dispenserList);  
        return SUCCESS;
    }
    
    public String dispenserGrid1() {
        //setTotalpages(1);
        //setCurrpage(1);
        //setTotalrecords(20);
        
        // retrieve the record store in session attribute
        if(session.getAttribute("dispenserList") != null) {
            dispenserList = (ArrayList) session.getAttribute("dispenserList");                        
        }
        return SUCCESS;
    }

    public String refillPeriod() {
        // retrieve the list stored as an attribute in session object
        if(session.getAttribute("dispenserList") != null) {
            dispenserList = (ArrayList) session.getAttribute("dispenserList");                        
        }
        
        String quantity = "";
        String s = "1,2,3,4,5";
        // find the target drug and update duration only regimentype ids 1,2,3,4,5
        for(int i = 0; i < dispenserList.size(); i++) {
            String id = (String) dispenserList.get(i).get("regimentypeId"); // retrieve regimentype id from list
            if(s.indexOf(id) != -1) {
                Double morn = Double.parseDouble(dispenserList.get(i).get("morning"));
                Double after = Double.parseDouble(dispenserList.get(i).get("afternoon"));
                Double even = Double.parseDouble(dispenserList.get(i).get("evening"));
                if(request.getParameter("refill").isEmpty()) {
                    quantity = Double.toString((morn + after + even));   
                } 
                else {
                    quantity = Double.toString((morn + after + even)*Integer.parseInt(request.getParameter("refill").trim()));                    
                }
                    
                dispenserList.get(i).remove("duration");
                dispenserList.get(i).put("duration", request.getParameter("refill"));
                dispenserList.get(i).remove("quantity");
                dispenserList.get(i).put("quantity", quantity);
            }
        }        
        // set dispenserList as a session-scoped attribute
        session.setAttribute("dispenserList", dispenserList);  
        return SUCCESS;
    }
    
    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the limit
     */
    public Integer getRows() {
        return rows;
    }

    /**
     * @param limit the limit to set
     */
    public void setRows(Integer rows) {
        this.rows = rows;
    }

    /**
     * @return the sidx
     */
    public String getSidx() {
        return sidx;
    }

    /**
     * @param sidx the sidx to set
     */
    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    /**
     * @return the sord
     */
    public String getSord() {
        return sord;
    }

    /**
     * @param sord the sord to set
     */
    public void setSord(String sord) {
        this.sord = sord;
    }

    /**
     * @return the totalpages
     */
    public Integer getTotalpages() {
        return totalpages;
    }

    /**
     * @param totalpages the totalpages to set
     */
    public void setTotalpages(Integer totalpages) {
        this.totalpages = totalpages;
    }

    /**
     * @return the currpage
     */
    public Integer getCurrpage() {
        return currpage;
    }

    /**
     * @param currpage the currpage to set
     */
    public void setCurrpage(Integer currpage) {
        this.currpage = currpage;
    }

    /**
     * @return the totalrecords
     */
    public Integer getTotalrecords() {
        return totalrecords;
    }

    /**
     * @param totalrecords the totalrecords to set
     */
    public void setTotalrecords(Integer totalrecords) {
        this.totalrecords = totalrecords;
    }

    /**
     * @return the dispenserList
     */
    public ArrayList<Map<String, String>> getDispenserList() {
        return dispenserList;
    }

    /**
     * @param dispenserList the dispenserList to set
     */
    public void setDispenserList(ArrayList<Map<String, String>> dispenserList) {
        this.dispenserList = dispenserList;
    }
}
