/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.controller.grid;

import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.opensymphony.xwork2.ActionSupport;
import java.io.IOException;
import java.sql.SQLException;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.builder.FacilityListBuilder;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.Constants;

public class FacilityGridAction extends ActionSupport {
    private Integer page;
    private Integer rows;
    private String sidx;
    private String sord;
    private Integer totalpages;
    private Integer currpage;
    private Integer totalrecords;

    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private HttpServletRequest request;
    
    private ArrayList<Map<String, String>> facilityList = new ArrayList<Map<String, String>>();
        
    public String facilityGrid() {
        //setTotalpages(1);
        //setCurrpage(1);
        //setTotalrecords(20);
        int level = 0;
        request = ServletActionContext.getRequest();
        try {
            // obtain a JDBC connect and execute query
            query = "SELECT * FROM facility";            
            if(request.getParameterMap().containsKey("stateId")) {
                if(request.getParameterMap().containsKey("lgaId")) {
                    level = 2;
                    query = "SELECT * FROM facility WHERE state_id = ? AND lga_id = ?";                    
                }
                else {
                    level = 1;
                    query = "SELECT * FROM facility WHERE state_id = ?";                                        
                }
            }

            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            if(level == 1) {                
                preparedStatement.setLong(1, Long.parseLong(request.getParameter("stateId")));
            }
            if(level == 2) {                
                preparedStatement.setLong(1, Long.parseLong(request.getParameter("stateId")));
                preparedStatement.setLong(2, Long.parseLong(request.getParameter("lgaId")));
            }
            resultSet = preparedStatement.executeQuery();
            
            new FacilityListBuilder().buildFacilityList(resultSet);
            facilityList = new FacilityListBuilder().retrieveFacilityList();
            resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return SUCCESS;
    }
    
    //This method controls the NDR page.
    public String facilitySelGrid() {
        //setTotalpages(1);
        //setCurrpage(1);
        //setTotalrecords(20);
        request = ServletActionContext.getRequest();
        try {
            jdbcUtil = new JDBCUtil();
            
            executeUpdate("DROP TABLE IF EXISTS actives");
            query = "CREATE TEMPORARY TABLE actives AS SELECT facility_id, COUNT(patient_id) AS count FROM patient WHERE current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND DATEDIFF(DAY, date_last_refill + last_refill_duration, CURDATE()) <= " + Constants.Reports.LTFU_GON + " AND date_started IS NOT NULL GROUP BY facility_id";
            executeUpdate(query);

            executeUpdate("DROP TABLE IF EXISTS counts");
            query = "CREATE TEMPORARY TABLE counts AS SELECT facility_id, COUNT(patient_id) AS count FROM patient GROUP BY facility_id";
            executeUpdate(query);
            
            query = "SELECT facility_id, name, datim_id FROM facility WHERE facility_id IN (SELECT DISTINCT facility_id FROM patient) AND state_id = ? ORDER BY name ASC";                                        
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, Long.parseLong(request.getParameter("stateId")));
            resultSet = preparedStatement.executeQuery();
            int i = 1;
            while(resultSet.next()) {
               String facilityId = Long.toString(resultSet.getLong("facility_id")); 
                String name = resultSet.getString("name");
                Long facId = resultSet.getLong("facility_id");
                String count = getFacilityPatientCount(facId);
                String active = getActiveClientsForFacility(facId);
                String datimId = resultSet.getString("datim_id");

                Map<String, String> map = new HashMap<>();
                map.put("facilityId", facilityId);
                map.put("name", name);
                map.put("count", count);
                map.put("active", active);
                map.put("datimId", datimId);
                map.put("sn", ""+i);
                map.put("sel", "0");
                facilityList.add(map);
                i++;
            }
            resultSet = null;            
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return SUCCESS;
    }
    
    private String getFacilityPatientCount(long facId) {
        String count = "0";
        JDBCUtil jdbcUtil1 = null;
        ResultSet resultSet1;
        try {
            jdbcUtil1 = new JDBCUtil();
            String query1 = "SELECT count FROM counts WHERE facility_id = ?";                                        
            PreparedStatement preparedStatement1 = jdbcUtil1.getStatement(query1);
            preparedStatement1.setLong(1, facId);
            resultSet1 = preparedStatement1.executeQuery();
            while(resultSet1.next()) {
                count = Integer.toString(resultSet1.getInt("count")); 
            }          
        }
        catch (IOException | IllegalStateException | SQLException exception) {
            resultSet1 = null;
            jdbcUtil1.disconnectFromDatabase();  //disconnect from database
        }
        return count;
    }

    public String getActiveClientsForFacility(long facId){
    
        String count = "0";
        JDBCUtil jdbcUtil1 = null;
        ResultSet resultSet2;
        try {
            jdbcUtil1 = new JDBCUtil();
            String query2 = "SELECT COUNT(patient_id) AS count FROM actives WHERE facility_id = ? "; 
            PreparedStatement preparedStatement2 = jdbcUtil1.getStatement(query2);
            preparedStatement2.setLong(1, facId);
            resultSet2 = preparedStatement2.executeQuery();
            while(resultSet2.next()) {
                count = Integer.toString(resultSet2.getInt("count")); 
            }
        }
        catch (IOException | IllegalStateException | SQLException exception) {
            exception.printStackTrace();
            resultSet2 = null;
            jdbcUtil1.disconnectFromDatabase();  //disconnect from database
        }
        return count;
    }
    

    private void executeUpdate(String query) {
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }
    
    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the limit
     */
    public Integer getRows() {
        return rows;
    }

    /**
     * @param limit the limit to set
     */
    public void setRows(Integer rows) {
        this.rows = rows;
    }

    /**
     * @return the sidx
     */
    public String getSidx() {
        return sidx;
    }

    /**
     * @param sidx the sidx to set
     */
    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    /**
     * @return the sord
     */
    public String getSord() {
        return sord;
    }

    /**
     * @param sord the sord to set
     */
    public void setSord(String sord) {
        this.sord = sord;
    }

    /**
     * @return the totalpages
     */
    public Integer getTotalpages() {
        return totalpages;
    }

    /**
     * @param totalpages the totalpages to set
     */
    public void setTotalpages(Integer totalpages) {
        this.totalpages = totalpages;
    }

    /**
     * @return the currpage
     */
    public Integer getCurrpage() {
        return currpage;
    }

    /**
     * @param currpage the currpage to set
     */
    public void setCurrpage(Integer currpage) {
        this.currpage = currpage;
    }

    /**
     * @return the totalrecords
     */
    public Integer getTotalrecords() {
        return totalrecords;
    }

    /**
     * @param totalrecords the totalrecords to set
     */
    public void setTotalrecords(Integer totalrecords) {
        this.totalrecords = totalrecords;
    }

    /**
     * @return the facilityList
     */
    public ArrayList<Map<String, String>> getFacilityList() {
        return facilityList;
    }

    /**
     * @param facilityList the facilityList to set
     */
    public void setFacilityList(ArrayList<Map<String, String>> facilityList) {
        this.facilityList = facilityList;
    }
}
