/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.controller.grid;

import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ActionSupport;
import org.fhi360.lamis.utility.JDBCUtil;

public class LabresultGridAction_version1 extends ActionSupport implements Preparable {
    private Integer page;
    private Integer rows;
    private String sidx;
    private String sord;   
    private Integer totalpages;
    private Integer currpage;
    private Integer totalrecords;

    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    private ArrayList<Map<String, String>> labresultList = new ArrayList<Map<String, String>>();
    
    @Override    
    public void prepare() {
        request = ServletActionContext.getRequest();
        session = request.getSession();
    }
    
    public String labresultGrid() {
        //setTotalpages(1);
        //setCurrpage(1);
        //setTotalrecords(20);

        String ids = request.getParameter("labtestIds"); 
        String[] idList =  ids.split(",");
        try {
           // obtain a JDBC connect and execute query
           jdbcUtil = new JDBCUtil();
           for(String id : idList) {
                // fetch the details of selected lab test
                query = "SELECT labtest_id, description, measureab, measurepc FROM labtest WHERE labtest_id = ?"; 
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setLong(1, Long.parseLong(id));
                resultSet = preparedStatement.executeQuery();

                // loop through resultSet for each row and put into Map
                while (resultSet.next()) {
                    String labtestId = Long.toString(resultSet.getInt("labtest_id")); 
                    String description = resultSet.getString("description");
                    String resultab = ""; 
                    String measureab = resultSet.getString("measureab");                 
                    String resultpc = "";                 
                    String measurepc = resultSet.getString("measurepc"); 
                    String comment = ""; 

                    Map<String, String> map = new HashMap<String, String>();                
                    map.put("labtestId", labtestId);
                    map.put("description", description);
                    map.put("resultab", resultab);                
                    map.put("measureab", measureab);                
                    map.put("resultpc", resultpc);                
                    map.put("measurepc", measurepc);                
                    map.put("comment", comment);                
                    labresultList.add(map);
                }  
           }
           session.setAttribute("labresultList", labresultList);
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }            
        return SUCCESS;
    }

    public String updateLabresultList() {
        // retrieve lab result information to be saved from request parameters 
        String labtestId = request.getParameter("id");                        
        String resultab = request.getParameter("resultab");
        String resultpc = request.getParameter("resultpc");
        String comment = request.getParameter("comment");
        
        // retrieve the defaulter list stored as an attribute in session object
        if(session.getAttribute("labresultList") != null) {
            labresultList = (ArrayList) session.getAttribute("labresultList");                        
        }

        // find the target labresult and update with values of request parameters
        for(int i = 0; i < labresultList.size(); i++) {
            String id = (String) labresultList.get(i).get("labtestId"); // retrieve labtest id from list
            if(id.equals(labtestId)) {
                labresultList.get(i).remove("resultab");
                labresultList.get(i).put("resultab", resultab);
                labresultList.get(i).remove("resultpc");
                labresultList.get(i).put("resultpc", resultpc);
                labresultList.get(i).remove("comment");
                labresultList.get(i).put("comment", comment);
            }
        }        
        // set labresultList as a session-scoped attribute
        session.setAttribute("labresultList", labresultList);                
        return SUCCESS;
    }
    
    public String labresultGrid1() {
        setTotalpages(1);
        setCurrpage(1);
        setTotalrecords(20);
        
        // retrieve the labresult record store in session attribute
        if(session.getAttribute("labresultList") != null) {
            labresultList = (ArrayList) session.getAttribute("labresultList");                        
        }
        return SUCCESS;
    }
    
    
    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the limit
     */
    public Integer getRows() {
        return rows;
    }

    /**
     * @param limit the limit to set
     */
    public void setRows(Integer rows) {
        this.rows = rows;
    }

    /**
     * @return the sidx
     */
    public String getSidx() {
        return sidx;
    }

    /**
     * @param sidx the sidx to set
     */
    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    /**
     * @return the sord
     */
    public String getSord() {
        return sord;
    }

    /**
     * @param sord the sord to set
     */
    public void setSord(String sord) {
        this.sord = sord;
    }

    /**
     * @return the totalpages
     */
    public Integer getTotalpages() {
        return totalpages;
    }

    /**
     * @param totalpages the totalpages to set
     */
    public void setTotalpages(Integer totalpages) {
        this.totalpages = totalpages;
    }

    /**
     * @return the currpage
     */
    public Integer getCurrpage() {
        return currpage;
    }

    /**
     * @param currpage the currpage to set
     */
    public void setCurrpage(Integer currpage) {
        this.currpage = currpage;
    }

    /**
     * @return the totalrecords
     */
    public Integer getTotalrecords() {
        return totalrecords;
    }

    /**
     * @param totalrecords the totalrecords to set
     */
    public void setTotalrecords(Integer totalrecords) {
        this.totalrecords = totalrecords;
    }

    /**
     * @return the labresultList
     */
    public ArrayList<Map<String, String>> getLabresultList() {
        return labresultList;
    }

    /**
     * @param labresultList the labresultList to set
     */
    public void setLabresultList(ArrayList<Map<String, String>> labresultList) {
        this.labresultList = labresultList;
    }
}
