/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.controller.grid;

import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ActionSupport;
import org.fhi360.lamis.utility.Constants;										  
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PatientNumberNormalizer;
import org.fhi360.lamis.utility.builder.PatientListBuilder;
import org.fhi360.lamis.utility.Scrambler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PatientGridAction extends ActionSupport implements Preparable {
    private static final Logger LOG = LoggerFactory.getLogger(PatientGridAction.class);
    private Integer page;
    private Integer rows;
    private String sidx;
    private String sord;
    private Integer totalpages;
    private Integer currpage;
    private Integer totalrecords;

    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    private ArrayList<Map<String, String>> patientList = new ArrayList<Map<String, String>>(); 
    
    @Override    
    public void prepare() {
        request = ServletActionContext.getRequest();
        session = request.getSession();
    }
    
    public String patientGrid() {
        long facilityId = (Long) session.getAttribute("facilityId");
        Scrambler scrambler = new Scrambler();
        Map<String, Object> pagerParams = new PaginationUtil().paginateGrid(getPage().intValue(), getRows().intValue(), "patient");
        int start = (Integer) pagerParams.get("start");
        int numberOfRows = getRows().intValue();
        try {           
            jdbcUtil = new JDBCUtil();
            if(request.getParameterMap().containsKey("name")) {
                String name = scrambler.scrambleCharacters(request.getParameter("name"));
                name = name.toUpperCase();
                if(name == null || name.isEmpty()) {
                    query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric"
                            + " FROM patient p WHERE facility_id = " + facilityId + " ORDER BY surname ASC LIMIT " + start + " , " + numberOfRows;                                                         
                    if(request.getParameterMap().containsKey("female")) query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND gender = 'Female' ORDER BY surname ASC LIMIT " + start + " , " + numberOfRows;                                                         
                }
                else {
                    query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND UPPER(surname) LIKE '" + name + "%' OR UPPER(other_names) LIKE '" + name + "%' OR UPPER(CONCAT(surname, ' ', other_names)) LIKE '" + name + "%'  OR UPPER(CONCAT(other_names, ' ', surname)) LIKE '" + name + "%'  ORDER BY surname ASC LIMIT " + start + " , " + numberOfRows;                                     
                    if(request.getParameterMap().containsKey("female")) query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND gender = 'Female' AND UPPER(surname) LIKE '" + name + "%' OR UPPER(other_names) LIKE '" + name + "%' OR UPPER(CONCAT(surname, ' ', other_names)) LIKE '" + name + "%'  OR UPPER(CONCAT(other_names, ' ', surname)) LIKE '" + name + "%' ORDER BY surname ASC LIMIT " + start + " , " + numberOfRows;  
                    if(request.getParameterMap().containsKey("unsuppressed")) query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND last_viral_load >=1000 AND UPPER(surname) LIKE '" + name + "%' OR UPPER(other_names) LIKE '" + name + "%' OR UPPER(CONCAT(surname, ' ', other_names)) LIKE '" + name + "%'  OR UPPER(CONCAT(other_names, ' ', surname)) LIKE '" + name + "%' ORDER BY surname ASC LIMIT " + start + " , " + numberOfRows;  
                }

            }
            else {
                query = "SELECT p.*,(select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " ORDER BY surname ASC LIMIT " + start + " , " + numberOfRows;                 
                if(request.getParameterMap().containsKey("female")) query = "SELECT p.*,(select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND gender = 'Female' ORDER BY surname ASC LIMIT " + start + " , " + numberOfRows;                 
                if(request.getParameterMap().containsKey("unsuppressed")) query = "SELECT p.*,(select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND patient.current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND patient.last_viral_load >=1000 ORDER BY surname ASC LIMIT " + start + " , " + numberOfRows;
            }
            LOG.info("Query 1: {}", query);
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            
            new PatientListBuilder().buildPatientListSorted(resultSet);
            patientList = new PatientListBuilder().retrievePatientList();
            resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        
        page = (Integer) pagerParams.get("page");
        currpage = (Integer) pagerParams.get("page");
        totalpages = (Integer) pagerParams.get("totalpages");
        totalrecords = (Integer) pagerParams.get("totalrecords");
        return SUCCESS;
    }
    
	public String patientDrugDispenseGrid() {
        long facilityId = (Long) session.getAttribute("facilityId");
        Scrambler scrambler = new Scrambler();
        Map<String, Object> pagerParams = new PaginationUtil().paginateGrid(getPage().intValue(), getRows().intValue(), "patient");
        int start = (Integer) pagerParams.get("start");
        int numberOfRows = getRows().intValue();
        try {           
            jdbcUtil = new JDBCUtil();
            if(request.getParameterMap().containsKey("name")) {
                String name = scrambler.scrambleCharacters(request.getParameter("name"));
                if(name == null || name.isEmpty()) {
                    query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p JOIN prescription pr ON p.patient_id = pr.patient_id WHERE p.facility_id = " + facilityId + " AND pr.status = " + Constants.Prescription.PRESCRIBED + " AND prescription_type = 'drug' ORDER BY p.surname ASC LIMIT " + start + " , " + numberOfRows;                                                         
                }
                else {
                    query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p JOIN prescription pr ON p.patient_id = pr.patient_id WHERE p.facility_id = " + facilityId + " AND p.surname LIKE '"+name+"%' OR p.other_names LIKE '"+name+"%' AND pr.status = " + Constants.Prescription.PRESCRIBED + " AND prescription_type = 'drug' ORDER BY p.surname ASC LIMIT " + start + " , " + numberOfRows;                                     
                }
            }
            else {
                query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p JOIN prescription pr ON p.patient_id = pr.patient_id WHERE p.facility_id = " + facilityId + " AND pr.status = " + Constants.Prescription.PRESCRIBED + " AND prescription_type = 'drug' ORDER BY p.surname ASC LIMIT " + start + " , " + numberOfRows;                 
            }
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            
            new PatientListBuilder().buildPatientListSorted(resultSet);
            patientList = new PatientListBuilder().retrievePatientList();
            resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        
        page = (Integer) pagerParams.get("page");
        currpage = (Integer) pagerParams.get("page");
        totalpages = (Integer) pagerParams.get("totalpages");
        totalrecords = (Integer) pagerParams.get("totalrecords");
        return SUCCESS;
    }
    
    public String patientLabtestDispenseGrid() {
        long facilityId = (Long) session.getAttribute("facilityId");
        Scrambler scrambler = new Scrambler();
        Map<String, Object> pagerParams = new PaginationUtil().paginateGrid(getPage().intValue(), getRows().intValue(), "patient");
        int start = (Integer) pagerParams.get("start");
        int numberOfRows = getRows().intValue();
        try {           
            jdbcUtil = new JDBCUtil();
            if(request.getParameterMap().containsKey("name")) {
                String name = scrambler.scrambleCharacters(request.getParameter("name"));
                if(name == null || name.isEmpty()) {
                    query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p JOIN prescription pr ON p.patient_id = pr.patient_id WHERE p.facility_id = " + facilityId + " AND pr.status = " + Constants.Prescription.PRESCRIBED + " AND prescription_type = 'labtest' ORDER BY p.surname ASC LIMIT " + start + " , " + numberOfRows;                                                         
                }
                else {
                    query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p JOIN prescription pr ON p.patient_id = pr.patient_id WHERE p.facility_id = " + facilityId + " AND p.surname LIKE '"+name+"%' OR p.other_names LIKE '"+name+"%' AND pr.status = " + Constants.Prescription.PRESCRIBED + " AND prescription_type = 'labtest' ORDER BY p.surname ASC LIMIT " + start + " , " + numberOfRows;                                     
                }
            }
            else {
                query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p JOIN prescription pr ON p.patient_id = pr.patient_id WHERE p.facility_id = " + facilityId + " AND pr.status = " + Constants.Prescription.PRESCRIBED + " AND prescription_type = 'labtest' ORDER BY p.surname ASC LIMIT " + start + " , " + numberOfRows;                 
            }
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            
            new PatientListBuilder().buildPatientListSorted(resultSet);
            patientList = new PatientListBuilder().retrievePatientList();
            resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        
        page = (Integer) pagerParams.get("page");
        currpage = (Integer) pagerParams.get("page");
        totalpages = (Integer) pagerParams.get("totalpages");
        totalrecords = (Integer) pagerParams.get("totalrecords");
        return SUCCESS;
    }
    
    public String patientGridSearch() {
        long facilityId = (Long) session.getAttribute("facilityId");
        Scrambler scrambler = new Scrambler();
       
        Map<String, Object> pagerParams = new PaginationUtil().paginateGrid(getPage().intValue(), getRows().intValue(), "patient");
        int start = (Integer) pagerParams.get("start");
        int numberOfRows = getRows().intValue();
        try {           
            jdbcUtil = new JDBCUtil();
            if(request.getParameterMap().containsKey("name")) {
                String name = scrambler.scrambleCharacters(request.getParameter("name"));
                if(name == null || name.isEmpty()) {
                    query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " ORDER BY time_stamp DESC LIMIT " + start + " , " + numberOfRows;                                                         
                    if(request.getParameterMap().containsKey("female")) query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND gender = 'Female' ORDER BY time_stamp DESC LIMIT " + start + " , " + numberOfRows;                                                         
                }
                else {
                    query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND surname LIKE '"+name+"%' OR other_names LIKE '"+name+"%' ORDER BY current_status LIMIT " + start + " , " + numberOfRows;                                     
                    if(request.getParameterMap().containsKey("female")) query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND gender = 'Female' AND surname LIKE '"+name+"%' OR other_names LIKE '"+name+"%' ORDER BY current_status LIMIT " + start + " , " + numberOfRows;                                     
                }

            }
            else {
                query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " ORDER BY time_stamp DESC LIMIT " + start + " , " + numberOfRows;                 
                if(request.getParameterMap().containsKey("female")) query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND gender = 'Female' ORDER BY time_stamp DESC LIMIT " + start + " , " + numberOfRows;                 
            }
            LOG.info("Query: {}", query);
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            
            new PatientListBuilder().buildPatientList(resultSet, "");
            patientList = new PatientListBuilder().retrievePatientList();
            resultSet = null;
        }
        catch (Exception exception) {
            LOG.info("Exception: {}", exception);
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            exception.printStackTrace();
        }
        
        page = (Integer) pagerParams.get("page");
        currpage = (Integer) pagerParams.get("page");
        totalpages = (Integer) pagerParams.get("totalpages");
        totalrecords = (Integer) pagerParams.get("totalrecords");
        return SUCCESS;
    }

    public String patientGridByNumber(){
        long facilityId = (Long) session.getAttribute("facilityId");
        String hospitalNum = request.getParameter("hospitalNum");

        Map<String, Object> pagerParams = new PaginationUtil().paginateGrid(getPage().intValue(), getRows().intValue(), "patient");
        int start = (Integer) pagerParams.get("start");
        int numberOfRows = getRows().intValue();
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            if(hospitalNum == null || hospitalNum.isEmpty()) {
                query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " ORDER BY current_status ASC LIMIT " + start + " , " + numberOfRows;                                                         
            }
            else {
                query = "SELECT p.*, (select case when count(b.*) > 0 then true else false end from biometric b inner join patient x on x.id_uuid = b.patient_id  where x.patient_id = p.patient_id and x.facility_id = p.facility_id) as biometric FROM patient p WHERE facility_id = " + facilityId + " AND TRIM(LEADING '0' FROM hospital_num) LIKE '" + PatientNumberNormalizer.unpadNumber(hospitalNum) + "%' ORDER BY current_status ASC LIMIT " + start + " , " + numberOfRows; 
            }
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            
            new PatientListBuilder().buildPatientListSorted(resultSet);
            patientList = new PatientListBuilder().retrievePatientList();
            resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        page = (Integer) pagerParams.get("page");
        currpage = (Integer) pagerParams.get("page");
        totalpages = (Integer) pagerParams.get("totalpages");
        totalrecords = (Integer) pagerParams.get("totalrecords");
        return SUCCESS;
    }
    
    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the limit
     */
    public Integer getRows() {
        return rows;
    }

    /**
     * @param limit the limit to set
     */
    public void setRows(Integer rows) {
        this.rows = rows;
    }

    /**
     * @return the sidx
     */
    public String getSidx() {
        return sidx;
    }

    /**
     * @param sidx the sidx to set
     */
    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    /**`nmkl;'
     * @return the sord
     */
    public String getSord() {
        return sord;
    }

    /**
     * @param sord the sord to set
     */
    public void setSord(String sord) {
        this.sord = sord;
    }

    /**
     * @return the totalpages
     */
    public Integer getTotalpages() {
        return totalpages;
    }

    /**
     * @param totalpages the totalpages to set
     */
    public void setTotalpages(Integer totalpages) {
        this.totalpages = totalpages;
    }

    /**
     * @return the currpage
     */
    public Integer getCurrpage() {
        return currpage;
    }

    /**
     * @param currpage the currpage to set
     */
    public void setCurrpage(Integer currpage) {
        this.currpage = currpage;
    }

    /**
     * @return the totalrecords
     */
    public Integer getTotalrecords() {
        return totalrecords;
    }

    /**
     * @param totalrecords the totalrecords to set
     */
    public void setTotalrecords(Integer totalrecords) {
        this.totalrecords = totalrecords;
    }
    
    /**
     * @return the patientList
     */
    public ArrayList<Map<String, String>> getPatientList() {
        return patientList;
    }

    /**
     * @param patientList the patientList to set
     */
    public void setPatientList(ArrayList<Map<String, String>> patientList) {
        this.patientList = patientList;
    }
}

//                    query = "SELECT * FROM patient WHERE facility_id = " + facilityId + " AND surname LIKE '"+name+"%' UNION ALL SELECT * FROM patient WHERE facility_id = " + facilityId + " AND UPPER(other_names) LIKE '"+name+"%' ORDER BY current_status LIMIT " + start + " , " + numberOfRows;                                     
