/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.controller.grid;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.dao.hibernate.PrescriptionDAO;
import org.fhi360.lamis.model.Prescription;
import org.fhi360.lamis.model.dto.PrescriptionDTO;
import org.fhi360.lamis.utility.Constants;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.builder.PharmacyListBuilder;
import org.fhi360.lamis.utility.builder.PrescriptionListBuilder;

/**
 *
 * @author user10
 */
public class PrescriptionGridAction extends ActionSupport implements Preparable {
    private Integer page;
    private Integer rows;
    private String sidx;
    private String sord;
    private Integer totalpages;
    private Integer currpage;
    private Integer totalrecords;
    private String regimentypeId;
    private String regimenId;
    private Long facilityId;
    private Long patientId;;
    private Long userId;
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private ArrayList<Map<String, String>> regimenList = new ArrayList<>();
    private List<String> drugList = new ArrayList<>();
    private List<String> labtestList = new ArrayList<>();
    private List<Map<String, String>> prescriptionList = new ArrayList<>();
    private ArrayList<Map<String, String>> dispenserList = new ArrayList<>();
    private ArrayList<Map<String, String>> prescribedList = new ArrayList<>();
    
    //For Lab Test
    private List<String> labtestPrescriptionList = new ArrayList<>();
    private ArrayList<Map<String, String>> labresultList = new ArrayList<>();
    private ArrayList<Map<String, String>> labPrescribedList = new ArrayList<>();
    
    
    @Override
    public void prepare() {
        request = ServletActionContext.getRequest();
        session = request.getSession();
        userId = (Long) session.getAttribute("userId");
    }
    
    public String getRegimenForSelectedType() {
        try {
            if(request.getParameterMap().containsKey("regimentypeId")){
                regimentypeId = request.getParameter("regimentypeId");
                query = "SELECT * FROM regimen WHERE regimentype_id = ? ORDER BY description";
                
                if(regimentypeId != null && !"".equals(regimentypeId)){
                    jdbcUtil = new JDBCUtil();
                    preparedStatement = jdbcUtil.getStatement(query);
                
                    preparedStatement.setLong(1, Long.parseLong(regimentypeId));
                    resultSet = preparedStatement.executeQuery();

                    new PharmacyListBuilder().buildRegimenList(resultSet, "");
                    regimenList = new PharmacyListBuilder().retrieveRegimenList();
                }else{
                    regimenList = new ArrayList<>();
                }
            }else if(request.getParameterMap().containsKey("regimen_ids")){
                regimenId = request.getParameter("regimen_ids");
                
                if(regimenId != null && !"".equals(regimenId)){
                    query = "SELECT * FROM regimen WHERE regimen_id IN ("+regimenId+") ORDER BY description";

                    jdbcUtil = new JDBCUtil();
                    preparedStatement = jdbcUtil.getStatement(query);
                    resultSet = preparedStatement.executeQuery();

                    new PharmacyListBuilder().buildRegimenList(resultSet, "");
                    regimenList = new PharmacyListBuilder().retrieveRegimenList();
                }else{
                    regimenList = new ArrayList<>();
                }
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            exception.printStackTrace();
        }
        return SUCCESS;
    }
         
    public String saveSelectedPrescriptions(){
        String[] labtest_ids = null;
        String[] regimen_ids = null;
        String dateVisit = "";
         try{
            facilityId = (Long) session.getAttribute("facilityId");
            if(request.getParameterMap().containsKey("regimen_map") && !request.getParameter("regimen_map").equals("")){
                patientId = Long.valueOf(request.getParameter("patient_id"));
                regimen_ids =  request.getParameter("regimen_map").split(",");
                dateVisit = request.getParameter("dateVisit");
            }
            if (request.getParameterMap().containsKey("labtest_ids") && !request.getParameter("labtest_ids").equals("")){
                patientId = Long.valueOf(request.getParameter("patient_id"));
                labtest_ids = request.getParameter("labtest_ids").split(",");
                dateVisit = request.getParameter("dateVisit");
            }
            String prescriptionType;
            List<Prescription> prescriptions = new ArrayList<>();
             
             //Save the Selected drugs...
            if(regimen_ids != null){
                for(int i = 0; i< regimen_ids.length; i+=3){
                    prescriptionType = "drug";
                    Prescription p = new Prescription();                    
                    //Check If that prescription exists for the given date and update if yes...
                    PrescriptionDTO prescription = getPrescriptionsByDate(facilityId, patientId, dateVisit, "drug", Integer.parseInt(regimen_ids[i+1]));
                    if(prescription != null){
                        p.setPrescriptionId(prescription.getPrescriptionId());
                    }
                    p.setPatientId(patientId);
                    p.setFacilityId(facilityId);
                    p.setPrescriptionType(prescriptionType);
                    p.setRegimenTypeId(Long.parseLong(regimen_ids[i]));
                    p.setRegimenId(Long.parseLong(regimen_ids[i+1]));
                    p.setTimeStamp(new Date(new java.util.Date().getTime()));
                    p.setUserId(userId);
                    p.setDuration(Integer.parseInt(regimen_ids[i+2]));
                    if(prescription == null){
                        p.setStatus(Constants.Prescription.PRESCRIBED);}
                    else{
                        p.setStatus(prescription.getStatus());}
                    p.setDateVisit(DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy"));
                    System.out.println("P in Drug is: "+p);
                    prescriptions.add(p);
                }
            }
            if(labtest_ids != null){
                for (String labtest_id : labtest_ids) {
                    prescriptionType = "labtest";
                    Prescription p = new Prescription();
                    PrescriptionDTO prescription = getPrescriptionsByDate(facilityId, patientId, dateVisit, "labtest", Integer.parseInt(labtest_id));                   
                    if(prescription != null){
                        p.setPrescriptionId(prescription.getPrescriptionId());
                    }
                    p.setPatientId(patientId);
                    p.setFacilityId(facilityId);
                    p.setPrescriptionType(prescriptionType);
                    p.setLabtestId(Long.parseLong(labtest_id));
                    p.setTimeStamp(new Date(new java.util.Date().getTime()));
                    p.setUserId(userId);
                    p.setDateVisit(DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy"));
                    if(prescription == null){
                        p.setStatus(Constants.Prescription.PRESCRIBED);}
                    else{
                        p.setStatus(prescription.getStatus());}
                    System.out.println("P in LAB is: "+p);
                    prescriptions.add(p);
                }
            }
            if(regimen_ids != null || labtest_ids != null){
                System.out.println(prescriptions);
                //Save the Prescriptions...
                PrescriptionDAO.saveBatch(prescriptions);
            }
         }catch(Exception ex){
             ex.printStackTrace();
         }
         
         return SUCCESS;
    }
    
    public String getDrugsByDate(){
         try{
             String prescriptionType = "drug";
            facilityId = (Long) session.getAttribute("facilityId");
            if(request.getParameter("patientId") != null){
                patientId = Long.valueOf(request.getParameter("patientId"));
                String dateVisit = request.getParameter("dateVisit");

                query = "SELECT * FROM prescription WHERE facility_id = ? AND patient_id = ? AND date_visit = ? AND prescription_type = ?"; 
                jdbcUtil = new JDBCUtil();
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setLong(1, facilityId);
                preparedStatement.setLong(2, patientId);
                preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy"));
                preparedStatement.setString(4, prescriptionType);
                ResultSet rs = preparedStatement.executeQuery();
                
                new PrescriptionListBuilder().buildDrugList(rs);
                drugList = new PrescriptionListBuilder().retrieveDrugList();
                resultSet = null;
            }
         }catch(IOException | IllegalStateException | NumberFormatException | SQLException ex){
             ex.printStackTrace();
         }
         
         return SUCCESS;
    }
    
    //Create an object to hold the prescription and then return the object here...
    public PrescriptionDTO getPrescriptionsByDate(Long facilityId, Long patientId, String dateVisit, String type, Integer idTofind){
        PrescriptionDTO prescription = null;
         try{
            facilityId = (Long) session.getAttribute("facilityId");
            if(patientId != null){
                query = "SELECT * FROM prescription WHERE facility_id = ? AND patient_id = ? AND date_visit = ? AND prescription_type = ?"; 
                if(type.equals("drug"))
                    query += " AND regimen_id = ?";
                else if(type.equals("labtest"))
                    query += " AND labtest_id = ?";
                jdbcUtil = new JDBCUtil();
                System.out.println(facilityId + " - "+  patientId + " - " + dateVisit + " - " + type + " - " + idTofind);
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setLong(1, facilityId);
                preparedStatement.setLong(2, patientId);
                preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy"));
                preparedStatement.setString(4, type);
                preparedStatement.setInt(5, idTofind);
                ResultSet rs = preparedStatement.executeQuery();
                while(rs.next()){
                    Long prescription_id = rs.getLong("prescription_id");
                    Long patient_id = rs.getLong("patient_id");
                    Long facility_id = rs.getLong("facility_id");
                    String prescription_type = rs.getString("prescription_type");
                    Long labtest_id = rs.getLong("labtest_id");
                    Long regimentype_id = rs.getLong("regimentype_id");
                    Long regimen_id = rs.getLong("regimen_id");
                    Date date_visit = rs.getDate("date_visit");
                    Integer duration = rs.getInt("duration");
                    Integer status = rs.getInt("status");
                    
                    prescription = new PrescriptionDTO(prescription_id, facility_id, patient_id, prescription_type, labtest_id, status, regimentype_id, regimen_id, duration, date_visit);
                }
            }
         }catch(Exception ex){
             ex.printStackTrace();
         }
         return prescription;
    }
    
    public String getLabTestsByDate(){
         try{
            facilityId = (Long) session.getAttribute("facilityId");
            String prescriptionType = "labtest";
            if(request.getParameter("patientId") != null){
                patientId = Long.valueOf(request.getParameter("patientId"));
                String dateVisit = request.getParameter("dateVisit");

                query = "SELECT * FROM prescription WHERE facility_id = ? AND patient_id = ? AND date_visit = ? AND prescription_type = ?"; 
                jdbcUtil = new JDBCUtil();
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setLong(1, facilityId);
                preparedStatement.setLong(2, patientId);
                preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy"));
                preparedStatement.setString(4, prescriptionType);
                ResultSet rs = preparedStatement.executeQuery();
                
                new PrescriptionListBuilder().buildLabTestList(rs);
                labtestList = new PrescriptionListBuilder().retrieveLabTestList();
                resultSet = null;
            }
         }catch(IOException | IllegalStateException | NumberFormatException | SQLException ex){
             ex.printStackTrace();
         }
         
         return SUCCESS;
    }
    
    public String retrieveDrugPrescriptionByPatientId() {
        ArrayList<Integer> regimen_ids = new ArrayList<>();
        try {
            facilityId = (Long)session.getAttribute("facilityId");
            String patient_id = request.getParameter("patientId");
            String prescriptionType = "drug";
            
            JDBCUtil jdbcUtil = new JDBCUtil();
            String query = "SELECT regimentype_id, regimen_id, duration FROM prescription WHERE patient_id = ? AND facility_id = ? AND prescription_type = ? AND status = ?";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, Long.parseLong(patient_id));
            preparedStatement.setLong(2, facilityId);
            preparedStatement.setString(3, prescriptionType);
            preparedStatement.setInt(4, Constants.Prescription.PRESCRIBED);
            ResultSet resultSet = preparedStatement.executeQuery();

            // loop through resultSet for each row and put into Map
            while (resultSet.next()) {
                String regimen_id = Long.toString(resultSet.getInt("regimen_id"));  //Double.toString(double) if it is a double
                String regimenTypeId = Long.toString(resultSet.getInt("regimentype_id"));
                String duration = Long.toString(resultSet.getInt("duration"));
                regimen_ids.add(Integer.parseInt(regimen_id));
                Map<String, String> prescriptionMap = new HashMap<>();
                prescriptionMap.put("regimenTypeId", regimenTypeId);
                prescriptionMap.put("regimenId", regimen_id);
                prescriptionMap.put("duration", duration);
                prescriptionList.add(prescriptionMap);
            }           
            boolean done = buildPrescriptionDispenser(regimen_ids.toString());
            if(done) return SUCCESS;
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            exception.printStackTrace();
            return ERROR;
        }
        return ERROR;
    }
    
    private boolean buildPrescriptionDispenser(String regimen_ids) {
        
        boolean done = false;
        try {
            
            //obtain a JDBC connect and execute query
            JDBCUtil jdbcUtil = new JDBCUtil();
            String query = "SELECT drug.name, drug.strength, drug.morning, drug.afternoon, drug.evening, regimendrug.regimendrug_id, regimendrug.regimen_id, regimendrug.drug_id, regimen.regimentype_id "
                    + " FROM drug JOIN regimendrug ON regimendrug.drug_id = drug.drug_id JOIN regimen ON regimendrug.regimen_id = regimen.regimen_id WHERE regimendrug.regimen_id IN (" + regimen_ids.replace("[", "").replace("]", "") + ")"; 
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            System.out.println(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            //loop through resultSet for each row and put into Map
            while (resultSet.next()) {
                String regimentype_id = Long.toString(resultSet.getLong("regimentype_id")); 
                String regimendrugId = Long.toString(resultSet.getLong("regimendrug_id")); 
                String regimen_id = Long.toString(resultSet.getLong("regimen_id")); 
                String drugId = Long.toString(resultSet.getLong("drug_id")); 
                String description = resultSet.getString("name")+ " "+ resultSet.getString("strength");
                String morning = Double.toString(resultSet.getDouble("morning"));                 
                String afternoon = Double.toString(resultSet.getDouble("afternoon"));                 
                String evening = Double.toString(resultSet.getDouble("evening"));                 
                String duration = "0";                 
                String quantity = "0.0";                 

                Map<String, String> map = new HashMap<>();                
                map.put("regimentypeId", regimentype_id);
                map.put("regimendrugId", regimendrugId);
                map.put("regimenId", regimen_id);
                map.put("drugId", drugId);
                map.put("description", description);
                map.put("morning", morning);                
                map.put("afternoon", afternoon);                
                map.put("evening", evening);                
                map.put("duration", duration);                
                map.put("quantity", quantity);                
                dispenserList.add(map);
                prescribedList.add(map);
            }                                
            session.setAttribute("fromPrescription", "true");
            session.setAttribute("dispenserList", dispenserList);
            session.setAttribute("prescribedList", prescribedList);
//            System.out.println("Prescribed List is "+prescribedList);
//            System.out.println("Dispenser List is "+dispenserList);
            done = true;
            resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            exception.printStackTrace();
            done = false;
        }                              
        
        return done;
    }
    
    public String retrieveLabTestPrescriptionByPatientId() {
        ArrayList<Integer> labtest_ids = new ArrayList<>();
        try {
            facilityId = (Long)session.getAttribute("facilityId");
            String patient_id = request.getParameter("patientId");
            String prescriptionType = "labtest";
            
            JDBCUtil jdbcUtil = new JDBCUtil();
            String query = "SELECT labtest_id FROM prescription WHERE patient_id = ? AND facility_id = ? AND prescription_type = ? AND status = ?";
            //System.out.println(query);
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, Long.parseLong(patient_id));
            preparedStatement.setLong(2, facilityId);
            preparedStatement.setString(3, prescriptionType);
            preparedStatement.setInt(4, Constants.Prescription.PRESCRIBED);
            ResultSet resultSet = preparedStatement.executeQuery();
            
            // loop through resultSet for each row and put into Map
            while (resultSet.next()) {
                String labTestId = Long.toString(resultSet.getInt("labtest_id"));
                labtest_ids.add(Integer.parseInt(labTestId));
                labtestPrescriptionList.add(labTestId);
            }           
            buildLabTests(labtest_ids.toString());
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            exception.printStackTrace();
        }
        return SUCCESS;
    }
    
    private void buildLabTests(String labtest_ids) {
        try {         
            
            // obtain a JDBC connect and execute query
           jdbcUtil = new JDBCUtil();
           // fetch the details of selected lab test
           query = "SELECT labtest_id, description, measureab, measurepc FROM labtest WHERE labtest_id IN (" + labtest_ids.replace("[", "").replace("]", "") + ")"; 
           preparedStatement = jdbcUtil.getStatement(query);
           resultSet = preparedStatement.executeQuery();

           // loop through resultSet for each row and put into Map
           while (resultSet.next()) {
               String labtestId = Long.toString(resultSet.getInt("labtest_id")); 
               String description = resultSet.getString("description");
               String resultab = ""; 
               String measureab = resultSet.getString("measureab");                 
               String resultpc = "";                 
               String measurepc = resultSet.getString("measurepc"); 
               String comment = ""; 
               
               Map<String, String> map = new HashMap<String, String>();                
               map.put("labtestId", labtestId);
               map.put("description", description);
               map.put("resultab", resultab);                
               map.put("measureab", measureab);                
               map.put("resultpc", resultpc);                
               map.put("measurepc", measurepc);                
               map.put("comment", comment);                
               labresultList.add(map);               
               labPrescribedList.add(map);               
           }                                 
            session.setAttribute("fromLabTest", "true");
            session.setAttribute("labresultList", labresultList);
            session.setAttribute("labPrescribedList", labPrescribedList);
            resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            exception.printStackTrace();
        }                               
    }
    
    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the limit
     */
    public Integer getRows() {
        return rows;
    }

    /**
     * @param limit the limit to set
     */
    public void setRows(Integer rows) {
        this.rows = rows;
    }

    /**
     * @return the sidx
     */
    public String getSidx() {
        return sidx;
    }

    /**
     * @param sidx the sidx to set
     */
    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    /**
     * @return the sord
     */
    public String getSord() {
        return sord;
    }

    /**
     * @param sord the sord to set
     */
    public void setSord(String sord) {
        this.sord = sord;
    }

    /**
     * @return the totalpages
     */
    public Integer getTotalpages() {
        return totalpages;
    }

    /**
     * @param totalpages the totalpages to set
     */
    public void setTotalpages(Integer totalpages) {
        this.totalpages = totalpages;
    }

    /**
     * @return the currpage
     */
    public Integer getCurrpage() {
        return currpage;
    }

    /**
     * @param currpage the currpage to set
     */
    public void setCurrpage(Integer currpage) {
        this.currpage = currpage;
    }

    /**
     * @return the totalrecords
     */
    public Integer getTotalrecords() {
        return totalrecords;
    }

    /**
     * @param totalrecords the totalrecords to set
     */
    public void setTotalrecords(Integer totalrecords) {
        this.totalrecords = totalrecords;
    }

    public String getRegimentypeId() {
        return regimentypeId;
    }

    public void setRegimentypeId(String regimentypeId) {
        this.regimentypeId = regimentypeId;
    }

    public ArrayList<Map<String, String>> getRegimenList() {
        return regimenList;
    }

    public void setRegimenList(ArrayList<Map<String, String>> regimenList) {
        this.regimenList = regimenList;
    }

    public List<String> getDrugList() {
        return drugList;
    }

    public void setDrugList(List<String> drugList) {
        this.drugList = drugList;
    }

    public List<String> getLabtestList() {
        return labtestList;
    }

    public void setLabtestList(List<String> labtestList) {
        this.labtestList = labtestList;
    }

    public List<Map<String, String>> getPrescriptionList() {
        return prescriptionList;
    }

    public void setPrescriptionList(List<Map<String, String>> prescriptionList) {
        this.prescriptionList = prescriptionList;
    }

    public ArrayList<Map<String, String>> getDispenserList() {
        return dispenserList;
    }

    public void setDispenserList(ArrayList<Map<String, String>> dispenserList) {
        this.dispenserList = dispenserList;
    }

    public ArrayList<Map<String, String>> getPrescribedList() {
        return prescribedList;
    }

    public void setPrescribedList(ArrayList<Map<String, String>> prescribedList) {
        this.prescribedList = prescribedList;
    }

    public List<String> getLabtestPrescriptionList() {
        return labtestPrescriptionList;
    }

    public void setLabtestPrescriptionList(List<String> labtestPrescriptionList) {
        this.labtestPrescriptionList = labtestPrescriptionList;
    }

    public ArrayList<Map<String, String>> getLabresultList() {
        return labresultList;
    }

    public void setLabresultList(ArrayList<Map<String, String>> labresultList) {
        this.labresultList = labresultList;
    }

    public ArrayList<Map<String, String>> getLabPrescribedList() {
        return labPrescribedList;
    }

    public void setLabPrescribedList(ArrayList<Map<String, String>> labPrescribedList) {
        this.labPrescribedList = labPrescribedList;
    }

    
}
