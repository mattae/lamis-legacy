/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.controller.grid;

import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ActionSupport;
import org.fhi360.lamis.model.Patient;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.builder.StatusHistoryBuilder;

public class StatusHistoryGridAction extends ActionSupport implements Preparable{
    private Integer page;
    private Integer rows;
    private String sidx;
    private String sord;
    private Integer totalpages;
    private Integer currpage;
    private Integer totalrecords;

    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    private Patient patientObjSession;
    private ArrayList<Map<String, String>> statusList = new ArrayList<Map<String, String>>();
        
    @Override    
    public void prepare() {
        request = ServletActionContext.getRequest();
        session = request.getSession();
    }
    
    public String statusGrid() {
        //setTotalpages(1);
        //setCurrpage(1);
        //setTotalrecords(20);
        long patientId;
        long facilityId = (Long) session.getAttribute("facilityId");
        patientId = Long.parseLong(request.getParameter("patientId"));
       // if (request.getParameter("patientId") != null) {
            
//        } else {
//            patientObjSession = (Patient) request.getSession().getAttribute("patientObjSession");                        
//            patientId = patientObjSession.getPatientId(); 
//        }
        
        try {
            query = "SELECT DISTINCT patient.facility_id, patient.patient_id, patient.date_registration, patient.status_registration, patient.date_started, statushistory.history_id, statushistory.current_status, statushistory.date_current_status "
                    + " FROM patient JOIN statushistory ON patient.patient_id = statushistory.patient_id WHERE patient.facility_id = " + facilityId + " AND statushistory.facility_id = " + facilityId + " AND patient.patient_id = " + patientId + " ORDER BY statushistory.date_current_status DESC";
            
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            new StatusHistoryBuilder().buildStatusList(resultSet);
            statusList = new StatusHistoryBuilder().retrieveStatusList();
            
            resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return SUCCESS;
    }
    
    
    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @return the limit
     */
    public Integer getRows() {
        return rows;
    }

    /**
     * @param limit the limit to set
     */
    public void setRows(Integer rows) {
        this.rows = rows;
    }

    /**
     * @return the sidx
     */
    public String getSidx() {
        return sidx;
    }

    /**
     * @param sidx the sidx to set
     */
    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    /**
     * @return the sord
     */
    public String getSord() {
        return sord;
    }

    /**
     * @param sord the sord to set
     */
    public void setSord(String sord) {
        this.sord = sord;
    }

    /**
     * @return the totalpages
     */
    public Integer getTotalpages() {
        return totalpages;
    }

    /**
     * @param totalpages the totalpages to set
     */
    public void setTotalpages(Integer totalpages) {
        this.totalpages = totalpages;
    }

    /**
     * @return the currpage
     */
    public Integer getCurrpage() {
        return currpage;
    }

    /**
     * @param currpage the currpage to set
     */
    public void setCurrpage(Integer currpage) {
        this.currpage = currpage;
    }

    /**
     * @return the totalrecords
     */
    public Integer getTotalrecords() {
        return totalrecords;
    }

    /**
     * @param totalrecords the totalrecords to set
     */
    public void setTotalrecords(Integer totalrecords) {
        this.totalrecords = totalrecords;
    }

    /**
     * @return the statusList
     */
    public ArrayList<Map<String, String>> getStatusList() {
        return statusList;
    }

    /**
     * @param statusList the statusList to set
     */
    public void setStatusList(ArrayList<Map<String, String>> statusList) {
        this.statusList = statusList;
    }
}
