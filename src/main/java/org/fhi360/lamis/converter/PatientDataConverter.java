/**
 *
 * @author aalozie
 */

package org.fhi360.lamis.converter;

import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.FileUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.model.Regimenhistory;
import org.fhi360.lamis.dao.jdbc.RegimenIntrospector;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.Scrambler;
import org.apache.commons.lang3.StringUtils;
import org.fhi360.lamis.exchange.radet.RegimenResolver;


public class PatientDataConverter {
    private HttpServletRequest request;
    private HttpSession session;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private long userId;
    private Boolean viewIdentifier = false;
    private Scrambler scrambler;
    private RegimenResolver regimenResolver;  
   
    private String regimentype1, regimentype2, regimen1, regimen2;
    private long facilityId, patientId;
    
    public PatientDataConverter() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession();        
        this.scrambler = new Scrambler();
        this.regimenResolver = new RegimenResolver();
        if(ServletActionContext.getRequest().getParameter("viewIdentifier") != null && !ServletActionContext.getRequest().getParameter("viewIdentifier").equals("false")) {
            this.viewIdentifier = true;                        
        }
    } 
    
    public synchronized String convertExcel() {
        String fileName = "";        
        DateFormat dateFormatExcel = new SimpleDateFormat("dd-MMM-yyyy");      
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        userId = (Long) session.getAttribute("userId");
        
        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);  // turn off auto-flushing and accumulate all rows in memory
        workbook.setCompressTempFiles(true); // temp files will be gzipped
        Sheet sheet = workbook.createSheet();        
        try{
            jdbcUtil = new JDBCUtil();
            
            int rownum = 0;
            int cellnum = 0;
            Row row = sheet.createRow(rownum++);
            Cell cell = row.createCell(cellnum++);
            cell.setCellValue("Facility Id");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Facility Name");
            cell = row.createCell(cellnum++);
            cell.setCellValue("State");
            cell = row.createCell(cellnum++);
            cell.setCellValue("LGA");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Patient Id");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Hospital Num");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Unique ID");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Surname");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Other Names");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date Birth");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Age");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Age Unit");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Gender");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Marital Status");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Education");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Occupation");
            cell = row.createCell(cellnum++);
            cell.setCellValue("State of Residence");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Lga of Residence");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Address");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Phone");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Care Entry Point");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date of Confirmed HIV Test");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date Registration");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Status at Registration");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Current Status");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date Current Status");
            cell = row.createCell(cellnum++);
            cell.setCellValue("ART Start Date");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Baseline CD4");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Baseline CD4p");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Systolic BP");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Diastolic BP");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Baseline Clinic Stage");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Baseline Functional Status");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Baseline Weight (kg)");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Baseline Height (cm)");
            cell = row.createCell(cellnum++);
            cell.setCellValue("First Regimen Line");
            cell = row.createCell(cellnum++);
            cell.setCellValue("First Regimen");
            cell = row.createCell(cellnum++);
            cell.setCellValue("First NRTI");
            cell = row.createCell(cellnum++);
            cell.setCellValue("First NNRTI");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Current Regimen Line");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Current Regimen");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Current NRTI");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Current NNRTI");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date Subsituted/Switched");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date of Last Refill");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Last Refill Duration (days)");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date of Next Refill");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Last Clinic Stage");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date of Last Clinic");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date of Next Clinic");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Last CD4");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Last CD4p");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date of Last CD4");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Last Viral Load");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Date of Last Viral Load");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Viral Load Due Date");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Viral Load Type");
            cell = row.createCell(cellnum++);
            cell.setCellValue("SMS consent");
                       
            //Check if the url has a parameter for entity;
            if(request.getParameterMap().containsKey("entity")) {
                preparedStatement = jdbcUtil.getStatement(getQueryNotification());
            }
            else if(request.getParameterMap().containsKey("casemanagerId")) {
                preparedStatement = jdbcUtil.getStatement(getQueryCaseManagement());                
            }
            else
            {
                preparedStatement = jdbcUtil.getStatement(getQuery());  
            }
            resultSet = preparedStatement.executeQuery();       
            
            facilityId = 0;
            patientId = 0;
            while(resultSet.next()) {
                regimentype1 = "";
                regimentype2 = "";
                regimen1 = "";
                regimen2 = "";
                if(resultSet.getLong("facility_id") != facilityId) {
                    executeUpdate("DROP TABLE IF EXISTS commence");        
                    executeUpdate("CREATE TEMPORARY TABLE commence AS SELECT * FROM clinic WHERE facility_id = " + resultSet.getLong("facility_id") + " AND commence = 1");
                    executeUpdate("CREATE INDEX idx_visit ON commence(patient_id)");             
                }
                if(resultSet.getLong("facility_id") != facilityId || resultSet.getLong("patient_id") != patientId) {
                    cellnum = 0;
                    row = sheet.createRow(rownum++);
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet.getLong("facility_id"));
                    facilityId = resultSet.getLong("facility_id"); 
                    Map facility = getFacility(facilityId);
                    cell = row.createCell(cellnum++);
                    cell.setCellValue((String) facility.get("facilityName"));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue((String) facility.get("state"));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue((String) facility.get("lga"));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet.getLong("patient_id"));
                    patientId = resultSet.getLong("patient_id"); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet.getString("hospital_num"));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet.getString("unique_id"));

                    System.out.println("Hospital: " +facility.get("facilityName"));
                    System.out.println("Patient No: " +patientId);
                }
                cell = row.createCell(cellnum++);
                String surname = resultSet.getObject("surname") == null ? "" : resultSet.getString("surname");
                surname = (viewIdentifier)? scrambler.unscrambleCharacters(surname) : surname;
                surname = StringUtils.upperCase(surname);                
                cell.setCellValue(surname);
                cell = row.createCell(cellnum++);
                String otherNames = resultSet.getObject("other_names") == null ? "" : resultSet.getString("other_names");
                otherNames = (viewIdentifier)? scrambler.unscrambleCharacters(otherNames) : otherNames;
                otherNames = StringUtils.capitalize(otherNames);
                cell.setCellValue(otherNames);
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_birth") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_birth")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(Integer.toString(resultSet.getInt("age")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("age_unit"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("gender"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("marital_status"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("education"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("occupation"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("state"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("lga"));
                cell = row.createCell(cellnum++);
                String address = resultSet.getString("address") == null ? "" : resultSet.getString("address");
                address = (viewIdentifier)? scrambler.unscrambleCharacters(address) : address;
                address = StringUtils.capitalize(address);                               
                cell.setCellValue(address);
                cell = row.createCell(cellnum++);
                String phone = resultSet.getString("phone") == null ? "" :resultSet.getString("phone");
                phone = (viewIdentifier)? scrambler.unscrambleNumbers(phone) : phone;
                cell.setCellValue(phone);
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("entry_point"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("date_confirmed_hiv"));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_registration") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_registration")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("status_registration"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("current_status"));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_current_status") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_current_status")));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_started") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_started")));

                //Adding baseline data
                preparedStatement = jdbcUtil.getStatement("SELECT * FROM commence WHERE patient_id = " + patientId );
                ResultSet rs = preparedStatement.executeQuery();
                if(rs.next()) {
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(Double.toString(rs.getDouble("cd4")));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(Double.toString(rs.getDouble("cd4p")));
                    //Solve the BP
                    String[] bpData = (rs.getString("bp") != "" && rs.getString("bp") != null) ? rs.getString("bp").split("/") : new String[]{};
                    if(bpData.length == 2){
                        cell = row.createCell(cellnum++);
                        cell.setCellValue(bpData[0] != "" ? bpData[0] : "");
                        cell = row.createCell(cellnum++);
                        cell.setCellValue(bpData[1] != "" ? bpData[1] : "");
                    }
                    else{
                        cell = row.createCell(cellnum++);
                        cell.setCellValue(""); 
                        cell = row.createCell(cellnum++);
                        cell.setCellValue(""); 
                    }
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(rs.getString("clinic_stage"));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(rs.getString("func_status"));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(Double.toString(rs.getDouble("body_weight")));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(Double.toString(rs.getDouble("height")));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(rs.getString("regimentype") == null? "" : rs.getString("regimentype"));
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(rs.getString("regimen") == null? "" : regimenResolver.getRegimen(rs.getString("regimen")));                    
                    cell = row.createCell(cellnum++);
                    String nrti = rs.getString("regimen") == null? "" : getNrti(rs.getString("regimen"));
                    cell.setCellValue(nrti);                    
                    cell = row.createCell(cellnum++);
                    String nnrti = rs.getString("regimen") == null? "" : getNnrti(rs.getString("regimen"));
                    cell.setCellValue(nnrti);                    
                    regimentype1 = rs.getString("regimentype") == null? "" : rs.getString("regimentype");
                    regimen1 = rs.getString("regimen") == null? "" : rs.getString("regimen");
                }
                else {
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(""); 
                }                
                
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("regimentype") == null? "" : resultSet.getString("regimentype"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("regimen") == null? "" : regimenResolver.getRegimen(resultSet.getString("regimen")));
                cell = row.createCell(cellnum++);
                String nrti = resultSet.getString("regimen") == null? "" : getNrti(resultSet.getString("regimen"));
                cell.setCellValue(nrti);                    
                cell = row.createCell(cellnum++);
                String nnrti = resultSet.getString("regimen") == null? "" : getNnrti(resultSet.getString("regimen"));
                cell.setCellValue(nnrti);                    

                //Determining when regimen was subsituted or switched
                cell = row.createCell(cellnum++);
                regimentype2 = resultSet.getString("regimentype") == null? "" : resultSet.getString("regimentype");
                regimen2 = resultSet.getString("regimen") == null? "" : resultSet.getString("regimen");
                if(!regimentype1.isEmpty() && !regimentype2.isEmpty() && !regimen1.isEmpty() && !regimen2.isEmpty()) {
                    //If regimen was substituted or swicted, get the date of change
                    if(RegimenIntrospector.substitutedOrSwitched(regimen1, regimen2)) {
                        Regimenhistory regimenhistory = RegimenIntrospector.getRegimenHistory(patientId, regimentype2, regimen2);
                        System.out.println("Regimen history..."+regimenhistory.getDateVisit());
                        cell.setCellValue(regimenhistory.getDateVisit() == null? "" : dateFormatExcel.format(regimenhistory.getDateVisit()));
                    }                    
                }
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_last_refill") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_last_refill")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(Integer.toString(resultSet.getInt("last_refill_duration")));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_next_refill") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_next_refill")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("last_clinic_stage"));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_last_clinic") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_last_clinic")));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_next_clinic") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_next_clinic")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(Double.toString(resultSet.getDouble("last_cd4")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(Double.toString(resultSet.getDouble("last_cd4p")));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_last_cd4") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_last_cd4")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(Double.toString(resultSet.getDouble("last_viral_load")));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_last_viral_load") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_last_viral_load")));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("viral_load_due_date") == null)? "" : dateFormatExcel.format(resultSet.getDate("viral_load_due_date")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("viral_load_type"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getInt("send_message"));
                
                if(rownum % 100 == 0) {
                    ((SXSSFSheet)sheet).flushRows(100); // retain 100 last rows and flush all others

                    // ((SXSSFSheet)sheet).flushRows() is a shortcut for ((SXSSFSheet)sheet).flushRows(0),
                    // this method flushes all rows
               }                                
            } 

            String directory = contextPath+"transfer/";            
            FileUtil fileUtil = new FileUtil();
            fileUtil.makeDir(directory);
            fileUtil.makeDir(request.getContextPath()+"/transfer/");
            
            fileName = request.getParameterMap().containsKey("facilityIds")? "patient_"+request.getParameter("state").toLowerCase()+"_"+Long.toString(userId)+".xlsx" : "patient_"+Long.toString(userId)+".xlsx";
            FileOutputStream outputStream = new FileOutputStream(new File(directory+fileName));
            workbook.write(outputStream);
            outputStream.close();
            workbook.dispose();  // dispose of temporary files backing this workbook on disk
            
            //for servlets in the default(root) context, copy file to the transfer folder in root 
            if(!contextPath.equalsIgnoreCase(request.getContextPath())) fileUtil.copyFile(fileName, contextPath+"transfer/", request.getContextPath()+"/transfer/");                    
            //resultSet = null;                        
        }
        catch (Exception exception) {
            resultSet = null;            
            exception.printStackTrace();
        }
        return "transfer/"+fileName;
    }

    private String getQuery(){
        String facilityIds = "";
        String query = "";
        if (request.getParameterMap().containsKey("facilityIds")){
            facilityIds = request.getParameter("facilityIds");
            System.out.println("Selected ids......"+facilityIds);
            query = "SELECT DISTINCT patient.*, DATEDIFF(YEAR, date_birth, CURDATE()) AS age FROM patient WHERE facility_id IN (" + facilityIds + ") ORDER BY facility_id, patient_id"; 
        }
        else {
            facilityIds = Long.toString((Long) session.getAttribute("facilityId"));
            query = "SELECT patient.*, DATEDIFF(YEAR, date_birth, CURDATE()) AS age FROM patient WHERE facility_id IN (" + facilityIds + ")"; 

            if(!request.getParameter("gender").trim().isEmpty() && !request.getParameter("gender").trim().equals("--All--")) query += " AND gender = '" + request.getParameter("gender") + "'";
            if(!request.getParameter("ageBegin").trim().isEmpty() && !request.getParameter("ageEnd").trim().isEmpty()) query += " AND age >= " + Integer.parseInt(request.getParameter("ageBegin")) + " AND age <= " + Integer.parseInt(request.getParameter("ageEnd"));
            if(!request.getParameter("state").trim().isEmpty()) query += " AND state = '" + request.getParameter("state") + "'";
            if(!request.getParameter("lga").trim().isEmpty()) query += " AND lga = '" + request.getParameter("lga") + "'";

            if(!request.getParameter("currentStatus").trim().isEmpty() && !request.getParameter("currentStatus").trim().equals("--All--")) {
                String currentStatus = (request.getParameter("currentStatus").trim().equals("HIV  non ART"))? "HIV+ non ART" : request.getParameter("currentStatus");
                query += " AND current_status = '" + currentStatus + "'";
            }

            if(!request.getParameter("dateCurrentStatusBegin").trim().isEmpty() && !request.getParameter("dateCurrentStatusEnd").trim().isEmpty()) query += " AND date_current_status >= '" + DateUtil.parseStringToSqlDate(request.getParameter("dateCurrentStatusBegin"), "MM/dd/yyyy") + "' AND date_current_status <= '" + DateUtil.parseStringToSqlDate(request.getParameter("dateCurrentStatusEnd"), "MM/dd/yyyy") + "'";
            if(!request.getParameter("regimentype").trim().isEmpty()) query += " AND regimentype = '" + request.getParameter("regimentype") + "'";
            if(!request.getParameter("dateRegistrationBegin").trim().isEmpty() && !request.getParameter("dateRegistrationEnd").trim().isEmpty()) query += " AND date_registration >= '" + DateUtil.parseStringToSqlDate(request.getParameter("dateRegistrationBegin"), "MM/dd/yyyy") + "' AND date_registration <= '" + DateUtil.parseStringToSqlDate(request.getParameter("dateRegistrationEnd"), "MM/dd/yyyy") + "'";
            if(!request.getParameter("artStartDateBegin").trim().isEmpty() && !request.getParameter("artStartDateEnd").trim().isEmpty()) query += " AND date_started >= '" + DateUtil.parseStringToSqlDate(request.getParameter("artStartDateBegin"), "MM/dd/yyyy") + "' AND date_started <= '" + DateUtil.parseStringToSqlDate(request.getParameter("artStartDateEnd"), "MM/dd/yyyy") + "'";
            if(!request.getParameter("clinicStage").trim().isEmpty()) query += " AND last_clinic_stage = '" + request.getParameter("clinicStage") + "'";
            if(!request.getParameter("cd4Begin").trim().isEmpty() && !request.getParameter("cd4End").trim().isEmpty()) query += " AND last_cd4 >= " + Double.parseDouble(request.getParameter("cd4Begin")) + " AND last_cd4 <= " + Double.parseDouble(request.getParameter("cd4End"));
            if(!request.getParameter("viralloadBegin").trim().isEmpty() && !request.getParameter("viralloadEnd").trim().isEmpty()) query += " AND last_viral_load >= " + Double.parseDouble(request.getParameter("viralloadBegin")) + " AND last_viral_load <= " + Double.parseDouble(request.getParameter("viralloadEnd"));
            query += " ORDER BY current_status";
        }
        return query;
    }
    
    public String getQueryNotification(){
    
        Integer entity = Integer.parseInt(request.getParameter("entity"));
        String facilityIds = Long.toString((Long) session.getAttribute("facilityId")); 
        String query = "";
        
        switch(entity){
            case 1:
                query = "SELECT patient.*, DATEDIFF(YEAR, date_birth, CURDATE()) AS age FROM patient WHERE facility_id IN (" + facilityIds + ") AND current_status IN ('HIV+ non ART', 'ART Start', 'ART Restart', 'ART Transfer In', 'Pre-ART Transfer In') AND date_started IS NULL ";
            break;
            
            case 2:
                query = "SELECT DISTINCT patient.* FROM patient WHERE facility_id IN (" + facilityIds + ") AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND TIMESTAMPDIFF(DAY, date_last_refill + last_refill_duration, CURDATE()) > 90 AND date_started IS NOT NULL";
            break;
            
            case 3:
                query = "SELECT patient.*, DATEDIFF(YEAR, date_birth, CURDATE()) AS age FROM patient WHERE facility_id IN (" + facilityIds + ") AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND date_last_refill IS NULL";
            break;
            
            case 4:
                query = "SELECT patient.*, DATEDIFF(YEAR, date_birth, CURDATE()) AS age FROM patient WHERE facility_id IN (" + facilityIds + ") AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND date_last_viral_load IS NULL AND DATEDIFF(MONTH, date_started, CURDATE()) >= 6";
            break;
            
            case 5:
                query = "SELECT patient.*, DATEDIFF(YEAR, date_birth, CURDATE()) AS age FROM patient WHERE facility_id IN (" + facilityIds + ") AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND last_viral_load >1000";
            break;
        }
        
        query += " ORDER BY current_status";
        return query;
    }
    
    public String getQueryCaseManagement(){
        Integer casemanagerId = Integer.parseInt(request.getParameter("casemanagerId"));
        Long facilityId = (Long) session.getAttribute("facilityId");         
        String query = "SELECT patient.*, DATEDIFF(YEAR, date_birth, CURDATE()) AS age FROM patient WHERE facility_id = " + facilityId + " AND casemanager_id = "+casemanagerId+" ORDER BY current_status";
        return query;
    }
      
    private String getNrti(String regimen) {
        String nrti = regimen == null? "" : "Other";
        if(regimen.contains("d4T")) {
            nrti = "D4T (Stavudine)";
        }
        else {
            if(regimen.contains("AZT")) {
                nrti = "AZT (Zidovudine)";
            }
            else {
                if(regimen.contains("TDF")) {
                    nrti = "TDF (Tenofovir)";
                }
                else {
                    if(regimen.contains("DDI")) {
                        nrti = "DDI (Didanosine)";
                    }
                }
            }
        } 
        return nrti;
    }

    private String getNnrti(String regimen) {
        String nnrti = regimen == null? "" : "Other";
        if(regimen.contains("EFV")) {
            nnrti = " EFV (Efavirenz)";
        }
        else {
            if(regimen.contains("NVP")) {
                nnrti = " NVP (Nevirapine)";
            }
        } 
        return nnrti;
    }
    

    private Map getFacility(long facilityId) {
        Map<String, Object> facilityMap = new HashMap<String, Object>(); 
        try{
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            String query = "SELECT DISTINCT facility.name, facility.address1, facility.address2, facility.phone1, facility.phone2, facility.email, lga.name AS lga, state.name AS state FROM facility JOIN lga ON facility.lga_id = lga.lga_id JOIN state ON facility.state_id = state.state_id WHERE facility_id = " + facilityId;
            preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            
            if(rs.next()) {
                facilityMap.put("facilityName", rs.getString("name"));  
                facilityMap.put("lga", rs.getString("lga"));            
                facilityMap.put("state", rs.getString("state")); 
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }                
        return facilityMap;
    }

    private void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }        
    }            

}
