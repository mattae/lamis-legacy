/**
 *
 * @author aalozie
 */

package org.fhi360.lamis.converter;

import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.FileUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.struts2.ServletActionContext;

public class PharmacyDataConverter {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private long userId;
    
    public PharmacyDataConverter() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession();        
    } 
    
    public synchronized String convertExcel() {
        String fileName = "";

        DateFormat dateFormatExcel = new SimpleDateFormat("dd-MMM-yyyy");      
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        String facilityIds = request.getParameter("facilityIds"); 
        String state = request.getParameter("state").toLowerCase();
        userId = (Long) session.getAttribute("userId");
        
        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);  // turn off auto-flushing and accumulate all rows in memory
        Sheet sheet = workbook.createSheet();
        
        try{
            jdbcUtil = new JDBCUtil();
            
            query = "SELECT MAX(visit) AS count FROM (SELECT patient_id, COUNT(DISTINCT date_visit) AS visit FROM pharmacy WHERE facility_id IN (" + facilityIds + ") GROUP BY facility_id, patient_id) AS t1";
            int max_col = getCount(query);

            int rownum = 0;
            int cellnum = 0;
            Row row = sheet.createRow(rownum++);
            Cell cell = row.createCell(cellnum++);
            cell.setCellValue("Facility Id");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Patient Id");
            cell = row.createCell(cellnum++);
            cell.setCellValue("Hospital Num");
            
            for(int i = 1; i <= max_col; i++) {
                cell = row.createCell(cellnum++);
                cell.setCellValue("Date Visit"+Integer.toString(i));
                cell = row.createCell(cellnum++);
                cell.setCellValue("Regimen Line"+Integer.toString(i));
                cell = row.createCell(cellnum++);
                cell.setCellValue("Regimen"+Integer.toString(i));                 
                cell = row.createCell(cellnum++);
                cell.setCellValue("Refill"+Integer.toString(i));                 
                cell = row.createCell(cellnum++);
                cell.setCellValue("Next Appoint"+Integer.toString(i));                 
            }

            query = "SELECT DISTINCT pharmacy.facility_id, pharmacy.patient_id, pharmacy.date_visit, pharmacy.regimentype_id, pharmacy.regimen_id, pharmacy.duration, pharmacy.next_appointment, patient.hospital_num FROM pharmacy JOIN patient ON pharmacy.patient_id = patient.patient_id WHERE pharmacy.facility_id IN (" + facilityIds + ") ORDER BY pharmacy.facility_id, pharmacy.patient_id, pharmacy.date_visit";
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();       
            
            long facilityId = 0;
            long patientId = 0;
            while(resultSet.next()) {
                if(resultSet.getLong("facility_id") != facilityId || resultSet.getLong("patient_id") != patientId) {
                    cellnum = 0;
                    row = sheet.createRow(rownum++);
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet.getLong("facility_id"));
                    facilityId = resultSet.getLong("facility_id"); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet.getLong("patient_id"));
                    patientId = resultSet.getLong("patient_id"); 
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet.getString("hospital_num"));
                }
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("date_visit") == null)? "" : dateFormatExcel.format(resultSet.getDate("date_visit")));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getLong("regimentype_id"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getLong("regimen_id"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getInt("duration"));
                cell = row.createCell(cellnum++);
                cell.setCellValue((resultSet.getDate("next_appointment") == null)? "" : dateFormatExcel.format(resultSet.getDate("next_appointment")));

                if(rownum % 100 == 0) {
                    ((SXSSFSheet)sheet).flushRows(100); // retain 100 last rows and flush all others

                    // ((SXSSFSheet)sheet).flushRows() is a shortcut for ((SXSSFSheet)sheet).flushRows(0),
                    // this method flushes all rows
               }                                
            }           
            
            String directory = contextPath+"transfer/";
            
            FileUtil fileUtil = new FileUtil();
            fileUtil.makeDir(directory);
            fileUtil.makeDir(request.getContextPath()+"/transfer/");
            
            fileName = "pharmacy_"+state+"_"+Long.toString(userId)+".xlsx";
            FileOutputStream outputStream = new FileOutputStream(new File(directory+fileName));
            workbook.write(outputStream);
            outputStream.close();
            workbook.dispose();  // dispose of temporary files backing this workbook on disk
            
            //for servlets in the default(root) context, copy file to the transfer folder in root 
            if(!contextPath.equalsIgnoreCase(request.getContextPath())) fileUtil.copyFile(fileName, contextPath+"transfer/", request.getContextPath()+"/transfer/");                    
            resultSet = null;                        
        }
        catch (Exception exception) {
            resultSet = null;            
            exception.printStackTrace();
        }
        return "transfer/"+fileName;
    }
    
    private void executeUpdate(String query) {
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }        
    
    private int getCount(String query) {
       int count  = 0;
       try {
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                count = resultSet.getInt("count");
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return count;
    }      

    private String getContextPath() {
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        //String contextPath = ServletActionContext.getServletContext().getRealPath(File.separator).replace("\\", "/");
        return contextPath;
    }

}
