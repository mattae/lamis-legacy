/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.converter;

import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.FileUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;

/**
 *
 * @author user1
 */
public class ViralLoadSummaryConverter {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet, resultSet2;
    private long userId;
    
    public ViralLoadSummaryConverter() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession();        
    } 
    
    public synchronized String convertExcel() {
        userId = (Long) session.getAttribute("userId");
        String fileName = "";
        
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        String facilityIds = request.getParameter("facilityIds"); 
        String state = request.getParameter("state").toLowerCase();
        
        System.out.println("date1: "+ request.getParameter("reportingDateBegin"));
        System.out.println("date2: "+ request.getParameter("reportingDateEnd"));
  
        String reportingDateBegin = DateUtil.formatDateString(request.getParameter("reportingDateBegin"), "MM/dd/yyyy", "yyyy-MM-dd");         
        String reportingDateEnd = DateUtil.formatDateString(request.getParameter("reportingDateEnd"), "MM/dd/yyyy", "yyyy-MM-dd");
     
       System.out.println("date con: "+ reportingDateBegin);
       System.out.println("date con: "+ reportingDateEnd);
         
        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);  // turn off auto-flushing and accumulate all rows in memory
        Sheet sheet = workbook.createSheet();
        
        try{
            jdbcUtil = new JDBCUtil();
          
            int rownum = 0;
            int cellnum = 0;
            Row row = sheet.createRow(rownum++);
            Cell cell = row.createCell(cellnum++);
            cell.setCellValue("Facility");
            cell = row.createCell(cellnum++);
            cell.setCellValue("No of Viral Load Test");
            cell = row.createCell(cellnum++);
            cell.setCellValue("No of Patients Test");
            
            query = "SELECT COUNT(*) AS total, laboratory.facility_id, facility.name FROM laboratory JOIN facility ON laboratory.facility_id = facility.facility_id WHERE laboratory.facility_id IN (" + facilityIds + ")"
                    + " AND laboratory.date_reported >= '" + reportingDateBegin + "' AND laboratory.date_reported <= '" + reportingDateEnd + "' AND laboratory.labtest_id = 16 GROUP BY facility.facility_id ORDER BY facility.name"; 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();       
            
            while(resultSet.next()) {
                long facilityId = resultSet.getLong("facility_id");
                
                cellnum = 0;
                row = sheet.createRow(rownum++);
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getString("name"));
                cell = row.createCell(cellnum++);
                cell.setCellValue(resultSet.getInt("total"));
                                
                // Count the number of patient tested during the reporting period
                query = "SELECT COUNT(DISTINCT patient_id) AS subtotal FROM laboratory WHERE facility_id = " + facilityId 
                        + " AND laboratory.date_reported >= '" + reportingDateBegin + "' AND laboratory.date_reported <= '" + reportingDateEnd + "' AND laboratory.labtest_id = 16";
                preparedStatement = jdbcUtil.getStatement(query);
                resultSet2 = preparedStatement.executeQuery();       
                if(resultSet2.next()) {
                    cell = row.createCell(cellnum++);
                    cell.setCellValue(resultSet2.getInt("subtotal"));       
                }                
            } 

            String directory = contextPath+"transfer/";
            
            FileUtil fileUtil = new FileUtil();
            fileUtil.makeDir(directory);
            fileUtil.makeDir(request.getContextPath()+"/transfer/");
            
            fileName = "summary_"+state+"_"+Long.toString(userId)+".xlsx";
            FileOutputStream outputStream = new FileOutputStream(new File(directory+fileName));
            workbook.write(outputStream);
            outputStream.close();
            workbook.dispose();  // dispose of temporary files backing this workbook on disk
            
            //for servlets in the default(root) context, copy file to the transfer folder in root 
            if(!contextPath.equalsIgnoreCase(request.getContextPath())) fileUtil.copyFile(fileName, contextPath+"transfer/", request.getContextPath()+"/transfer/");                    
            resultSet = null;                        
        }
        catch (Exception exception) {
            resultSet = null;            
            exception.printStackTrace();
        }
        return "transfer/"+fileName;
    }    
}
