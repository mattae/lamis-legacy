/**
 *
 * @author Alex
 */
package org.fhi360.lamis.dao.hibernate;

import java.util.*;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.fhi360.lamis.model.Patient;
import org.fhi360.lamis.service.beans.ContextProvider;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

public class PatientDAO {

    private static final HibernateTemplate hibernateTemplate = ContextProvider.getBean(HibernateTemplate.class);

    @Transactional
    public static Long save(Patient patient) {
        return (Long) hibernateTemplate.save(patient);
    }

    public static void update(Patient patient) {
        hibernateTemplate.update(patient);
    }

    public static void delete(Long id) {
        hibernateTemplate.delete(find(id));
    }

    public static Patient find(Long id) {
        return hibernateTemplate.get(Patient.class, id);
    }

    public static Set<Patient> list() {
        return new HashSet<>(hibernateTemplate.loadAll(Patient.class));
    }

    public static String findPhone(Long patientId) {
        String phone = null;
        String query = "select p.phone from Patient p where p.patientId = :id";
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            session.beginTransaction();
            phone = (String) session.createQuery(query).setLong("id", patientId).uniqueResult();
            session.getTransaction().commit();
        } catch (HibernateException e) {
            throw e;
        }
        return phone;
    }
}
