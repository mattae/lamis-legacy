/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.dao.jdbc;

import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.builder.AncListBuilder;
import org.fhi360.lamis.utility.builder.ChildfollowupListBuilder;
import org.fhi360.lamis.utility.builder.DeliveryListBuilder;
import org.fhi360.lamis.utility.builder.MaternalfollowupListBuilder;
import org.fhi360.lamis.utility.builder.PartnerInformationListBuilder;

public class AncJDBC {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public AncJDBC() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession(); 
        try {
            this.jdbcUtil = new JDBCUtil(); 
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }

    public void findAnc(long patientId, String dateVisit) {
        try {
            query = "SELECT * FROM anc WHERE facility_id = ? AND patient_id = ? AND date_visit = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy")); // convert java util date to java sql date format           
            resultSet = preparedStatement.executeQuery();
            new AncListBuilder().buildAncList(resultSet);
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase(); 
        }          
    }
	
    public ResultSet findLastAncVisit(long patientId) {
        try {
            query = "SELECT * FROM anc WHERE facility_id = ? AND patient_id = ? ORDER BY date_visit DESC LIMIT 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase(); 
        }          
        return resultSet;        
    }
    
    public ResultSet findFirstAncVisit(long patientId) {
        try {
            query = "SELECT * FROM anc WHERE facility_id = ? AND patient_id = ? ORDER BY date_visit ASC LIMIT 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase(); 
        }          
        return resultSet;        
    }
    
    public void findDelivery(long patientId, String dateDelivery) {
        try {
            query = "SELECT * FROM delivery WHERE facility_id = ? AND patient_id = ? AND date_delivery = ?";
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateDelivery, "MM/dd/yyyy")); // convert java util date to java sql date format           
            resultSet = preparedStatement.executeQuery();
            new DeliveryListBuilder().buildDeliveryList(resultSet);
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase(); 
        }          
    }
	
    public void findMaternalfollowup(long patientId, String dateVisit) {
        try {
            query = "SELECT * FROM maternalfollowup WHERE facility_id = ? AND patient_id = ? AND date_visit = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId); 
            preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy")); 
            resultSet = preparedStatement.executeQuery();   
			
            new MaternalfollowupListBuilder().buildMaternalfollowupList(resultSet);
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase(); 
        }          
    }
	
    public void findChildfollowup(long childId, String dateVisit) {
        try {
            query = "SELECT * FROM childfollowup WHERE facility_id = ? AND child_id = ? AND date_visit = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, childId);
            preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy")); // convert java util date to java sql date format           
            resultSet = preparedStatement.executeQuery();
            new ChildfollowupListBuilder().buildChildfollowupList(resultSet);
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase(); 
        }          
    }

    public void findPartnerInformation(long patientId) {
	try {
            query = "SELECT * FROM partnerinformation WHERE facility_id = ? AND patient_id = ? AND date_visit = (SELECT MAX(date_visit) FROM partnerinformation WHERE facility_id = ? AND patient_id = ?)";
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);           
            preparedStatement.setLong(3, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(4, patientId);           
            resultSet = preparedStatement.executeQuery();
            new PartnerInformationListBuilder().buildPartnerinformationList(resultSet);
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase(); 
        }          
    }
    
    
    public int getChildCount(long patientId) {
        int count = 0;
        try {
            query = "SELECT COUNT(*) AS count FROM child WHERE facility_id = ? AND patient_id = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) count =  resultSet.getInt("count");
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase(); 
        }          
        return count;
    }
}
