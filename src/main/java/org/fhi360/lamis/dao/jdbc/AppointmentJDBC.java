/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author user10
 */
public class AppointmentJDBC {
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public long getAppointmentId(long facilityId, long patientId, String dateTracked) {
        try { 
            query = "SELECT appointment_id FROM Appointment WHERE facility_id = " + facilityId + " AND patient_id = " + patientId + " AND date_tracked = '" + dateTracked + "'"; 
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) return resultSet.getLong("appointment_id");
        }
        catch (Exception exception) { 
            jdbcUtil.disconnectFromDatabase();  
        }       
        return 0L;
    }
    
    
    
}
