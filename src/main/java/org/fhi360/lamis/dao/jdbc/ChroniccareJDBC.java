/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.dao.jdbc;

import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.builder.ChroniccareListBuilder;

public class ChroniccareJDBC {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public ChroniccareJDBC() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession(); 
    }

    public void findChroniccare() {    
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM chroniccare WHERE facility_id = ? AND patient_id = ? AND date_visit = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, Long.parseLong(request.getParameter("patientId")));
            preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(request.getParameter("dateVisit"), "MM/dd/yyyy")); // convert java util date to java sql date format           
            resultSet = preparedStatement.executeQuery();
            new ChroniccareListBuilder().buildChroniccareList(resultSet);  
         }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }
    
    public long getChroniccareId(long facilityId, long patientId, String dateVisit) {
        try { 
            jdbcUtil = new JDBCUtil();
            query = "SELECT chroniccare_id FROM chroniccare WHERE facility_id = " + facilityId + " AND patient_id = " + patientId + " AND date_visit = '" + dateVisit + "'"; 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) return resultSet.getLong("chroniccare_id");
        }
        catch (Exception exception) { 
            jdbcUtil.disconnectFromDatabase();  
        }       
        return 0L;
    }

    public ResultSet getLastClinicVisit() {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM chroniccare WHERE facility_id = ? AND patient_id = ? ORDER BY date_visit DESC LIMIT 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, Long.parseLong(request.getParameter("patientId")));
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;        
    }
}
