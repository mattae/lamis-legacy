/**
 *
 * @author AALOZIE
 */

package org.fhi360.lamis.dao.jdbc;

import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.builder.ClinicListBuilder;

public class ClinicJDBC {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public ClinicJDBC() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession(); 
    }

    public void findCommence(long patientId) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM clinic WHERE facility_id = ? AND patient_id = ? AND commence = 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            resultSet = preparedStatement.executeQuery();
            
            new ClinicListBuilder().buildClinicList(resultSet);
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }
    
    public void findClinic(long patientId, String dateVisit) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM clinic WHERE facility_id = ? AND patient_id = ? AND date_visit = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy")); // convert java util date to java sql date format           
            resultSet = preparedStatement.executeQuery();
            new ClinicListBuilder().buildClinicList(resultSet);
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }

    public ResultSet getLastClinicVisit(long patientId) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM clinic WHERE facility_id = ? AND patient_id = ? ORDER BY date_visit DESC LIMIT 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;        
    }

    public ResultSet getArtCommencement(long patientId) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM clinic WHERE facility_id = ? AND patient_id = ? AND commence = 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return resultSet;        
    }    
}
