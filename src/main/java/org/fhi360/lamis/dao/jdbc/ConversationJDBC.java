/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

/**
 *
 * @author Alozie
 */

import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class ConversationJDBC {
    
    public ConversationJDBC() {
    }
    
    public static ResultSet findRecentMessage(String phone) {
       ResultSet resultSet = null;
       JDBCUtil jdbcUtil = null;
       try {
            String query = "SELECT * FROM conversation WHERE phone = '" + phone + "' AND time_stamp = SELECT MAX(time_stamp) FROM conversation WHERE originator_id = 0  AND phone = '" + phone + "'";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery(); 
       }
       catch (Exception exception) {
            resultSet = null;                        
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
       }
       return resultSet;
    }    

    public static ResultSet findMessageByTime(String phone, Timestamp time) {
       ResultSet resultSet = null;
       JDBCUtil jdbcUtil = null;
       try {
            String query = "SELECT participant.*, conversation.* FROM participant JOIN conversation ON participant.phone = conversation.phone WHERE conversation.originator_id = 0  AND conversation.phone = '" + phone + "' AND conversation.time_stamp = '" + time + "'";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery(); 
       }
       catch (Exception exception) {
            resultSet = null;                        
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
       }
       return resultSet;
    }    
    
}
