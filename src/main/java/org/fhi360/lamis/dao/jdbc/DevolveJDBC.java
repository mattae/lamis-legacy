/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.builder.DevolveListBuilder;

/**
 *
 * @author user1
 */
public class DevolveJDBC {
    private String query;
    private static JDBCUtil jdbcUtil;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;
    private HttpServletRequest request;
    private HttpSession session;

    public DevolveJDBC() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession();        
    }
    
    public void devolved(long patientId, long communitypharmId) {
        System.out.println("Paient ID" + patientId + "  " + communitypharmId);
        try {
            query = "UPDATE patient SET communitypharm_id = " +  communitypharmId + " WHERE patient_id = " + patientId; 
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate(); 
        }
        catch (Exception exception) {
            resultSet = null;                        
            jdbcUtil.disconnectFromDatabase();
        }
    }

    public void devolved(long patientId) {
        long communitypharmId = 0;
        ResultSet rs = getLastDevolvement(patientId);
        try {
            if(resultSet.next()) {
                communitypharmId = resultSet.getLong("communitypharm_id");
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        devolved(patientId, communitypharmId); 
    }
    
    public ResultSet getFirstDevolvement(long patientId) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM devolve WHERE patient_id = " + patientId + " ORDER BY date_devolved ASC LIMIT 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;        
    }
    
    public ResultSet getLastDevolvement(long patientId) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM devolve WHERE patient_id = " + patientId + " ORDER BY date_devolved DESC LIMIT 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;        
    }
    
    public void findDevolve(long patientId, String dateVisit) {
        
        
        try {
        System.out.println("DevolveJDBC..........finding DMOC");
            jdbcUtil = new JDBCUtil();
            query = "SELECT devolve.*, patient.patient_id, patient.hospital_num, patient.unique_id, patient.gender, patient.age, patient.surname, patient.other_names, patient.date_started "
                    + " FROM devolve JOIN  patient ON devolve.patient_id = patient.patient_id WHERE devolve.patient_id = ? AND devolve.date_devolved = ?  LIMIT 1";
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, patientId);
            preparedStatement.setDate(2, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy")); // convert java util date to java sql date format           
            resultSet = preparedStatement.executeQuery();
            new DevolveListBuilder().buildDevolveDetailsList(resultSet);
            resultSet = null;                        
        }
        catch (Exception exception) {
            resultSet = null;                        
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }

    
    public void findPatientToDevolve(long patientId) {
        try {
            jdbcUtil = new JDBCUtil();

            executeUpdate("DROP TABLE IF EXISTS commence");        
            query = "CREATE TEMPORARY TABLE commence AS SELECT * FROM clinic WHERE patient_id = " + patientId + " AND commence = 1";
            executeUpdate(query);            

            query = "SELECT patient.patient_id, patient.facility_id, patient.hospital_num, patient.unique_id, patient.surname, patient.other_names, patient.gender, patient.date_birth, patient.age_unit, patient.age, patient.address, patient.date_started, patient.current_status, patient.date_current_status, "
                + " patient.last_viral_load, patient.date_last_viral_load, patient.last_cd4, patient.last_cd4p, patient.date_last_cd4, patient.last_clinic_stage, patient.date_last_clinic, patient.date_last_refill, patient.date_next_clinic, patient.date_next_refill, "
                + " commence.date_visit, commence.clinic_stage AS clinic_stage_commence, commence.cd4 AS cd4_commence, commence.cd4p AS cd4p_commence, commence.regimentype AS regimentype_commence, commence.regimen AS regimen_commence "
                + " FROM patient LEFT OUTER JOIN commence ON patient.patient_id = commence.patient_id WHERE patient.patient_id = " + patientId;
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            new DevolveListBuilder().buildPatientList(resultSet);
            resultSet = null;                        
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase();
        }
    }
    
    private void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }        
        
}
