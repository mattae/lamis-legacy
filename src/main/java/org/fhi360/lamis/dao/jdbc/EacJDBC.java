/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.builder.EacListBuilder;

/**
 *
 * @author user10
 */
public class EacJDBC {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public EacJDBC() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession(); 
    }

    public void findEac(long patientId, String dateVisit) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM eac WHERE facility_id = ? AND patient_id = ? AND date_visit = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy")); // convert java util date to java sql date format           
            resultSet = preparedStatement.executeQuery();
            new EacListBuilder().buildEacList(resultSet);
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }

    public ResultSet getLastEacVisit(long patientId) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM eac WHERE facility_id = ? AND patient_id = ? ORDER BY date_visit DESC LIMIT 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;        
    }
    
}
