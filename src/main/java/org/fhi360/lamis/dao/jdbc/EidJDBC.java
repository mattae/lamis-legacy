/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

/**
 *
 * @author Alozie
 */

import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.struts2.ServletActionContext;

public class EidJDBC {
    
    public EidJDBC() {
    }
    
    public static ResultSet findEidByLabno(String labno) {
       ResultSet resultSet = null;
       JDBCUtil jdbcUtil = null;
       long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
       try {
            String query = "SELECT * FROM eid WHERE facility_id = " + facilityId + " AND labno = '" + labno + "'";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery(); 
       }
       catch (Exception exception) {
            resultSet = null;                        
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
       }
       return resultSet;
    }    

}
