/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author user1
 */
public class EncounterJDBC {
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public long getEncounterId(long facilityId, long patientId, String dateVisit) {
        try { 
            query = "SELECT encounter_id FROM encounter WHERE facility_id = " + facilityId + " AND patient_id = " + patientId + " AND date_visit = '" + dateVisit + "'"; 
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) return resultSet.getLong("encounter_id");
        }
        catch (Exception exception) { 
            jdbcUtil.disconnectFromDatabase();  
        }       
        return 0L;
    }
    
}
