/**
 *
 * @author user1
 */

package org.fhi360.lamis.dao.jdbc;

import org.fhi360.lamis.model.Facility;
import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class FacilityJDBC {
 
    public FacilityJDBC() {
    }
    
    public static void save(Facility facility) {
       long facilityId =  facility.getFacilityId();
       long stateId = facility.getStateId();
       long lgaId = facility.getLga().getLgaId();      
       String name = facility.getName();
       String facilityType = facility.getFacilityType();
       String address1 = facility.getAddress1();
       String address2 = facility.getAddress2();
       String phone1 = facility.getPhone1();
       String phone2 = facility.getPhone2();
       String email = facility.getEmail();
       int padHospitalNum = facility.getPadHospitalNum();

       ResultSet resultSet = null;
       JDBCUtil jdbcUtil = null;
       PreparedStatement preparedStatement = null;
       try {
            String query = "SELECT facility_id FROM facility WHERE facility_id = " + facilityId;
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery(); 
            if(resultSet.next()) {
                query = "UPDATE facility SET state_id = " + stateId + ", lga_id = " + lgaId +  ", name = '" + name + "', facility_type = '" + facilityType + "', address1 = '" + address1 + "', address2 = '" + address2 + "', phone1 = '" + phone1 + "', phone2 = '" + phone2 + "', email = '" + email + "', pad_hospital_num = " + padHospitalNum + " WHERE facility_id = " + facilityId; 
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.executeUpdate();                        
            }
            else {
                query = "INSERT INTO facility(facility_id, state_id, lga_id, name, facility_type, address1, address2, phone1, phone2, email, pad_hospital_num) VALUES(" + facilityId + "," + stateId + "," + lgaId + ",'" + name + "','" + facilityType + "','" + address1 + "','" + address2 + "','" + phone1 + "','" + phone2 + "','" + email + "'," + padHospitalNum + ")"; 
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.executeUpdate();
                updateExchange(facilityId);
            }
            resultSet = null;                                    
        }
        catch (Exception exception) {
            resultSet = null;                        
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }

    public static String getSMSPrinter(long facilityId) {
       String phone = "";
       ResultSet resultSet = null;
       JDBCUtil jdbcUtil = null;
       try {
            String query = "SELECT phone2 FROM facility WHERE facility_id = " + facilityId;
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();             
            if(resultSet.next()) {
                phone = resultSet.getString("phone2");                
            }
            resultSet = null;                                    
        }
        catch (Exception exception) {
            resultSet = null;                                    
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
       return phone;
    }
    
    public static void setSMSPrinter(long facilityId, String phone) {
       JDBCUtil jdbcUtil = null;
       try {
            String query = "Update facility SET phone2 = '" + phone + "' WHERE facility_id = " + facilityId;
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();                        
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }
    
    public static String getFacilityName(long facilityId) {
       String name = "";
       ResultSet resultSet = null;
       JDBCUtil jdbcUtil = null;
       try {
            String query = "SELECT name FROM facility WHERE facility_id = " + facilityId;
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();                       
            if(resultSet.next()) {
                name = resultSet.getString("name");                
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return name;
    }
    
    public static void updateExchange(long facilityId) {
       JDBCUtil jdbcUtil = null;
       PreparedStatement preparedStatement = null;
       try {
            jdbcUtil = new JDBCUtil();
            String query = "INSERT INTO exchange (facility_id, patient, clinic, pharmacy, laboratory, adrhistory, oihistory, adherehistory, statushistory, regimenhistory, chroniccare, dmscreenhistory, tbscreenhistory, specimen, eid, labno, monitor) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, facilityId);
            preparedStatement.setTimestamp(2, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(6, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(7, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(11, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(12, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(13, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(14, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(15, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(16, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setTimestamp(17, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.executeUpdate();                
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }                        
    }
}
