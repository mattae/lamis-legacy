/**
 *
 * @author aalozie
 */

package org.fhi360.lamis.dao.jdbc;

import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.builder.LaboratoryListBuilder;

public class LaboratoryJDBC {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public LaboratoryJDBC() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession();        
    }

    // Retrieve a laboratory record in database
    public void findLaboratory(long patientId, String dateReported) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            query = "SELECT laboratory.laboratory_id, laboratory.patient_id, laboratory.facility_id, laboratory.date_collected, laboratory.date_reported, laboratory.labno, laboratory.resultab, laboratory.resultpc, laboratory.comment, laboratory.labtest_id, labtest.description, labtest.measureab, labtest.measurepc "
                    + " FROM laboratory JOIN labtest ON laboratory.labtest_id = labtest.labtest_id WHERE laboratory.facility_id = ? AND laboratory.patient_id = ? AND laboratory.date_reported = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateReported, "MM/dd/yyyy")); // convert java util date to java sql date format           
            resultSet = preparedStatement.executeQuery();
            new LaboratoryListBuilder().buildLaboratoryList(resultSet);
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }
    
    public ResultSet getLastCd4(long patientId) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 1 ORDER BY date_reported DESC LIMIT 1"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;
    }
    
    public ResultSet getHighestCd4Absolute(long patientId) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            //query = "SELECT resultab, date_reported FROM laboratory WHERE facility_id = ? AND patient_id = ? AND resultab = (SELECT MAX(CONVERT(resultab, INT)) FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 1)"; 
            query = "SELECT resultab, date_reported FROM laboratory WHERE facility_id = ? AND patient_id = ? AND resultab = (SELECT MAX(resultab REGEXP('(^[0-9]+$)')) FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 1)"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setLong(3, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(4, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;        
    }
    
    public ResultSet getHighestCd4AbsoluteAfterArt(long patientId, Date dateStarted) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            //query = "SELECT resultab, date_reported FROM laboratory WHERE facility_id = ? AND patient_id = ? AND date_reported > '" + dateStarted + "' AND resultab = (SELECT MAX(CONVERT(resultab, INT)) FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 1)"; 
            query = "SELECT resultab, date_reported FROM laboratory WHERE facility_id = ? AND patient_id = ? AND date_reported > '" + dateStarted + "' AND resultab = (SELECT MAX(resultab REGEXP('(^[0-9]+$)')) FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 1)"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setLong(3, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(4, patientId);
            resultSet = preparedStatement.executeQuery();
                                   System.out.println("Executed....");

        }
        catch (Exception exception) {
            System.out.println("Error....");
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;        
    }

    public ResultSet getHighestCd4Percentage(long patientId) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            //query = "SELECT resultpc, date_reported FROM laboratory WHERE facility_id = ? AND patient_id = ? AND resultpc = (SELECT MAX(CONVERT(resultpc, INT)) FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 1)"; 
            query = "SELECT resultpc, date_reported FROM laboratory WHERE facility_id = ? AND patient_id = ? AND resultpc = (SELECT MAX(resultpc REGEXP('(^[0-9]+$)')) FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 1)"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setLong(3, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(4, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;        
    }

    public ResultSet getHighestCd4PercentageAfterArt(long patientId, Date dateStarted) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            //query = "SELECT resultpc, date_reported FROM laboratory WHERE facility_id = ? AND patient_id = ? AND date_reported > '" + dateStarted + "' AND resultpc = (SELECT MAX(CONVERT(resultpc, INT)) FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 1)"; 
            query = "SELECT resultpc, date_reported FROM laboratory WHERE facility_id = ? AND patient_id = ? AND date_reported > '" + dateStarted + "' AND resultpc = (SELECT MAX(resultpc REGEXP('(^[0-9]+$)')) FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 1)"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setLong(3, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(4, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
        return resultSet;        
    }
    
    public ResultSet getLastViralLoad(long patientId) {
        try {
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = 16 ORDER BY date_reported DESC LIMIT 1"; 
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return resultSet;
    }        

    public ResultSet getLastViralLoad(long patientId, String dateReported) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM laboratory WHERE patient_id = " + patientId + " AND date_reported <= '" + dateReported + "' AND labtest_id = 16 ORDER BY date_reported DESC LIMIT 1"; 
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return resultSet;
    }        

}


//query = "SELECT resultab, resultpc, date_reported FROM laboratory WHERE facility_id = ? AND patient_id = ? AND date_reported = (SELECT MAX(date_reported) FROM laboratory WHERE facility_id = ? AND patient_id = ? AND labtest_id = (SELECT DISTINCT labtest_id FROM labtest WHERE description = 'HIV Viral Load'))"; 
