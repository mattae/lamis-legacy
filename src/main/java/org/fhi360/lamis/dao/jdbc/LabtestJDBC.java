/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author user1
 */
public class LabtestJDBC {
    private static JDBCUtil jdbcUtil;
    
    public static String getLabtest(Long id) {
        String description = "";
        try {
            String query = "SELECT description FROM labtest WHERE labtest_id = ?";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            // loop through resultSet for each row and put into Map
            while (resultSet.next()) {
                return resultSet.getString("description");
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return description;
    }
    
}
