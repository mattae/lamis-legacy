/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.utility.JDBCUtil;


/**
 *
 * @author user10
 */
public class PatientJDBC {
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public String getHospitalNum(long patientId) {
        String hospitalNum = "";
        String query = "SELECT hospital_num FROM patient WHERE patient_id = " + patientId;
        try {
           jdbcUtil = new JDBCUtil();
           preparedStatement = jdbcUtil.getStatement(query);
           resultSet = preparedStatement.executeQuery();
           if(resultSet.next()) {
                hospitalNum =  resultSet.getString("hospital_num"); 
           }             
           resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return hospitalNum;
    }
    
    public boolean dueViralLoad(long patientId) {
        //Check if client is due for Viral Load test
        boolean due = false;
        try {
           jdbcUtil = new JDBCUtil();
           query = "SELECT patient_id FROM patient WHERE patient_id = " + patientId +" AND viral_load_due_date <= CURDATE()";
           preparedStatement = jdbcUtil.getStatement(query);
           resultSet = preparedStatement.executeQuery();
           if(resultSet.next()){
                due = true;        
           }
        }
        catch (Exception exception) {
            resultSet = null;
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }

    
        return due;
    }

    public ResultSet getCurrentStatus(long patientId) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT DISTINCT current_status, date_current_status FROM statushistory WHERE patient_id = " + patientId + " ORDER BY date_current_status DESC LIMIT 1";  
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return resultSet;        
    }

    public ResultSet getCurrentStatus(long patientId, String dateCurrentStatus) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT DISTINCT current_status, date_current_status FROM statushistory WHERE patient_id = " + patientId + " AND date_current_status <= '" + dateCurrentStatus + "' ORDER BY date_current_status DESC LIMIT 1";  
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return resultSet;        
    }
    
}
