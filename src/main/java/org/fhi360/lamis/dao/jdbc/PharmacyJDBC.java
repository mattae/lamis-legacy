/**
 *
 * @author aalozie
 */

package org.fhi360.lamis.dao.jdbc;

import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.builder.PharmacyListBuilder;

public class PharmacyJDBC {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public PharmacyJDBC() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession();        
    }
    
    // Retrieve a pharmacy record in database
    public void findPharmacy(long patientId, String dateVisit) {
        try { 
            // fetch the required records from the database
            jdbcUtil = new JDBCUtil();
            query = "SELECT pharmacy.pharmacy_id, pharmacy.patient_id, pharmacy.facility_id, pharmacy.date_visit, pharmacy.duration, pharmacy.morning, pharmacy.afternoon, pharmacy.evening, pharmacy.morning+pharmacy.afternoon+pharmacy.evening AS quantity, pharmacy.next_appointment, pharmacy.prescrip_error, pharmacy.adherence, pharmacy.adr_screened, pharmacy.adr_ids, pharmacy.regimentype_id, pharmacy.regimendrug_id, regimendrug.regimen_id, regimendrug.drug_id, drug.name, drug.strength "
                    + " FROM pharmacy JOIN regimendrug ON pharmacy.regimendrug_id = regimendrug.regimendrug_id JOIN drug ON regimendrug.drug_id = drug.drug_id WHERE pharmacy.facility_id = ? AND pharmacy.patient_id = ? AND pharmacy.date_visit = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, patientId);
            preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(request.getParameter("dateVisit"), "MM/dd/yyyy")); // convert java util date to java sql date format           
            resultSet = preparedStatement.executeQuery(); 
            new PharmacyListBuilder().buildPharmacyList(resultSet);
        }
        catch (Exception exception) { 
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }
        
    public ResultSet getLastRefillVisit(long patientId) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT DISTINCT regimentype_id, regimen_id, date_visit, duration FROM pharmacy WHERE patient_id = " + patientId + " AND regimentype_id IN (1, 2, 3, 4, 14) ORDER BY date_visit DESC LIMIT 1";  
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return resultSet;        
    }


    public ResultSet getLastRefillVisit(long patientId, String dateVisit) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT DISTINCT regimentype_id, regimen_id, date_visit, duration FROM pharmacy WHERE patient_id = " + patientId + " AND date_visit <= '" + dateVisit + "' AND regimentype_id IN (1, 2, 3, 4, 14) ORDER BY date_visit DESC LIMIT 1";  
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return resultSet;        
    }

}
