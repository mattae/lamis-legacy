/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.dao.hibernate.PrescriptionDAO;
import org.fhi360.lamis.model.dto.PrescriptionDTO;
import org.fhi360.lamis.utility.Constants;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.ResultSetMapper;
import org.fhi360.lamis.utility.builder.PharmacyListBuilder;

/**
 *
 * @author user10
 */
public class PrescriptionJDBC {
    
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public PrescriptionJDBC() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession();        
    }
    
    // Retrieve a pharmacy record in database
    public static List<PrescriptionDTO> findActivePrescriptions(long patientId, long facilityId) {
        ResultSetMapper<PrescriptionDTO> resultSetMapper = new ResultSetMapper<PrescriptionDTO>();
        ResultSet resultSet = null;
        List<PrescriptionDTO> prescriptionList = null;
        
        try {
            String prescriptionType = "drug";
            JDBCUtil jdbcUtil = new JDBCUtil();
            String query = "SELECT * FROM prescription WHERE patient_id = ? AND facility_id = ? AND prescription_type = ? AND status = ?";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, patientId);
            preparedStatement.setLong(2, facilityId);
            preparedStatement.setString(3, prescriptionType);
            preparedStatement.setInt(4, Constants.Prescription.PRESCRIBED);
            resultSet = preparedStatement.executeQuery();

            prescriptionList = resultSetMapper.mapRersultSetToObject(resultSet, PrescriptionDTO.class);
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        
        return prescriptionList;
    }
    
    public static void changeDrugPrescriptionStatus(Long patientId, Long facilityId, int regimenId, int regimenTypeId, Integer oldStatus, Integer newStatus){
        try{
            String prescriptionType = "drug";
            JDBCUtil jdbcUtil = new JDBCUtil();
            String query = "UPDATE prescription SET status = ?, time_stamp = ? WHERE patient_id = ? AND facility_id = ? AND prescription_type = ? AND status = ? AND regimen_id = ? AND regimentype_id = ?";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setInt(1, newStatus);
            preparedStatement.setTimestamp(2, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setLong(3, patientId);
            preparedStatement.setLong(4, facilityId);
            preparedStatement.setString(5, prescriptionType);
            preparedStatement.setInt(6, oldStatus);
            preparedStatement.setInt(7, regimenId);
            preparedStatement.setInt(8, regimenTypeId);
            
            preparedStatement.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public static void changeLabtestPrescriptionStatus(Long patientId, Long facilityId, int labtestId, Integer oldStatus, Integer newStatus){
        try{
            String prescriptionType = "labtest";
            JDBCUtil jdbcUtil = new JDBCUtil();
            String query = "UPDATE prescription SET status = ?, time_stamp = ? WHERE patient_id = ? AND facility_id = ? AND prescription_type = ? AND status = ? AND labtest_id = ?";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setInt(1, newStatus);
            preparedStatement.setTimestamp(2, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setLong(3, patientId);
            preparedStatement.setLong(4, facilityId);
            preparedStatement.setString(5, prescriptionType);
            preparedStatement.setInt(6, oldStatus);
            preparedStatement.setInt(7, labtestId);
            
            preparedStatement.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public static void changePrescriptionStatusUn(Long patientId, Long facilityId, Integer oldStatus, Integer newStatus, String prescriptionType){
        try{
            //String prescriptionType = "drug";
            JDBCUtil jdbcUtil = new JDBCUtil();
            String query = "UPDATE prescription SET status = ?, time_stamp = ? WHERE patient_id = ? AND facility_id = ? AND prescription_type = ? AND status = ?";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setInt(1, newStatus);
            preparedStatement.setTimestamp(2, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setLong(3, patientId);
            preparedStatement.setLong(4, facilityId);
            preparedStatement.setString(5, prescriptionType);
            preparedStatement.setInt(6, oldStatus);
            
            preparedStatement.executeUpdate();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
        
}
