/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author aalozie
 */
public class RegimenJDBC {
    private static JDBCUtil jdbcUtil;
    
    public static String getRegimentype(Long id) {
        String description = "";
        try {
            jdbcUtil = new JDBCUtil();
            String query = "SELECT description FROM regimentype WHERE regimentype_id = ?";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("description");
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return description;
    }

    public static long getRegimentypeId(String regimentype) {
        long id = 0;
        try {
            jdbcUtil = new JDBCUtil();
            String query = "SELECT regimentype_id FROM regimentype WHERE description = '" + regimentype + "'";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                return resultSet.getLong("regimentype_id");
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return id;
    }
    
    public static String getRegimen(Long id) {
        String description = "";
        try {
            jdbcUtil = new JDBCUtil();
            String query = "SELECT description FROM regimen WHERE regimen_id = ?";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("description");
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return description;
    }    

    public static long getRegimenId(String regimen) {
        long id = 0;
        try {
            jdbcUtil = new JDBCUtil();
            String query = "SELECT regimen_id FROM regimen WHERE description = '" + regimen + "'";
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                return resultSet.getLong("regimen_id");
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return id;
    }

}
