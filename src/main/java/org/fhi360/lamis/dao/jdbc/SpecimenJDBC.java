/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

/**
 *
 * @author Alozie
 */

import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.struts2.ServletActionContext;

public class SpecimenJDBC {
    
    public SpecimenJDBC() {
    }
    
    public static ResultSet findSpecimenByNumber(String labno) {
       ResultSet resultSet = null;
       JDBCUtil jdbcUtil = null;
       String query = "";
       long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
       try {
            query = "SELECT specimen.specimen_id, specimen.specimen_type, specimen.labno, specimen.barcode, specimen.facility_id, specimen.state_id, specimen.lga_id, specimen.treatment_unit_id, specimen.date_received, specimen.date_collected, specimen.date_assay, specimen.date_reported, specimen.date_dispatched, specimen.quality_cntrl, "
                    + " specimen.hospital_num, specimen.result, specimen.reason_no_test, specimen.surname, specimen.other_names, specimen.gender, specimen.date_birth, specimen.age, specimen.age_unit, specimen.address, specimen.phone, specimen.time_stamp, facility.name FROM specimen "
                    + " JOIN facility ON specimen.treatment_unit_id = facility.facility_id WHERE specimen.facility_id = " + facilityId + " AND (specimen.labno LIKE '" + labno + "%'" + " OR specimen.barcode LIKE '" + labno + "%')"; 
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery(); 
       }
       catch (Exception exception) {
            resultSet = null;                        
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
       }
       return resultSet;
    }    
}
