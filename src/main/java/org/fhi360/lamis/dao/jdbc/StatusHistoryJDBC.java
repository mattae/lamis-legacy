/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.dao.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.builder.StatusHistoryBuilder;

/**
 *
 * @author user10
 */
public class StatusHistoryJDBC {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public StatusHistoryJDBC() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession(); 
    }

    public void findStatus(long patientId, String dateCurrentStatus) {
        long facilityId = (Long) session.getAttribute("facilityId");
        try {
            jdbcUtil = new JDBCUtil();

            query = "SELECT DISTINCT patient.facility_id, patient.patient_id, patient.date_registration, patient.status_registration, patient.date_started, statushistory.history_id, statushistory.current_status, statushistory.date_current_status "
                    + " FROM patient JOIN statushistory ON patient.patient_id = statushistory.patient_id WHERE patient.facility_id = ? AND statushistory.facility_id = ? AND patient.patient_id = ? AND statushistory.date_current_status = ?";
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(3, patientId);
            preparedStatement.setDate(4, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy"));         
            resultSet = preparedStatement.executeQuery();
            new StatusHistoryBuilder().buildStatusList(resultSet);
            resultSet = null;                        
        }
        catch (Exception exception) {
            resultSet = null;                        
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }
    
    public void findStatusHistory(long historyId, String dateCurrentStatus) {
        
        long facilityId = (Long) session.getAttribute("facilityId");
        try {
            jdbcUtil = new JDBCUtil();

            query = "SELECT DISTINCT patient.facility_id, patient.patient_id, patient.date_registration, patient.status_registration, patient.date_started, statushistory.history_id, statushistory.current_status, statushistory.date_current_status "
                    + " FROM patient JOIN statushistory ON patient.patient_id = statushistory.patient_id WHERE patient.facility_id = ? AND statushistory.facility_id = ? AND statushistory.history_id = ? "
                    + " AND (statushistory.date_current_status = ? OR statushistory.date_tracked = ?)";
            
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(3, historyId); 
            preparedStatement.setDate(4, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy"));         
            preparedStatement.setDate(5, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy"));  
            resultSet = preparedStatement.executeQuery();
            
            new StatusHistoryBuilder().buildStatusList(resultSet);
            resultSet = null;                        
        }
        catch (Exception exception) {
            resultSet = null;                        
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }
}
