/**
 *
 * @author aalozie
 */
package org.fhi360.lamis.dao.trigger;

import java.sql.PreparedStatement;
import org.fhi360.lamis.utility.JDBCUtil;

public class InitialTriggers {
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    
    public void init() {      
        String[] queries = {"CREATE TRIGGER IF NOT EXISTS after_lga_update AFTER UPDATE ON lga FOR EACH ROW CALL \"com.agama.lamis.dao.trigger.AfterLgaUpdateTrigger\"",
                            "CREATE TRIGGER IF NOT EXISTS after_state_update AFTER UPDATE ON state FOR EACH ROW CALL \"com.agama.lamis.dao.trigger.AfterStateUpdateTrigger\"",
                            "CREATE TRIGGER IF NOT EXISTS after_regimen_update AFTER UPDATE ON regimen FOR EACH ROW CALL \"com.agama.lamis.dao.trigger.AfterRegimenUpdateTrigger\"",
                            "CREATE TRIGGER IF NOT EXISTS after_regimentype_update AFTER UPDATE ON regimentype FOR EACH ROW CALL \"com.agama.lamis.dao.trigger.AfterRegimentypeUpdateTrigger\"",
                            "CREATE TRIGGER IF NOT EXISTS after_status_update AFTER UPDATE ON status FOR EACH ROW CALL \"com.agama.lamis.dao.trigger.AfterStatusUpdateTrigger\""};        
        try {
            jdbcUtil = new JDBCUtil();
            for(String query : queries) {
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.executeUpdate();                
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }
         
    }   
}
