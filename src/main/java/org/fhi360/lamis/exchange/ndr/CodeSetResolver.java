/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

/**
 *
 * @author user1
 */

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.exchange.ndr.schema.CodedSimpleType;
import org.fhi360.lamis.exchange.ndr.schema.FacilityType;
import org.fhi360.lamis.utility.JDBCUtil;

public class CodeSetResolver {    
    private static JDBCUtil jdbcUtil;

    public CodeSetResolver() {
    }
    
    public static CodedSimpleType getRegimen(String regimen) {       
        CodedSimpleType cst = new CodedSimpleType();
        try {
            String query = "SELECT composition, regimentype_id FROM regimen WHERE description = '" + regimen + "'";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                String regimentypeId = Long.toString(rs.getLong("regimentype_id"));
                regimen = rs.getString("composition").trim()+"_"+regimentypeId;
                query = "SELECT * FROM ndrcodeset WHERE sys_description = '" + regimen + "'";
                preparedStatement = preparedStatement = jdbcUtil.getStatement(query);
                rs = preparedStatement.executeQuery();
                if(rs.next()) {
                    cst.setCode(rs.getString("code"));             
                    cst.setCodeDescTxt(rs.getString("code_description"));   
                }
                else {
                    regimen = "Others_"+regimentypeId;
                    query = "SELECT * FROM ndrcodeset WHERE sys_description = '" + regimen + "'";
                    preparedStatement = preparedStatement = jdbcUtil.getStatement(query);
                    rs = preparedStatement.executeQuery();
                    if(rs.next()) {
                        cst.setCode(rs.getString("code"));  
                        cst.setCodeDescTxt(rs.getString("code_description"));  
                    }
                    else {
                        cst = null;
                    }
                }
            }
            else {
                cst = null;
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        } 
        return cst;
    }
    
    public static CodedSimpleType getRegimenById(long regimenId) {       
        CodedSimpleType cst = new CodedSimpleType();
        try {
            String query = "SELECT composition, regimentype_id FROM regimen WHERE regimen_id = " + regimenId;
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                String regimentypeId = Long.toString(rs.getLong("regimentype_id"));
                String regimen = rs.getString("composition").trim()+"_"+regimentypeId;

                query = "SELECT * FROM ndrcodeset WHERE sys_description = '" + regimen + "'";
                preparedStatement = jdbcUtil.getStatement(query);
                rs = preparedStatement.executeQuery();
                if(rs.next()) {
                    cst.setCode(rs.getString("code"));             
                    cst.setCodeDescTxt(rs.getString("code_description")); 
                }
                else {
                    regimen = "Others_"+regimentypeId;
                    query = "SELECT * FROM ndrcodeset WHERE sys_description = '" + regimen + "'";
                    preparedStatement = jdbcUtil.getStatement(query);
                    rs = preparedStatement.executeQuery();
                    if(rs.next()) {
                        cst.setCode(rs.getString("code"));            
                        cst.setCodeDescTxt(rs.getString("code_description"));  
                    }
                    else {
                        cst = null;
                    }
                }
            }
            else {
                cst = null;
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        } 
        return cst;
    }
    
    public static CodedSimpleType getCodedSimpleType(String codeSetNm, String description) {
        CodedSimpleType cst = new CodedSimpleType();
        try {
            String query = "SELECT * FROM ndrcodeset WHERE code_set_nm = '" + codeSetNm + "' AND sys_description = '" + description + "'";
            if(codeSetNm.contains(",")){
                codeSetNm = codeSetNm.replace(",", "', '");
                codeSetNm = "'" + codeSetNm + "'";
                query = "SELECT * FROM ndrcodeset WHERE code_set_nm IN (" + codeSetNm + ") AND sys_description = '" + description + "'";
            }
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                cst.setCode(rs.getString("code")); 
                cst.setCodeDescTxt(rs.getString("code_description")); 
            }
            else {
                cst = null;
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();

        }
        return cst;
    }

    public static String getCode(String codeSetNm, String description) {
        String id = "";
        try {
            String query = "SELECT * FROM ndrcodeset WHERE code_set_nm = '" + codeSetNm + "' AND sys_description = '" + description + "'";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                id = rs.getString("code");    
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        } 
        return id;
    }
        
    
    //This method retrieves the FMoH assigned facility ID from the NDR codeset table
    //This ID is used in place of the internal facility ID
    public static FacilityType getFacility(long facilityId) {
	FacilityType facility = new FacilityType();  
        String description = Long.toString(facilityId);
        try {
            String query = "SELECT * FROM ndrcodeset WHERE sys_description = '" + description + "'";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
		facility.setFacilityName(rs.getString("code_description"));
		facility.setFacilityID(rs.getString("code"));
		facility.setFacilityTypeCode("FAC");
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        } 
        return facility;
    }
    
       
}
