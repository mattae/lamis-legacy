/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Map;
import org.fhi360.lamis.exchange.ndr.schema.CommonQuestionsType;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PropertyAccessor;

/**
 *
 * @author user1
 */
public class CommonQuestionsTypeMapper {    
    private JDBCUtil jdbcUtil;
    
    public CommonQuestionsTypeMapper() {
    }
    
    public CommonQuestionsType commonQuestionsType(long patientId) {
        String databaseName = new PropertyAccessor().getDatabaseName();
	CommonQuestionsType common = new CommonQuestionsType();
        try {            
            String query = "SELECT facility_id, hospital_num, TIMESTAMPDIFF(YEAR, date_birth, CURDATE()) AS age, gender, date_registration, status_registration, current_status FROM entity WHERE patient_id = " + patientId;           
            if(databaseName.equalsIgnoreCase("h2")) query = "SELECT facility_id, hospital_num, DATEDIFF(YEAR, date_birth, CURDATE()) AS age, gender, date_registration, status_registration, current_status FROM entity WHERE patient_id = " + patientId;
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
		//Common Questions
                common.setHospitalNumber(rs.getString("hospital_num"));
                if(!rs.getString("status_registration").contains("Transfer In")) {
                    common.setDiagnosisFacility(TreatmentFacility.getFacility(rs.getLong("facility_id")));
                    if(rs.getString("date_registration") != null) common.setDiagnosisDate(DateUtil.getXmlDate(rs.getDate("date_registration")));
                }

	        boolean died = false;
                if(rs.getString("current_status") != null) {
                    died = rs.getString("current_status").equalsIgnoreCase("Known Death");
                }
        	common.setPatientDieFromThisIllness(died); 
                
                // Check pregnancy status is patient is a female
                if(rs.getString("gender").equalsIgnoreCase("Female")) {
                    Map map = PregnancyStatus.getPregnancyStatus(patientId);
                    common.setPatientPregnancyStatusCode((String) map.get("status"));
                    if(map.get("edd") != null) common.setEstimatedDeliveryDate(DateUtil.getXmlDate((Date) map.get("edd")));
                    if(rs.getInt("age") > 0) common.setPatientAge(rs.getInt("age"));                    
                }
            }              
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return common;               
    }
    
}
