/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

import org.fhi360.lamis.exchange.ndr.NdrConverter;
import org.fhi360.lamis.utility.FileUtil;

/**
 *
 * @author user10
 */
public class ConverterUtil {
    
    public static void performNdrConversion(String stateId, String facilityIds, NdrConverter converter, FileUtil fileUtil){
        if(facilityIds.trim().isEmpty() && stateId.trim().isEmpty()) {
            //Extract all facilities
            String query = "SELECT DISTINCT facility_id FROM patient WHERE facility_id IN (SELECT facility_id FROM facility WHERE active = 1 AND datim_id IS NOT NULL AND datim_id != '') ORDER BY facility_id";
            converter.buildMessage(query, true);     
        }
        else {
            //Extract for selected facilities
            if(!facilityIds.trim().isEmpty()) {
                String query = "SELECT DISTINCT facility_id FROM patient WHERE facility_id IN (SELECT facility_id FROM facility WHERE facility_id IN (" + facilityIds + ") AND active = 1 AND datim_id IS NOT NULL AND datim_id != '') ORDER BY facility_id";
//                System.out.println(query);
                converter.buildMessage(query, false);     

//                String facilities[] = facilityIds.split(",");
//                if(!Arrays.toString(facilities).isEmpty() || facilities != null) {
//                    for (int i = 0; i < facilities.length; i++) {
//                        String facilityId = facilities[i];
//                        String folder = ServletActionContext.getServletContext().getInitParameter("contextPath")+ "transfer/temp/" + facilityId + "/";  
//                        fileUtil.makeDir(folder);        
//                        if(uploadFolderService.getUploadFolderStatus(folder).equalsIgnoreCase("unlocked")) {
//                            fileUtil.deleteFileWithExtension(folder, ".xml");
//                            fileUtil.deleteFileWithExtension(folder, ".zip");
//                            uploadFolderService.lockUploadFolder(folder); 
//                            setFileName(converter.buildMessage(Long.parseLong(facilityId)));            
//                            uploadFolderService.unlockUploadFolder(folder); 
//                        }                                
//                    }            
//                }            
            }
            else {
                //Extract for selected State
                if(!stateId.trim().isEmpty()) {
                    String query = "SELECT DISTINCT facility_id FROM patient WHERE facility_id IN (SELECT facility_id FROM facility WHERE state_id = " + stateId + " AND active = 1 AND datim_id IS NOT NULL AND datim_id != '') ORDER BY facility_id";
                    converter.buildMessage(query, false);     
                }
            }
        }
    }
}
