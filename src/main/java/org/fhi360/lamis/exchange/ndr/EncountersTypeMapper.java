/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.dao.jdbc.RegimenJDBC;
import org.fhi360.lamis.exchange.ndr.schema.CodedSimpleType;
import org.fhi360.lamis.exchange.ndr.schema.EncountersType;
import org.fhi360.lamis.exchange.ndr.schema.HIVEncounterType;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PropertyAccessor;
import org.fhi360.lamis.utility.StringUtil;

/**
 *
 * @author user1
 */
public class EncountersTypeMapper {
    private JDBCUtil jdbcUtil;

    public EncountersTypeMapper() {
    }
    
    public EncountersType encounterType(long patientId) {
	EncountersType encounter = new EncountersType();        
        String databaseName = "h2"; //new PropertyAccessor().getDatabaseName();

        //If the statusCode is INITIAL all record is retrieved, if the status Code is UPDATED then the last message timestamp is used to retrieve the appropriate records
        String query = "SELECT clinic_id, date_visit, clinic_stage, func_status, bp, tb_status, body_weight, height, next_appointment FROM visit WHERE patient_id = " + patientId;
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                String clinicStage = resultSet.getString("clinic_stage") == null ? "" : resultSet.getString("clinic_stage");                
                String funcStatus = resultSet.getString("func_status") == null ? "" : resultSet.getString("func_status");
                String bp = resultSet.getString("bp") == null ? "" : resultSet.getString("bp");
                String tbStatus = resultSet.getString("tb_status") == null ? "" : resultSet.getString("tb_status");

                //Encounters
		HIVEncounterType hivEncounter = new HIVEncounterType();
		hivEncounter.setVisitDate(DateUtil.getXmlDate(resultSet.getDate("date_visit")));
		hivEncounter.setVisitID(Long.toString(resultSet.getLong("clinic_id")));
                
                if(!clinicStage.isEmpty()) {
                    clinicStage = CodeSetResolver.getCode("WHO_STAGE", clinicStage);
                    if(!clinicStage.isEmpty()) hivEncounter.setWHOClinicalStage(clinicStage);
                }
                if(!funcStatus.isEmpty()) {
                    funcStatus = CodeSetResolver.getCode("FUNCTIONAL_STATUS", funcStatus);
                    if(!funcStatus.isEmpty())  hivEncounter.setFunctionalStatus(funcStatus);
                }
		if(!tbStatus.isEmpty()) {
                    tbStatus = CodeSetResolver.getCode("TB_STATUS", tbStatus);
                    if(!tbStatus.isEmpty()) hivEncounter.setTBStatus(tbStatus);
                }
                if(!bp.isEmpty()) hivEncounter.setBloodPressure(bp);

                int weight = (int) resultSet.getDouble("body_weight");
                int height = (int) resultSet.getDouble("height");
                if(weight > 0) hivEncounter.setWeight(weight);
                if(height > 0) hivEncounter.setChildHeight(height);
                if(resultSet.getDate("next_appointment") != null) hivEncounter.setNextAppointmentDate(DateUtil.getXmlDate(resultSet.getDate("next_appointment")));
                
                //Check for refill visit close to the date of this clinic visit  
                //and populate the refill variables in the encounter object
                //ARV drug

                query = "SELECT DISTINCT regimentype_id, regimen_id, duration FROM pharm WHERE patient_id = " + patientId + " AND date_visit >= DATE_ADD(" + resultSet.getDate("date_visit") + ", INTERVAL -7 DAY) AND date_visit <= DATE_ADD(" + resultSet.getDate("date_visit") + ", INTERVAL 7 DAY) AND regimentype_id IN (1, 2, 3, 4, 14)";
                if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DISTINCT regimentype_id, regimen_id, duration FROM pharm WHERE patient_id = " + patientId + " AND date_visit >= DATEADD('DAY', -7, '" + resultSet.getDate("date_visit") + "') AND date_visit <= DATEADD('DAY', 7, '" + resultSet.getDate("date_visit") + "') AND regimentype_id IN (1, 2, 3, 4, 14)";
                               
                preparedStatement = jdbcUtil.getStatement(query);
                ResultSet rs = preparedStatement.executeQuery();
                if(rs.next()) {
                    CodedSimpleType cst = CodeSetResolver.getRegimenById(rs.getLong("regimen_id"));
                    if(cst != null) hivEncounter.setARVDrugRegimen(cst);
                }

                //cotrim
                query = "SELECT DISTINCT regimentype_id, regimen_id, duration FROM pharm WHERE patient_id = " + patientId + " AND date_visit >= DATE_ADD(" + resultSet.getDate("date_visit") + ", INTERVAL -7 DAY) AND date_visit <= DATE_ADD(" + resultSet.getDate("date_visit") + ", INTERVAL 7 DAY) AND regimentype_id = 8";
                if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DISTINCT regimentype_id, regimen_id, duration FROM pharm WHERE patient_id = " + patientId + " AND date_visit >= DATEADD('DAY', -7, '" + resultSet.getDate("date_visit") + "') AND date_visit <= DATEADD('DAY', 7, '" + resultSet.getDate("date_visit") + "') AND regimentype_id = 8";

                preparedStatement = jdbcUtil.getStatement(query);
                rs = preparedStatement.executeQuery();
                if(rs.next()) {
                    String description = RegimenJDBC.getRegimentype(rs.getLong("regimentype_id"));
                    CodedSimpleType cst = CodeSetResolver.getCodedSimpleType("REGIMEN_TYPE", description);
                    if(cst != null) hivEncounter.setCotrimoxazoleDose(cst); 
                }
                
                //Check for lab investigation close to the date of this clinic visit  
                //and populate the CD4 variables in the encounter object
                query = "SELECT DISTINCT date_reported, resultab, resultpc FROM lab WHERE patient_id = " + patientId + " AND date_reported >= DATE_ADD(" + resultSet.getDate("date_visit") + ", INTERVAL -7 DAY) AND date_reported <= DATE_ADD(" + resultSet.getDate("date_visit") + ", INTERVAL -7 DAY) AND labtest_id = 1";
                if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DISTINCT date_reported, resultab, resultpc FROM lab WHERE patient_id = " + patientId + " AND date_reported >= DATEADD('DAY', -7, '" + resultSet.getDate("date_visit") + "') AND date_reported <= DATEADD('DAY', 7, '" + resultSet.getDate("date_visit") + "') AND labtest_id = 1";

                preparedStatement = jdbcUtil.getStatement(query);
                rs = preparedStatement.executeQuery();
                if(rs.next()) {
                    String resultab = rs.getString("resultab") == null ? "" : rs.getString("resultab");                 
                    String resultpc = rs.getString("resultpc") == null ? "" : rs.getString("resultpc");

                    int cd4 = resultab.isEmpty()? 0 : StringUtil.isInteger(resultab)? (int) Double.parseDouble(StringUtil.stripCommas(resultab)) : 0;
                    int cd4p = resultpc.isEmpty()? 0 : StringUtil.isInteger(resultpc)? (int) Double.parseDouble(StringUtil.stripCommas(resultpc)) : 0;                   
                    if(cd4 > 0) {
                        hivEncounter.setCD4(cd4);
                        hivEncounter.setCD4TestDate(DateUtil.getXmlDate(rs.getDate("date_reported")));
                    }
                    else {
                        if(cd4p > 0) {
                            hivEncounter.setCD4(cd4p);
                            hivEncounter.setCD4TestDate(DateUtil.getXmlDate(rs.getDate("date_reported")));
                        }
                    }
                }
                encounter.getHIVEncounter().add(hivEncounter);		
            }              
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return encounter;        
    }

}
