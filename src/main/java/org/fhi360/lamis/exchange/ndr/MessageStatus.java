/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

/**
 *
 * @author user1
 */

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import org.fhi360.lamis.utility.JDBCUtil;

public class MessageStatus {
    private static JDBCUtil jdbcUtil;

    public  MessageStatus() {
    }
    
    public static Map getMessageStatus(long patientId) {        
        Map<String, Object> map = new HashMap<String, Object>();  
        map.put("statusCode", "INITIAL");
        map.put("lastMessage", null);

        String query = "SELECT last_message FROM msg WHERE patient_id = " + patientId;
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                if(rs.getTimestamp("last_message") != null) {
                    map.put("statusCode", "UPDATED");
                    map.put("lastMessage", rs.getTimestamp("last_message"));                    
                }
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return map;        
    }
    
    public static long getLastMessageId() {
        long messageId = 0L;
        try {
            String query = "SELECT MAX(message_id) AS message_id FROM ndrmessagelog"; 
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                messageId =  rs.getLong("message_id");
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return messageId;
    }
}
