/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.ndr;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.Date;
import org.fhi360.lamis.dao.jdbc.RegimenJDBC;
import org.fhi360.lamis.exchange.ndr.schema.CodedSimpleType;
import org.fhi360.lamis.exchange.ndr.schema.ConditionType;
import org.fhi360.lamis.exchange.ndr.schema.RegimenType;
import org.fhi360.lamis.model.Regimenhistory;
import org.fhi360.lamis.dao.jdbc.RegimenIntrospector;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author user1
 */
public class RegimenTypeMapper {
    private long patientId;
    private JDBCUtil jdbcUtil;

    public RegimenTypeMapper() {
    }
    
    public ConditionType regimenType(long patientId, ConditionType condition) {
        this.patientId = patientId;
               
        RegimenType regimenType = null;
        String dateVisit = "";
        long previousRegimenId = 0;
        Date dateRegimenStarted = null;
        String reasonSwitchedSubs = "";
        //for the last record in the resultset
        Date date  = null;
        long regimenId =  0;
        
        try { 
            //The query is sorted by date of visit and then by regimen id, beacuse we need to know when a regimen changes chronologically
            String query = "SELECT DISTINCT pharmacy_id, date_visit, regimen_id, regimentype_id, duration FROM pharm WHERE regimentype_id IN (1, 2, 3, 4, 14) AND duration > 0.0 AND patient_id = " + patientId + " ORDER BY date_visit, regimen_id";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                date = rs.getDate("date_visit");
                regimenId = rs.getLong("regimen_id");
                //If this is a new dispensing encounter, reset previous regimen line
                if(!dateVisit.equals(DateUtil.parseDateToString(rs.getDate("date_visit"), "yyyy-MM-dd"))) {
                    //Add regimen to condition if this a new dispensing encounter and regimentype object is not null (ie the first time in the loop)
                    if(regimenType != null) {
                        //Check for change of regime here and set date the previous regimen ended 
                        if(previousRegimenId != 0) {
                            if(RegimenIntrospector.substitutedOrSwitched(previousRegimenId, rs.getLong("regimen_id"))){
                                regimenType.setDateRegimenEnded(DateUtil.getXmlDate(rs.getDate("date_visit")));
                            }                            
                        }
                        //Add other drugs dispensed on the same date before adding regimen to condition
                        regimenType = addOtherDrugs(dateVisit, regimenType);
                        condition.getRegimen().add(regimenType);
                        System.out.println("Regimen type added....");
                    }
                    
                    //Any time the date changes reset the dateVisit variable
                    dateVisit = DateUtil.parseDateToString(rs.getDate("date_visit"), "yyyy-MM-dd");                
                    //Instantiate a new regimen report for each date
                    regimenType = new RegimenType();                   
                    regimenType.setVisitID(Long.toString(rs.getLong("pharmacy_id")));     
                    regimenType.setVisitDate(DateUtil.getXmlDate(rs.getDate("date_visit")));
                }                
                    
                CodedSimpleType cst = CodeSetResolver.getRegimenById(rs.getLong("regimen_id"));
                if(cst != null  && regimenType != null) {
                    regimenType.setPrescribedRegimen(cst);
                    regimenType.setPrescribedRegimenDispensedDate(DateUtil.getXmlDate(rs.getDate("date_visit")));

                    String description = RegimenJDBC.getRegimentype(rs.getLong("regimentype_id"));
                    if(description.contains("ART")) description = "ART";
                    else if(description.contains("TB")) description = "TB";
                    else if(description.contains("PEP")) description = "PEP";
                    else if(description.contains("ARV")) description = "PMTCT";
                    else if(description.contains("CTX")) description = "CTX";
                    regimenType.setPrescribedRegimenTypeCode(CodeSetResolver.getCode("REGIMEN_TYPE", description)); 

                    String regimenLineCode = "";                       
                    if(rs.getLong("regimentype_id") == 1 || rs.getLong("regimentype_id") == 3) {
                        regimenLineCode = "First Line";    
                        if(previousRegimenId != 0 && previousRegimenId != rs.getLong("regimen_id")) {
                            if(RegimenIntrospector.substitutedOrSwitched(previousRegimenId, rs.getLong("regimen_id"))) regimenLineCode = "First Line Substitution";
                        }
                    }
                    if(rs.getLong("regimentype_id") == 2 || rs.getLong("regimentype_id") == 4) {
                        regimenLineCode = "Second Line";
                        if(previousRegimenId != 0 && previousRegimenId != rs.getLong("regimen_id")) {
                            if(RegimenIntrospector.substitutedOrSwitched(previousRegimenId, rs.getLong("regimen_id"))) regimenLineCode = "Second Line Substitution";
                        }
                    }
                    if(rs.getLong("regimentype_id") == 14) regimenLineCode = "Third Line";
                    regimenType.setPrescribedRegimenLineCode(CodeSetResolver.getCode("REGIMEN_LINE", regimenLineCode));

                    int duration = (int) rs.getDouble("duration");
                    regimenType.setPrescribedRegimenDuration(Integer.toString(duration));
                    //regimenType.setReasonForPoorAdherence("");
                    //regimenType.setPoorAdherenceIndicator(1);

                    //If regimen changes set the start date of the regimen
                    if(previousRegimenId != rs.getLong("regimen_id")) {
                        Regimenhistory regimenhistory = RegimenIntrospector.getRegimenHistory(patientId, rs.getLong("regimentype_id"), rs.getLong("regimen_id"));
                        dateRegimenStarted = regimenhistory.getDateVisit();
                        reasonSwitchedSubs = regimenhistory.getReasonSwitchedSubs() == null? "" : regimenhistory.getReasonSwitchedSubs();
                        previousRegimenId = rs.getLong("regimen_id");
                    }
                    
                    if(dateRegimenStarted == null) dateRegimenStarted = getDateRegimenStarted(patientId, rs.getLong("regimen_id"));
                    regimenType.setDateRegimenStarted(DateUtil.getXmlDate(dateRegimenStarted));   
                    if(!reasonSwitchedSubs.isEmpty()) {
                        regimenType.setReasonForRegimenSwitchSubs(reasonSwitchedSubs);
                        //regimenType.setSubstitutionIndicator();
                        //regimenType.setSwitchIndicator();
                    }
                    regimenType.setPrescribedRegimenCurrentIndicator(true);
                }
            }
            if(regimenType != null) {
                //Check for change of regime here and set date the previous regimen ended 
                if(previousRegimenId != 0) {
                    if(RegimenIntrospector.substitutedOrSwitched(previousRegimenId, regimenId)){
                        regimenType.setDateRegimenEnded(DateUtil.getXmlDate(date));
                    }
                }
                //Add other drugs dispensed on the same date before adding regimen to condition
                regimenType = addOtherDrugs(dateVisit, regimenType);
                condition.getRegimen().add(regimenType);
                System.out.println("Regimen type added....");
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }        
        return condition;        
    }
    
    private RegimenType addOtherDrugs(String dateVisit, RegimenType regimenType) {
        long previousRegimenId = 0;
        Date dateRegimenStarted = null;
        try {            
            String query = "SELECT DISTINCT pharmacy_id, date_visit, regimen_id, regimentype_id, duration FROM pharm WHERE regimentype_id IN (5, 6, 7, 8, 9, 10, 11, 12) AND duration > 0.0 AND patient_id = " + patientId + " AND date_visit = '" + dateVisit + "' ORDER BY date_visit, regimentype_id";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String description = RegimenJDBC.getRegimen(rs.getLong("regimen_id"));
                CodedSimpleType cst = CodeSetResolver.getCodedSimpleType("OI_REGIMEN,TB_REGIMEN", description);

                if(cst != null && regimenType != null) {
                    System.out.println("Coded simple....."+cst.getCodeDescTxt());
                    regimenType.setPrescribedRegimen(cst);
                    regimenType.setPrescribedRegimenDispensedDate(DateUtil.getXmlDate(rs.getDate("date_visit")));                        
                    description = RegimenJDBC.getRegimentype(rs.getLong("regimentype_id"));
                    regimenType.setPrescribedRegimenTypeCode(CodeSetResolver.getCode("REGIMEN_TYPE", description));                         

                    int duration = (int) rs.getDouble("duration");
                    regimenType.setPrescribedRegimenDuration(Integer.toString(duration));

                    //If regimen changes set the start date of the regimen
                    if(previousRegimenId != rs.getLong("regimen_id")) {
                        dateRegimenStarted = getDateRegimenStarted(patientId, rs.getLong("regimen_id"));
                        previousRegimenId = rs.getLong("regimen_id");
                    }
                    if(dateRegimenStarted != null) {
                        regimenType.setDateRegimenStarted(DateUtil.getXmlDate(dateRegimenStarted));                        
                    }
                    regimenType.setPrescribedRegimenCurrentIndicator(true);
                }
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return regimenType;
    }

    public  Date getDateRegimenEnded(long patientId, long regimenId) {
        Date dateRegimenEnded = null;
        try {
            String query = "SELECT date_visit, duration FROM pharm WHERE patient_id = " + patientId + " AND regimen_id = " + regimenId + " ORDER BY date_visit DESC LIMIT 1";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                dateRegimenEnded = DateUtil.addDay(rs.getDate("date_visit"), (int) rs.getDouble("duration"));
            }  //Math.round(rs.getDouble("duration"));
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }          
        return dateRegimenEnded;        
    } 

    public Date getDateRegimenStarted(long patientId, long regimenId) {
        Date dateRegimenEnded = null;
        try {
            String query = "SELECT date_visit, duration FROM pharm WHERE patient_id = " + patientId + " AND regimen_id = " + regimenId + " ORDER BY date_visit ASC LIMIT 1";
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                dateRegimenEnded = rs.getDate("date_visit");
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }          
        return dateRegimenEnded;        
    } 

  public void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }        
    }
  
}
