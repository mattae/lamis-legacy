/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.radet;

import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.FileUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.dao.jdbc.DevolveJDBC;
import org.fhi360.lamis.utility.Constants;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.StringUtil;

/**
 *
 * @author user1
 */
public class RadetConverter {
    private Date reportingDateBegin;
    private Date reportingDateEnd;
    private long facilityId;
    private String facilityName;

    private String query;
    private HttpServletRequest request;
    private HttpSession session;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    
    public RadetConverter() {
        this.request = ServletActionContext.getRequest();
        this.session = ServletActionContext.getRequest().getSession();        
        try {
            this.jdbcUtil = new JDBCUtil(); 
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    } 

    public String convertExcel() {
        String fileName = "";
        
        //DateFormat dateFormatExcel = new SimpleDateFormat("dd-MMM-yyyy");        
        //DateFormat dateFormatExcel = new SimpleDateFormat("dd/MM/yy");        
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        int cohortMonthBegin = DateUtil.getMonth(request.getParameter("cohortMonthBegin"));
        int cohortYearBegin = Integer.parseInt(request.getParameter("cohortYearBegin"));
        int cohortMonthEnd = DateUtil.getMonth(request.getParameter("cohortMonthEnd"));
        int cohortYearEnd = Integer.parseInt(request.getParameter("cohortYearEnd")); 
        String cohortDateBegin = DateUtil.parseDateToString(DateUtil.getFirstDateOfMonth(cohortYearBegin, cohortMonthBegin), "yyyy-MM-dd");
        String cohortDateEnd = DateUtil.parseDateToString(DateUtil.getLastDateOfMonth(cohortYearEnd, cohortMonthEnd), "yyyy-MM-dd");
        
        int reportingMonth = DateUtil.getMonth(request.getParameter("reportingMonth"));
        int reportingYear = Integer.parseInt(request.getParameter("reportingYear"));        
        reportingDateEnd = DateUtil.getLastDateOfMonth(reportingYear, reportingMonth);
        reportingDateBegin = DateUtil.getFirstDateOfMonth(reportingYear, reportingMonth);
        
        facilityId = (Long) session.getAttribute("facilityId");
        facilityName = (String) session.getAttribute("facilityName");
        
        //Retrieve all viral load test value done on or before the end of the reporting period
        executeUpdate("DROP TABLE IF EXISTS viralload");
        executeUpdate("CREATE TEMPORARY TABLE viralload AS SELECT patient_id, resultab, date_reported, comment FROM laboratory WHERE facility_id = " + facilityId + " AND date_reported <= '" + DateUtil.parseDateToString(reportingDateEnd, "yyyy-MM-dd") + "' AND labtest_id = 16 ORDER BY patient_id");
        //executeUpdate("CREATE TEMPORARY TABLE viralload AS SELECT patient_id, resultab, date_reported, comment FROM laboratory WHERE facility_id = " + facilityId + " AND date_reported <= '" + dateFormat.format(reportingDateEnd)  + "' AND resultab REGEXP('(^[0-9]+$)') AND labtest_id = 16 ORDER BY patient_id");
        
        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);  // turn off auto-flushing and accumulate all rows in memory
        Sheet sheet = workbook.createSheet();

        //Create a new font
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short)12);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setColor(new HSSFColor.WHITE().getIndex());
        
        //Create a style and set the font into it
        CellStyle style = workbook.createCellStyle();
        //style.setFillForegroundColor(new HSSFColor.WHITE().getIndex());
        style.setFillBackgroundColor(new HSSFColor.BLUE().getIndex());
        style.setFillPattern(HSSFCellStyle.FINE_DOTS );
        style.setFont(font);
        
        int rownum = 0;
        int cellnum = 0;
        Row row = sheet.createRow(rownum++);
        Cell cell = row.createCell(cellnum++);
        cell.setCellValue("S/No.");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Patient Id");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Hospital Num");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Sex");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Age at Start of ART (Years)");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Age at Start of ART (Months) Enter for under 5s");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("ART Start Date (yyyy-mm-dd");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Last Pickup Date (yyyy-mm-dd)");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Months of ARV Refill");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Regimen Line at ART Start");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Regimen at ART Start");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Current Regimen Line");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Current ART Regimen");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Pregnancy Status");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Current Viral Load (c/ml)");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Date of Current Viral Load (yyyy-mm-dd)");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Viral Load Indication");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Current ART Status");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("ART Enrollment Setting");
        cell.setCellStyle(style);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Client Receiving DMOC Service?");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Date Commenced DMOC (yyyy-mm-dd)");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("DMOC of Type");
        cell.setCellStyle(style);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Enhanced Adherence Counselling (EAC) Commenced?");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Date of Commencement of EAC (yyyy-mm-dd)");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Number of EAC Sessions Completed");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Repeat Viral Load - Post EAC VL Sample Collected?");
        cell.setCellStyle(style);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Date of Repeat Viral Load - Post EAC VL Sample Collected");
        cell.setCellStyle(style);
        

        //Create a date format for date columns
        style = workbook.createCellStyle();
        CreationHelper createHelper = workbook.getCreationHelper();
        style.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
        //style.setAlignment(CellStyle.ALIGN_RIGHT);

        try {
            executeUpdate("DROP INDEX IF EXISTS idx_visit");                       
            executeUpdate("DROP TABLE IF EXISTS visit");        
            executeUpdate("CREATE TEMPORARY TABLE visit AS SELECT * FROM clinic WHERE facility_id = " + facilityId + " AND date_visit >= DATEADD('MONTH', -9, '" + DateUtil.parseDateToString(reportingDateBegin, "yyyy-MM-dd") + "') AND date_visit <= '" + DateUtil.parseDateToString(reportingDateEnd, "yyyy-MM-dd") + "'");
            executeUpdate("CREATE INDEX idx_visit ON visit(patient_id)");
            
            executeUpdate("DROP INDEX IF EXISTS idx_commence");                       
            executeUpdate("DROP TABLE IF EXISTS commence");        
            executeUpdate("CREATE TEMPORARY TABLE commence AS SELECT * FROM clinic WHERE facility_id = " + facilityId + " AND commence = 1");
            executeUpdate("CREATE INDEX idx_commence ON commence(patient_id)");

            executeUpdate("DROP INDEX IF EXISTS idx_pharm");                       
            executeUpdate("DROP TABLE IF EXISTS pharm");        
            executeUpdate("CREATE TEMPORARY TABLE pharm AS SELECT * FROM pharmacy WHERE facility_id = " + facilityId + " AND date_visit <= '" + DateUtil.parseDateToString(reportingDateEnd, "yyyy-MM-dd") + "' AND regimentype_id IN (1, 2, 3, 4, 14)");
            executeUpdate("CREATE INDEX idx_pharm ON pharm(patient_id)");

            //Cohort of ART
            query = "SELECT DISTINCT patient_id, hospital_num, unique_id, enrollment_setting, gender, date_birth, date_started FROM patient WHERE facility_id = " + facilityId + " AND date_started >= '" + cohortDateBegin + "' AND date_started <= '" + cohortDateEnd + "'";                         
            resultSet = executeQuery(query);       
            int sno = 1;
            while(resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String uniqueId = resultSet.getString("unique_id") == null ? "" : resultSet.getString("unique_id");
                String hospitalNum = resultSet.getString("hospital_num");
                String gender = resultSet.getString("gender");
                String enrollmentSetting = resultSet.getString("enrollment_setting") == null ? "" : resultSet.getString("enrollment_setting");
                cellnum = 0;
                row = sheet.createRow(rownum++); //Create new row                
                cell = row.createCell(cellnum++); //Create a cell on the new row
                cell.setCellValue(sno++); 
                cell = row.createCell(cellnum++); 
                cell.setCellValue(uniqueId); 
                cell = row.createCell(cellnum++);
                cell.setCellValue(hospitalNum);
                cell = row.createCell(cellnum++);
                cell.setCellValue(gender);
                
                System.out.println("Analysing patent Id"+patientId);
                Date dateBirth = resultSet.getDate("date_birth");
                Date dateStarted = resultSet.getDate("date_started");
                int age = DateUtil.yearsBetweenIgnoreDays(dateBirth, dateStarted); 

                cell = row.createCell(cellnum++);
                if(age >= 5) cell.setCellValue(age);          //cell.setCellValue(Integer.toString(age));
                cell = row.createCell(cellnum++);
                if(age < 5) {
                    age = DateUtil.monthsBetweenIgnoreDays(dateBirth, dateStarted); 
                    cell.setCellValue(age);
                }

                cell = row.createCell(cellnum++);
                if(resultSet.getDate("date_started") != null) {
                    cell.setCellValue(resultSet.getDate("date_started"));  //Set date value
                    cell.setCellStyle(style);                              //Appply date format
                }
               
                //Date of last pickup before the reporting date
                query = "SELECT pharm.date_visit, pharm.duration, regimentype.description AS regimentype, regimen.description AS regimen FROM pharm JOIN regimentype ON pharm.regimentype_id = regimentype.regimentype_id JOIN regimen ON pharm.regimen_id = regimen.regimen_id WHERE pharm.patient_id = " + patientId + " ORDER BY pharm.date_visit DESC LIMIT 1"; 
                ResultSet rs = executeQuery(query); 
                
                Date dateLastRefill = null;
                int duration = 0;
                int monthRefill = 0;
                String regimentype = "";
                String regimen = "";
                
                if(rs.next()) {
                    dateLastRefill = rs.getDate("date_visit");
                    duration = rs.getInt("duration");
                    monthRefill = duration/30;
                    if(monthRefill <= 0) monthRefill = 1;
                    //regimen at last pickup
                    regimentype = rs.getString("regimentype") == null ? "" : rs.getString("regimentype");
                    if(regimentype.contains("ART First Line Adult")) {
                        regimentype = "Adult.1st.Line";
                    }
                    else {
                        if(regimentype.contains("ART Second Line Adult")) {
                            regimentype = "Adult.2nd.Line";
                        }
                        else {
                            if(regimentype.contains("ART First Line Children")) {
                                regimentype = "Peds.1st.Line";
                            }
                            else {
                                if(regimentype.contains("ART Second Line Children")) {
                                    regimentype = "Peds.2nd.Line";
                                }
                                else {
                                    if(regimentype.contains("Third Line")) {
                                        if(age < 5) {
                                            regimentype = "Peds.3rd.Line";
                                        }
                                        else {
                                            regimentype = "Adult.3rd.Line";                                            
                                        }
                                    }
                                    else {
                                        regimentype = "";
                                    }
                                }     
                            }
                        }
                    }
                    if(!regimentype.trim().isEmpty()) {
                        regimen = rs.getString("regimen") == null ? "" : resolveRegimen(rs.getString("regimen"));
                    }
                }                                
                
                cell = row.createCell(cellnum++);
                if(dateLastRefill != null) {
                    cell.setCellValue(dateLastRefill);  //Set date value
                    cell.setCellStyle(style);           //Appply date format
                }

                cell = row.createCell(cellnum++);
                if(dateLastRefill != null) cell.setCellValue(monthRefill);                
                
                //Regimen at start of ART
                query = "SELECT regimentype, regimen FROM commence WHERE patient_id = " + patientId;                
                rs = executeQuery(query);
                String regimentypeStart = "";
                String regimenStart = "";
                if(rs.next()) {
                    regimentypeStart = rs.getString("regimentype") == null ? "" : rs.getString("regimentype");                  
                    if(regimentypeStart.contains("ART First Line Adult")) {
                        regimentypeStart = "Adult.1st.Line";
                    }
                    else {
                        if(regimentypeStart.contains("ART Second Line Adult")) {
                            regimentypeStart = "Adult.2nd.Line";
                        }
                        else {
                            if(regimentypeStart.contains("ART First Line Children")) {
                                regimentypeStart = "Peds.1st.Line";
                            }
                            else {
                                if(regimentypeStart.contains("ART Second Line Children")) {
                                    regimentypeStart = "Peds.2nd.Line";
                                }
                                else {
                                    if(regimentypeStart.contains("Third Line")) {
                                        if(age < 5) {
                                            regimentypeStart = "Peds.3rd.Line";
                                        }
                                        else {
                                            regimentypeStart = "Adult.3rd.Line";                                            
                                        }
                                    }
                                    else {
                                        regimentypeStart = "";
                                    }
                                }     
                            }
                        }
                    }
                    if(!regimentypeStart.trim().isEmpty()) {
                        regimenStart = rs.getString("regimen") == null ? "" : resolveRegimen(rs.getString("regimen"));
                    }
                }
                
                //Regimen at start of ART
                cell = row.createCell(cellnum++);
                cell.setCellValue(regimentypeStart);
                cell = row.createCell(cellnum++);
                cell.setCellValue(regimenStart);
                
                //Current regimen i.e regimen at last pickup
                cell = row.createCell(cellnum++);
                cell.setCellValue(regimentype);
                cell = row.createCell(cellnum++);
                cell.setCellValue(regimen);
                
                //Pregnancy status as at the last clinic visit before the reporting date
                boolean pregnant = false;
                boolean breastfeeding = false;
                if(gender.trim().equals("Female")) {
                    query = "SELECT pregnant, breastfeeding FROM visit WHERE patient_id = " + patientId + " ORDER BY date_visit DESC LIMIT 1"; 
                    rs = executeQuery(query);
                    if(rs.next()) {
                        if(rs.getInt("pregnant") == 1) {
                            pregnant = true;
                        }
                        if(rs.getInt("breastfeeding") == 1) {
                            breastfeeding = true;
                        }
                    }                    
                }
                    
                cell = row.createCell(cellnum++);
                if(gender.trim().equals("Female")) {
                    if(pregnant) {
                        cell.setCellValue("Pregnant");
                    }
                    else {
                        if(breastfeeding) {
                            cell.setCellValue("Breastfeeding");
                        }
                        else {                           
                            cell.setCellValue("Not pregnant");                           
                        }
                    }    

                }                                    
                
                //Last viral load test value on or before the end of reporting date
                String viralLoad = "";
                Date dateOfViralLoad = null;
                boolean unsuppressed = false;
                String comment = "";
                if(DateUtil.monthsBetweenIgnoreDays(dateStarted, reportingDateEnd) >= 6) {
                    query = "SELECT resultab, date_reported, comment FROM viralload WHERE patient_id = " + patientId + " ORDER BY date_reported DESC LIMIT 1";
                    rs = executeQuery(query);
                    if (rs.next()) {
                        viralLoad = rs.getString("resultab") == null ? "" : rs.getString("resultab"); 
                        dateOfViralLoad = rs.getDate("date_reported");
                        comment = rs.getString("comment") == null ? "" : rs.getString("comment"); 
                    }                    
                }
                cell = row.createCell(cellnum++);
                if(!viralLoad.trim().isEmpty()) {
                    if(!StringUtil.isInteger(viralLoad))  viralLoad = "0.0";
                    Double value = Double.valueOf(StringUtil.stripCommas(viralLoad));
                    
                    //Determine is this patient is unsuppressed based on this VL test result
                    if(value > 1000) unsuppressed = true;
                    cell.setCellValue(value.intValue());                   
                }
                cell = row.createCell(cellnum++);
                if(dateOfViralLoad != null) {
                    cell.setCellValue(dateOfViralLoad);  //Set date value
                    cell.setCellStyle(style);           //Appply date format
                }
                cell = row.createCell(cellnum++);
                cell.setCellValue(comment);  
                
                //Current status on or before the reporting date
                query = "SELECT current_status FROM statushistory WHERE facility_id = " + facilityId + " AND patient_id = " + patientId + " AND date_current_status <= '" + DateUtil.parseDateToString(reportingDateEnd, "yyyy-MM-dd") + "' ORDER BY date_current_status DESC LIMIT 1";  
                rs = executeQuery(query);
                String currentStatus = "";
                if (rs.next()) {
                    currentStatus = rs.getString("current_status") == null ? "" : rs.getString("current_status");     
                }
                                               
                if(currentStatus.trim().equalsIgnoreCase("ART Transfer Out")) {
                    currentStatus = "Transferred Out";
                } 
                else {
                    if(currentStatus.trim().equalsIgnoreCase("Stopped Treatment")) {
                        currentStatus = "Stopped";
                    }
                    else {
                        if(currentStatus.trim().equalsIgnoreCase("Known Death")) {
                            currentStatus = "Dead";
                        }
                        else {
                            if(dateLastRefill != null) {
                                //If the last refill date plus refill duration plus 28 days (or 90 days) is before the last day of the reporting month this patient is LTFU    
                                if(!DateUtil.addYearMonthDay(dateLastRefill, duration+Constants.Reports.LTFU_PEPFAR, "DAY").after(reportingDateBegin)) {
                                    currentStatus = "LTFU";
                                }
                                else {
                                    if(currentStatus.trim().equalsIgnoreCase("Lost to Follow Up")) {
                                        currentStatus = "LTFU";
                                    }
                                    else {
                                        currentStatus = currentStatus.trim().equalsIgnoreCase("ART Transfer In") ? "Active-Transfer In" : "Active";                                            
                                    }
                                }                    
                            }
                            else {
                                //If no refill visit mark as Active if Transfer In or date started is not more than 28 days ( or 90 days)
                                if(currentStatus.trim().equalsIgnoreCase("ART Transfer In")) {
                                    currentStatus = "Active-Transfer In";
                                }
                                else {
                                    if(DateUtil.addYearMonthDay(dateStarted, Constants.Reports.LTFU_PEPFAR, "DAY").before(reportingDateBegin)) {
                                        currentStatus = "Active"; 
                                    }
                                    else {
                                        currentStatus = "LTFU";
                                    }                              
                                }            
                            }                                                                            
                        }
                    }
                } 
                cell = row.createCell(cellnum++);
                cell.setCellValue(currentStatus); 
                
                cell = row.createCell(cellnum++);
                cell.setCellValue(enrollmentSetting);  

                //Devolvement Information
                String devolved = "";
                String dateDevolved = "";
                String typeDmoc = "";
                if(unsuppressed) {
                   devolved = "N/A"; 
                }
                else {
                    rs = new DevolveJDBC().getFirstDevolvement(patientId);
                    if(rs != null && rs.next()) {
                        devolved = "Yes";
                        dateDevolved = DateUtil.parseDateToString(rs.getDate("date_devolved"), "yyyy-MM-dd");  
                        typeDmoc = rs.getString("type_dmoc"); 
                    }
                    else {
                        if(DateUtil.addYearMonthDay(dateStarted, 12, "MONTH").before(reportingDateBegin)) {
                            devolved = "N/A";
                        }
                        else {
                            devolved = "No";
                        }
                    }                    
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue(devolved);              
                cell = row.createCell(cellnum++);
                cell.setCellValue(dateDevolved);  
                cell = row.createCell(cellnum++);
                cell.setCellValue(typeDmoc);  

                //Viral Load Monitoring/Enhanced Adherence Counseling
                String adherence = "";
                String dateAdherence = "";
                int sessions = 0;
                String dateSampleCollected = "";
                if(unsuppressed) {
                    query = "SELECT * FROM eac WHERE patient_id = " + patientId + " ORDER BY date_eac1 DESC LIMIT 1"; 
                    rs = executeQuery(query);
                    if(rs.next()) {
                        adherence = "Yes";
                        dateAdherence = DateUtil.parseDateToString(rs.getDate("date_eac1"), "yyyy-MM-dd");  
                        if(rs.getDate("date_eac1") != null) sessions = 1;
                        if(rs.getDate("date_eac2") != null) sessions = 2;
                        if(rs.getDate("date_eac3") != null) sessions = 3;
                        dateSampleCollected = rs.getDate("date_sample_collected") == null? "" : DateUtil.parseDateToString(rs.getDate("date_sample_collected"), "yyyy-MM-dd");  
                    }
                    else {
                        adherence = "No";
                    }
                }
                cell = row.createCell(cellnum++);
                cell.setCellValue(adherence);              
                cell = row.createCell(cellnum++);
                cell.setCellValue(dateAdherence);  
                cell = row.createCell(cellnum++);
                cell.setCellValue(sessions);  
                cell = row.createCell(cellnum++);
                cell.setCellValue(dateSampleCollected);              
            }

            String directory = contextPath+"transfer/";
            
            FileUtil fileUtil = new FileUtil();
            fileUtil.makeDir(directory);
            fileUtil.makeDir(request.getContextPath()+"/transfer/");
            
            fileName = "RADET_"+facilityName+"_"+Integer.toString(cohortYearBegin)+" to "+Integer.toString(cohortYearEnd)+".xlsx";
            FileOutputStream outputStream = new FileOutputStream(new File(directory+fileName));
            workbook.write(outputStream);
            outputStream.close();
            workbook.dispose();  // dispose of temporary files backing this workbook on disk

            //for servlets in the default(root) context, copy file to the transfer folder in root 
            if(!contextPath.equalsIgnoreCase(request.getContextPath())) fileUtil.copyFile(fileName, contextPath+"transfer/", request.getContextPath()+"/transfer/");                    
            resultSet = null;            
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }          
        return "transfer/"+fileName;
    }
   
    private String resolveRegimen(String regimensys) {       
        String regimen = "";
        query = "SELECT regimen FROM regimenresolver WHERE regimensys = '" + regimensys + "'";
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) regimen = rs.getString("regimen");
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return regimen;
    }
    
    private ResultSet executeQuery(String query) {
        ResultSet rs = null;
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return rs;
    }        

    private void executeUpdate(String query) {
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        } 
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }
}



//    if(dateLastRefill != null) {
//        //If the last refill date plus refill duration plus 90 days is before the last day of the reporting month this patient is LTFU     if(DateUtil.addYearMonthDay(lastRefill, duration+90, "day(s)").before(reportingDateEnd)) 
//        if(!DateUtil.addYearMonthDay(dateLastRefill, monthRefill+1, "MONTH").after(reportingDateBegin)) {
//            currentStatus = "LTFU";
//        }
//        else {
//            if(currentStatus.trim().equalsIgnoreCase("Lost to Follow Up")) {
//                currentStatus = "LTFU";
//            }
//            else {
//                currentStatus = currentStatus.trim().equalsIgnoreCase("ART Transfer In") ? "Active-Transfer In" : "Active";                                            
//            }
//        }                    
//    }
