/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.exchange.radet;

/**
 *
 * @author user1
 */
import au.com.bytecode.opencsv.CSVReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.apache.commons.lang3.StringUtils;
import org.fhi360.lamis.dao.jdbc.RegimenJDBC;
import org.fhi360.lamis.dao.jdbc.PharmacyJDBC;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PatientNumberNormalizer;
import org.fhi360.lamis.utility.Scrambler;

public class RadetService_1 {

    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private Scrambler scrambler = new Scrambler();
    
    private long facilityId;
    private long patientId;
    private String uniqueId;
    private String hospitalNum;
    private String surname;
    private String otherNames;
    private String dateBirth;
    private String age;
    private String ageUnit;
    private String gender;
    private String maritalStatus;
    private String address;
    private String phone;
    private String state;
    private String lga;
    private String dateRegistration;
    private String statusRegistration;
    private String dateStarted;
    private String currentStatus;
    private String dateCurrentStatus;
    private String clinicStage;
    private String funcStatus;
    private String cd4;
    private String cd4p;
    private String dateLastRefill;
    private String duration;
    private String regimentype;
    private String regimen;
    private String regimentypeId;
    private String regimenId;
    private String regimenStart;
    private String regimentypeStart;
    private String sortname;
    private String updated;
    
    private ArrayList<Map<String, String>> clientList = new ArrayList<Map<String, String>>();
    private Map<String, Map<String, String>> sortedMaps = new TreeMap<String, Map<String, String>>();

    public RadetService_1() {
        try {
            this.jdbcUtil = new JDBCUtil(); 
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    } 
    
    public void parseFile(String attachmentFileName) {
        executeUpdate("DROP TABLE IF EXISTS radet");
        executeUpdate("CREATE TEMPORARY TABLE radet(hospital_num VARCHAR(25), unique_id VARCHAR(25), found VARCHAR(1))");
        
        facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        String fileName = contextPath + "transfer/" + attachmentFileName;
        if (attachmentFileName.toLowerCase().endsWith("csv")) {
            csvFile(fileName);
        } else {
            if (attachmentFileName.toLowerCase().endsWith("xls") || attachmentFileName.toLowerCase().endsWith("xlsx")) {
                //excelFile(fileName);
            }
        }
    }

    public void csvFile(String fileName) {
        String[] row = null;
        int rowcount = 0;
        try {
            CSVReader csvReader = new CSVReader(new FileReader(fileName));
            while ((row = csvReader.readNext()) != null) {
                initVariables();
                rowcount++;
                if (rowcount > 2) {
                    uniqueId = row[0].replace("'", "");
                    hospitalNum = row[1].replace("'", "");
                    gender = row[2];
                    dateStarted = row[5];
                    if (row[0].trim().equals("") && row[1].trim().equals("") && row[2].trim().equals("")) {
                        break;
                    }

                    dateStarted = DateUtil.formatDateString(dateStarted, "yyyy-MM-dd", "MM/dd/yyyy");
                    //dateStarted = DateUtil.formatDateString(dateStarted, "dd-MMM-yyyy", "MM/dd/yyyy");  //eg String dateString = "13-Sep-2000" to "09/13/2000"; 
                    dateLastRefill = row[6];
                    dateLastRefill = DateUtil.formatDateString(dateLastRefill, "yyyy-MM-dd", "MM/dd/yyyy");
                    regimentype = row[10];
                    regimen = row[11];
                    currentStatus = row[13];
                    
                    addRowToList(rowcount);
                    executeUpdate("INSERT INTO radet (hospital_num, unique_id, found) VALUES('" + hospitalNum + "', '" + uniqueId + "', '" + updated + "')");               
                }
            }
            unmatched(rowcount);
            for (Map.Entry<String, Map<String, String>> entry : sortedMaps.entrySet()) {
                clientList.add(entry.getValue());
            }

            ServletActionContext.getRequest().getSession().setAttribute("clientList", clientList);
            csvReader.close();
        } 
        catch (Exception exception) {
            exception.printStackTrace();
        }          
        finally {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        } 
    }

    private void addRowToList(int rowcount) {
        try {
            //ResultSet rs = executeQuery("SELECT * FROM patient WHERE hospital_num = '" + hospitalNum + "' AND facility_id = " + facilityId + " AND gender = '" + gender + "' AND date_started = '" + DateUtil.parseDateToString(dateStarted, "yyyy-mm-dd") + "'" );                   
            ResultSet rs = executeQuery("SELECT * FROM patient WHERE (TRIM(LEADING '0' FROM hospital_num) = '" + PatientNumberNormalizer.unpadNumber(hospitalNum) + "' OR TRIM(LEADING '0' FROM hospital_num) = '" + uniqueId + "') AND facility_id = " + facilityId);
            if (rs.next()) {
                patientId = rs.getLong("patient_id");
                String id = (rs.getString("unique_id") == null)? "" : rs.getString("unique_id");
                if(!id.trim().equals("")) uniqueId = rs.getString("unique_id");
                surname = (rs.getString("surname") == null)? "" : rs.getString("surname");
                surname = scrambler.unscrambleCharacters(surname);
                surname = StringUtils.upperCase(surname);
                otherNames = (rs.getString("other_names") == null)? "" : rs.getString("other_names");
                otherNames = scrambler.unscrambleCharacters(otherNames);
                otherNames = StringUtils.capitalize(otherNames);
                dateBirth = (rs.getDate("date_birth") == null) ? "" : DateUtil.parseDateToString(rs.getDate("date_birth"), "MM/dd/yyyy");
                age = (rs.getInt("age") == 0) ? "" : Integer.toString(rs.getInt("age"));
                ageUnit = (rs.getString("age_unit") == null) ? "" : rs.getString("age_unit");
                gender = (rs.getString("gender") == null) ? "" : rs.getString("gender");
                maritalStatus = (rs.getString("marital_status") == null) ? "" : rs.getString("marital_status");
                dateRegistration = (rs.getDate("date_registration") == null) ? "" : DateUtil.parseDateToString(rs.getDate("date_registration"), "MM/dd/yyyy");
                currentStatus = (rs.getString("current_status") == null) ? "" : rs.getString("current_status");
                statusRegistration = (rs.getString("status_registration") == null) ? "" : rs.getString("status_registration");
                dateCurrentStatus = (rs.getDate("date_current_status") == null) ? "" : DateUtil.parseDateToString(rs.getDate("date_current_status"), "MM/dd/yyyy");
                state = (rs.getString("state") == null) ? "" : rs.getString("state");
                lga = (rs.getString("lga") == null) ? "" : rs.getString("lga");
                address = (rs.getString("address") == null) ? "" : rs.getString("address");
                address = scrambler.unscrambleCharacters(address);
                address = StringUtils.capitalize(address);
                phone = (rs.getString("phone") == null) ? "" : rs.getString("phone");
                phone = scrambler.unscrambleNumbers(phone);
            }

            if (patientId != 0L) {
                rs = executeQuery("SELECT * FROM clinic WHERE patient_id = " + patientId + " AND facility_id = " + facilityId + " AND commence = 1");
                if (rs.next()) {
                    regimentypeStart = (rs.getString("regimentype") == null) ? "" : rs.getString("regimentype");
                    regimenStart = (rs.getString("regimen") == null) ? "" : rs.getString("regimen");
                    clinicStage = (rs.getString("clinic_stage") == null) ? "" : rs.getString("clinic_stage");
                    funcStatus = (rs.getString("func_status") == null) ? "" : rs.getString("func_status");
                    cd4 = (rs.getDouble("cd4") == 0) ? "" : Double.toString(rs.getDouble("cd4"));
                    cd4p = (rs.getDouble("cd4p") == 0) ? "" : Double.toString(rs.getDouble("cd4p"));
                }
                updated = "2";
                sortname = "B" + Integer.toString(rowcount);
            } 
            else {
                updated = "1";
                sortname = "A" + Integer.toString(rowcount);
            }

            if (patientId != 0L) {
                rs = new PharmacyJDBC().getLastRefillVisit(patientId);
                if (rs.next()) {
                    regimentypeId = Long.toString(rs.getLong("regimentype_id"));
                    regimenId = Long.toString(rs.getLong("regimen_id"));
                    regimentype = RegimenJDBC.getRegimentype(rs.getLong("regimentype_id"));
                    regimen = RegimenJDBC.getRegimen(rs.getLong("regimen_id"));
                    duration = Integer.toString(rs.getInt("duration"));
                    dateLastRefill = (rs.getDate("date_visit") == null) ? "" : DateUtil.parseDateToString(rs.getDate("date_visit"), "MM/dd/yyyy");
                }
            }

            // create an array from object properties 
            Map<String, String> map = new HashMap<String, String>();
            map.put("patientId", (patientId == 0L) ? "" : Long.toString(patientId));
            map.put("uniqueId", uniqueId);
            map.put("hospitalNum", hospitalNum);
            map.put("name", surname + " " + otherNames);
            map.put("surname", surname);
            map.put("otherNames", otherNames);
            map.put("dateBirth", dateBirth);
            map.put("age", age);
            map.put("ageUnit", ageUnit);
            map.put("gender", gender);
            map.put("maritalStatus", maritalStatus);
            map.put("address", address);
            map.put("phone", phone);
            map.put("state", state);
            map.put("lga", lga);
            map.put("dateRegistration", dateRegistration);
            map.put("statusRegistration", statusRegistration);
            map.put("dateStarted", dateStarted);
            map.put("currentStatus", currentStatus);
            map.put("dateCurrentStatus", dateCurrentStatus);
            map.put("clinicStage", clinicStage);
            map.put("funcStatus", funcStatus);
            map.put("cd4", cd4);
            map.put("cd4p", cd4p);
            map.put("dateLastRefill", dateLastRefill);
            map.put("duration", duration);
            map.put("regimentype", regimentype);
            map.put("regimen", regimen);
            map.put("regimentypeId", regimentypeId);
            map.put("regimenId", regimenId);
            map.put("regimentypeStart", regimentypeStart);
            map.put("regimenStart", regimenStart);
            map.put("rowcount", Integer.toString(rowcount));
            map.put("updated", updated);
            sortedMaps.put(sortname, map);

            System.out.println(hospitalNum);
            System.out.println(surname);
            System.out.println(otherNames);
        } 
        catch (Exception exception) {
            exception.printStackTrace();
        }
    }
    
    private void unmatched(int rowcount) {
        try {
            query = "SELECT patient.patient_id, patient.hospital_num, patient.unique_id, patient.surname, patient.other_names, patient.date_birth, patient.age, patient.age_unit, patient.gender, patient.marital_status, patient.date_registration, patient.current_status, patient.status_registration, patient.date_current_status, patient.state, patient.lga, patient.address, patient.phone, patient.date_started, clinic.regimentype, clinic.regimen, clinic.clinic_stage, clinic.func_status, clinic.cd4, clinic.cd4p "
                    + "FROM patient JOIN clinic ON patient.facility_id = clinic.facility_id AND patient.patient_id = clinic.patient_id WHERE patient.facility_id = " + facilityId + " AND patient.date_started IS NOT NULL AND clinic.commence = 1";
            ResultSet rs = executeQuery(query);
            while(rs.next()) {
                initVariables();
                rowcount++;
                hospitalNum = rs.getString("hospital_num");
                String num = PatientNumberNormalizer.unpadNumber(hospitalNum);
                ResultSet r = executeQuery("SELECT * FROM radet WHERE (TRIM(LEADING '0' FROM hospital_num) = '" + num + "' OR TRIM(LEADING '0' FROM unique_id) = '" + num + "')");
                if(!r.next()) {
                    patientId = rs.getLong("patient_id");
                    String id = (rs.getString("unique_id") == null)? "" : rs.getString("unique_id");
                    if(!id.trim().equals("")) uniqueId = rs.getString("unique_id");
                    surname = (rs.getString("surname") == null)? "" : rs.getString("surname");
                    surname = scrambler.unscrambleCharacters(surname);
                    surname = StringUtils.upperCase(surname);
                    otherNames = (rs.getString("other_names") == null)? "" : rs.getString("other_names");
                    otherNames = scrambler.unscrambleCharacters(otherNames);
                    otherNames = StringUtils.capitalize(otherNames);
                    dateBirth = (rs.getDate("date_birth") == null) ? "" : DateUtil.parseDateToString(rs.getDate("date_birth"), "MM/dd/yyyy");
                    age = (rs.getInt("age") == 0) ? "" : Integer.toString(rs.getInt("age"));
                    ageUnit = (rs.getString("age_unit") == null) ? "" : rs.getString("age_unit");
                    gender = (rs.getString("gender") == null) ? "" : rs.getString("gender");
                    maritalStatus = (rs.getString("marital_status") == null) ? "" : rs.getString("marital_status");
                    dateRegistration = (rs.getDate("date_registration") == null) ? "" : DateUtil.parseDateToString(rs.getDate("date_registration"), "MM/dd/yyyy");
                    currentStatus = (rs.getString("current_status") == null) ? "" : rs.getString("current_status");
                    statusRegistration = (rs.getString("status_registration") == null) ? "" : rs.getString("status_registration");
                    dateCurrentStatus = (rs.getDate("date_current_status") == null) ? "" : DateUtil.parseDateToString(rs.getDate("date_current_status"), "MM/dd/yyyy");
                    dateStarted = (rs.getDate("date_started") == null) ? "" : DateUtil.parseDateToString(rs.getDate("date_started"), "MM/dd/yyyy");
                    state = (rs.getString("state") == null) ? "" : rs.getString("state");
                    lga = (rs.getString("lga") == null) ? "" : rs.getString("lga");
                    address = (rs.getString("address") == null) ? "" : rs.getString("address");
                    address = scrambler.unscrambleCharacters(address);
                    address = StringUtils.capitalize(address);
                    phone = (rs.getString("phone") == null) ? "" : rs.getString("phone");
                    phone = scrambler.unscrambleNumbers(phone);
                    regimentypeStart = (rs.getString("regimentype") == null) ? "" : rs.getString("regimentype");
                    regimenStart = (rs.getString("regimen") == null) ? "" : rs.getString("regimen");
                    clinicStage = (rs.getString("clinic_stage") == null) ? "" : rs.getString("clinic_stage");
                    funcStatus = (rs.getString("func_status") == null) ? "" : rs.getString("func_status");
                    cd4 = (rs.getDouble("cd4") == 0) ? "" : Double.toString(rs.getDouble("cd4"));
                    cd4p = (rs.getDouble("cd4p") == 0) ? "" : Double.toString(rs.getDouble("cd4p"));
                    sortname = "C" + Integer.toString(rowcount);
                    updated = "3";
                    
                    r = new PharmacyJDBC().getLastRefillVisit(patientId);
                    if (r.next()) {
                        regimentypeId = Long.toString(r.getLong("regimentype_id"));
                        regimenId = Long.toString(r.getLong("regimen_id"));
                        regimentype = RegimenJDBC.getRegimentype(r.getLong("regimentype_id"));
                        regimen = RegimenJDBC.getRegimen(r.getLong("regimen_id"));
                        duration = Integer.toString(r.getInt("duration"));
                        dateLastRefill = (r.getDate("date_visit") == null) ? "" : DateUtil.parseDateToString(r.getDate("date_visit"), "MM/dd/yyyy");
                    }
                    
                    // create an array from object properties 
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("patientId", (patientId == 0L) ? "" : Long.toString(patientId));
                    map.put("uniqueId", uniqueId);
                    map.put("hospitalNum", hospitalNum);
                    map.put("name", surname + " " + otherNames);
                    map.put("surname", surname);
                    map.put("otherNames", otherNames);
                    map.put("dateBirth", dateBirth);
                    map.put("age", age);
                    map.put("ageUnit", ageUnit);
                    map.put("gender", gender);
                    map.put("maritalStatus", maritalStatus);
                    map.put("address", address);
                    map.put("phone", phone);
                    map.put("state", state);
                    map.put("lga", lga);
                    map.put("dateRegistration", dateRegistration);
                    map.put("statusRegistration", statusRegistration);
                    map.put("dateStarted", dateStarted);
                    map.put("currentStatus", currentStatus);
                    map.put("dateCurrentStatus", dateCurrentStatus);
                    map.put("clinicStage", clinicStage);
                    map.put("funcStatus", funcStatus);
                    map.put("cd4", cd4);
                    map.put("cd4p", cd4p);
                    map.put("dateLastRefill", dateLastRefill);
                    map.put("duration", duration);
                    map.put("regimentype", regimentype);
                    map.put("regimen", regimen);
                    map.put("regimentypeId", regimentypeId);
                    map.put("regimenId", regimenId);
                    map.put("regimentypeStart", regimentypeStart);
                    map.put("regimenStart", regimenStart);
                    map.put("rowcount", Integer.toString(rowcount));
                    map.put("updated", updated);
                    sortedMaps.put(sortname, map);
                }              
            }
        } 
        catch (Exception exception) {
            exception.printStackTrace();
        }
        
    }

    private void excelFile(String fileName) {
        int rowcount = 0;
        try {
            //Create the input stream from xlsx/xls file
            File file = new File(fileName);
            FileInputStream inputStream = new FileInputStream(file);

            //Create Workbook instance for xlsx/xls file input stream
            Workbook workbook = null;
            if (fileName.toLowerCase().endsWith("xlsx")) {
                workbook = new XSSFWorkbook(inputStream);
            } else if (fileName.toLowerCase().endsWith("xls")) {
                workbook = new HSSFWorkbook(inputStream);
            }

            Sheet sheet = workbook.getSheetAt(1);
            Row row = null;
            Cell cell = null;
            Iterator<Row> iterator = sheet.iterator();
            while (iterator.hasNext()) {
                initVariables();
                rowcount++;
                row = iterator.next();
                if (rowcount > 2) {
                    cell = row.getCell(0);
                    if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                        switch (cell.getCellType()) {
                            case Cell.CELL_TYPE_STRING:
                                uniqueId = cell.getStringCellValue();
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                uniqueId = Double.toString(cell.getNumericCellValue());
                                break;
                        }
                    }
                    cell = row.getCell(1);
                    if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                        switch (cell.getCellType()) {
                            case Cell.CELL_TYPE_STRING:
                                hospitalNum = cell.getStringCellValue();
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                hospitalNum = Double.toString(cell.getNumericCellValue());
                                break;
                        }
                    }
                    cell = row.getCell(2);
                    if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                            gender = cell.getStringCellValue();
                        }
                    }
                    cell = row.getCell(5);
                    if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            if (HSSFDateUtil.isCellDateFormatted(cell)) {
                                dateStarted = DateUtil.parseDateToString(cell.getDateCellValue(), "MM/dd/yyyy");
                            }
                        }
                    }
                    cell = row.getCell(6);
                    if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                            if (HSSFDateUtil.isCellDateFormatted(cell)) {
                                dateLastRefill = DateUtil.parseDateToString(cell.getDateCellValue(), "MM/dd/yyyy");    
                            }
                        }
                    }
                    cell = row.getCell(10);
                    if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                            regimentype = cell.getStringCellValue();
                        }
                    }
                    cell = row.getCell(11);
                    if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                            regimen = cell.getStringCellValue();
                        }
                    }
                    cell = row.getCell(13);
                    if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                            currentStatus = cell.getStringCellValue();
                        }
                    }
                    System.out.println(hospitalNum);
                    System.out.println(uniqueId);
                    System.out.println(gender);
                    System.out.println(dateStarted);
                    System.out.println(dateLastRefill);
                    System.out.println(regimentype);
                    //addRowToList(rowcount);
                }
            }
            for (Map.Entry<String, Map<String, String>> entry : sortedMaps.entrySet()) {
                clientList.add(entry.getValue());
            }

            ServletActionContext.getRequest().getSession().setAttribute("clientList", clientList);
            inputStream.close();
            file = null;
            iterator = null;
            workbook = null;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
    
    private void initVariables() {
        patientId = 0L;
        uniqueId = "";
        hospitalNum = "";
        surname = "";
        otherNames = "";
        dateBirth = "";
        age = "";
        ageUnit = "";
        gender = "";
        maritalStatus = "";
        address = "";
        phone = "";
        state = "";
        lga = "";
        dateRegistration = "";
        statusRegistration = "";
        dateStarted = "";
        currentStatus = "";
        dateCurrentStatus = "";
        clinicStage = "";
        funcStatus = "";
        cd4 = "";
        cd4p = "";
        dateLastRefill = "";
        duration = "";
        regimentype = "";
        regimen = "";
        regimentypeId = "";
        regimenId = "";
        regimenStart = "";
        regimentypeStart = "";
        sortname = "";        
    }
    
    public ArrayList<Map<String, Object>> radetReport() {
        ArrayList<Map<String, Object>> reportList = new ArrayList<Map<String, Object>>();
        try {
            preparedStatement = jdbcUtil.getStatement("SELECT * FROM radet WHERE found = '1'");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Map map = new HashMap(); 
                map.put("hospitalNum", rs.getString("hospital_num"));
                map.put("uniqueId", rs.getString("unique_id"));
                reportList.add(map);                      
            }                      
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }          
        return reportList;                
    }

    public ArrayList<Map<String, String>> retrieveClientList() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("clientList") != null) {
            clientList = (ArrayList) session.getAttribute("clientList");
        }
        return clientList;
    }

    public void clearClientList() {
        clientList = retrieveClientList();
        clientList.clear();
        ServletActionContext.getRequest().getSession().setAttribute("clientList", clientList);
    }

    
    private void executeUpdate(String query) {
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }

    private ResultSet executeQuery(String query) {
        ResultSet rs = null;
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return rs;
    }
}
