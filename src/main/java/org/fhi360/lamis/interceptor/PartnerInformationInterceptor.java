/**
 *
 * @author aalozie
 */
package org.fhi360.lamis.interceptor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.builder.PartnerInformationListBuilder;

public class PartnerInformationInterceptor extends AbstractInterceptor {
    private HttpServletRequest request;
    private HttpSession session;
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public String intercept(ActionInvocation invocation) throws Exception {
        String result = invocation.invoke();
        findPartnerInformation();
        return result;
    }
    
    private void findPartnerInformation() {
        request = ServletActionContext.getRequest();
        session = ServletActionContext.getRequest().getSession();
	query = "SELECT * FROM partnerinformation WHERE facility_id = ? AND patient_id = ? AND date_visit = (SELECT MAX(date_visit) FROM  partnerinformation WHERE facility_id = ? AND patient_id = ?)";
	try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(2, Long.parseLong(request.getParameter("patientId")));           
            preparedStatement.setLong(3, (Long) session.getAttribute("facilityId"));
            preparedStatement.setLong(4, Long.parseLong(request.getParameter("patientId")));           
            resultSet = preparedStatement.executeQuery();
            new PartnerInformationListBuilder().buildPartnerinformationList(resultSet);
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }                 
    }
}
