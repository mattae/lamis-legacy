/**
 *
 * @author user1
 */
package org.fhi360.lamis.interceptor.updater;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;

public class DefaulterAttributeUpdater {
    
    public static void nullifyTrackingOutcome(long facilityId, long patientId, String dateVisit) {
        JDBCUtil jdbcUtil = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            jdbcUtil = new JDBCUtil();
            String query = "SELECT current_status, date_started, date_tracked FROM patient WHERE facility_id = ? AND patient_id = ? AND date_tracked <= ?";
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, facilityId); 
            preparedStatement.setLong(2, patientId);               
            preparedStatement.setDate(3, (dateVisit.isEmpty()? null : DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy")));            
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                String currentStatus = resultSet.getString("current_status");
                if((currentStatus.trim().equalsIgnoreCase("Lost to Follow Up") || currentStatus.trim().equalsIgnoreCase("Stopped Treatment") || currentStatus.trim().equalsIgnoreCase("ART Transfer Out")) && resultSet.getDate("date_started") != null) {
                    currentStatus = "ART Restart";  
                    query = "UPDATE patient SET current_status = ?, date_current_status = ?, time_stamp = ?, date_tracked = null, outcome = '', agreed_date = null WHERE facility_id = ? AND patient_id = ?";
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setString(1, currentStatus); 
                    preparedStatement.setDate(2, (dateVisit.isEmpty()? null : DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy")));            
                    preparedStatement.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));                
                    preparedStatement.setLong(4, facilityId); 
                    preparedStatement.setLong(5, patientId);               
                    preparedStatement.executeUpdate();
                    
                    //log ART Restart date in statushistory
                    new StatusHistoryUpdater().logStatusChange(patientId, currentStatus, dateVisit);                    
                }
                else {
                    //Clear tracking outcome and resolve status manually by running DQA
                    query = "UPDATE patient SET time_stamp = ?, date_tracked = null, outcome = '',  agreed_date = null WHERE facility_id = ? AND patient_id = ?";
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setTimestamp(1, new java.sql.Timestamp(new java.util.Date().getTime()));                
                    preparedStatement.setLong(2, facilityId); 
                    preparedStatement.setLong(3, patientId);               
                    preparedStatement.executeUpdate();                                                    
                }
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }    
    }        
}
