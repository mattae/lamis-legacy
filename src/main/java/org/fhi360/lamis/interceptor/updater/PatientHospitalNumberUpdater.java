/**
 *
 * @author aalozie
 */
package org.fhi360.lamis.interceptor.updater;

import java.sql.PreparedStatement;
import org.fhi360.lamis.utility.JDBCUtil;

public class PatientHospitalNumberUpdater {
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    
    public void changeHospitalNum(String hospitalNum, String newHospitalNum, long facilityId) {
        try{
            jdbcUtil = new JDBCUtil();
            query = "UPDATE patient SET hospital_num = ?, time_stamp = ? WHERE facility_id = ? AND hospital_num = ?"; 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setString(1, newHospitalNum);
            preparedStatement.setTimestamp(2, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.setLong(3, facilityId);
            preparedStatement.setString(4, hospitalNum);
            preparedStatement.executeUpdate();                               
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }
    }
    
}
