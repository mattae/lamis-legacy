/**
 *
 * @author user1
 */

package org.fhi360.lamis.interceptor.updater;

import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.dao.jdbc.DevolveJDBC;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.dao.jdbc.RegimenJDBC;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.builder.PharmacyListBuilder;

public class PatientRefillAttributeUpdater {
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private HttpServletRequest request;
    private ResultSet resultSet;

    //This method is called each time a refill record is added or modfied to update the patient refill attribute
    //This method function can be implemented with trigger in the database
    public void lastRefillDate(boolean delete) {
        request = ServletActionContext.getRequest();
        long patientId = Long.parseLong(request.getParameter("patientId"));
        long facilityId = (Long) request.getSession().getAttribute("facilityId");

        // check if ARV was dispensed -> ids 1,2,3,4,14
        ArrayList<Map<String, String>> dispenserList = new ArrayList<Map<String, String>>();
        dispenserList = new PharmacyListBuilder().retrieveDispenserList();
        String s = "1,2,3,4,14";
        boolean ARV = false;
        for(int i = 0; i < dispenserList.size(); i++) {
            String id = (String) dispenserList.get(i).get("regimentypeId"); // retrieve regimentype id from list
            if(s.indexOf(id) != -1) {
                ARV = true;
            }
        }
        
        if(ARV) {
            try {
                query = "SELECT date_visit, regimen_id, regimentype_id, duration, next_appointment FROM pharmacy WHERE patient_id = " + patientId + " AND regimentype_id IN (1, 2, 3, 4, 14) ORDER BY date_visit DESC LIMIT 1";
                ResultSet rs = executeQuery(query);
                if(rs.next()) {
                    String dateVisit = DateUtil.parseDateToString(rs.getDate("date_visit"), "yyyy-MM-dd");
                    double duration = rs.getDouble("duration");
                    String nextAppointment = (rs.getDate("next_appointment") == null)? "" : DateUtil.parseDateToString(rs.getDate("next_appointment"), "yyyy-MM-dd");
                    if(nextAppointment.isEmpty()) nextAppointment = DateUtil.parseDateToString(DateUtil.addDay(rs.getDate("date_visit"), rs.getInt("duration")), "yyyy-MM-dd");
                    String regimentype = RegimenJDBC.getRegimentype(rs.getLong("regimentype_id"));
                    String regimen = RegimenJDBC.getRegimen(rs.getLong("regimen_id"));
                    executeUpdate("UPDATE patient SET regimentype = '" + regimentype + "', regimen = '" + regimen + "', date_last_refill = '" + dateVisit + "', date_next_refill = '" + nextAppointment + "', last_refill_duration = " + duration + ", time_stamp = NOW() WHERE patient_id = " + patientId);                                                                                        

                    //check if the last regimen was logged and log into the regimen history table if not
                    rs = executeQuery("SELECT patient_id FROM regimenhistory WHERE patient_id = " + patientId + " AND regimentype = '" + regimentype + "' AND regimen = '" + regimen + "'");                
                    if(!rs.next()) {
                        executeUpdate("INSERT INTO regimenhistory (patient_id, facility_id, regimentype, regimen, date_visit, reason_switched_subs, time_stamp) VALUES(" + patientId + ", " + facilityId + ", '" + regimentype + "', '" + regimen + "', '" + dateVisit + "', '', NOW())");                
                    }            
                }
                else {
                    executeUpdate("UPDATE patient SET regimentype = '', regimen = '', date_last_refill = null, date_next_refill = null, last_refill_duration = null, time_stamp = NOW() WHERE patient_id = " + patientId);                                                                                                            
                    executeUpdate("DELETE FROM regimenhistory WHERE patient_id = " + patientId);                
                }
                if(!delete) statusUpdateAfterRefill();
            }
            catch (Exception exception) {
                jdbcUtil.disconnectFromDatabase();  //disconnect from database            
            }              
            
        }
    }
    
    private void statusUpdateAfterRefill() {
        System.out.println("Updating status....");
        request = ServletActionContext.getRequest();
        String patientId = request.getParameter("patientId");
        String currentStatus = request.getParameter("currentStatus");
        String dateCurrentStatus = request.getParameter("dateCurrentStatus");
        String dateStarted = request.getParameter("dateStarted");
        String dateVisit = request.getParameter("dateVisit");

        try {
            jdbcUtil = new JDBCUtil();
            if(!dateVisit.isEmpty() && (DateUtil.parseStringToDate(dateVisit, "MM/dd/yyyy")).after(DateUtil.parseStringToDate(dateCurrentStatus, "MM/dd/yyyy"))) {
                if(!dateStarted.isEmpty() && (currentStatus.trim().equalsIgnoreCase("Lost to Follow Up") || currentStatus.trim().equalsIgnoreCase("Stopped Treatment") || currentStatus.trim().equalsIgnoreCase("ART Transfer Out"))) {
                    currentStatus = "ART Restart";                                            
                    query = "UPDATE patient SET current_status = ?, date_current_status = ?, time_stamp = ?, date_tracked = null, outcome = '', agreed_date = null WHERE facility_id = ? AND patient_id = ?";                    
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setString(1, currentStatus); 
                    preparedStatement.setDate(2, DateUtil.parseStringToSqlDate(dateVisit, "MM/dd/yyyy"));            
                    preparedStatement.setTimestamp(3, new java.sql.Timestamp(new Date().getTime()));
                    preparedStatement.setLong(4, (Long) request.getSession().getAttribute("facilityId"));
                    preparedStatement.setLong(5, Long.parseLong(patientId));
                    preparedStatement.executeUpdate();

                    //log ART Restart date in statushistory
                    new StatusHistoryUpdater().logStatusChange(Long.parseLong(patientId), currentStatus, dateVisit);                    
                }
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }    
    }
    
    private void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();  
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }            
    
    private ResultSet executeQuery(String query) {
        ResultSet rs = null;
        try {
            jdbcUtil = new JDBCUtil();  
            preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        } 
        return rs;
    }            
    
}
