/**
 *
 * @author user1
 */
package org.fhi360.lamis.interceptor.updater;

import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.Constants;

public class PatientStatusAttributeUpdater {

    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private HttpServletRequest request;
    private ResultSet resultSet;

    public void updateStatus() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date lastStatusDate;
        Date currentDate;

        request = ServletActionContext.getRequest();
        String patientId = request.getParameter("patientId");
        String currentStatus = request.getParameter("currentStatus");
        String dateCurrentStatus = request.getParameter("dateCurrentStatus");
        String dateTracked = request.getParameter("dateTracked");
        String agreedDate = request.getParameter("agreedDate");
        String causeDeath = request.getParameter("causeDeath");
        String outcome = request.getParameter("outcome");

        System.out.println(".......current Status: " + currentStatus);
        System.out.println(".......outcome: " + outcome);
        System.out.println(".......cause of death: " + causeDeath);
        System.out.println(".......agreed date: " + agreedDate);

        try {
            jdbcUtil = new JDBCUtil();

            lastStatusDate = dateFormat.parse(request.getParameter("lastStatusDate"));
            currentDate = dateFormat.parse(request.getParameter("dateCurrentStatus"));

            if (currentDate.compareTo(lastStatusDate) >= 0) {

                if (outcome.equals(Constants.ArtStatus.ART_START)) {
                    query = "UPDATE patient SET current_status = ?, date_current_status = ?, date_started = ?, outcome = ?, time_stamp = ? WHERE facility_id = ? AND patient_id = ?";
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setString(1, Constants.ArtStatus.ART_START);
                    preparedStatement.setDate(2, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy")); // convert java util date to java sql date format           
                    preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy")); // convert java util date to java sql date format  
                    preparedStatement.setString(4, Constants.ArtStatus.ART_START);
                    preparedStatement.setTimestamp(5, new java.sql.Timestamp(new Date().getTime()));
                    preparedStatement.setLong(6, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
                    preparedStatement.setLong(7, Long.parseLong(request.getParameter("patientId")));
                } else {
                    outcome = outcome.trim();
                    System.out.println(".......Current Status 0: " + outcome.equals(Constants.TxMlStatus.TX_ML_TRANSFER));
                    if (outcome.equalsIgnoreCase(Constants.TxMlStatus.TX_ML_NOT_TRACED)
                            || outcome.equalsIgnoreCase(Constants.TxMlStatus.TX_ML_TRACED)
                            || outcome.equalsIgnoreCase(Constants.TxMlStatus.TX_ML_TRANSFER)) {
                        System.out.println(".......Current Status 0: " + currentStatus);
                        query = "UPDATE patient SET outcome = ?, date_tracked = ?, agreed_date = ?, time_stamp = ? WHERE facility_id = ? AND patient_id = ?";
                        preparedStatement = jdbcUtil.getStatement(query);
                        preparedStatement.setString(1, outcome);
                        preparedStatement.setDate(2, DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy")); // convert java util date to java sql date format
                        preparedStatement.setDate(3, agreedDate.isEmpty() ? null : DateUtil.parseStringToSqlDate(agreedDate, "MM/dd/yyyy")); // convert java util date to java sql date format
                        preparedStatement.setTimestamp(4, new java.sql.Timestamp(new Date().getTime()));
                        preparedStatement.setLong(5, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
                        preparedStatement.setLong(6, Long.parseLong(patientId));
                    } else {
                        if (outcome.equals(Constants.TxMlStatus.TX_ML_DIED)) {
                            System.out.println(".......Current Status 1: " + currentStatus);
                            query = "UPDATE patient SET current_status = ?, date_current_status = ?, outcome = ?, cause_death = ?, date_tracked = ?, time_stamp = ? WHERE facility_id = ? AND patient_id = ?";
                            preparedStatement = jdbcUtil.getStatement(query);
                            preparedStatement.setString(1, currentStatus);
                            preparedStatement.setDate(2, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy")); // convert java util date to java sql date format
                            preparedStatement.setString(3, outcome);
                            preparedStatement.setString(4, causeDeath);

                            preparedStatement.setDate(5, dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy")); // convert java util date to java sql date format
                            preparedStatement.setTimestamp(6, new java.sql.Timestamp(new Date().getTime()));
                            preparedStatement.setLong(7, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
                            preparedStatement.setLong(8, Long.parseLong(patientId));
                        } else {
                            query = "UPDATE patient SET current_status = ?, date_current_status = ?, outcome = ?, date_tracked = ?, time_stamp = ? WHERE facility_id = ? AND patient_id = ?";
                            System.out.println(".......Current Status 2: " + currentStatus);
                            System.out.println(".......Date Current Status 2: " + dateCurrentStatus);
                            System.out.println(".......Patient ID 2: " + Long.parseLong(patientId));
                            preparedStatement = jdbcUtil.getStatement(query);
                            preparedStatement.setString(1, currentStatus);
                            preparedStatement.setDate(2, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy")); // convert java util date to java sql date format
                            preparedStatement.setString(3, outcome);
                            preparedStatement.setDate(4, dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy")); // convert java util date to java sql date format
                            preparedStatement.setTimestamp(5, new java.sql.Timestamp(new Date().getTime()));
                            preparedStatement.setLong(6, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
                            preparedStatement.setLong(7, Long.parseLong(patientId));
                        }
                    }

                }
                preparedStatement.executeUpdate();

            }
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }

    public void updateWithPreviousStatus() {
        request = ServletActionContext.getRequest();
        String patientId = request.getParameter("patientId");
        long facilityId = (Long) request.getSession().getAttribute("facilityId");
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT current_status, date_current_status FROM statushistory WHERE facility_id = " + facilityId + " AND patient_id = " + Long.parseLong(patientId) + " ORDER BY date_current_status DESC LIMIT 1";
            resultSet = executeQuery(query);
            if (resultSet.next()) {
                if (resultSet.getDate("date_current_status") != null) {
                    String currentStatus = resultSet.getString("current_status");
                    String dateCurrentStatus = dateFormat.format(resultSet.getDate("date_current_status"));

                    query = "UPDATE patient SET current_status = ?, date_current_status = ?, time_stamp = ? WHERE facility_id = ? AND patient_id = ?";
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setString(1, currentStatus);
                    preparedStatement.setDate(2, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy")); // convert java util date to java sql date format 
                    preparedStatement.setTimestamp(3, new java.sql.Timestamp(new Date().getTime()));
                    preparedStatement.setLong(4, facilityId);
                    preparedStatement.setLong(5, Long.parseLong(patientId));
                    preparedStatement.executeUpdate();
                }
            } else {
                //If no status is found in the status history table reset current status to status at registration
                query = "UPDATE patient SET current_status = status_registration, date_current_status = date_registration, time_stamp = NOW() WHERE facility_id = ? AND patient_id = ?";
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setLong(1, facilityId);
                preparedStatement.setLong(2, Long.parseLong(patientId));
                preparedStatement.executeUpdate();
            }
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }

    public void updateStatusDefaulter() {
        request = ServletActionContext.getRequest();
        String patientId = request.getParameter("patientId");
        String dateTracked = request.getParameter("dateTracked");
        String outcome = request.getParameter("outcome");
        String agreedDate = request.getParameter("agreedDate");
        try {
            jdbcUtil = new JDBCUtil();
            if (!dateTracked.equals("") && !dateTracked.isEmpty()) {
                if (!agreedDate.equals("") && !agreedDate.isEmpty()) {
                    query = "UPDATE patient SET current_status = ?, date_current_status = ?, date_tracked = ?, outcome = ?, agreed_date = ?, date_next_clinic = ?, date_next_refill = ?, time_stamp = ? WHERE facility_id = ? AND patient_id = ?";
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setString(1, outcome);
                    preparedStatement.setDate(2, (dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy"))); // convert java util date to java sql date format           
                    preparedStatement.setDate(3, (dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy")));
                    preparedStatement.setString(4, outcome);
                    preparedStatement.setDate(5, (agreedDate.isEmpty() ? null : DateUtil.parseStringToSqlDate(agreedDate, "MM/dd/yyyy")));
                    preparedStatement.setDate(6, (agreedDate.isEmpty() ? null : DateUtil.parseStringToSqlDate(agreedDate, "MM/dd/yyyy")));
                    preparedStatement.setDate(7, (agreedDate.isEmpty() ? null : DateUtil.parseStringToSqlDate(agreedDate, "MM/dd/yyyy")));
                    preparedStatement.setTimestamp(8, new java.sql.Timestamp(new Date().getTime()));
                    preparedStatement.setLong(9, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
                    preparedStatement.setLong(10, Long.parseLong(patientId));
                } else {
                    query = "UPDATE patient SET current_status = ?, date_current_status = ?, date_tracked = ?, outcome = ?, agreed_date = ?, time_stamp = ? WHERE facility_id = ? AND patient_id = ?";
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setString(1, outcome);
                    preparedStatement.setDate(2, (dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy"))); // convert java util date to java sql date format           
                    preparedStatement.setDate(3, (dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy")));
                    preparedStatement.setString(4, outcome);
                    preparedStatement.setDate(5, null);
                    preparedStatement.setTimestamp(6, new java.sql.Timestamp(new Date().getTime()));
                    preparedStatement.setLong(7, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
                    preparedStatement.setLong(8, Long.parseLong(patientId));
                }
                preparedStatement.executeUpdate();
            }
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }
    }

    public void updateStatusDefaulter(Map map) {
        String patientId = (String) map.get("patientId");
        String dateTracked = (String) map.get("dateTracked");
        String outcome = (String) map.get("outcome");
        String agreedDate = (String) map.get("agreedDate");
        try {
            jdbcUtil = new JDBCUtil();
            if (!dateTracked.equals("") && !dateTracked.isEmpty()) {
                if (!agreedDate.equals("") && !agreedDate.isEmpty()) {
                    query = "UPDATE patient SET current_status = ?, date_current_status = ?, date_tracked = ?, outcome = ?, agreed_date = ?, date_next_clinic = ?, date_next_refill = ?, time_stamp = ? WHERE facility_id = ? AND patient_id = ?";
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setString(1, outcome);
                    preparedStatement.setDate(2, (dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy"))); // convert java util date to java sql date format           
                    preparedStatement.setDate(3, (dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy")));
                    preparedStatement.setString(4, outcome);
                    preparedStatement.setDate(5, (agreedDate.isEmpty() ? null : DateUtil.parseStringToSqlDate(agreedDate, "MM/dd/yyyy")));
                    preparedStatement.setDate(6, (agreedDate.isEmpty() ? null : DateUtil.parseStringToSqlDate(agreedDate, "MM/dd/yyyy")));
                    preparedStatement.setDate(7, (agreedDate.isEmpty() ? null : DateUtil.parseStringToSqlDate(agreedDate, "MM/dd/yyyy")));
                    preparedStatement.setTimestamp(8, new java.sql.Timestamp(new Date().getTime()));
                    preparedStatement.setLong(9, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
                    preparedStatement.setLong(10, Long.parseLong(patientId));
                } else {
                    query = "UPDATE patient SET current_status = ?, date_current_status = ?, date_tracked = ?, outcome = ?, agreed_date = ?, time_stamp = ? WHERE facility_id = ? AND patient_id = ?";
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setString(1, outcome);
                    preparedStatement.setDate(2, (dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy"))); // convert java util date to java sql date format           
                    preparedStatement.setDate(3, (dateTracked.isEmpty() ? null : DateUtil.parseStringToSqlDate(dateTracked, "MM/dd/yyyy")));
                    preparedStatement.setString(4, outcome);
                    preparedStatement.setDate(5, null);
                    preparedStatement.setTimestamp(6, new java.sql.Timestamp(new Date().getTime()));
                    preparedStatement.setLong(7, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
                    preparedStatement.setLong(8, Long.parseLong(patientId));
                }
                preparedStatement.executeUpdate();
            }
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }
    }

    private ResultSet executeQuery(String query) {
        ResultSet resultSet = null;
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return resultSet;
    }

}
