/**
 *
 * @author user1
 */

package org.fhi360.lamis.interceptor.updater;

import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.model.Patient;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;

public class StatusHistoryUpdater {
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private HttpServletRequest request;
    private Patient patientObjSession;
    private Patient patient;

    //This method is called from the AfterUpdateInterceptor when a new patient is saved or modified
    public void logStatusChange() {
        request = ServletActionContext.getRequest();
        System.out.println("StatusHistoryUpdater...... called");

        if(request.getSession().getAttribute("patient") != null) {
 
            patient = (Patient) request.getSession().getAttribute("patient");  //retrieve patient object from session                                
           if(request.getSession().getAttribute("patientObjSession") != null) {
                patientObjSession = (Patient) request.getSession().getAttribute("patientObjSession");                        
                System.out.println("StatusHistoryUpdate......  status modify: " +patientObjSession.getStatusRegistration());
                modifyStatus(patient.getPatientId(), patient.getStatusRegistration(), patient.getDateRegistration(), patientObjSession.getStatusRegistration(), patientObjSession.getDateRegistration());
            }
            else {
            System.out.println("StatusHistoryUpdate......  status save: " +patient.getStatusRegistration());
                saveStatus(patient.getPatientId(), patient.getStatusRegistration(), patient.getDateRegistration());                        
            }
        }
    }
    
    //This method is called by DefaulterAttributeUpdater class when LTFU and Stopped Treatment changes to Restart
    //It is also called by PatientClinicAttributeUpdater class when ART commencement record is entered
    //It checks for the status ART Start or ART Restart and insert if not exist
    public void logStatusChange(Long patientId, String currentStatus, String dateCurrentStatus) {
        long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT history_id FROM statushistory WHERE facility_id = ? AND patient_id = ? AND current_status = ?";  
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, facilityId); 
            preparedStatement.setLong(2, patientId);                
            preparedStatement.setString(3, currentStatus);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {                   
                query = "UPDATE statushistory SET date_current_status = ?, time_stamp = ? WHERE history_id = ?";  
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setDate(1, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy"));           
                preparedStatement.setTimestamp(2, new java.sql.Timestamp(new Date().getTime()));
                preparedStatement.setLong(3, resultSet.getLong("history_id")); 
                preparedStatement.executeUpdate();                    
            }
            else {
                query = "INSERT INTO statushistory(patient_id, facility_id, current_status, date_current_status, time_stamp) VALUES (?, ?, ?, ?, ?)";
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setLong(1, patientId);
                preparedStatement.setLong(2, facilityId);
                preparedStatement.setString(3, currentStatus);
                preparedStatement.setDate(4,  DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy"));                            
                preparedStatement.setTimestamp(5, new java.sql.Timestamp(new Date().getTime()));
                preparedStatement.executeUpdate();                    
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }
    }
    
    //This method is called by StatusHistoryAction class when client tracking details is saved
    //It checks if the outcome does not exist in statushistory table and insert it
    //If the same date of outcome is found the status is update 
    public void logTrackingOutcome(Long patientId, String currentStatus, String dateCurrentStatus) {
        long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
        try {
            jdbcUtil = new JDBCUtil();
            query = "SELECT * FROM statushistory WHERE facility_id = ? AND patient_id = ? AND (current_status = ? OR date_current_status = ?)";  
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, facilityId); 
            preparedStatement.setLong(2, patientId);                
            preparedStatement.setString(3, currentStatus);
            preparedStatement.setDate(4, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy")); 
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {                   
                query = "SELECT * FROM statushistory WHERE facility_id = ? AND patient_id = ? AND date_current_status = ?";  
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setLong(1, facilityId); 
                preparedStatement.setLong(2, patientId);                
                preparedStatement.setDate(3, DateUtil.parseStringToSqlDate(dateCurrentStatus, "MM/dd/yyyy")); 
                ResultSet resultSet1 = preparedStatement.executeQuery();
                if(resultSet1.next()) {
                    modifyStatus(resultSet1.getLong("history_id"), currentStatus, DateUtil.parseStringToDate(dateCurrentStatus, "MM/dd/yyyy"));
                }
                else {
                    query = "SELECT * FROM statushistory WHERE facility_id = ? AND patient_id = ? AND current_status = ?";  
                    preparedStatement = jdbcUtil.getStatement(query);
                    preparedStatement.setLong(1, facilityId); 
                    preparedStatement.setLong(2, patientId);                
                    preparedStatement.setString(3, currentStatus);
                    resultSet1 = preparedStatement.executeQuery();
                    if(resultSet1.next()) {
                        if(!resultSet1.getString("current_status").equals("Lost to Follow Up")) {
                            modifyStatus(resultSet1.getLong("history_id"), currentStatus, DateUtil.parseStringToDate(dateCurrentStatus, "MM/dd/yyyy"));
                        }
                        else {
                            saveStatus(patientId, currentStatus, DateUtil.parseStringToDate(dateCurrentStatus, "MM/dd/yyyy"));
                        }
                    }                    
                }
            }
            else {
                saveStatus(patientId, currentStatus, DateUtil.parseStringToDate(dateCurrentStatus, "MM/dd/yyyy"));
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }
    }

    public void saveStatus(Long patientId, String currentStatus, Date dateCurrentStatus) {
        long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
        try {
            jdbcUtil = new JDBCUtil();
            query = "INSERT INTO statushistory(patient_id, facility_id, current_status, date_current_status, time_stamp) VALUES (?, ?, ?, ?, ?)";
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, patientId);
            preparedStatement.setLong(2, facilityId);
            preparedStatement.setString(3, currentStatus);
            preparedStatement.setDate(4,  new java.sql.Date(dateCurrentStatus.getTime()));                            
            preparedStatement.setTimestamp(5, new java.sql.Timestamp(new Date().getTime()));
            preparedStatement.executeUpdate();                    
        }
        catch (Exception exception) {
            exception.printStackTrace();
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }
    }
        
    public void modifyStatus(Long patientId, String currentStatus, Date dateCurrentStatus, String previousStatus, Date datePreviousStatus) {
        try {
            jdbcUtil = new JDBCUtil();
            // status at registration or date of registration modified
            if(!currentStatus.equals(previousStatus) || dateCurrentStatus.compareTo(datePreviousStatus) != 0) {   
                query = "UPDATE statushistory SET current_status = ?, date_current_status = ?, time_stamp = ? WHERE patient_id = ? AND facility_id = ? AND current_status = ? AND date_current_status = ?";
                preparedStatement = jdbcUtil.getStatement(query);
                preparedStatement.setString(1, currentStatus);
                preparedStatement.setDate(2, new java.sql.Date(dateCurrentStatus.getTime()));
                preparedStatement.setTimestamp(3, new java.sql.Timestamp(new Date().getTime()));
                preparedStatement.setLong(4, patientId);
                preparedStatement.setLong(5, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
                preparedStatement.setString(6, previousStatus);
                preparedStatement.setDate(7, new java.sql.Date(datePreviousStatus.getTime())); // convert java util date to java sql date format           
                preparedStatement.executeUpdate();                
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }
    }
    
    public void modifyStatus(Long historyId, String currentStatus, Date dateCurrentStatus) {
        try {
            jdbcUtil = new JDBCUtil();
            query = "UPDATE statushistory SET current_status = ?, date_current_status = ?, time_stamp = ? WHERE history_id = ?";
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setString(1, currentStatus);
            preparedStatement.setDate(2, new java.sql.Date(dateCurrentStatus.getTime())); // convert java util date to java sql date format           
            preparedStatement.setTimestamp(3, new java.sql.Timestamp(new Date().getTime()));
            preparedStatement.setLong(4, historyId);
            preparedStatement.executeUpdate();                
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }
    }

    private ResultSet executeQuery(String query) {
        ResultSet resultSet = null;
        try {
            jdbcUtil = new JDBCUtil();            
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return resultSet;
    }        

}
