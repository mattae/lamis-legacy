/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.resource;

/**
 *
 * @author user1
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.dao.hibernate.ApplicationinfoDAO;
import org.fhi360.lamis.model.ApplicationInfo;
import org.fhi360.lamis.utility.DatabaseUpdater;
import org.fhi360.lamis.utility.DateUtil;

public class ApplicationUpdatesWebserviceInvocator {
    private static String serverUrl;
    private static String contextPath;
    private static String message;
    
    public static synchronized String invokeUpdatesService() {
        //Compare verison manifest date on the client against the manifest date on the server
        //if the server date is after the client manifest date, then ask the server to send 
        //the files in the manifest 
        contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        serverUrl = ServletActionContext.getServletContext().getInitParameter("serverUrl");
        
        try {
            File file = new File(contextPath+"version/manifest.txt");
            if (file.exists()) {
                InputStream input = new FileInputStream(file);
                InputStreamReader reader = new InputStreamReader(input);
                BufferedReader bufferedReader = new BufferedReader(reader);

                String manifestDate = "", schemaDate = "", applicationVersion = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    if (line.contains("Manifest-Date")){
                        String split[] = line.split("#");
                        manifestDate = split[1];
                    }
                    if (line.contains("Schema-Date")){
                        String split[] = line.split("#");
                        schemaDate = split[1];
                    }
                    if (line.contains("Manifest-Version")){
                        String split[] = line.split("#");
                        applicationVersion = split[1];
                    }
                    break;
                }
//                String split[] = line.split("#");
//                manifestDate = split[1];
                input.close();
                reader.close();
                
                //Get the Application version if equals to Manifest Version then skip this
                //and check for database version
                Set<ApplicationInfo> aAppInfo = ApplicationinfoDAO.list();
                ApplicationInfo info = aAppInfo.iterator().next();
                if(info != null){
                    if(info.getApplicationVersion().equals(applicationVersion)){
                        //Skip Updates and check for DB version
                        if(!info.getDatabaseVersion().equals(applicationVersion)){
                            //Update only the database from the schema moved folder from the last date down;
                            File schemaFolder = new File(contextPath+"schema/updated/");
                            if(schemaFolder.exists() && schemaFolder.isDirectory()){
                                DatabaseUpdater.findUpdate(info.getDateDatabaseUpdate().toString());
                            }
                        }
                    }
                    else{    
                        String resourceUrl = ServletActionContext.getServletContext().getInitParameter("serverUrl") + "resources/webservice/manifest/"+manifestDate.trim()+"/"+schemaDate.trim();     
                        URLConnection connection = getconnectUrl(resourceUrl, "text/plain"); 

                        String token = "";
                        bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        while ((line = bufferedReader.readLine()) != null) {
                            token += line;
                        }
                        System.out.println("Manifest..."+token);
                        bufferedReader.close();
                        if(!token.trim().isEmpty()) getUpdates(token, applicationVersion, manifestDate);
                        message = "Download Completed";
                    }
                }
            }
        } 
        catch (Exception exception) {
            message = "Error While Uploading Data";
            exception.printStackTrace();
        }
        return message;
    }
    
    private static void getUpdates(String manifestToken, String applicationVersion, String manifestDate) {
        //Get the update files from the server using the manifest information sent during checkUpdates() method
        String[] filenames = manifestToken.split(",");
        try {
            for (String filename : filenames) {
                ServletActionContext.getRequest().getSession().setAttribute("processingStatus", filename);
                String resourceUrl = serverUrl + "resources/webservice/updates/"+filename.trim();                
                URLConnection connection = getconnectUrl(resourceUrl, "text/plain");
                                          
                //The content of input stream (files) returned by the server should be written to disk
                filename = filename.equalsIgnoreCase("manifest.txt")? contextPath+"version/"+filename : contextPath+"version/updates/"+filename;
                InputStream stream = connection.getInputStream();                
                FileUtils.copyInputStreamToFile(stream, new File(filename));
                stream.close();
//              Save or Update APplication Info...
                Set<ApplicationInfo> aAppInfo = ApplicationinfoDAO.list();
                Date lastUpdateDate = DateUtil.parseStringToDate(manifestDate, "yyyy-mm-dd");
                if(aAppInfo.isEmpty()){
//               Save the app info...
                    ApplicationInfo appInfo = new ApplicationInfo();
                    appInfo.setApplicationVersion(applicationVersion);
                    appInfo.setDatabaseVersion(applicationVersion);
                    appInfo.setDateApplicationUpdate(lastUpdateDate);
                    appInfo.setDateDatabaseUpdate(lastUpdateDate);
                    ApplicationinfoDAO.save(appInfo);
                }else{
                    ApplicationInfo info = aAppInfo.iterator().next();
                    if(info != null){
                        info.setApplicationVersion(applicationVersion);
                        info.setDatabaseVersion(applicationVersion);
                        info.setDateApplicationUpdate(lastUpdateDate);
                        info.setDateDatabaseUpdate(lastUpdateDate);
                        ApplicationinfoDAO.update(info);
                    }
                }
                System.out.println("Update downloaded...");
            }
            //After receiving updates files from server, lock the version folder until the current updates 
            //is applied by the LAMIS tomcat launcher when restarted  
            lockVersionFolder();
            ServletActionContext.getRequest().getSession().setAttribute("processingStatus", "completed");
        } 
        catch (Exception exception) {
            ServletActionContext.getRequest().getSession().setAttribute("processingStatus", "terminated");
            throw new RuntimeException(exception); 
        }       
    }

    private static void lockVersionFolder() {
        try {
            File file = new File(contextPath+"version/lock.ser");
            FileOutputStream stream = new FileOutputStream(file);
            stream.close();
        }
        catch (Exception exception) {
            throw new RuntimeException(exception); 
        }                       
    }
    
    private static URLConnection getconnectUrl(String resourceUrl, String contentType) {
        URLConnection connection = null;
        try {
            URL url = new URL(resourceUrl);
            connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", contentType);
            connection.setConnectTimeout(120000);
            connection.setReadTimeout(0);
        } 
        catch (Exception exception) {
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
        return connection;
    }
        
}
