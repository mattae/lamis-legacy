/**
 *
 * @author Alozie
 */
package org.fhi360.lamis.resource;

import org.fhi360.lamis.utility.JDBCUtil;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.service.JsonBuilderDelegator;
import org.fhi360.lamis.service.XmlBuilderDelegator;
import org.fhi360.lamis.utility.FileUtil;

public class ResultSetSerializer {
    private String query;
    private static JDBCUtil jdbcUtil;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;
    
    public void ResultSetSerializer() {
    }
    
    public byte[] serializeAll(String table, String format, long facilityId) {
        query = "SELECT * FROM " + table + " WHERE facility_id = " + facilityId;        
        return (format.equalsIgnoreCase("xml"))? getXml(table, query, facilityId) : getJson(table, query, facilityId);
    }
    
    public byte[] serialize(String table, String format, long facilityId) {
        query = "SELECT * FROM " + table + " WHERE facility_id = " + facilityId + " AND (uploaded != 1 OR time_stamp >= (SELECT " + table + " FROM exchange WHERE facility_id = " + facilityId + "))";  
        if(table.equals("monitor")) query = "SELECT * FROM " + table + " WHERE facility_id = " + facilityId + " AND operation_id = 3 AND (uploaded != 1 OR time_stamp >= (SELECT " + table + " FROM exchange WHERE facility_id = " + facilityId + "))";  ;
        return (format.equalsIgnoreCase("xml"))? getXml(table, query, facilityId) : getJson(table, query, facilityId);
    }
    
    //Download system files (not specific to any facility) from the server
    public byte[] serialize(String table, String format) {
        query = "SELECT * FROM " + table; 
        return (format.equalsIgnoreCase("xml"))? getXml(table, query, 0L) : getJson(table, query, 0L);
    }

    public byte[] getXml(String table, String query, long facilityId) {
       String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
       String directory = contextPath+"exchange/";
       
       byte[] bytes = new byte[1024];
       try {
            jdbcUtil = new JDBCUtil(); 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            new XmlBuilderDelegator().delegate(resultSet, table, directory, facilityId);
            
            String filename = contextPath+"exchange/"+table+".xml";
            FileUtil fileUtil = new FileUtil();
            fileUtil.deflateFile(filename);  //compress the file before sending
            bytes = FileUtils.readFileToByteArray(new File(filename));
       }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            throw new RuntimeException(exception);
        }      
        return bytes;
    }    

    private byte[] getJson(String table, String query, long facilityId) {
        byte[] bytes = null;        
        try {
            jdbcUtil = new JDBCUtil();        
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            String json = new JsonBuilderDelegator().delegate(resultSet, table, facilityId);
            bytes = new byte[json.length()];
            bytes = json.getBytes();   //Convert string to an array of bytes
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            throw new RuntimeException(exception);
        }       
        return bytes; 
    }

}

