/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.resource;

import java.io.BufferedOutputStream;
import org.fhi360.lamis.utility.JDBCUtil;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.Constants;
import org.fhi360.lamis.utility.StringUtil;

/**
 *
 * @author Alozie
 */
public class SyncWebserviceInvocator {
    private static JDBCUtil jdbcUtil;
    private static PreparedStatement preparedStatement;
    private static String message;

    private static int BUFFER_SIZE = 32769;
            
    public static synchronized String invokeSyncService() {
        int i = 0;
        String[] tables = Constants.Tables.TRANSACTION_TABLES.split(",");   //Defualt sync tables are trhe transaction tables
        if(ServletActionContext.getRequest().getParameterMap().containsKey("System")) {
            tables = Constants.Tables.SYSTEM_TABLES .split(",");
        }

        if(ServletActionContext.getRequest().getParameterMap().containsKey("Auxillary")) {
            tables = Constants.Tables.AUXILLARY_TABLES .split(",");
        }
        
        //Upload table records to the server. The server receives the data as deflated bytes and writes the records to table
        //The server returns the entire record in the server table back to the client and inflated after all records have been synchronized
        //the client finally writes the xml files into the correponding database tables
        try {
            downloadOrSync(tables);
            long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
            WebserviceResponseHandler.process(tables, facilityId);            
            message = "Download & Sync Completed";
        }         
        catch (Exception exception) {
            ServletActionContext.getRequest().getSession().setAttribute("processingStatus", "terminated");
            message = "Error While Syncing Data";
            exception.printStackTrace();
        }
        ServletActionContext.getRequest().getSession().setAttribute("processingStatus", "completed");
        return message;
    }

    private static void downloadOrSync(String[] tables) {
        Timestamp timestamp = new Timestamp(new java.util.Date().getTime());
        long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");

        String entities = Constants.Tables.TRANSACTION_TABLES;
        try {
            for (String table : tables) {
                ServletActionContext.getRequest().getSession().setAttribute("processingStatus", table);                

                String resourceUrl = "";
                if(StringUtil.found(table, entities.split(","))) { 
                    //Tables in the entity string should first be uploaded before downloading records from the server 
                    resourceUrl = ServletActionContext.getServletContext().getInitParameter("serverUrl") + "resources/webservice/sync/" + table.trim() + "/" + facilityId; 
                } 
                else {
                    //These are system tables. Records should only be downloaded from the server
                    resourceUrl = ServletActionContext.getServletContext().getInitParameter("serverUrl") + "resources/webservice/download/" + table.trim();                    
                }
                
                URLConnection connection = getconnectUrl(resourceUrl, "application/xml");
                
                BufferedOutputStream output = new BufferedOutputStream(connection.getOutputStream());
                if(entities.contains(table.toLowerCase())) {
                    //Select records from table, convert resultset to xml and return as array of bytes
                    //the read and write until last byte is encountered
                    byte[] buffer = new ResultSetSerializer().serialize(table, "xml", facilityId);               
                    for(int i = 0; i < buffer.length; i++) {
                        byte oneByte = buffer[i];
                        output.write(oneByte); 
                    }                
                    output.close();
                }
                
                //The content of input stream (records) returned by the server should be written to disk
                InputStream stream = connection.getInputStream();                
                WebserviceResponseHandler.writeStreamToFile(stream, table);
                stream.close();
                
//                BufferedWriter output = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()), BUFFER_SIZE);
//                
//                //Only content of transaction tables should be written to the output stream and synced to the server during download
//                String content = "";
//                if(entities.contains(table.toLowerCase())) {
//                    content = new ResultSetSerializer().serialize(table, "xml", facilityId);  //Select records and convert to xml
//                    output.write(content);   //Invoke /sync POST method with table name and facility id as parameters 
//                    output.close();
//                    content = null;                
//                    updateLastUploadTime(table, timestamp, facilityId);
//                }   
                
            }               
        } 
        catch (Exception exception) {
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

    private static void updateLastUploadTime(String table, Timestamp timestamp, long facilityId) {
        try {
            String query = "UPDATE exchange SET [entity] = ? WHERE facility_id = ?";
            jdbcUtil = new JDBCUtil(); 
            preparedStatement = jdbcUtil.getStatement(query.replace("[entity]", table));
            preparedStatement.setTimestamp(1, timestamp);
            preparedStatement.setLong(2, facilityId);
            preparedStatement.executeUpdate();                
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }

    private static URLConnection getconnectUrl(String resourceUrl, String contentType) {
        URLConnection connection = null;
        try {
            URL url = new URL(resourceUrl);
            connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", contentType);
            connection.setConnectTimeout(120000);
            connection.setReadTimeout(0);
        } 
        catch (Exception exception) {
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
        return connection;
    }
}
