/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.resource;

import java.io.File;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.UUID;
import net.sf.json.JSONSerializer;
import net.sf.json.xml.XMLSerializer;
import org.apache.commons.io.FileUtils;
import org.fhi360.lamis.utility.StringUtil;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.service.UploadFolderService;
import org.fhi360.lamis.service.XmlParserDelegator;
import org.fhi360.lamis.utility.FileUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PropertyAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author user1
 */
public class WebserviceRequestHandler {

    private static final Logger LOG = LoggerFactory.getLogger(WebserviceRequestHandler.class);

    //This method is called from upload service in the webservice
    //It writes the content of the input stream to file, inflate the file and then process the xml into table records
    public static void uploadStream(InputStream stream, String table, long facilityId) {
        try {
            String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
            String directory = contextPath + "exchange/sync/" + Long.toString(facilityId) + "/";
            String fileName = directory + table + ".xml";  // Write the content to facility folder
            FileUtils.copyInputStreamToFile(stream, new File(fileName));
            stream = null;
        } catch (Exception exception) {
            stream = null;
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

    //This method is called from sync service in the webservice. The method performs two tasks.
    //First, it writes the content of the input stream to file, inflate the file and then process the xml into table records
    //Second, it reads all the records in the corresponding table, convert records to xml file, deflate it and send as array of bytes
    public static synchronized byte[] syncStream(InputStream stream, String table, long facilityId) {
        try {
            String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
            String fileName = contextPath + "exchange/sync/" + table + ".xml";
            FileUtils.copyInputStreamToFile(stream, new File(fileName));
            FileUtil fileUtil = new FileUtil();
            
            Map<String, Object> map = new PropertyAccessor().getSystemProperties();
            String appInstance = (String) map.get("appInstance");
            fileUtil.inflateFile(fileName);  //decompress the file 
            if(appInstance.equalsIgnoreCase("server")) {
                   fileUtil.inflateFile(fileName, facilityId +"");  //decompress the file 
            }
            stream = null;
            new XmlParserDelegator().delegate(table, fileName);  // process the xml file that has been written to file
            return new ResultSetSerializer().serializeAll(table, "xml", facilityId);  // return the records from the table
        } catch (Exception exception) {
            stream = null;
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

    //This method is invoked by the upload service when an xml string is recieved instead of an input stream
    public static void uploadXml(String content, String table, long facilityId) {
        initTableUUID(table);
        try {
            String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
            String directory = contextPath + "exchange/sync/" + Long.toString(facilityId) + "/";
            String fileName = directory + table + ".xml";  // Write the content to facility folder
            FileUtils.writeStringToFile(new File(fileName), content);
            content = null;
        } catch (Exception exception) {
            content = null;
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

    //This method is invokes by the sync service when an xml string is recieved instead of an input stream
    //The file deflater is not called here, because string are not delated
    public static synchronized byte[] syncXml(String content, String table, long facilityId) {
        try {
            String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
            String fileName = contextPath + "exchange/sync/" + table + ".xml";
            FileUtils.writeStringToFile(new File(fileName), content);
            content = null;
            new XmlParserDelegator().delegate(table, fileName);  // process the xml file that has been written to file
            return new ResultSetSerializer().serializeAll(table, "xml", facilityId);  // return the records from the table
        } catch (Exception exception) {
            content = null;
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

    //This method is invoked by the upload service when a json string is recieved instead of an input stream
    public static void uploadJson(String content, String table, long facilityId) {
        XMLSerializer serializer = new XMLSerializer();
        serializer.setRootName(StringUtil.pluralize(table));
        serializer.setTypeHintsEnabled(false);
        serializer.setElementName(table);

        try {
            String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
            String fileName = contextPath + "exchange/sync/" + Long.toString(facilityId) + "/" + table + ".xml"; // Write the content to facility folder
            FileUtils.writeStringToFile(new File(fileName), serializer.write(JSONSerializer.toJSON(content)));
            content = null;
            serializer = null;
        } catch (Exception exception) {
            content = null;
            serializer = null;
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

    //This method is invoked by the sync service when a json string is recieved instead of an input stream
    public static synchronized byte[] syncJson(String content, String table, long facilityId) {
        XMLSerializer serializer = new XMLSerializer();
        serializer.setRootName(StringUtil.pluralize(table));
        serializer.setTypeHintsEnabled(false);
        serializer.setElementName(table);

        try {
            String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
            String fileName = contextPath + "exchange/sync/" + table + ".xml";
            FileUtils.writeStringToFile(new File(fileName), serializer.write(JSONSerializer.toJSON(content)));
            content = null;
            serializer = null;
            new XmlParserDelegator().delegate(table, fileName);  // process the xml file that has been written to file
            return new ResultSetSerializer().serializeAll(table, "xml", facilityId);  // return the records from the table
        } catch (Exception exception) {
            content = null;
            serializer = null;
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

    public static synchronized byte[] getXml(String table) {
        try {
            return new ResultSetSerializer().serialize(table, "xml");
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

    public static synchronized byte[] getJson(String table) {
        try {
            return new ResultSetSerializer().serialize(table, "json");
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

    public static String getUploadFolderStatus(long facilityId) {
        return new UploadFolderService().getUploadFolderStatus(facilityId);
    }

    public static void lockUploadFolder(long facilityId) {
        new UploadFolderService().lockUploadFolder(facilityId);
    }

    private static void initTableUUID(String table) {
        try {
            JDBCUtil jdbcu = new JDBCUtil();
            LOG.info("Selecting from {} table", table);
            PreparedStatement statement = jdbcu.getStatement("select count(*) from information_schema.columns "
                    + "where table_name = ? and column_name = 'id_uuid'");
            statement.setString(1, table);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int count = rs.getInt(1);
                if (count == 0) {
                    statement = jdbcu.getStatement("alter table " + table + " add id_uuid varchar(36)");
                    statement.executeUpdate();
                } else {
                    LOG.info("Updating table {}", table);
                    String idColumn = table + "_id";
                    if (table.contains("history")) {
                        idColumn = "history_id";
                    }
                    statement = jdbcu.getStatement("select " + idColumn + " as id from " + table
                            + " where id_uuid is null limit 2000");
                    rs = statement.executeQuery();

                    while (rs.next()) {
                        Long id = rs.getLong("id");
                        LOG.info("Updating {} id {}", table, id);
                        statement = jdbcu.getStatement(
                                String.format("update %s set id_uuid = '%s' where %s = %s",
                                        table, UUID.randomUUID().toString(), idColumn, id));
                        statement.executeUpdate();
                    }
                    jdbcu.getConnection().commit();
                }
            }
            jdbcu.getConnection().close();
        } catch (Exception ex) {
            LOG.error("Error: {}", ex.getMessage());
        }
    }
}
