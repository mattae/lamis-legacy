/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.resource;

import org.fhi360.lamis.dao.hibernate.FacilityDAO;
import org.fhi360.lamis.dao.jdbc.FacilityJDBC;
import org.fhi360.lamis.model.Facility;
import org.fhi360.lamis.service.ApplicationUpdatesService;
import org.fhi360.lamis.service.parser.FacilityJsonParser;
import org.fhi360.lamis.utility.PatientNumberNormalizer;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;

/**
 * REST Web Service
 *
 * @author user1
 */
@RequestMapping("/webservice")
@RestController
public class WebserviceResource {

    /**
     * Creates a new instance of GenericserviceResource
     */
    public WebserviceResource() {
    }

    /**
     * Retrieves representation of an instance of org.fhi360.lamis.resource.WebserviceResource
     *
     * @param facilityId
     * @param action
     * @return an instance of java.lang.String
     */

    //Get the lock status of the upload folder or lock the folder after upload of files
    @GetMapping(value = "/uploadfolder/{facilityId}/{action}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String getText(@PathVariable("facilityId") long facilityId, @PathVariable("action") int action) {
        System.out.println("Folder status ...");
        if (action == 1) {
            return WebserviceRequestHandler.getUploadFolderStatus(facilityId);
        } else {
            WebserviceRequestHandler.lockUploadFolder(facilityId);
            return "locked";
        }

    }

    //Get the updates manifest
    @GetMapping(value = "/manifest/{dateManifest}/{dateSchema}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String getToken(@PathVariable("dateManifest") String dateManifest,
                           @PathVariable("dateSchema") String dateSchema) {
        return ApplicationUpdatesService.getManifestToken(dateManifest, dateSchema);
    }

    //Get the updates manifest
    @GetMapping(value = "/updates/{filename}", produces = MediaType.TEXT_PLAIN_VALUE)
    public byte[] getUpdates(@PathVariable("filename") String filename) {
        return ApplicationUpdatesService.getUpdates(filename);
    }

    /**
     * PUT method for updating or creating an instance of WebserviceResource
     *
     * @param stream
     * @return an HTTP response with content of the updated or created resource.
     */
    @PutMapping(consumes = MediaType.APPLICATION_ATOM_XML_VALUE)
    public void putXml(InputStream stream) {
    }

    /**
     * POST method for creating an instance of WebserviceResource
     *
     * @param stream
     * @param table
     * @param facilityId
     * @return an HTTP response with content of the created resource.
     */

    //Upload transaction data to the server
    @PostMapping(value = "/upload/{table}/{facilityId}", consumes = MediaType.APPLICATION_ATOM_XML_VALUE)
    public void postXml(InputStream stream, @PathVariable("table") String table, @PathVariable("facilityId") long facilityId) {
        WebserviceRequestHandler.uploadStream(stream, table, facilityId);
    }

    //Download system files (not specific to any facility)from the server
    @GetMapping(value = "/download/{table}", produces = MediaType.APPLICATION_ATOM_XML_VALUE)
    public byte[] getXml(@PathVariable("table") String table) {
        return WebserviceRequestHandler.getXml(table);
    }

    /**
     * POST method for creating an instance of WebserviceResource
     *
     * @param stream
     * @param table
     * @param facilityId
     * @return an HTTP response with content of the created resource.
     */

    //Upload transaction data and download files from the server
    @PostMapping(value = "/sync/{table}/{facilityId}", produces = MediaType.APPLICATION_ATOM_XML_VALUE,
            consumes = MediaType.APPLICATION_ATOM_XML_VALUE)
    public byte[] syncXml(InputStream stream, @PathVariable("table") String table, @PathVariable("facilityId") long facilityId) {
        return WebserviceRequestHandler.syncStream(stream, table, facilityId);
    }

    /**
     * POST method for creating an instance of Facilityservice
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the created resource.
     */

    //Save a new facility in the server and return the generated facility id to the client
    @PostMapping(value = "/entity", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String postJson(String content) {
        Long generatedID = 0L;
        try {
            Facility facility = FacilityJsonParser.jsonObject(content); // Convert content to a facility object
            generatedID = FacilityDAO.save(facility);  // Save facility in the server
            FacilityJDBC.updateExchange(generatedID);  //Update exchange table on the server
        } catch (JAXBException | IOException exception) {
            exception.printStackTrace();
        }
        System.out.println("Saved ID" + generatedID);
        return generatedID.toString();
    }

    //Update an existing facility in the server
    //Normalize hospital numbers if hospital numbers for this facility are to be normalized
    @PutMapping(value = "/entity/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void putJson(String content) {
        try {
            Facility facility = FacilityJsonParser.jsonObject(content);
            PatientNumberNormalizer.normalize(facility);  //Normalize hospital number if padHospitalNumber field is set
            FacilityDAO.update(facility);
        } catch (JAXBException | IOException exception) {
            exception.printStackTrace();
        }
    }

    /***************************************************************************************************
     * This part of the webservice is used by the LAMIS mobile to exchange data with the server
     * @param stream
     * @param table
     ***************************************************************************************************/

    //Upload encounter to the server
    @PostMapping(value = "/mobile/sync/{table}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void syncJsonMobile(InputStream stream, @PathVariable("table") String table) {
        WebserviceRequestHandlerMobile.syncStream(stream, table);
    }

    //Download newly devolved patients from server
    @PostMapping("/mobile/devolved/{communitypharmId}")
    public byte[] syncGetJsonMobile(InputStream stream, @PathVariable("communitypharmId") int communitypharmId) {
        return WebserviceRequestHandlerMobile.getPatient(communitypharmId);
    }

    //Get configuration data for account, facility, regimen & labtest 
    @GetMapping("/mobile/activate/{userName}/{pin}")
    public byte[] getInitializationData(@PathVariable("userName") String userName, @PathVariable("pin") String pin) {
        return WebserviceRequestHandlerMobile.getInitializationData(userName, pin);
    }
}

//@Consumes(MediaType.TEXT_PLAIN)
//@Consumes(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_XML)

//"text/plain" 
//"application/json"
//"application/xml"