/**
 *
 * @author aalozie
 */
package org.fhi360.lamis.service;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.JDBCUtil;

public class EntityIdentifier {
    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    private Map<Long, String> map;
    
    public EntityIdentifier() {
    }
    
    public synchronized void createEntities(long facilityId, String table) {
        executeUpdate("DROP TABLE IF EXISTS entity");        
        executeUpdate("DROP TABLE IF EXISTS dependant");        
        executeUpdate("DROP TABLE IF EXISTS dependant1");        
        executeUpdate("DROP INDEX IF EXISTS idx_entity");        
        executeUpdate("DROP INDEX IF EXISTS idx_dependant");      
        executeUpdate("DROP INDEX IF EXISTS idx_dependant1");      
        executeUpdate("DROP TABLE IF EXISTS mapper");
        
        String entities = "patient clinic pharmacy laboratory adrhistory oihistory adherehistory statushistory regimenhistory chroniccare dmscreenhistory tbscreenhistory anc delivery child maternalfollowup partnerinformation nigqual devolve patientcasemanager";
        if(entities.contains(table.toLowerCase())) {
            executeUpdate("CREATE TEMPORARY TABLE entity AS SELECT patient_id, patient_id AS id_on_server, hospital_num FROM patient WHERE facility_id = " + facilityId);  
            executeUpdate("CREATE INDEX idx_entity ON entity(hospital_num)");            
        } 

        if(table.equals("patient")) {
            executeUpdate("CREATE TEMPORARY TABLE mapper(id_on_client bigint, id_on_server bigint, table_name varchar(25))"); 
            executeUpdate("INSERT INTO mapper SELECT casemanager_id, local_id, 'casemanager' FROM casemanager WHERE facility_id = " + facilityId); 
            executeUpdate("INSERT INTO mapper SELECT user_id, local_id, 'user' FROM user WHERE facility_id = " + facilityId); 
        }                         
        
        if(table.equals("clinic")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT clinic.clinic_id AS id_on_server, clinic.date_visit, patient.hospital_num FROM clinic JOIN patient ON clinic.patient_id = patient.patient_id WHERE clinic.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_visit)");
        }                         
        if(table.equals("pharmacy")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT pharmacy.pharmacy_id AS id_on_server, pharmacy.date_visit, pharmacy.regimendrug_id, patient.hospital_num FROM pharmacy JOIN patient ON pharmacy.patient_id = patient.patient_id WHERE pharmacy.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_visit, regimendrug_id)");
        }                         
        if(table.equals("laboratory")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT laboratory.laboratory_id AS id_on_server, laboratory.date_reported, laboratory.labtest_id, patient.hospital_num FROM laboratory JOIN patient ON laboratory.patient_id = patient.patient_id WHERE laboratory.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_reported, labtest_id)");
        }                         
        if(table.equals("adrhistory")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT adrhistory.history_id AS id_on_server, adrhistory.date_visit, adrhistory.adr, adrhistory.screener, patient.hospital_num FROM adrhistory JOIN patient ON adrhistory.patient_id = patient.patient_id WHERE adrhistory.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_visit, adr, screener)");
        }                              
        if(table.equals("oihistory")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT oihistory.history_id AS id_on_server, oihistory.date_visit, oihistory.oi, patient.hospital_num FROM oihistory JOIN patient ON oihistory.patient_id = patient.patient_id WHERE oihistory.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_visit, oi)");
        }                         
        if(table.equals("adherehistory")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT adherehistory.history_id AS id_on_server, adherehistory.date_visit, patient.hospital_num FROM adherehistory JOIN patient ON adherehistory.patient_id = patient.patient_id WHERE adherehistory.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_num ON dependant(hospital_num, date_visit)");
        }                     
        if(table.equals("regimenhistory")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT regimenhistory.history_id AS id_on_server, regimenhistory.date_visit, regimenhistory.regimentype, regimenhistory.regimen, patient.hospital_num FROM regimenhistory JOIN patient ON regimenhistory.patient_id = patient.patient_id WHERE regimenhistory.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_num ON dependant(hospital_num, date_visit, regimentype, regimen)");
        }                    
        if(table.equals("statushistory")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT statushistory.history_id AS id_on_server, statushistory.date_current_status, statushistory.current_status, patient.hospital_num FROM statushistory JOIN patient ON statushistory.patient_id = patient.patient_id WHERE statushistory.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_num ON dependant(hospital_num, date_current_status, current_status)");
        }                       

        if(table.equals("eac")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT eac.eac_id AS id_on_server, eac.date_eac1, patient.hospital_num FROM eac JOIN patient ON eac.patient_id = patient.patient_id WHERE eac.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_num ON dependant(hospital_num, date_eac1)");
        }                       
        
        //Chroniccare section
        if(table.equals("chroniccare")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT chroniccare.chroniccare_id AS id_on_server, chroniccare.date_visit, patient.hospital_num FROM chroniccare JOIN patient ON chroniccare.patient_id = patient.patient_id WHERE chroniccare.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_visit)");
        }
        if(table.equals("tbscreenhistory")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT tbscreenhistory.history_id AS id_on_server, tbscreenhistory.date_visit, tbscreenhistory.description, patient.hospital_num FROM tbscreenhistory JOIN patient ON tbscreenhistory.patient_id = patient.patient_id WHERE tbscreenhistory.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_num ON dependant(hospital_num, date_visit, description)");
        }                    
        if(table.equals("dmscreenhistory")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT dmscreenhistory.history_id AS id_on_server, dmscreenhistory.date_visit, dmscreenhistory.description, patient.hospital_num FROM dmscreenhistory JOIN patient ON dmscreenhistory.patient_id = patient.patient_id WHERE dmscreenhistory.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_num ON dependant(hospital_num, date_visit, description)");
        }                    
        
        //PCR lab section
        if(table.equals("specimen")) {
            executeUpdate("CREATE TEMPORARY TABLE entity AS SELECT specimen_id, specimen_id AS id_on_server, labno FROM specimen WHERE facility_id = " + facilityId);  
            executeUpdate("CREATE INDEX idx_entity on entity(labno)");
        }                       
        if(table.equals("eid")) {
            executeUpdate("CREATE TEMPORARY TABLE entity AS SELECT eid_id, eid_id AS id_on_server, labno FROM eid WHERE facility_id = " + facilityId);  
            executeUpdate("CREATE INDEX idx_entity on entity(labno)");
        }                       

        //PMTCT section
        if(table.equals("anc")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT anc.anc_id AS id_on_server, anc.date_visit, patient.hospital_num FROM anc JOIN patient ON anc.patient_id = patient.patient_id WHERE anc.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_visit)");
        }                         
        if(table.equals("delivery")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT delivery.delivery_id AS id_on_server, delivery.date_delivery, patient.hospital_num FROM delivery JOIN patient ON delivery.patient_id = patient.patient_id WHERE delivery.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_delivery)");

            executeUpdate("CREATE TEMPORARY TABLE dependant1 AS SELECT anc_id AS id_on_server, anc_num FROM anc WHERE facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant1(anc_num)");
        }                         
        if(table.equals("child")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT child.child_id AS id_on_server, child.reference_num, child.patient_id FROM child WHERE clinic.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(reference_num, patient_id)");
        }                         
        if(table.equals("childfollowup")) {
            //Child followup entity table is created from the child table and not from patient 
            executeUpdate("CREATE TEMPORARY TABLE entity AS SELECT child_id, child_id AS id_on_server, reference_num FROM child WHERE facility_id = " + facilityId);  
            executeUpdate("CREATE INDEX idx_entity on entity(reference_num)"); 
            
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT childfollowup.childfollowup_id AS id_on_server, childfollowup.date_visit, child.reference_num FROM childfollowup JOIN child ON childfollowup.child_id = child.child_id WHERE childfollowup.facility_id = " + facilityId + " AND child.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(reference_num, date_visit)");
        }                         
        if(table.equals("maternalfollowup")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT maternalfollowup.maternalfollowup_id AS id_on_server, maternalfollowup.date_visit, patient.hospital_num FROM maternalfollowup JOIN patient ON maternalfollowup.maternalfollowup_id = patient.patient_id WHERE maternalfollowup.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_visit)");

            executeUpdate("CREATE TEMPORARY TABLE dependant1 AS SELECT anc_id AS id_on_server, anc_num FROM anc WHERE facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant1(anc_num)");
        }                         
        if(table.equals("partnerinformation")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT partnerinformation.partnerinformation_id AS id_on_server, patient.hospital_num FROM partnerinformation JOIN patient ON partnerinformation.partnerinformation_id = patient.patient_id WHERE partnerinformation.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num)");
        }
        
        //NIGQUAL section
        if(table.equals("nigqual")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT nigqual.nigqual_id AS id_on_server, nigqual.review_period_id, patient.hospital_num FROM nigqual JOIN patient ON nigqual.patient_id = patient.patient_id WHERE nigqual.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, review_period_id)");
        }                              

        //ARV Refill Devolvement section
        if(table.equals("devolve")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT devolve.devolve_id AS id_on_server, devolve.date_devolved, patient.hospital_num FROM devolve JOIN patient ON devolve.patient_id = patient.patient_id WHERE devolve.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_devolved)");
        }                         

        //Others
        if(table.equals("validated")) {
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT validated.validated_id AS id_on_server, validated.date_validated, patient.hospital_num FROM validated JOIN patient ON validated.patient_id = patient.patient_id WHERE validated.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, date_validated)");
        }                         
        
        //Patient Case manager
        if(table.equals("patientcasemanager")){
            executeUpdate("CREATE TEMPORARY TABLE dependant AS SELECT patientcasemanager.patientcasemanager_id AS id_on_server, patientcasemanager.date_assigned, patient.hospital_num FROM patientcasemanager JOIN patient ON patientcasemanager.patient_id = patient.patient_id WHERE patientcasemanager.facility_id = " + facilityId + " AND patient.facility_id = " + facilityId);
            executeUpdate("CREATE INDEX idx_dependant ON dependant(hospital_num, assigned_date)");
        }
         
    }
    
    public void serialize(long facilityId) {  
        String directory = ServletActionContext.getServletContext().getInitParameter("contextPath") + "exchange/sync/"; 
        String fileName = directory+"entity"+Long.toString(facilityId)+".ser";    
        map = getPatientEntities(facilityId);
        try {
            File file = new File(fileName);
            FileOutputStream fileOut = new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(map);
            out.close();
            fileOut.close();
        }
        catch (Exception exception) {
            throw new RuntimeException(exception); 
        }       
        
    }

    public Map<Long, String> deserialize(long facilityId) {
        String directory = ServletActionContext.getServletContext().getInitParameter("contextPath") + "exchange/sync/"; 
        String fileName = directory+"entity"+Long.toString(facilityId)+".ser";    
        map = new HashMap<Long, String>();
        try {
            FileInputStream fileIn = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            map = (Map<Long, String>) in.readObject();
            in.close();
            fileIn.close();
            return map;
        } 
        catch(Exception exception) {
           throw new RuntimeException(exception); 
        }
    }    
    
    public Map<Long, String> getPatientEntities(long facilityId) {  
        map = new HashMap<Long, String>();
        query = "SELECT DISTINCT patient_id, hospital_num FROM patient WHERE facility_id = " + facilityId;
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()) {
                map.put(resultSet.getLong("patient_id"), resultSet.getString("hospital_num"));
            }
            resultSet = null;
            return map;
        } catch(Exception exception) {
           resultSet = null;
           jdbcUtil.disconnectFromDatabase();  //disconnect from database
           throw new RuntimeException(exception); 
        }
    }
    
    public Map<Long, String> getChildEntities(long facilityId) {  
        map = new HashMap<Long, String>();
        query = "SELECT DISTINCT child_id, reference_num FROM child WHERE facility_id = " + facilityId;
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()) {
                map.put(resultSet.getLong("child_id"), resultSet.getString("reference_num"));
            }
            resultSet = null;
            return map;
        } catch(Exception exception) {
           resultSet = null;
           jdbcUtil.disconnectFromDatabase();  //disconnect from database
           throw new RuntimeException(exception); 
        }
    }

    public Map<Long, String> getAncEntities(long facilityId) {  
        map = new HashMap<Long, String>();
        query = "SELECT DISTINCT anc_id, anc_num FROM anc WHERE facility_id = " + facilityId;
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()) {
                map.put(resultSet.getLong("anc_id"), resultSet.getString("anc_num"));
            }
            resultSet = null;
            return map;
        } catch(Exception exception) {
           resultSet = null;
           jdbcUtil.disconnectFromDatabase();  //disconnect from database
           throw new RuntimeException(exception); 
        }
    }

    public long getIdOnServer(String query) {
        long idOnServer = 0;
        try {
           jdbcUtil = new JDBCUtil();
           preparedStatement = jdbcUtil.getStatement(query);
           resultSet = preparedStatement.executeQuery();
           if(resultSet.next()) {
                idOnServer =  resultSet.getLong("id_on_server"); 
           }             
           resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;        
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return idOnServer;
    } 

    public String getHospitalNum(long patientId) {
        String hospitalNum = "";
        String query = "SELECT hospital_num FROM entity WHERE patient_id = " + patientId;
        try {
            jdbcUtil = new JDBCUtil();
           preparedStatement = jdbcUtil.getStatement(query);
           resultSet = preparedStatement.executeQuery();
           if(resultSet.next()) {
                hospitalNum =  resultSet.getString("hospital_num"); 
           }             
           resultSet = null;
        }
        catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return hospitalNum;
    }
    
    public void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }        
    }
    
}

