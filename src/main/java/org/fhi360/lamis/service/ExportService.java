/**
 *
 * @author aalozie
 */

package org.fhi360.lamis.service;

import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.FileUtil;
import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import org.fhi360.lamis.utility.Constants;

public class ExportService {
    private static JDBCUtil jdbcUtil;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;
    private static Timestamp timestamp;
    
    public static synchronized String buildXml() {
        timestamp = new Timestamp(new java.util.Date().getTime());
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = ServletActionContext.getRequest().getSession();
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        String directory = contextPath + "exchange/";        
        long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
        
        String xmlFiles = "";
        
        String[] tables = Constants.Tables.TRANSACTION_TABLES.split(",");
        try {
            for(String table : tables) {
                session.setAttribute("processingStatus", table);
                
                xmlFiles += directory + table + ".xml,";
                
                resultSet = getResultSet(table);
                new XmlBuilderDelegator().delegate(resultSet, table, directory, facilityId);
            }
        }
        catch (Exception exception) {
            session.setAttribute("processingStatus", "terminated");
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            throw new RuntimeException(exception);
        }
        
        FileUtil fileUtil = new FileUtil();
        fileUtil.makeDir(contextPath+"transfer/");
        fileUtil.makeDir(request.getContextPath()+"/transfer/");

        String fileName = "lamis.zip";
        if(session.getAttribute("facilityName") != null) {
            fileName = (String) session.getAttribute("facilityName")+".zip";
        } 
        String zipFile = contextPath+"transfer/"+fileName;
        
        
        try {
            String[] files = xmlFiles.split(",");
            fileUtil.zip(files, zipFile);
            //for servlets in the default(root) context, copy file to the transfer folder in root 
            if(!contextPath.equalsIgnoreCase(request.getContextPath())) fileUtil.copyFile(fileName, contextPath+"transfer/", request.getContextPath()+"/transfer/");                    
            updateExportTime();
        } 
        catch (Exception exception) {
            session.setAttribute("processingStatus", "terminated");
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
        session.setAttribute("processingStatus", "completed");
        return "transfer/"+fileName;
    }
    
    private static ResultSet getResultSet(String table) {
        long facilityId = (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId");
        boolean recordsAll = false;
        if(ServletActionContext.getRequest().getParameter("recordsAll") != null && !ServletActionContext.getRequest().getParameter("recordsAll").equals("false")) {
            recordsAll = true;                        
        }
        try {
            String query = (recordsAll)? "SELECT * FROM " + table + " WHERE facility_id = " + facilityId : "SELECT * FROM " + table + " WHERE facility_id = " + facilityId + " AND time_stamp > SELECT export FROM exchange WHERE facility_id = " + facilityId;                 
            jdbcUtil = new JDBCUtil(); 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            throw new RuntimeException(exception);
        }        
        return resultSet;
    }
    
    private static void updateExportTime() {
        try {
            String query = "UPDATE exchange SET export = ? WHERE facility_id = ?";                
            jdbcUtil = new JDBCUtil(); 
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setTimestamp(1, timestamp);
            preparedStatement.setLong(2, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
            preparedStatement.executeUpdate();                
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            throw new RuntimeException(exception);
        }        
    }
}
//query = query.replace("[table]", table);
