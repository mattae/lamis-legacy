/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service;

/**
 *
 * @author user1
 */

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import org.fhi360.lamis.dao.hibernate.IndicatorDAO;
import org.fhi360.lamis.model.Indicatorvalue;
import org.fhi360.lamis.utility.ChartUtil;
import org.fhi360.lamis.utility.Constants;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PropertyAccessor;

public class FacilityPerformanceAnalyzer {
    private ChartUtil chartUtil;
    
    private long stateId;
    private long lgaId;
    private long facilityId;
    private Date startDate;
    private Date endDate;

    private JDBCUtil jdbcUtil;
    private String query;
    
    public FacilityPerformanceAnalyzer() {
    }
    
    public void analyze() {
        this.chartUtil = new ChartUtil();
        String databaseName = new PropertyAccessor().getDatabaseName();
        try {
            Map<String, Object> map = new PropertyAccessor().getSystemProperties();
            String appInstance = (String) map.get("appInstance");
            if(appInstance.equalsIgnoreCase("server")) {
                //Select all facilities that has uploaded data in the last 3 days and run their performance analysis
                //query = "SELECT DISTINCT facility_id FROM patient WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE()) UNION SELECT DISTINCT facility_id FROM clinic WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE()) UNION SELECT DISTINCT facility_id FROM pharmacy WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE()) UNION SELECT DISTINCT facility_id FROM laboratory WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE())";
                query = "SELECT DISTINCT facility_id FROM patient";
                //query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE()";
                //if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE()";
            }
            else {  
                query = "SELECT DISTINCT facility_id FROM patient";
                //query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE()";
                //if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE()";
            }
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) { 
                System.out.println("Analyzing....."+resultSet.getLong("facility_id"));
                facilityId = resultSet.getLong("facility_id");
                startDate = DateUtil.addMonth(new Date(), -6); 
                endDate = DateUtil.addMonth(new Date(), -1);
                
                preparedStatement = jdbcUtil.getStatement("SELECT * FROM facility WHERE facility_id = " + facilityId);
                ResultSet rs = preparedStatement.executeQuery();
                if(rs.next()) {
                    stateId = rs.getLong("state_id");
                    lgaId = rs.getLong("lga_id");
                }

                executeUpdate("DROP TABLE IF EXISTS entity");                       
                executeUpdate("CREATE TEMPORARY TABLE entity AS SELECT * FROM patient WHERE facility_id = " + facilityId);
                executeUpdate("CREATE INDEX idx_entity ON entity(patient_id)");

                executeUpdate("DROP TABLE IF EXISTS visit");        
                executeUpdate("CREATE TEMPORARY TABLE visit AS SELECT * FROM clinic WHERE facility_id = " + facilityId);
                executeUpdate("CREATE INDEX idx_visit ON visit(patient_id)");

                executeUpdate("DROP TABLE IF EXISTS pharm");        
                executeUpdate("CREATE TEMPORARY TABLE pharm AS SELECT * FROM pharmacy WHERE facility_id = " + facilityId);
                executeUpdate("CREATE INDEX idx_pharm ON pharm(patient_id)");

                executeUpdate("DROP TABLE IF EXISTS lab");        
                executeUpdate("CREATE TEMPORARY TABLE lab AS SELECT * FROM laboratory WHERE facility_id = " + facilityId);
                executeUpdate("CREATE INDEX idx_lab ON lab(patient_id)");

                executeUpdate("DROP TABLE IF EXISTS viralload"); 
                executeUpdate("CREATE TEMPORARY TABLE viralload (patient_id BIGINT, result INT, date_reported DATE)");
                viralload();
                
                doTotalEnrollment();
                doTotalCurrent();
                doTotalVlSuppressed();
                doEligibleViralload();
                doViralSuppression();
                doEacUnsuppressed();
                doDocumentedTb();
                doEverCurrentArt();
                doNewlyEnrolled();
                doCurrentART();
                doEnrolledStart();
                doMissedRefill();
            }
            new PropertyAccessor().setDateLastAsyncTask(new Date());
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /*
     * Clinical Outcomes/EWIs
    */    
    
    // Number Enrolled
    private void doTotalEnrollment() {
        int indicatorNum = 1;
        String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'MALE' AND date_registration <= CURDATE()"; 
        int male = getCount(query);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'FEMALE' AND date_registration <= CURDATE()"; 
        int female = getCount(query);
        
        int month = DateUtil.getMonth(new Date());
        int year = DateUtil.getYear(new Date());
        
        Indicatorvalue indicator = new Indicatorvalue();
        indicator.setFacilityId(facilityId);
        indicator.setStateId(stateId);
        indicator.setLgaId(lgaId);
        //indicator.setIndicatorNum(indicatorNum);
        indicator.setMonth(month);
        indicator.setYear(year);
        //indicator.setMale(male);
        //indicator.setFemale(female);
        long indicatorId = getIndicatorId(indicatorNum, month, year);
        if(indicatorId == 0) {
            IndicatorDAO.save(indicator);
        }
        else {
           // indicator.setIndicatord(indicatorId);
            IndicatorDAO.update(indicator);
        }
    }
    
    // Number Currently on ART
    private void doTotalCurrent() {
        int indicatorNum = 1;
        String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'MALE' AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND DATEDIFF(DAY, date_last_refill + last_refill_duration, CURDATE()) <= " + Constants.Reports.LTFU_PEPFAR + " AND date_started IS NOT NULL"; 
        int male = getCount(query);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'FEMALE' AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND DATEDIFF(DAY, date_last_refill + last_refill_duration, CURDATE()) <= " + Constants.Reports.LTFU_PEPFAR + " AND date_started IS NOT NULL"; 
        int female = getCount(query);
        
        int month = DateUtil.getMonth(new Date());
        int year = DateUtil.getYear(new Date());
        
        Indicatorvalue indicator = new Indicatorvalue();
        indicator.setFacilityId(facilityId);
        indicator.setStateId(stateId);
        indicator.setLgaId(lgaId);
       // indicator.setIndicatorNum(indicatorNum);
        indicator.setMonth(month);
        indicator.setYear(year);
        //indicator.setMale(male);
        //indicator.setFemale(female);
        
        long indicatorId = getIndicatorId(indicatorNum, month, year);
        if(indicatorId == 0) {
            IndicatorDAO.save(indicator);
        }
        else {
            //indicator.setIndicatord(indicatorId);
            IndicatorDAO.update(indicator);
        }
        
    }

    // Number of Virally suppressed
    private void doTotalVlSuppressed(){
        int indicatorNum = 3;
        String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'MALE' AND last_viral_load < 1000"; 
        int male = getCount(query);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'FEMALE' AND last_viral_load < 1000";  
        int female = getCount(query);
        
        int month = DateUtil.getMonth(new Date());
        int year = DateUtil.getYear(new Date());
        
        Indicatorvalue indicator = new Indicatorvalue();
        indicator.setFacilityId(facilityId);
        indicator.setStateId(stateId);
        indicator.setLgaId(lgaId);
       // indicator.setIndicatorNum(indicatorNum);
        indicator.setMonth(month);
        indicator.setYear(year);
        //indicator.setMale(male);
        //indicator.setFemale(female);
        long indicatorId = getIndicatorId(indicatorNum, month, year);
        if(indicatorId == 0) {
            IndicatorDAO.save(indicator);
        }
        else {
           // indicator.setIndicatord(indicatorId);
            IndicatorDAO.update(indicator);
        }        
    }

    
    /*
     * Clinical Outcomes/EWIs
    */    
    
    // Number eligible for Viral Load vs Test done
    private void doEligibleViralload() {
        int indicatorNum = 4;
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");

            int denominator = 0;
            int numerator  = 0;
            
            //Number eligible for VL during the reporting period
            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND MONTH(viral_load_due_date) = " + month + " AND YEAR(viral_load_due_date) = " + year; 
            denominator = getCount(query); 

            //Number VL test done during the reporting period
            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM lab WHERE labtest_id = 16 AND MONTH(date_reported) = " + month + " AND YEAR(date_reported) = " + year; 
            numerator = getCount(query); 
            
            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
            //indicator.setIndicatorNum(indicatorNum);
            indicator.setMonth(month);
            indicator.setYear(year);
           // indicator.setDenominator(denominator);
            //indicator.setNumerator(numerator);
            long indicatorId = getIndicatorId(indicatorNum, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
               // indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }        
    }

    // % of VL results with viral suppression
    private void doViralSuppression() {
        int indicatorNum = 5;
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
 
            int denominator = 0;
            int numerator  = 0;
           
            //Number VL test during the reporting period
            query = "SELECT COUNT(*) AS count FROM lab WHERE labtest_id = 16 AND MONTH(date_reported) = " + month + " AND YEAR(date_reported) = " + year; 
            denominator = getCount(query); 

            //Number VL test suppressed during the reporting period
            query = "SELECT COUNT(*) AS count FROM viralload WHERE labtest_id = 16 AND viralload REGEXP '[0123456789]' AND CONVERT(resultab, SIGNED) < 1000 AND MONTH(date_reported) = " + month + " AND YEAR(date_reported) = " + year; 
            numerator = getCount(query); 
           
            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
           // indicator.setIndicatorNum(indicatorNum);
            indicator.setMonth(month);
            indicator.setYear(year);
            ///indicator.setDenominator(denominator);
           // indicator.setNumerator(numerator);
            long indicatorId = getIndicatorId(indicatorNum, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
                //indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }        
    }

    // Propoation of unsuppressed patients who attended EAC
    private void doEacUnsuppressed() {
        int indicatorNum = 6;

        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
 
            int denominator = 0;
            int numerator  = 0;

            //query = "SELECT COUNT(*) AS count FROM viralload WHERE labtest_id = 16 AND viralload REGEXP '[0123456789]' AND CONVERT(resultab, SIGNED) >= 1000 AND MONTH(date_reported) = " + month + " AND YEAR(date_reported) = " + year; 
            
            //Number VL test unsuppressed during the reporting period
            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM viralload WHERE labtest_id = 16 AND result >= 1000 AND MONTH(date_reported) = " + month + " AND YEAR(date_reported) = " + year; 
            denominator = getCount(query); 

            String reportingDateBegin  = DateUtil.parseDateToString(DateUtil.getFirstDateOfMonth(year, month), "yyyy-MM-dd");

            //Number VL test unsuppressed during the reporting period who attended EAC
            query = "SELECT COUNT(DISTINCT patient_id) FROM eac WHERE patient_id IN (SELECT DISTINCT patient_id FROM viralload WHERE labtest_id = 16 AND result >= 1000 AND MONTH(date_reported) = " + month + " AND YEAR(date_reported) = " + year + " AND (date_eac1 >= " + reportingDateBegin + " OR date_eac2 >= " + reportingDateBegin + " OR date_eac3 >= " + reportingDateBegin + "))"; 
            numerator = getCount(query); 
           
            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
            //indicator.setIndicatorNum(indicatorNum);
            indicator.setMonth(month);
            indicator.setYear(year);
            //indicator.setDenominator(denominator);
            //indicator.setNumerator(numerator);
            long indicatorId = getIndicatorId(indicatorNum, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
                //indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }        
    }

    // Clinic visit with documented TB status
    private void doDocumentedTb() {
        int indicatorNum = 7;
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            
            int denominator = 0;
            int numerator  = 0;
            
            //denominator - all clinic visits during the reporting month
            String query = "SELECT COUNT(*) AS count FROM visit WHERE MONTH(date_visit) =  " + month + " AND YEAR(date_visit) = " + year; 
            denominator = getCount(query);
            
            //numerator - all clinic visit during the reporting month with TB status not equal to null 
            query = "SELECT COUNT(*) AS count FROM visit WHERE MONTH(date_visit) =  " + month + " AND YEAR(date_visit) = " + year + " AND tb_status != '' AND tb_status IS NOT NULL"; 
            numerator = getCount(query);
            
            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
            //indicator.setIndicatorNum(indicatorNum);
            indicator.setMonth(month);
            indicator.setYear(year);
            //indicator.setDenominator(denominator);
            //indicator.setNumerator(numerator);
            long indicatorId = getIndicatorId(indicatorNum, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
                //indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }    
    }
 
    
    /*
     * Retention
    */        
    
    //Ever enrolled vs current on care
    private void doEverCurrentArt() {
        int indicatorNum = 8;
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            
            int denominator = 0;
            int numerator  = 0;

            String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE status_registration NOT IN ('Pre-ART Transfer In', 'ART Transfer In') AND MONTH(date_registration) =  " + month + " AND YEAR(date_registration) = " + year; 
            denominator = getCount(query);

            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status IN ('HIV+ non ART', 'ART Start', 'ART Restart', 'ART Transfer In', 'Pre-ART Transfer In') AND MONTH(date_registration) =  " + month + " AND YEAR(date_registration) = " + year; 
            numerator = getCount(query);

            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
            //indicator.setIndicatorNum(indicatorNum);
            indicator.setMonth(month);
            indicator.setYear(year);
            //indicator.setDenominator(denominator);
           // indicator.setNumerator(numerator);
            long indicatorId = getIndicatorId(indicatorNum, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
               // indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }    
    }

    //Newly enrolled (disaggregated by sex)
    private void doNewlyEnrolled() {
        int indicatorNum = 9;
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            
            int male = 0;
            int female  = 0;

            String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'MALE' AND status_registration NOT IN ('Pre-ART Transfer In', 'ART Transfer In') AND MONTH(date_registration) =  " + month + " AND YEAR(date_registration) = " + year; 
            male = getCount(query);

            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'FEMALE' AND status_registration NOT IN ('Pre-ART Transfer In', 'ART Transfer In') AND MONTH(date_registration) =  " + month + " AND YEAR(date_registration) = " + year; 
            female = getCount(query);

            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
           // indicator.setIndicatorNum(indicatorNum);
            indicator.setMonth(month);
            indicator.setYear(year);
           // indicator.setMale(male);
            //indicator.setFemale(female);
            long indicatorId = getIndicatorId(indicatorNum, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
              //  indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }    
    }
   
    //Current on ART (disaggregated by sex)
    private void doCurrentART() {
        int indicatorNum = 10;
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            
            int male  = 0;
            int female  = 0;

            String reportingDateEnd  = DateUtil.parseDateToString(DateUtil.getLastDateOfMonth(year, month), "yyyy-MM-dd");
            
            String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'MALE' AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND DATEDIFF(DAY, date_last_refill + last_refill_duration, '" + reportingDateEnd + "') <= " + Constants.Reports.LTFU_PEPFAR + " AND date_started IS NOT NULL"; 
            male = getCount(query);

            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE UPPER(gender) = 'FEMALE' AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND DATEDIFF(DAY, date_last_refill + last_refill_duration, '" + reportingDateEnd + "') <= " + Constants.Reports.LTFU_PEPFAR + " AND date_started IS NOT NULL"; 
            female = getCount(query);

            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
           // indicator.setIndicatorNum(indicatorNum);
            indicator.setMonth(month);
            indicator.setYear(year);
           // indicator.setMale(male);
           // indicator.setFemale(female);
            long indicatorId = getIndicatorId(indicatorNum, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
               // indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }    
    }

    //Number of those enrolled vs started on ART
    private void doEnrolledStart() {
        int indicatorNum = 11;
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            
            int denominator = 0;
            int numerator  = 0;

            String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE status_registration NOT IN ('Pre-ART Transfer In', 'ART Transfer In') AND MONTH(date_registration) =  " + month + " AND YEAR(date_registration) = " + year; 
            denominator = getCount(query);

            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE MONTH(date_started) =  " + month + " AND YEAR(date_started) = " + year; 
            numerator = getCount(query);

            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
          //  indicator.setIndicatorNum(indicatorNum);
            indicator.setMonth(month);
            indicator.setYear(year);
           // indicator.setDenominator(denominator);
            //indicator.setNumerator(numerator);
            long indicatorId = getIndicatorId(indicatorNum, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
                //indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }    
    }

    
    // Number of missed appointment for ART refill
    private void doMissedRefill() {
        int indicatorNum = 12;
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            
            int denominator = 0;
            int numerator  = 0;

            String query = "CREATE TEMPORARY TABLE appointment AS SELECT patient_id FROM pharm WHERE MONTH(next_appointment) = " + month + " AND YEAR(next_appointment) = " + year; 
            executeUpdate(query);
            
            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM appointment)";
            denominator = getCount(query);

            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM pharm WHERE patient_id IN (SELECT patient_id FROM appointment) AND MONTH(date_started) =  " + month + " AND YEAR(date_started) = " + year; 
            numerator = getCount(query);

            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
           // indicator.setIndicatorNum(indicatorNum);
            indicator.setMonth(month);
            indicator.setYear(year);
            //indicator.setDenominator(denominator);
           // indicator.setNumerator(numerator);
            long indicatorId = getIndicatorId(indicatorNum, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
                //indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }    
    }
           
    private boolean isInteger(String s) {
        int radix = 10;
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i), radix) < 0) return false;
        }
        return true;
    }    
        
    private void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }        
    }        
    
    private ResultSet executeQuery(String query) {
        ResultSet rs = null;
        try {
            jdbcUtil = new JDBCUtil();  
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        } 
        return rs;
    }            
        
    private int getCount(String query) {
       int count  = 0;
       try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                count = rs.getInt("count");               
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return count;
    }

    private long getIndicatorId(int indicatorNum, int month, int year) {
        String query = "SELECT indicator_id FROM indicator WHERE facility_id = " + facilityId + " AND month = " + month + " AND year = " + year +" AND indicator_num = " + indicatorNum;
        long id  = 0;
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                id = rs.getLong("indicator_id");               
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return id;
    }


    private void viralload() {
        int result = 0;
        try{
            query = "SELECT patient_id, resultab, date_reported FROM lab WHERE labtest_id = 16";
            ResultSet rs = executeQuery(query);
            if(rs.next()) {
                Long patientId = rs.getLong("patient_id");
                String resultab = rs.getString("resultab");                
                String dateReported = DateUtil.parseDateToString(rs.getDate("date_reported"), "yyyy-MM-dd");
                if(isInteger(resultab)) {
                    result = Integer.valueOf(resultab);
                }
                query = "INSERT INTO viralload(patient_id, result, date_reported) VALUES(" + patientId + ", " + result + ", '" + dateReported + "')";
                executeUpdate(query);
            }                   
        }
        catch(Exception exception) {
            exception.printStackTrace();            
        }
    }

    private int suppressed(int month, int year) {
        ArrayList<Long> suppressed;
        ArrayList<Long> unsuppressed;
        int count = 0;
        try{
            suppressed = new ArrayList<>();
            unsuppressed = new ArrayList<>();
            query = "SELECT patient_id, resultab FROM lab WHERE labtest_id = 16 AND MONTH(date_reported) = " + month + " AND YEAR(date_reported) = " + year;
            ResultSet rs = executeQuery(query);
            if(rs.next()) {
                Long patientId = rs.getLong("patient_id");
                String resultab = rs.getString("resultab");
                if(isInteger(resultab)) {
                    int result = Integer.valueOf(resultab);
                    if(result < 1000) {
                        count++;
                        suppressed.add(patientId);
                    }
                    else {
                        unsuppressed.add(patientId);
                    }
                }
                else {
                    count++;
                }
            }                   
        }
        catch(Exception exception) {
            exception.printStackTrace();            
        }
        return count;
    }
}


//        executeUpdate("DROP TABLE IF EXISTS cl");
//        
//        query = "CREATE TEMPORARY TABLE cl AS SELECT DISTINCT facility_id, patient_id, date_visit, time_stamp FROM clinic WHERE facility_id = " + facilityId + " AND time_stamp >= DATEADD('MONTH', -8, CURDATE()) AND time_stamp <= DATEADD('MONTH', -1, CURDATE())";
//        executeUpdate(query);
//                
//        query = "SELECT COUNT(DISTINCT patient.patient_id) AS count FROM patient JOIN statushistory ON patient.patient_id = statushistory.patient_id AND patient.facility_id = statushistory.facility_id WHERE patient.facility_id = " + facilityId + " AND patient.gender = 'Male'  AND statushistory.current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND statushistory.date_current_status <= '" + reportingDateEnd + "'"; 
