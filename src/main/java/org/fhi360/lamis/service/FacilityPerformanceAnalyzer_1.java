/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.fhi360.lamis.dao.hibernate.IndicatorDAO;
import org.fhi360.lamis.model.Indicatorvalue;
import org.fhi360.lamis.utility.ChartUtil;
import org.fhi360.lamis.utility.Constants;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PropertyAccessor;

/**
 *
 * @author user10
 */
public class FacilityPerformanceAnalyzer_1 {
    private ChartUtil chartUtil;
    
    private long stateId;
    private long lgaId;
    private long facilityId;
    private Date startDate;
    private Date endDate;

    private JDBCUtil jdbcUtil;
    private String query;
    
    public void analyze() {
        this.chartUtil = new ChartUtil();
        String databaseName = new PropertyAccessor().getDatabaseName();
        try {
            Map<String, Object> map = new PropertyAccessor().getSystemProperties();
            String appInstance = (String) map.get("appInstance");
            if(appInstance.equalsIgnoreCase("server")) {
                //Select all facilities that has uploaded data in the last 3 days and run their performance analysis
                //query = "SELECT DISTINCT facility_id FROM patient WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE()) UNION SELECT DISTINCT facility_id FROM clinic WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE()) UNION SELECT DISTINCT facility_id FROM pharmacy WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE()) UNION SELECT DISTINCT facility_id FROM laboratory WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE())";
                query = "SELECT DISTINCT facility_id FROM patient";
                //query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE()";
                //if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE()";
            }
            else {  
                query = "SELECT DISTINCT facility_id FROM patient";
                //query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE()";
                //if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE()";
            }
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) { 
                System.out.println("Analyzing....."+resultSet.getLong("facility_id"));
                facilityId = resultSet.getLong("facility_id");
                startDate = DateUtil.addMonth(new Date(), -6); 
                endDate = DateUtil.addMonth(new Date(), -1);
                
                preparedStatement = jdbcUtil.getStatement("SELECT * FROM facility WHERE facility_id = " + facilityId);
                ResultSet rs = preparedStatement.executeQuery();
                if(rs.next()) {
                    stateId = rs.getLong("state_id");
                    lgaId = rs.getLong("lga_id");
                }

                executeUpdate("DROP TABLE IF EXISTS entity");                       
                executeUpdate("CREATE TEMPORARY TABLE entity AS SELECT * FROM patient WHERE facility_id = " + facilityId);
                executeUpdate("CREATE INDEX idx_entity ON entity(patient_id)");

                executeUpdate("DROP TABLE IF EXISTS visit");        
                executeUpdate("CREATE TEMPORARY TABLE visit AS SELECT * FROM clinic WHERE facility_id = " + facilityId);
                executeUpdate("CREATE INDEX idx_visit ON visit(patient_id)");

                executeUpdate("DROP TABLE IF EXISTS pharm");        
                executeUpdate("CREATE TEMPORARY TABLE pharm AS SELECT * FROM pharmacy WHERE facility_id = " + facilityId);
                executeUpdate("CREATE INDEX idx_pharm ON pharm(patient_id)");

                executeUpdate("DROP TABLE IF EXISTS lab");        
                executeUpdate("CREATE TEMPORARY TABLE lab AS SELECT * FROM laboratory WHERE facility_id = " + facilityId);
                executeUpdate("CREATE INDEX idx_lab ON lab(patient_id)");

                doEverCurrentAnalysis();
                doStatusSummaryAnalysis();
                doEnrolledAnalysis();
                doCurrentArtAnalysis();
                doScreenedTbAnalyis();
                doStatusUpdateAnalysis();
                doViralLoadAnalysis();
                doCd4Analysis();
                
                doTotalEnrollmentAnalysis();
                doTotalCurrentAnalysis();
                doTotalVlSuppressedAnalysis();
            }
            new PropertyAccessor().setDateLastAsyncTask(new Date());
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
    }
    
    //Calculating ever enrolled vs current on care
    private void doEverCurrentAnalysis() {
        String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE status_registration NOT IN ('Pre-ART Transfer In', 'ART Transfer In') AND date_registration <= CURDATE()"; 
        int ever = getCount(query);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status IN ('HIV+ non ART', 'ART Start', 'ART Restart', 'ART Transfer In', 'Pre-ART Transfer In') AND date_registration <= CURDATE()"; 
        int current = getCount(query);

        int month = DateUtil.getMonth(new Date());
        int year = DateUtil.getYear(new Date());
        
        Indicatorvalue indicator = new Indicatorvalue();
        indicator.setFacilityId(facilityId);
        indicator.setStateId(stateId);
        indicator.setLgaId(lgaId);
       // indicator.setIndicatorNum(1);
        indicator.setMonth(month);
        indicator.setYear(year);
        //indicator.setEverEnrolled(ever);
        //indicator.setCurrentCare(current);
        long indicatorId = getIndicatorId(1, month, year);
        if(indicatorId == 0) {
            IndicatorDAO.save(indicator);
        }
        else {
            //indicator.setIndicatord(indicatorId);
            IndicatorDAO.update(indicator);
        }
    }
    
    //Calculating status summary
    private void doStatusSummaryAnalysis() {
        int month = DateUtil.getMonth(new Date());
        int year = DateUtil.getYear(new Date());
        
        Indicatorvalue indicator = new Indicatorvalue();
        indicator.setFacilityId(facilityId);
        indicator.setStateId(stateId);
        indicator.setLgaId(lgaId);
        //indicator.setIndicatorNum(2);
        indicator.setMonth(month);
        indicator.setYear(year);
        
        String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status = 'HIV+ non ART' AND date_registration <= CURDATE()"; 
        int total = getCount(query);       
        //indicator.setPreArt(total);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status = 'ART Start' AND date_registration <= CURDATE()"; 
        total = getCount(query);               
        //indicator.setArt(total);

        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status = 'ART Restart' AND date_registration <= CURDATE()"; 
        total = getCount(query);       
        //indicator.setRestart(total);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status IN ('ART Transfer In', 'Pre-ART Transfer In') AND date_registration <= CURDATE()"; 
        total = getCount(query);       
        //indicator.setTransferIn(total);

        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status IN ('ART Transfer Out', 'Pre-ART Transfer Out') AND date_registration <= CURDATE()"; 
        total = getCount(query);       
        //indicator.setTransferOut(total);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status = 'Stopped Treatment' AND date_registration <= CURDATE()"; 
        total = getCount(query);       
        //indicator.setStopped(total);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status = 'Lost to Follow Up' AND date_registration <= CURDATE()"; 
        total = getCount(query);       
        //indicator.setLost(total);

        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status = 'Known Death' AND date_registration <= CURDATE()"; 
        total = getCount(query);       
        //indicator.setDead(total);

        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE current_status IN ('HIV negative', 'HIV exposed Infant status unknown') AND date_registration <= CURDATE()"; 
        total = getCount(query);       

        //indicator.setOthers(total);
        long indicatorId = getIndicatorId(2, month, year);
        if(indicatorId == 0) {
            IndicatorDAO.save(indicator);
        }
        else {
           // indicator.setIndicatord(indicatorId);
            IndicatorDAO.update(indicator);
        }
    }
    
    //Enrolled Male vs Female
    private void doEnrolledAnalysis() {
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");

            String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'Male' AND status_registration = 'HIV+ non ART' AND MONTH(date_registration) =  " + month + " AND YEAR(date_registration) = " + year;
            int male = getCount(query);
            
            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'Female' AND status_registration = 'HIV+ non ART' AND MONTH(date_registration) = " + month + " AND YEAR(date_registration) = " + year;
            int female = getCount(query);

            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
           // indicator.setIndicatorNum(3);
            indicator.setMonth(month);
            indicator.setYear(year);
          //  indicator.setMale(male);
            //indicator.setFemale(female);
            long indicatorId = getIndicatorId(3, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
               // indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }        
    }

    //Current on ART Male vs Female
    private void doCurrentArtAnalysis() {
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            String reportingDateEnd = dateFormat.format(DateUtil.getLastDateOfMonth(year, month));
        
            String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'Male' AND ((current_status IN ('ART Start', 'ART Restart', 'ART Transfer In')) OR (current_status IN ('ART Transfer Out', 'Lost to Follow Up', 'Stopped Treatment', 'Known Death') AND date_current_status > '" + reportingDateEnd + "')) AND date_started IS NOT NULL AND date_started <= '" + reportingDateEnd + "'"; 
            int male = getCount(query);
            
            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'Female' AND ((current_status IN ('ART Start', 'ART Restart', 'ART Transfer In')) OR (current_status IN ('ART Transfer Out', 'Lost to Follow Up', 'Stopped Treatment', 'Known Death') AND date_current_status > '" + reportingDateEnd + "')) AND date_started IS NOT NULL AND date_started <= '" + reportingDateEnd + "'";
            int female = getCount(query);

            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
           // indicator.setIndicatorNum(4);
            indicator.setMonth(month);
            indicator.setYear(year);
           // indicator.setMale(male);
            //indicator.setFemale(female);
            long indicatorId = getIndicatorId(4, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
               // indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }        
    }
        
    private void doScreenedTbAnalyis() {
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            
            int denominator = 0;
            int numerator  = 0;
            double percentage = 0.0;
            
            //denominator - all clinic visits during the reporting month
            String query = "SELECT COUNT(*) AS count FROM visit WHERE MONTH(date_visit) =  " + month + " AND YEAR(date_visit) = " + year; 
            denominator = getCount(query);
            
            //numerator - all clinic visit during the reporting month with TB status not equal to null 
            query = "SELECT COUNT(*) AS count FROM visit WHERE MONTH(date_visit) =  " + month + " AND YEAR(date_visit) = " + year + " AND tb_status != '' AND tb_status IS NOT NULL"; 
            numerator = getCount(query);
            
            percentage = chartUtil.getPercentage(numerator, denominator);
            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
            //indicator.setIndicatorNum(5);
            indicator.setMonth(month);
            indicator.setYear(year);
            //indicator.setPercentage(percentage);
            long indicatorId = getIndicatorId(5, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
                //indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }    
    }
        
    private void doStatusUpdateAnalysis() {
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            double percentage = 0.0;
            
            String reportingDateEnd = new SimpleDateFormat("yyyy-MM-dd").format(DateUtil.getLastDateOfMonth(year, month));
            Map cal = calculate(facilityId, reportingDateEnd);
            int denominator = cal.isEmpty()? 0 : (Integer) cal.get("denominator");
            int numerator = cal.isEmpty()? 0 : (Integer) cal.get("numerator");                        
            percentage = chartUtil.getPercentage(numerator, denominator);

            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
            //indicator.setIndicatorNum(6);
            indicator.setMonth(month);
            indicator.setYear(year);
            //indicator.setPercentage(percentage);
            long indicatorId = getIndicatorId(6, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
               // indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }        
    }
    
    private void doViralLoadAnalysis() {
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            
            //Number of CD4 count test done during the reporting month
            String query = "SELECT COUNT(*) AS count FROM lab WHERE MONTH(date_reported) =  " + month + " AND YEAR(date_reported) = " + year + " AND labtest_id = 16"; 
            int number = getCount(query);  
            
            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
          //  indicator.setIndicatorNum(7);
            indicator.setMonth(month);
            indicator.setYear(year);
            //indicator.setNumber(number);
            long indicatorId = getIndicatorId(7, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
               // indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }        
    }
    
    private void doCd4Analysis() {
        int monthsBetween = DateUtil.monthsBetweenIgnoreDays(startDate, endDate);
        for (int i = 0; i <= monthsBetween; i++) {					
            Map<String, Object> period = chartUtil.getPeriod(startDate, i);
            int year = (Integer) period.get("year");
            int month = (Integer) period.get("month");
            
            //Number of CD4 count test done during the reporting month
            String query = "SELECT COUNT(*) AS count FROM lab WHERE MONTH(date_reported) =  " + month + " AND YEAR(date_reported) = " + year + " AND labtest_id = 1"; 
            int number = getCount(query);  
            
            Indicatorvalue indicator = new Indicatorvalue();
            indicator.setFacilityId(facilityId);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
           // indicator.setIndicatorNum(8);
            indicator.setMonth(month);
            indicator.setYear(year);
            //indicator.setNumber(number);
            long indicatorId = getIndicatorId(8, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
               // indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        }        
    }
    
    private void doTotalEnrollmentAnalysis() {
        String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'MALE' AND date_registration <= CURDATE()"; 
        int male = getCount(query);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'FEMALE' AND date_registration <= CURDATE()"; 
        int female = getCount(query);
        
        int month = DateUtil.getMonth(new Date());
        int year = DateUtil.getYear(new Date());
        
        Indicatorvalue indicator = new Indicatorvalue();
        indicator.setFacilityId(facilityId);
        indicator.setStateId(stateId);
        indicator.setLgaId(lgaId);
        //indicator.setIndicatorNum(20);
        indicator.setMonth(month);
        indicator.setYear(year);
        //indicator.setMale(male);
        //indicator.setFemale(female);
        long indicatorId = getIndicatorId(20, month, year);
        if(indicatorId == 0) {
            IndicatorDAO.save(indicator);
        }
        else {
          //  indicator.setIndicatord(indicatorId);
            IndicatorDAO.update(indicator);
        }
    }
    
    private void doTotalCurrentAnalysis() {
        String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'FEMALE' AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND DATEDIFF(DAY, date_last_refill + last_refill_duration, CURDATE()) <= " + Constants.Reports.LTFU_PEPFAR + " AND date_started IS NOT NULL"; 
        int male = getCount(query);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'MALE' AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND DATEDIFF(DAY, date_last_refill + last_refill_duration, CURDATE()) <= " + Constants.Reports.LTFU_PEPFAR + " AND date_started IS NOT NULL"; 
        int female = getCount(query);
        
        int month = DateUtil.getMonth(new Date());
        int year = DateUtil.getYear(new Date());
        
        Indicatorvalue indicator = new Indicatorvalue();
        indicator.setFacilityId(facilityId);
        indicator.setStateId(stateId);
        indicator.setLgaId(lgaId);
       // indicator.setIndicatorNum(21);
        indicator.setMonth(month);
        indicator.setYear(year);
        //indicator.setMale(male);
       // indicator.setFemale(female);
        
        long indicatorId = getIndicatorId(21, month, year);
        if(indicatorId == 0) {
            IndicatorDAO.save(indicator);
        }
        else {
           // indicator.setIndicatord(indicatorId);
            IndicatorDAO.update(indicator);
        }
        
    }

    private void doTotalVlSuppressedAnalysis(){
        String query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'Female' AND last_viral_load < 1000"; 
        int male = getCount(query);
        
        query = "SELECT COUNT(DISTINCT patient_id) AS count FROM entity WHERE gender = 'Male' AND last_viral_load < 1000";  
        int female = getCount(query);
        
        int month = DateUtil.getMonth(new Date());
        int year = DateUtil.getYear(new Date());
        
        Indicatorvalue indicator = new Indicatorvalue();
        indicator.setFacilityId(facilityId);
        indicator.setStateId(stateId);
        indicator.setLgaId(lgaId);
       // indicator.setIndicatorNum(22);
        indicator.setMonth(month);
        indicator.setYear(year);
       // indicator.setMale(male);
        //indicator.setFemale(female);
        long indicatorId = getIndicatorId(22, month, year);
        if(indicatorId == 0) {
            IndicatorDAO.save(indicator);
        }
        else {
           // indicator.setIndicatord(indicatorId);
            IndicatorDAO.update(indicator);
        }        
    }
    
    private Map calculate(long facilityId, String reportingDateEnd) {
        int denominator  = 0;
        int numerator = 0;
        Map<String, Object> map = new HashMap<String, Object>();
        
        //current ART patients whos last refill was more than 3 months ago 
        executeUpdate("DROP TABLE IF EXISTS lastrefill");        
        String query = "CREATE TEMPORARY TABLE lastrefill AS SELECT DISTINCT patient_id, MAX(date_visit) AS date_visit, duration FROM pharmacy WHERE facility_id = " + facilityId + " AND date_visit < '" + reportingDateEnd + "'  AND regimentype_id IN (1, 2, 3, 4, 14) GROUP BY patient_id, duration"; 
        executeUpdate(query);   

        executeUpdate("DROP TABLE IF EXISTS refill");        
        query = "CREATE TEMPORARY TABLE refill AS SELECT DISTINCT pharmacy.patient_id FROM pharmacy JOIN lastrefill ON pharmacy.patient_id = lastrefill.patient_id WHERE pharmacy.facility_id = " + facilityId + "  AND pharmacy.date_visit > lastrefill.date_visit AND (lastrefill.date_visit + lastrefill.duration + 90) <= pharmacy.date_visit";
        executeUpdate(query);
        try {
            query = "SELECT patient_id FROM lastrefill WHERE patient_id NOT IN (SELECT patient_id FROM refill)"; //selecting defaulters for refill
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()) {
                denominator  = denominator  + 1;
                long patientId = resultSet.getLong("patient_id");
                query = "SELECT patient_id FROM statushistory WHERE facility_id = " + facilityId + " AND patient_id = " + patientId + " AND current_status IN ('Lost to Follow Up', 'Stopped Treatment', 'ART Restart', 'Known Death', 'ART Transfer Out') AND date_current_status <= '" + reportingDateEnd + "'";
                preparedStatement = jdbcUtil.getStatement(query);
                ResultSet rs = preparedStatement.executeQuery();
                if(rs.next()) numerator = numerator + 1;
            }
            map.put("denominator", denominator );
            map.put("numerator", numerator);
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return map;
    }
    
    private void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }        
    }        
        
    private int getCount(String query) {
       int count  = 0;
       try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                count = rs.getInt("count");               
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return count;
    }

    private long getIndicatorId(int indicatorNum, int month, int year) {
        String query = "SELECT indicator_id FROM indicator WHERE facility_id = " + facilityId + " AND month = " + month + " AND year = " + year +" AND indicator_num = " + indicatorNum;
        long id  = 0;
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                id = rs.getLong("indicator_id");               
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return id;
    }
}


//        executeUpdate("DROP TABLE IF EXISTS cl");
//        
//        query = "CREATE TEMPORARY TABLE cl AS SELECT DISTINCT facility_id, patient_id, date_visit, time_stamp FROM clinic WHERE facility_id = " + facilityId + " AND time_stamp >= DATEADD('MONTH', -8, CURDATE()) AND time_stamp <= DATEADD('MONTH', -1, CURDATE())";
//        executeUpdate(query);
//                
//        query = "SELECT COUNT(DISTINCT patient.patient_id) AS count FROM patient JOIN statushistory ON patient.patient_id = statushistory.patient_id AND patient.facility_id = statushistory.facility_id WHERE patient.facility_id = " + facilityId + " AND patient.gender = 'Male'  AND statushistory.current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND statushistory.date_current_status <= '" + reportingDateEnd + "'"; 
