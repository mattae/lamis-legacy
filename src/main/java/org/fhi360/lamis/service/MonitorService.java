/**
 *
 * @author user1
 */


package org.fhi360.lamis.service;

import org.fhi360.lamis.utility.JDBCUtil;
import java.sql.PreparedStatement;
import org.apache.struts2.ServletActionContext;

public class MonitorService{   
    public static void logEntity(String entityId, String tableName, int operationId) {
        JDBCUtil jdbcUtil = null;
        PreparedStatement preparedStatement = null;
        String query = "INSERT INTO monitor (facility_id, entity_id, table_name, operation_id, user_id, time_stamp) VALUES(?, ?, ?, ?, ?, ?)";
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.setLong(1, (Long) ServletActionContext.getRequest().getSession().getAttribute("facilityId"));
            preparedStatement.setString(2, entityId);
            preparedStatement.setString(3, tableName);
            preparedStatement.setInt(4, operationId);
            preparedStatement.setLong(5, (Long) ServletActionContext.getRequest().getSession().getAttribute("userId"));
            preparedStatement.setTimestamp(6, new java.sql.Timestamp(new java.util.Date().getTime()));
            preparedStatement.executeUpdate();                
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }                
    }
    
}
