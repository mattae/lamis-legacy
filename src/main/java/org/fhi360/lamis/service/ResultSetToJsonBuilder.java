/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author user1
 */
package org.fhi360.lamis.service;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.Map;
import net.sf.json.JSONArray;

public class ResultSetToJsonBuilder {

    public ResultSetToJsonBuilder() {
    } 
    
    public String build(ResultSet resultSet, String table) {
        JSONArray jsonArray = new JSONArray();     //ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); 
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int colCount = metaData.getColumnCount();
            while(resultSet.next()) {
                long id = 0;

                Map<String, Object> record = new HashMap<String, Object>();
                for(int i = 1; i <= colCount; i++) {
                    String columnName = metaData.getColumnName(i).toLowerCase();
                    Object value = resultSet.getObject(i) == null? "" : resultSet.getObject(i);
                    record.put(columnName, value.toString());
                }
                jsonArray.add(record);   //list.add(map);
            }
            resultSet = null;
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
        return jsonArray.toString();
    }
    
    public String build(ResultSet resultSet, String table, Map<Long, String> patientEntityMap) {
        JSONArray jsonArray = new JSONArray();     //ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); 
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int colCount = metaData.getColumnCount();
            while(resultSet.next()) {
                if(patientEntityMap.get(resultSet.getLong("patient_id")) != null) {
                    long id = 0;
                    Map<String, Object> record = new HashMap<String, Object>();
                    for(int i = 1; i <= colCount; i++) {
                        String columnName = metaData.getColumnName(i).toLowerCase();
                        Object value = resultSet.getObject(i) == null? "" : resultSet.getObject(i);

                        if(columnName.equals("patient_id")) id = (Long) value;
                        record.put(columnName, value.toString());
                    }
                    record.put("hospital_num", patientEntityMap.get(id));
                    jsonArray.add(record);   //list.add(map);                    
                }
            }
            resultSet = null;
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
        return jsonArray.toString();
    }

    public String buildChild(ResultSet resultSet, String table, Map<Long, String> patientEntityMap) {
        JSONArray jsonArray = new JSONArray();     //ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); 
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int colCount = metaData.getColumnCount();
            while(resultSet.next()) {
                if(patientEntityMap.get(resultSet.getLong("patient_id")) != null) {
                    long id = 0;
                    Map<String, Object> record = new HashMap<String, Object>();
                    for(int i = 1; i <= colCount; i++) {
                        String columnName = metaData.getColumnName(i).toLowerCase();
                        Object value = resultSet.getObject(i) == null? "" : resultSet.getObject(i);

                        if(columnName.equals("patient_id_mother")) id = (Long) value;
                        record.put(columnName, value.toString());
                    }
                    record.put("hospital_num_mother", patientEntityMap.get(id));
                    jsonArray.add(record);   //list.add(map);                    
                }
            }
            resultSet = null;
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
        return jsonArray.toString();
    }
    
    public String buildDeliveryOrMaternalFollowup(ResultSet resultSet, String table, Map<Long, String> patientEntityMap, Map<Long, String> ancEntityMap) {
        JSONArray jsonArray = new JSONArray();     //ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); 
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int colCount = metaData.getColumnCount();
            while(resultSet.next()) {
                if(patientEntityMap.get(resultSet.getLong("patient_id")) != null) {
                    long id = 0;
                    long ancId = 0;
                    Map<String, Object> record = new HashMap<String, Object>();
                    for(int i = 1; i <= colCount; i++) {
                        String columnName = metaData.getColumnName(i).toLowerCase();
                        Object value = resultSet.getObject(i) == null? "" : resultSet.getObject(i);

                        if(columnName.equals("patient_id")) id = (Long) value;
                        if(columnName.equals("anc_id")) ancId = (Long) value;
                        record.put(columnName, value.toString());
                    }
                    record.put("hospital_num", patientEntityMap.get(id));
                    record.put("anc_num", ancEntityMap.get(ancId));
                    jsonArray.add(record);   //list.add(map);                    
                }
            }
            resultSet = null;
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
        return jsonArray.toString();
    }

    public String buildChildFollowup(ResultSet resultSet, String table, Map<Long, String> childEntityMap) {
        JSONArray jsonArray = new JSONArray();     //ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); 
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int colCount = metaData.getColumnCount();
            while(resultSet.next()) {
                long id = 0;
                Map<String, Object> record = new HashMap<String, Object>();
                for(int i = 1; i <= colCount; i++) {
                    String columnName = metaData.getColumnName(i).toLowerCase();
                    Object value = resultSet.getObject(i) == null? "" : resultSet.getObject(i);

                    if(columnName.equals("child_id")) id = (Long) value;
                    record.put(columnName, value.toString());
                }
                record.put("reference_num", childEntityMap.get(id) == null? "" : childEntityMap.get(id));
                jsonArray.add(record);   //list.add(map);
            }
            resultSet = null;
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
        return jsonArray.toString();
    }
    
}
