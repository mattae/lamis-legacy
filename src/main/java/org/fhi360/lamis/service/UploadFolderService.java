/**
 *
 * @author user1
 */

package org.fhi360.lamis.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.struts2.ServletActionContext;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PropertyAccessor;

public class UploadFolderService {
    
    public void lockUploadFolder(long facilityId) {  
        //To lock a folder create a file named lock.ser
        Map<String, Object> map = new PropertyAccessor().getSystemProperties();
        String contextPath = (String) map.get("contextPath");
        String fileName = contextPath+"exchange/sync/" + Long.toString(facilityId) + "/lock.ser";    
        try {
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);
            stream.close();
            /*
            Added
            */
            insertFacilityUploaded(facilityId);
        }
        catch (Exception exception) {
            throw new RuntimeException(exception); 
        }               
    }

    public void unlockUploadFolder(long facilityId) { 
        //To unlock a folder delete the file lock.ser
        Map<String, Object> map = new PropertyAccessor().getSystemProperties();
        String contextPath = (String) map.get("contextPath");
        String fileName = contextPath+"exchange/sync/" + Long.toString(facilityId) + "/lock.ser";    
        try {
            File file = new File(fileName);
            if(file.exists()) {
                file.delete();
            }
        }
        catch (Exception exception) {
            throw new RuntimeException(exception); 
        }       
    }
    
    public String getUploadFolderStatus(long facilityId) {
        // If file lock.ser if found in the folder, that folder is locked and cannot be accessed by the client to write data into
        //if the file is not present the folder is unlock and data can be writen into it
        
        Map<String, Object> map = new PropertyAccessor().getSystemProperties();
        String contextPath = (String) map.get("contextPath");
        String folder = contextPath+"exchange/sync/" + Long.toString(facilityId) + "/"; 
        if(new File(folder + "lock.ser").exists()) {
            return "locked";
        }
        else {
            return "unlocked";
        }
       
    }
  
    public void lockUploadFolder(String folder) {  
        //To lock a folder create a file named lock.ser
        Map<String, Object> map = new PropertyAccessor().getSystemProperties();
        String fileName = folder + "lock.ser";    
        try {
            File file = new File(fileName);
            FileOutputStream stream = new FileOutputStream(file);
            stream.close();
            
            /*
            Added: Not sure how to get facility ID from the folder name; You could adjust this
            */
            int index = folder.lastIndexOf("\\");
            Long facilityId = Long.parseLong(folder.substring(index));
            insertFacilityUploaded(facilityId);
        }
        catch (Exception exception) {
            throw new RuntimeException(exception); 
        }               
    }

    public void unlockUploadFolder(String folder) { 
        //To unlock a folder delete the file lock.ser
        Map<String, Object> map = new PropertyAccessor().getSystemProperties();
        String fileName = folder + "lock.ser";    
        try {
            File file = new File(fileName);
            if(file.exists()) {
                file.delete();
            }
        }
        catch (Exception exception) {
            throw new RuntimeException(exception); 
        }       
    }
    
    public String getUploadFolderStatus(String folder) {
        // If file lock.ser if found in the folder, that folder is locked and cannot be accessed by the client to write data into
        //if the file is not present the folder is unlock and data can be writen into it
        
        Map<String, Object> map = new PropertyAccessor().getSystemProperties();
        if(new File(folder + "lock.ser").exists()) {
            return "locked";
        }
        else {
            return "unlocked";
        }
       
    }

    public static String getDisableRecordsAll() {
        String contextPath = ServletActionContext.getServletContext().getInitParameter("contextPath");
        try {
            File file = new File(contextPath+"exchange/disable.ser");
            if(file.exists()) {
                return "disable";
            }
            else {
                return "enable";
            }
        }
        catch (Exception exception) {
            throw new RuntimeException(exception); 
        }                       
       
    }
    
    /*
    Added
    */
    private void insertFacilityUploaded(Long facilityId) {
        try {
            JDBCUtil jdbcu = new JDBCUtil();
            PreparedStatement statement = jdbcu.getStatement("select name from facility where facility_id = ?");
            statement.setLong(1, facilityId);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                String facilityName = rs.getString("name");
                statement = jdbcu.getStatement("insert into facility_sync (facility_name, facility_id, sync_date) "
                        + "values(?, ?, ?)");
                statement.setString(1, facilityName);
                statement.setLong(2, facilityId);
                statement.setDate(3, new java.sql.Date(new Date().getTime()));
                statement.execute();
            }
        } catch (SQLException | IOException ex) {
            Logger.getLogger(UploadFolderService.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
}
