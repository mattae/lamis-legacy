/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service;

import java.sql.ResultSet;

/**
 *
 * @author user1
 */
public class XmlBuilderDelegator {

    private ResultSetToXmlBuilder resultSetToXmlBuilder;
    private EntityIdentifier entityIdentifier;

    public XmlBuilderDelegator() {
        this.resultSetToXmlBuilder = new ResultSetToXmlBuilder();
        this.entityIdentifier = new EntityIdentifier();
    }

    public void delegate(ResultSet resultSet, String table, String directory, long facilityId) {
        try {
            if (table.equals("monitor")) {
                resultSetToXmlBuilder.build(resultSet, table, directory);
            }
            if (table.equals("user")) {
                resultSetToXmlBuilder.build(resultSet, table, directory);
            }
            if (table.equals("casemanager")) {
                resultSetToXmlBuilder.build(resultSet, table, directory);
            }
            if (table.equals("communitypharm")) {
                resultSetToXmlBuilder.build(resultSet, table, directory);
            }
            if (table.equals("patient")) {
                resultSetToXmlBuilder.build(resultSet, table, directory);
            }
            if (table.equals("clinic")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("pharmacy")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("laboratory")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("adrhistory")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("oihistory")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("adherehistory")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("statushistory")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("regimenhistory")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }

            if (table.equals("chroniccare")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("tbscreenhistory")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("dmscreenhistory")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }

            if (table.equals("anc")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("delivery")) {
                resultSetToXmlBuilder.buildDeliveryOrMaternalFollowup(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId), entityIdentifier.getAncEntities(facilityId));
            }
            if (table.equals("child")) {
                resultSetToXmlBuilder.buildChild(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("childfollowup")) {
                resultSetToXmlBuilder.buildChildFollowup(resultSet, table, directory, entityIdentifier.getChildEntities(facilityId));
            }
            if (table.equals("maternalfollowup")) {
                resultSetToXmlBuilder.buildDeliveryOrMaternalFollowup(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId), entityIdentifier.getAncEntities(facilityId));
            }
            if (table.equals("partnerinformation")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }

            if (table.equals("specimen")) {
                resultSetToXmlBuilder.build(resultSet, table, directory);
            }
            if (table.equals("eid")) {
                resultSetToXmlBuilder.build(resultSet, table, directory);
            }
            if (table.equals("labno")) {
                resultSetToXmlBuilder.build(resultSet, table, directory);
            }
            if (table.equals("biometric")) {
                resultSetToXmlBuilder.build(resultSet, table, directory);
            }
            if (table.equals("nigqual")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("devolve")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("patientcasemanager")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
            if (table.equals("eac")) {
                resultSetToXmlBuilder.build(resultSet, table, directory, entityIdentifier.getPatientEntities(facilityId));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }
}
