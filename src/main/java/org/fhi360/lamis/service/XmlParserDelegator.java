/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service;

import org.fhi360.lamis.service.parser.AdhereXmlParser;
import org.fhi360.lamis.service.parser.AdrXmlParser;
import org.fhi360.lamis.service.parser.AncXmlParser;
import org.fhi360.lamis.service.parser.CaseManagerXmlParser;
import org.fhi360.lamis.service.parser.ChildXmlParser;
import org.fhi360.lamis.service.parser.ChildfollowupXmlParser;
import org.fhi360.lamis.service.parser.ChroniccareXmlParser;
import org.fhi360.lamis.service.parser.ClinicXmlParser;
import org.fhi360.lamis.service.parser.CommunitypharmXmlParser;
import org.fhi360.lamis.service.parser.DeliveryXmlParser;
import org.fhi360.lamis.service.parser.DevolveXmlParser;
import org.fhi360.lamis.service.parser.DmscreenhistoryXmlParser;
import org.fhi360.lamis.service.parser.EacXmlParser;
import org.fhi360.lamis.service.parser.EidXmlParser;
import org.fhi360.lamis.service.parser.LabnoXmlParser;
import org.fhi360.lamis.service.parser.LaboratoryXmlParser;
import org.fhi360.lamis.service.parser.MaternalfollowupXmlParser;
import org.fhi360.lamis.service.parser.MonitorXmlParser;
import org.fhi360.lamis.service.parser.MotherInformationXmlParser;
import org.fhi360.lamis.service.parser.NigqualXmlParser;
import org.fhi360.lamis.service.parser.OiXmlParser;
import org.fhi360.lamis.service.parser.PartnerinformationXmlParser;
import org.fhi360.lamis.service.parser.PatientXmlParser;
import org.fhi360.lamis.service.parser.PatientcasemanagerXmlParser;
import org.fhi360.lamis.service.parser.PharmacyXmlParser;
import org.fhi360.lamis.service.parser.RegimenXmlParser;
import org.fhi360.lamis.service.parser.SpecimenXmlParser;
import org.fhi360.lamis.service.parser.StatusXmlParser;
import org.fhi360.lamis.service.parser.TbscreenhistoryXmlParser;
import org.fhi360.lamis.service.parser.UserXmlParser;

/**
 *
 * @author user1
 */
public class XmlParserDelegator {
    private MonitorXmlParser monitorXmlParser;
    private UserXmlParser userXmlParser;
    private CaseManagerXmlParser caseManagerXmlParser;
    private CommunitypharmXmlParser communitypharmXmlParser;
    private PatientXmlParser patientXmlParser;
    private ClinicXmlParser clinicXmlParser;
    private PharmacyXmlParser pharmacyXmlParser;
    private LaboratoryXmlParser laboratoryXmlParser;
    private AdrXmlParser adrXmlParser;
    private OiXmlParser oiXmlParser;
    private AdhereXmlParser adhereXmlParser;
    private StatusXmlParser statusXmlParser;
    private RegimenXmlParser regimenXmlParser;
    private ChroniccareXmlParser chroniccareXmlParser; 
    private TbscreenhistoryXmlParser tbscreenhistoryXmlParser;
    private DmscreenhistoryXmlParser dmscreenhistoryXmlParser;
    private AncXmlParser ancXmlParser;
    private DeliveryXmlParser deliveryXmlParser;
    private ChildXmlParser childXmlParser;
    private MaternalfollowupXmlParser maternalfollowupXmlParser;
    private ChildfollowupXmlParser childfollowupXmlParser;
    private PartnerinformationXmlParser partnerinformationXmlParser;
    private SpecimenXmlParser specimenXmlParser;
    private EidXmlParser eidXmlParser;
    private LabnoXmlParser labnoXmlParser;
    private NigqualXmlParser nigqualXmlParser;
    private DevolveXmlParser devolveXmlParser;
    private PatientcasemanagerXmlParser patientcasemanagerXmlParser;
    private EacXmlParser eacXmlParser;
    private MotherInformationXmlParser motherinformationXmlParser;
    
    public XmlParserDelegator() {
        this.monitorXmlParser = new MonitorXmlParser();
        this.userXmlParser = new UserXmlParser();
        this.caseManagerXmlParser = new CaseManagerXmlParser();
        this.communitypharmXmlParser = new CommunitypharmXmlParser();
        this.patientXmlParser = new PatientXmlParser();
        this.clinicXmlParser = new ClinicXmlParser();
        this.pharmacyXmlParser = new PharmacyXmlParser();
        this.laboratoryXmlParser = new LaboratoryXmlParser();
        this.adrXmlParser = new AdrXmlParser();
        this.oiXmlParser = new OiXmlParser();
        this.adhereXmlParser = new AdhereXmlParser();
        this.statusXmlParser = new StatusXmlParser();
        this.regimenXmlParser = new RegimenXmlParser();
        this.chroniccareXmlParser = new ChroniccareXmlParser(); 
        this.tbscreenhistoryXmlParser = new TbscreenhistoryXmlParser();
        this.dmscreenhistoryXmlParser = new DmscreenhistoryXmlParser();
        this.ancXmlParser = new AncXmlParser();
        this.deliveryXmlParser = new DeliveryXmlParser();
        this.childXmlParser = new ChildXmlParser();
        this.maternalfollowupXmlParser = new MaternalfollowupXmlParser();
        this.childfollowupXmlParser = new ChildfollowupXmlParser();
        this.partnerinformationXmlParser = new PartnerinformationXmlParser();
        this.specimenXmlParser = new SpecimenXmlParser();
        this.eidXmlParser = new EidXmlParser();
        this.labnoXmlParser = new LabnoXmlParser();
        this.nigqualXmlParser = new NigqualXmlParser();
        this.devolveXmlParser = new DevolveXmlParser();
        this.patientcasemanagerXmlParser = new PatientcasemanagerXmlParser();
        this.eacXmlParser = new EacXmlParser();
        this.motherinformationXmlParser = new MotherInformationXmlParser();
        
    }
            
    public void delegate(String table, String fileName) {
        try {
            //Create temporary table to hold local ids and server generated ids
//            if(table.equals("monitor")) monitorXmlParser.parseXml(fileName); 
//            if(table.equals("user")) userXmlParser.parseXml(fileName); 
//            if(table.equals("casemanager")) caseManagerXmlParser.parseXml(fileName);
//            if(table.equals("communitypharm")) communitypharmXmlParser.parseXml(fileName);
//            
            if(table.equals("patient")) patientXmlParser.parseXml(fileName); 
//            if(table.equals("clinic")) clinicXmlParser.parseXml(fileName); 
//            if(table.equals("pharmacy")) pharmacyXmlParser.parseXml(fileName); 
//            if(table.equals("laboratory")) laboratoryXmlParser.parseXml(fileName); 
//            if(table.equals("adrhistory")) adrXmlParser.parseXml(fileName); 
//            if(table.equals("oihistory")) oiXmlParser.parseXml(fileName); 
//            if(table.equals("adherehistory")) adhereXmlParser.parseXml(fileName); 
          if(table.equals("statushistory")) statusXmlParser.parseXml(fileName); 
//            if(table.equals("regimenhistory")) regimenXmlParser.parseXml(fileName); 
//            
//            if(table.equals("chroniccare")) chroniccareXmlParser.parseXml(fileName); 
//            if(table.equals("tbscreenhistory")) tbscreenhistoryXmlParser.parseXml(fileName); 
//            if(table.equals("dmscreenhistory")) dmscreenhistoryXmlParser.parseXml(fileName); 
//            
//            if(table.equals("motherinformation")) motherinformationXmlParser.parseXml(fileName); 
//            if(table.equals("anc")) ancXmlParser.parseXml(fileName); 
//            if(table.equals("delivery")) deliveryXmlParser.parseXml(fileName); 
//            if(table.equals("child")) childXmlParser.parseXml(fileName); 
//            if(table.equals("maternalfollowup")) maternalfollowupXmlParser.parseXml(fileName); 
//            if(table.equals("childfollowup")) childfollowupXmlParser.parseXml(fileName); 
//            if(table.equals("partnerinformation")) partnerinformationXmlParser.parseXml(fileName);
//
//            if(table.equals("specimen")) specimenXmlParser.parseXml(fileName); 
//            if(table.equals("eid")) eidXmlParser.parseXml(fileName); 
//            if(table.equals("labno")) labnoXmlParser.parseXml(fileName);            
//
//            if(table.equals("nigqual")) nigqualXmlParser.parseXml(fileName);
//            if(table.equals("devolve")) devolveXmlParser.parseXml(fileName);
//            if(table.equals("patientcasemanager")) patientcasemanagerXmlParser.parseXml(fileName);
//
//            if(table.equals("eac")) eacXmlParser.parseXml(fileName); 
        } 
        catch (Exception exception) {
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }        
    } 
}
