/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.chart;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author user10
 */
public class CurrentOnArtRegimenChartService {
    
    private JDBCUtil jdbcUtil;
    
    
 public CurrentOnArtRegimenChartService () {
        try {
            this.jdbcUtil = new JDBCUtil();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
     }
   
    
    public Map<String, Object> getChartData(long ipId, long stateId, long lgaId, long facilityId, int month, int year, String dataElementIds) {
        
        Map<String, Object> map = new HashMap<String, Object>();                
        map.put("month", month);
        map.put("year", year);

        String query = "SELECT SUM(value) AS value, data_element_id  FROM indicatorvalue WHERE data_element_id IN ( " + dataElementIds + ") AND month = " + month + " AND year = " + year;
        if(stateId !=0) query = query + " AND state_id = " + stateId;
        if(lgaId !=0) query = query + " AND lga_id = " + lgaId;
        if(facilityId !=0) query = query + " AND facility_id = " + facilityId;
        
        query = query + " GROUP BY data_element_id"; 
        
        try {
            ResultSet resultSet = execute(query);
            if(resultSet !=null) {
                while(resultSet.next()) {
                    int dataElementId = resultSet.getInt("data_element_id");
                    if( dataElementId == 10) {
                        map.put("value1", resultSet.getInt("value"));
                        //System.out.println(".....Current on ART:"+resultSet.getInt("value"));
                    } 
                    else {
                        map.put("value2", resultSet.getInt("value"));
                        //System.out.println(".....TLD:"+resultSet.getInt("value"));
                    }
                }
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }        
        return map;        
    }
    
    private ResultSet execute(String query) {
        ResultSet rs = null;
        try {
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return rs;
    }
   
    
    
}
