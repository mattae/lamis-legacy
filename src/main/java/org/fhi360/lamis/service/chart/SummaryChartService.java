/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.chart;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author user10
 */
public class SummaryChartService {
    private JDBCUtil jdbcUtil;
 
 public SummaryChartService () {
        try {
            this.jdbcUtil = new JDBCUtil();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
     }
    
    public ArrayList<Map<String, Object>> getChartData(long ipId, long stateId, long lgaId, long facilityId,  String dataElementIds, String categoryFemaleIds, String categoryMaleIds) {       
        ArrayList<Map<String, Object>> analysisList = new ArrayList<Map<String, Object>>();

        String [] ids = dataElementIds.split(",");
        for(String dataElementId : ids) {
            Map<String, Object> map = new HashMap<String, Object>();  
            
            System.out.println(".......data element: "+dataElementId);
            System.out.println(".......category:"+categoryFemaleIds);
            System.out.println(".......category:"+categoryMaleIds);
            
            try {
                String query = "SELECT SUM(value) AS value FROM indicatorvalue WHERE data_element_id = " + Integer.parseInt(dataElementId) + " AND category_id IN (" + categoryFemaleIds + ")";
                if(stateId !=0) query = query + " AND state_id = " + stateId;
                if(lgaId !=0) query = query + " AND lga_id = " + lgaId;
                if(facilityId !=0) query = query + " AND facility_id = " + facilityId;

                int female = 0;

                ResultSet resultSet = execute(query);
                if(resultSet != null) {
                    if(resultSet.next()) {
                       female = resultSet.getInt("value"); 
                    }
                }
                System.out.println(".....female:"+female);
                map.put("female", female); 


                query = "SELECT SUM(value) AS value FROM indicatorvalue WHERE data_element_id = " + dataElementId + " AND category_id IN (" + categoryMaleIds + ")";
                if(stateId !=0) query = query + " AND state_id = " + stateId;
                if(lgaId !=0) query = query + " AND lga_id = " + lgaId;
                if(facilityId !=0) query = query + " AND facility_id = " + facilityId;

                int male = 0;

                resultSet = execute(query);
                if(resultSet != null) {
                    if(resultSet.next()) {
                       male = resultSet.getInt("value"); 
                    }
                }
                System.out.println(".....male:"+male);
                map.put("male", male); 
                
            }
            catch (Exception exception) {
                exception.printStackTrace();
            }
            analysisList.add(map);            
        }
        return analysisList;     
    }

    private ResultSet execute(String query) {
        ResultSet rs = null;
        try {
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return rs;
    }

}
