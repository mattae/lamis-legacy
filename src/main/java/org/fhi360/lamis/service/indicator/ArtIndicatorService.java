/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.indicator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import org.fhi360.lamis.dao.hibernate.IndicatorDAO;
import org.fhi360.lamis.model.Indicatorvalue;
import org.fhi360.lamis.utility.Constants;
import org.fhi360.lamis.utility.DateUtil;
import org.fhi360.lamis.utility.JDBCUtil;
import static org.fhi360.lamis.utility.StringUtil.isInteger;

/**
 *
 * @author user10
 */
public class ArtIndicatorService {
    private int reportingMonth;
    private int reportingYear;
    private String reportingDateBegin;
    private String reportingDateEnd;
    private Long stateId;
    private Long lgaId;

    private String query;
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private HashMap parameterMap;
    private long facilityId;
    //private static final Log log = LogFactory.getLog(ArtSummaryProcessor.class);

    private int agem1, agem2, agem3, agem4, agem5, agem6, agem7, agem8, agem9, agem10, agem11, agem12;;
    private int agef1, agef2, agef3, agef4, agef5, agef6, agef7, agef8, agef9, agef10, agef11, agef12;
    private int agem13_1, agem14_1, agem15_1, agem13_2, agem14_2, agem15_2;
    private int agef13_1, agef14_1, agef15_1, agef13_2, agef14_2, agef15_2;
    private int preg, feeding, tbm, tbf;
    
    private int dataElementId = 0;

    public ArtIndicatorService() {
    }

    public void process(long facilityId, int month, int year) {
        this.facilityId = facilityId;
        this.reportingMonth = month;
        this.reportingYear = year;
        reportingDateBegin = dateFormat.format(DateUtil.getFirstDateOfMonth(reportingYear, reportingMonth));
        reportingDateEnd = dateFormat.format(DateUtil.getLastDateOfMonth(reportingYear, reportingMonth));
        try {
            jdbcUtil = new JDBCUtil();
            getStateId(); // stateId and lgaId
        

            ResultSet rs;
            System.out.println("Computing ART1.....");
            //ART 1
            //Total Number of HIV-positive newly enrolled in clinical care during the month (excludes transfer-in)
            initVariables();
            query = "SELECT DISTINCT patient_id, gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age, pregnant, breastfeeding, tb_status FROM patient WHERE facility_id = " + facilityId + " AND YEAR(date_registration) = " + reportingYear + " AND MONTH(date_registration) = " + reportingMonth + " AND status_registration = 'HIV+ non ART'";
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                int pregnant = resultSet.getInt("pregnant");
                int breastfeeding = resultSet.getInt("breastfeeding");
                String tbStatus = resultSet.getString("tb_status") == null ? "" : resultSet.getString("tb_status");

                disaggregate(gender, age);
                if (gender.trim().equalsIgnoreCase("Male")) {
                    //check for TB status during enrolmemnt
                    if (tbStatus.equalsIgnoreCase("Currently on TB treatment") || tbStatus.equalsIgnoreCase("TB positive not on TB drugs")) {
                        tbm++;
                    }
                } else {
                    //check if client is pregnant or breast feeding during enrolment
                    if (pregnant == 1) {
                        preg++;
                    } else {
                        if (breastfeeding == 1) {
                            feeding++;
                        }
                    }
                    //check for TB status during enrolmemnt
                    if (tbStatus.equalsIgnoreCase("Currently on TB treatment") || tbStatus.equalsIgnoreCase("TB positive not on TB drugs")) {
                        tbf++;
                    }
                }
            }
            //Populate the report parameter map with values computed for ART 1
            dataElementId = 1;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+
            
            dataElementId = 2;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, preg);         

            dataElementId = 3;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, feeding);         

            dataElementId = 5;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, tbm);         
            dataElementId = 4;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, tbf);         

            System.out.println("Computing ART2.....");
            //ART 2
            //Total number of people living with HIV who are currently in HIV care who received at least one of the following
            //by the end of the month: clinical assessment(WHO staging) OR CD4 count OR viral load OR current on treatment           
            initVariables();

            executeUpdate("DROP INDEX IF EXISTS idx_visit");        
            executeUpdate("DROP TABLE IF EXISTS visit");        
            executeUpdate("CREATE TEMPORARY TABLE visit AS SELECT * FROM clinic WHERE facility_id = " + facilityId + " AND date_visit >= DATEADD('MONTH', -6, '" + reportingDateBegin + "') AND date_visit <= '" + reportingDateEnd + "' AND clinic_stage IS NOT NULL OR clinic_stage != ''");
            executeUpdate("CREATE INDEX idx_visit ON visit(patient_id)");
            
            executeUpdate("DROP INDEX IF EXISTS idx_preg");        
            executeUpdate("DROP TABLE IF EXISTS preg");        
            executeUpdate("CREATE TEMPORARY TABLE preg AS SELECT * FROM clinic WHERE facility_id = " + facilityId + " AND date_visit >= DATEADD('MONTH', -9, '" + reportingDateBegin + "') AND date_visit <= '" + reportingDateEnd + "' ORDER BY date_visit DESC LIMIT 1");
            executeUpdate("CREATE INDEX idx_preg ON preg(patient_id)");

            executeUpdate("DROP INDEX IF EXISTS idx_pharm");        
            executeUpdate("DROP TABLE IF EXISTS pharm");        
            executeUpdate("CREATE TEMPORARY TABLE pharm AS SELECT * FROM pharmacy WHERE facility_id = " + facilityId + " AND date_visit <= '" + reportingDateEnd + "' AND regimentype_id IN (1, 2, 3, 4, 14)");
            executeUpdate("CREATE INDEX idx_pharm ON pharm(patient_id)");
 
            executeUpdate("DROP INDEX IF EXISTS idx_lab");        
            executeUpdate("DROP TABLE IF EXISTS lab");        
            executeUpdate("CREATE TEMPORARY TABLE lab AS SELECT * FROM laboratory WHERE facility_id = " + facilityId + " AND date_reported >= DATEADD('MONTH', -6, '" + reportingDateBegin + "') AND date_reported <= '" + reportingDateEnd + "' AND labtest_id IN (1, 16)");
            executeUpdate("CREATE INDEX idx_lab ON lab(patient_id)");

            query = "SELECT DISTINCT patient_id, gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age FROM patient WHERE facility_id = " + facilityId + " AND current_status IN (" + Constants.ClientStatus.ON_TREATMENT + ") AND DATEDIFF(DAY, date_last_refill + last_refill_duration, CURDATE()) <= " + Constants.Reports.LTFU_PEPFAR + " AND date_started IS NOT NULL ORDER BY current_status"; 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                Date dateStarted = resultSet.getDate("date_started");

                int pregnant = 0;
                int breastfeeding = 0;

                boolean count = false;

                query = "SELECT DISTINCT patient_id FROM visit WHERE patient_id = " + patientId;
                if (found(query)) {
                    count = true;
                } 
                else {
                    query = "SELECT DISTINCT patient_id FROM lab WHERE patient_id = " + patientId ;
                    if (found(query)) {
                        count = true;
                    } 
                    else {
                        if (dateStarted != null) {
                            rs = getLastRefillVisit(patientId);
                            while (rs != null && rs.next()) {
                                Date dateLastRefill = rs.getDate("date_visit");
                                int duration = rs.getInt("duration");
                                int monthRefill = duration / 30;
                                if (monthRefill <= 0) {
                                    monthRefill = 1;
                                }

                                if (dateLastRefill != null) {
                                    //If the last refill date plus refill duration plus 90 days is before the last day of the reporting month this patient is LTFU     if(DateUtil.addYearMonthDay(lastRefill, duration+90, "day(s)").before(reportingDateEnd)) 
                               if (DateUtil.addYearMonthDay(dateLastRefill, duration + Constants.Reports.LTFU_PEPFAR, "DAY").after(DateUtil.getLastDateOfMonth(reportingYear, reportingMonth))) {
                                        count = true;
                                    }
                                }
                            }
                        }
                    }
                }

                if (count) {
                    disaggregate(gender, age);
                    if (gender.trim().equalsIgnoreCase("Female")) {
                        //check if client is pregnant or breast feeding during enrolment
                        query = "SELECT pregnant, breastfeeding FROM preg WHERE patient_id = " + patientId + " ORDER BY date_visit DESC LIMIT 1";
                        rs = executeQuery(query);
                        if (rs.next()) {
                            pregnant = rs.getInt("pregnant");
                            breastfeeding = rs.getInt("breastfeeding");
                        }
                        if (pregnant == 1) {
                            preg++;
                        } else {
                            if (breastfeeding == 1) {
                                feeding++;
                            }
                        }
                    }
                }
            }
            //Populate the report parameter map with values computed for ART 2
            
            dataElementId = 6;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+
            
            dataElementId = 7;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, preg);         


            System.out.println("Computing ART3.....");
            //ART 3
            //Total number of people living with HIV newly started on ART during the month (excludes ART transfer-in)
            initVariables();

            executeUpdate("DROP INDEX IF EXISTS idx_preg");        
            executeUpdate("DROP TABLE IF EXISTS preg");        
            executeUpdate("CREATE TEMPORARY TABLE preg AS SELECT * FROM clinic WHERE facility_id = " + facilityId + " AND commence = 1");
            executeUpdate("CREATE INDEX idx_preg ON preg(patient_id)");

            query = "SELECT DISTINCT patient_id, gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age FROM patient WHERE facility_id = " + facilityId + " AND YEAR(date_started) = " + reportingYear + " AND MONTH(date_started) = " + reportingMonth + " AND status_registration != 'ART Transfer In'";
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");

                int pregnant = 0;
                int breastfeeding = 0;

                disaggregate(gender, age);
                
                if (gender.trim().equalsIgnoreCase("Female")) {
                    //check if client is pregnant or breast feeding during visit
                    query = "SELECT pregnant, breastfeeding FROM preg WHERE patient_id = " + patientId + " ORDER BY date_visit DESC LIMIT 1";
                    rs = executeQuery(query);
                    if (rs.next()) {
                        pregnant = rs.getInt("pregnant");
                        breastfeeding = rs.getInt("breastfeeding");
                    }
                    if (pregnant == 1) {
                        preg++;
                    } else {
                        if (breastfeeding == 1) {
                            feeding++;
                        }
                    }
                }
            }
            //Populate the report parameter map with values computed for ART 3
            dataElementId = 8;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+
            
            dataElementId = 9;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, preg);         

            dataElementId = 10;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, feeding);         


            System.out.println("Computing ART4.....");
            //ART 4
            //Total number of people living with HIV who are currently receiving ART during the month (All regimen)
            initVariables();

            executeUpdate("DROP INDEX IF EXISTS idx_preg");        
            executeUpdate("DROP TABLE IF EXISTS preg");        
            executeUpdate("CREATE TEMPORARY TABLE preg AS SELECT * FROM clinic WHERE facility_id = " + facilityId + " AND date_visit >=  DATEADD('MONTH', -9, '" + reportingDateBegin + "') AND date_visit <= '" + reportingDateEnd + "'");
            executeUpdate("CREATE INDEX idx_preg ON preg(patient_id)");

            query = "SELECT DISTINCT patient_id, gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age FROM patient WHERE facility_id = " + facilityId + " AND current_status IN (" + Constants.ClientStatus.ON_TREATMENT + ") AND DATEDIFF(DAY, date_last_refill + last_refill_duration, CURDATE()) <= " + Constants.Reports.LTFU_PEPFAR + " AND date_started IS NOT NULL ORDER BY current_status"; 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");

                int pregnant = 0;
                int breastfeeding = 0;
                rs = getLastRefillVisit(patientId);
                while (rs != null && rs.next()) {
                    Date dateLastRefill = rs.getDate("date_visit");
                    int duration = rs.getInt("duration");
                    int monthRefill = duration / 30;
                    if (monthRefill <= 0) {
                        monthRefill = 1;
                    }

                    if (dateLastRefill != null) {
                        //If the last refill date plus refill duration plus 90 days is before the last day of the reporting month this patient is LTFU     if(DateUtil.addYearMonthDay(lastRefill, duration+90, "day(s)").before(reportingDateEnd)) 
                        if(!DateUtil.addYearMonthDay(dateLastRefill, duration+Constants.Reports.LTFU_PEPFAR, "DAY").after(DateUtil.getLastDateOfMonth(reportingYear, reportingMonth))) {
                            disaggregate(gender, age);

                            long regimentypeId = rs.getLong("regimentype_id");
                            if (gender.trim().equalsIgnoreCase("Male")) {
                                if (age < 15) {
                                    if (regimentypeId == 1 || regimentypeId == 3) {
                                        agem13_1++;
                                    } else {
                                        if (regimentypeId == 2 || regimentypeId == 4) {
                                            agem14_1++;
                                        } else {
                                            agem15_1++;
                                        }
                                    }
                                } else {
                                    if (regimentypeId == 1 || regimentypeId == 3) {
                                        agem13_2++;
                                    } else {
                                        if (regimentypeId == 2 || regimentypeId == 4) {
                                            agem14_2++;
                                        } else {
                                            agem15_2++;
                                        }
                                    }
                                }
                            } else {
                                if (age < 15) {
                                    if (regimentypeId == 1 || regimentypeId == 3) {
                                        agef13_1++;
                                    } else {
                                        if (regimentypeId == 2 || regimentypeId == 4) {
                                            agef14_1++;
                                        } else {
                                            agef15_1++;
                                        }
                                    }
                                } else {
                                    if (regimentypeId == 1 || regimentypeId == 3) {
                                        agef13_2++;
                                    } else {
                                        if (regimentypeId == 2 || regimentypeId == 4) {
                                            agef14_2++;
                                        } else {
                                            agef15_2++;
                                        }
                                    }
                                }
                            }

                            if (gender.trim().equalsIgnoreCase("Female")) {
                                //check if client is pregnant or breast feeding during enrolment
                                query = "SELECT pregnant, breastfeeding FROM preg WHERE patient_id = " + patientId + " ORDER BY date_visit DESC LIMIT 1";
                                ResultSet rs1 = executeQuery(query);
                                if (rs1.next()) {
                                    pregnant = rs1.getInt("pregnant");
                                    breastfeeding = rs1.getInt("breastfeeding");
                                }

                                if (pregnant == 1) {
                                    preg++;
                                } else {
                                    if (breastfeeding == 1) {
                                        feeding++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //Populate the report parameter map with values computed for ART 4
            dataElementId = 11;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+
            
            dataElementId = 12;
            populateIndicatorValue(dataElementId, 27, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem13_1);         

            dataElementId = 12;
            populateIndicatorValue(dataElementId, 28, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem13_2);    

            dataElementId = 12;
            populateIndicatorValue(dataElementId, 25, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef13_1);         

            dataElementId = 12;
            populateIndicatorValue(dataElementId, 26, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef13_2);    
 
            
            
            dataElementId = 13;
            populateIndicatorValue(dataElementId, 27, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem14_1);         

            dataElementId = 13;
            populateIndicatorValue(dataElementId, 28, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem14_2);    

            dataElementId = 13;
            populateIndicatorValue(dataElementId, 25, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef14_1);         

            dataElementId = 13;
            populateIndicatorValue(dataElementId, 26, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef14_2);    
 

            dataElementId = 14;
            populateIndicatorValue(dataElementId, 27, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem15_1);         

            dataElementId = 14;
            populateIndicatorValue(dataElementId, 28, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem15_2);    

            dataElementId = 14;
            populateIndicatorValue(dataElementId, 25, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef15_1);         

            dataElementId = 14;
            populateIndicatorValue(dataElementId, 26, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef15_2);    


            dataElementId = 15;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, preg);         

            dataElementId = 16;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, feeding);    

            
            System.out.println("Computing ART5.....");
            //ART 5
            //Number of people living with HIV and on ART with a viral load test result during the month
            initVariables();

            executeUpdate("DROP INDEX IF EXISTS idx_lab");        
            executeUpdate("DROP TABLE IF EXISTS lab");        
            executeUpdate("CREATE TEMPORARY TABLE lab AS SELECT * FROM laboratory WHERE facility_id = " + facilityId + " AND YEAR(date_reported) = " + reportingYear + " AND MONTH(date_reported) = " + reportingMonth + " AND labtest_id = 16");
            executeUpdate("CREATE INDEX idx_lab ON lab(patient_id)");

            query = "SELECT DISTINCT patient_id, gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age FROM patient WHERE facility_id = " + facilityId + " AND date_registration <= '" + reportingDateEnd + "' AND date_started IS NOT NULL AND DATEDIFF(MONTH, date_started, '" + reportingDateEnd + "') >= 6 AND date_started <= '" + reportingDateEnd + "'";
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");

                //Check for viral load this reporting month
                query = "SELECT patient_id FROM lab WHERE patient_id = " + patientId + " ORDER BY date_reported DESC LIMIT 1";
                preparedStatement = jdbcUtil.getStatement(query);
                rs = preparedStatement.executeQuery();
                if (rs.next()) {
                    disaggregate(gender, age);
                }
            }
            //Populate the report parameter map with values computed for ART 5
            dataElementId = 17;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+

            System.out.println("Computing ART6.....");
            //ART 6
            //Number of people living with HIV and on ART who are virologically suppressed (viral load < 1000 c/ml) during the month
            initVariables();

            executeUpdate("DROP INDEX IF EXISTS idx_lab");        
            executeUpdate("DROP TABLE IF EXISTS lab");        
            executeUpdate("CREATE TEMPORARY TABLE lab AS SELECT * FROM laboratory WHERE facility_id = " + facilityId + " AND YEAR(date_reported) = " + reportingYear + " AND MONTH(date_reported) = " + reportingMonth + " AND labtest_id = 16");
            executeUpdate("CREATE INDEX idx_lab ON lab(patient_id)");

            query = "SELECT DISTINCT patient_id, gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age FROM patient WHERE facility_id = " + facilityId + " AND date_registration <= '" + reportingDateEnd + "' AND date_started IS NOT NULL AND DATEDIFF(MONTH, date_started, '" + reportingDateEnd + "') >= 6 AND date_started <= '" + reportingDateEnd + "'";
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");

                //Check if the last viral load before the reporting month is less than 1000
                query = "SELECT * FROM lab WHERE patient_id = " + patientId + " ORDER BY date_reported DESC LIMIT 1";
                preparedStatement = jdbcUtil.getStatement(query);
                rs = preparedStatement.executeQuery();
                if (rs.next()) {
                    String resultab = rs.getString("resultab");
                    if (isInteger(resultab)) {
                        if (Double.valueOf(resultab) < 1000) {
                            disaggregate(gender, age);
                        }
                    }
                }
            }
            //Populate the report parameter map with values computed for ART 6
            dataElementId = 18;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+
           
            
            initVariables();
            while (resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");

                
            //Number VL test unsuppressed during the reporting period who attended EAC
            query = "SELECT COUNT(DISTINCT patient_id) FROM eac WHERE patient_id IN (SELECT DISTINCT patient_id FROM lab WHERE labtest_id = 16 AND result >= 1000 AND MONTH(date_reported) = " + month + " AND YEAR(date_reported) = " + year + " AND (date_eac1 >= " + reportingDateBegin + " OR date_eac2 >= " + reportingDateBegin + " OR date_eac3 >= " + reportingDateBegin + "))"; 
                
                //Check if the last viral load before the reporting month is less than 1000
                query = "SELECT * FROM lab WHERE patient_id = " + patientId + " ORDER BY date_reported DESC LIMIT 1";
                preparedStatement = jdbcUtil.getStatement(query);
                rs = preparedStatement.executeQuery();
                if (rs.next()) {
                    String resultab = rs.getString("resultab");
                    if (isInteger(resultab)) {
                        if (Double.valueOf(resultab) >= 1000) {
                            disaggregate(gender, age);
                        }
                    }
                }              
            }            
            //Populate the indicatior value table with virallly unsuppressed  
            dataElementId = 42;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+

            initVariables();
            while (resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                
                //Check if the last viral load before the reporting month is less than 1000
                query = "SELECT * FROM lab WHERE patient_id = " + patientId + " ORDER BY date_reported DESC LIMIT 1";
                preparedStatement = jdbcUtil.getStatement(query);
                rs = preparedStatement.executeQuery();
                if (rs.next()) {
                   String resultab = rs.getString("resultab");
                   
                    if (isInteger(resultab)) {
                        if (Double.valueOf(resultab) >= 1000) {
                            String dateReported = DateUtil.parseDateToString(rs.getDate("date_reported"), "yyyy-MM-dd");
                            
                            Date date = DateUtil.addDay(rs.getDate("date_reported"), 30);                           
                            String dateEnd = DateUtil.parseDateToString(date, "yyyy-MM-dd");

                            //Number VL test unsuppressed during the reporting period who attended EAC
                            query = "SELECT patient_id FROM eac WHERE patient_id = " + patientId + " AND  ((date_eac1 >= " + dateReported  + "  AND date_eac1 <=  " + dateEnd + ") OR (date_eac2 >= " + dateReported  + "  AND date_eac2 <=  " + dateEnd + ")   OR (date_eac3 >= " + dateReported  + "  AND date_eac3 <=  " + dateEnd + "))"; 
                            preparedStatement = jdbcUtil.getStatement(query);
                            ResultSet rs1 = preparedStatement.executeQuery();
                            if(rs1.next()) {
                                disaggregate(gender, age);                               
                            }

                        }
                    }
                }              
            }            
            //Populate the report parameter map with values computed for Eligible for VL
            dataElementId = 43;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+

            
            System.out.println("Computing ART7.....");
            //ART 7
            //Total number of people living with HIV known to have died during the month
            initVariables();
            query = "SELECT DISTINCT gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age FROM patient WHERE facility_id = " + facilityId + " AND current_status = 'Known Death' AND YEAR(date_current_status) = " + reportingYear + " AND MONTH(date_current_status) = " + reportingMonth;
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                if (gender.trim().equalsIgnoreCase("Male")) {
                    agem1++;
                } else {
                    agef1++;
                }
            }
 
            System.out.println("Computing ART8.....");
            //ART 8
            //Number of People living with HIV who are lost to follow up during the month
            initVariables();
            query = "SELECT DISTINCT gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age FROM patient WHERE patient_id IN (SELECT DISTINCT patient_id FROM statushistory WHERE facility_id = " + facilityId + " AND current_status = 'Lost to Follow Up' AND YEAR(date_current_status) = " + reportingYear + " AND MONTH(date_current_status) = " + reportingMonth + ")";
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                if (gender.trim().equalsIgnoreCase("Male")) {
                    agem1++;
                } else {
                    agef1++;
                }
            }
            
            dataElementId = 19;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female 

            //Indicattor  41
            //Number of patients eligible for viral load test during the reporting month
            System.out.println(".....Compute eligible for viral load test");
            initVariables();
                        
            query = "SELECT DISTINCT gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age FROM patient WHERE facility_id = " + facilityId + "  AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') AND MONTH(viral_load_due_date) = " + month + " AND YEAR(viral_load_due_date) = " + year; 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                disaggregate(gender, age);
            }
            //Populate the report parameter map with values computed for Eligible for VL
            dataElementId = 41;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+
 
                      System.out.println("......Computing TB documentation");

            //Clinic visit with documented TB status
             //denominator - all clinic visits during the reporting month
            query = "SELECT COUNT(*) AS count FROM visit WHERE MONTH(date_visit) =  " + month + " AND YEAR(date_visit) = " + year; 
            dataElementId = 44;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, getCount(query));        //Total clinic visti
            
            //numerator - all clinic visit during the reporting month with TB status not equal to null 
            query = "SELECT COUNT(*) AS count FROM visit WHERE MONTH(date_visit) =  " + month + " AND YEAR(date_visit) = " + year + " AND tb_status != '' AND tb_status IS NOT NULL"; 
            dataElementId = 45;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, getCount(query));        //Total clinic visti
           
 
            
            executeUpdate("DROP TABLE IF EXISTS appoint");
            query = "CREATE TEMPORARY TABLE appoint AS SELECT ph.patient_id, ph.date_visit, ph.next_appointment, pa.gender, DATEDIFF(YEAR, pa.date_birth, '" + reportingDateBegin + "') AS age FROM pharm ph, patient pa WHERE (ph.patient_id = pa.patient_id) AND (MONTH(ph.next_appointment) = " + month + " AND YEAR(ph.next_appointment) = " + year + ")"; 
            executeUpdate(query);
            
            //denominator - all refill appointment during the repiorting month
            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM appoint";
            dataElementId = 46;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId,  getCount(query));        //Total refill appointtment
 
            query = "SELECT COUNT(DISTINCT patient_id) AS count FROM appoint WHERE patient_id NOT IN (SELECT patient_id FROM pharm WHERE MONTH(date_visit) =  " + month + " AND YEAR(date_visit) = " + year +")"; 
            
            dataElementId = 47;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId,  getCount(query));        //Total missed appointment
            
            //Missed appointment
           executeUpdate("DROP TABLE IF EXISTS missed");
           query = "CREATE TEMPORARY TABLE missed AS SELECT patient_id, date_visit, next_appointment, gender, age FROM appoint WHERE patient_id NOT IN (SELECT patient_id FROM pharm WHERE MONTH(date_visit) =  " + month + " AND YEAR(date_visit) = " + year +")";
           executeUpdate(query);
            
           System.out.println("......Computing number of patient who returned into care");
            query = "SELECT * FROM missed";            
            preparedStatement = jdbcUtil.getStatement(query);
            rs  = preparedStatement.executeQuery();
            while (rs.next()) {
                long patientId = rs.getLong("patient_id");
                String gender = rs.getString("gender");
                int age = rs.getInt("age");
                
                
                String nextAppointment = DateUtil.parseDateToString(rs.getDate("next_appointment"), "yyyy-MM-dd");

                Date date = DateUtil.addDay(rs.getDate("next_appointment"), 30);                           
                String dateEnd = DateUtil.parseDateToString(date, "yyyy-MM-dd");
                
                System.out.println(".... Next Appointment Database value:  " + rs.getDate("next_appointment"));
                System.out.println(".... Next Appointment " + nextAppointment);
                
                //Number  of patients who missed apointment and returned to care
                query = "SELECT patient_id FROM pharm WHERE patient_id = " + patientId + " AND  (date_visit >= " + nextAppointment  + "  AND date_visit <=  " + dateEnd + ")"; 
                preparedStatement = jdbcUtil.getStatement(query);
                ResultSet rs1 = preparedStatement.executeQuery();
                
                System.out.println(".... Resultset query successful ");
                
                if(rs1.next()) {
                    disaggregate(gender, age);                               
                }
            }
          
          System.out.println("......Computing number of patient who returned into care2");

            //Populate number of patient who returned into care
            dataElementId = 48;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+
                        
            //Number of people living with HIV newly initiated on/transitioned to TLD during the month
           query = "SELECT DISTINCT patient_id, gender, DATEDIFF(YEAR, date_birth, '" + reportingDateBegin + "') AS age FROM patient WHERE facility_id = " + facilityId + " AND current_status IN (" + Constants.ClientStatus.ON_TREATMENT + ") AND DATEDIFF(DAY, date_last_refill + last_refill_duration, CURDATE()) <= " + Constants.Reports.LTFU_PEPFAR + " AND date_started IS NOT NULL ORDER BY current_status"; 
           preparedStatement = jdbcUtil.getStatement(query);
           resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");

                rs = getLastRefillVisit(patientId);
                while (rs != null && rs.next()) {
                    long regimenId = rs.getLong("regimen_id");
                    Date dateLastRefill = rs.getDate("date_visit");
                    int duration = rs.getInt("duration");
                    int monthRefill = duration / 30;
                    if (monthRefill <= 0) {
                        monthRefill = 1;
                    }

                    if (dateLastRefill != null) {
                        //If the last refill date plus refill duration plus 28 days is before the last day of the reporting month this patient is LTFU     if(DateUtil.addYearMonthDay(lastRefill, duration+90, "day(s)").before(reportingDateEnd
                         if(!DateUtil.addYearMonthDay(dateLastRefill, duration+Constants.Reports.LTFU_PEPFAR, "DAY").after(DateUtil.getLastDateOfMonth(reportingYear, reportingMonth))) {
                           
                             if(regimenId >= 116 && regimenId <= 119) {                                 
                                 disaggregate(gender, age);
                             }
                        }
                    }
                }
            }
            dataElementId = 49;
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+
           
            
            int mms = 0;
            int mmd = 0;
            int carc = 0;
            int cparp = 0;
            
            query = "SELECT DISTINCT patient.patient_id, patient.gender, DATEDIFF(YEAR, patient.date_birth, '" + reportingDateBegin + "') AS age, devolve.type_dmoc FROM patient JOIN devolve ON patient.patient_id = devolve.patient_id WHERE MONTH(devolve.date_devolved) = " + month + " AND YEAR(devolve.date_devolved) = " + year ; 
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long patientId = resultSet.getLong("patient_id");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                String typeDmoc = resultSet.getString("type_dmoc");
                
                if(typeDmoc.equalsIgnoreCase("MMS")) mms++;
                if(typeDmoc.equalsIgnoreCase("MMD")) mmd++;
                if(typeDmoc.equalsIgnoreCase("CARC")) carc++;
                if(typeDmoc.equalsIgnoreCase("CPARP")) cparp++;
                
                disaggregate(gender, age);
            }
            
            //Populate the report parameter map with values computed for Eligible for VL
            
            dataElementId = 50;
            
            populateIndicatorValue(dataElementId, 13, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem1);         // male <1   
            populateIndicatorValue(dataElementId, 1, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef1);        //female <1

            populateIndicatorValue(dataElementId, 14, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem2);         // male 1-4  
            populateIndicatorValue(dataElementId, 2, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef2);        //female 1-4
           
            populateIndicatorValue(dataElementId, 15, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem3);         // male  5-9
            populateIndicatorValue(dataElementId, 3, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef3);        //female 5-9
           
            populateIndicatorValue(dataElementId, 16, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem4);         // male 10-14
            populateIndicatorValue(dataElementId, 4, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef4);        //female 10-14

            populateIndicatorValue(dataElementId, 17, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem5);         // male 15-19
            populateIndicatorValue(dataElementId, 5, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef5);        //female 15-19

            populateIndicatorValue(dataElementId, 18, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem6);         // male 20-24
            populateIndicatorValue(dataElementId, 6, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef6);        //female 20-24

            populateIndicatorValue(dataElementId, 19, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem7);         // male 25-29
            populateIndicatorValue(dataElementId, 7, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef7);        //female 25-29
            
            populateIndicatorValue(dataElementId, 20, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem8);         // male 30-34
            populateIndicatorValue(dataElementId, 8, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef8);        //female 30-34
            
            populateIndicatorValue(dataElementId, 21, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem9);         // male 35-39
            populateIndicatorValue(dataElementId, 9, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef9);        //female 35-39
            
            populateIndicatorValue(dataElementId, 22, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem10);         // male 40-44
            populateIndicatorValue(dataElementId, 10, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef10);        //female 40-44
            
            populateIndicatorValue(dataElementId, 23, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem11);         // male 45-49
            populateIndicatorValue(dataElementId, 11, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef11);        //female 45-49
                        
            populateIndicatorValue(dataElementId, 24, reportingMonth, reportingYear, stateId, lgaId, facilityId, agem12);         // male 50+
            populateIndicatorValue(dataElementId, 12, reportingMonth, reportingYear, stateId, lgaId, facilityId, agef12);        //female 50+
            
            
             dataElementId = 51;
            
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, mms);         // mms
            
             dataElementId = 52;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, mmd);        //mmd

             dataElementId = 53;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, carc);         // carc
            
             dataElementId = 54;
            populateIndicatorValue(dataElementId, 0, reportingMonth, reportingYear, stateId, lgaId, facilityId, cparp);        //cparp

            
            System.out.println("Completed");
            
            
        } catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }

    private void disaggregate(String gender, int age) {
        if (gender.trim().equalsIgnoreCase("Male")) {
            if (age < 1) {
                agem1++;
            } else {
                if (age >= 1 && age <= 4) {
                    agem2++;
                } else {
                    if (age >= 5 && age <= 9) {
                        agem3++;
                    } else {
                        if (age >= 10 && age <= 14) {
                            agem4++;
                        } else {
                            if (age >= 15 && age <= 19) {
                                agem5++;
                            } else {
                                if (age >= 20 && age <= 24) {
                                    agem6++;
                                } else {
                                    if (age >= 25 && age <= 29) {
                                        agem7++;
                                    } else {
                                        if (age >= 30 && age <= 34) {
                                            agem8++;
                                        } else {
                                            if (age >= 35 && age <= 39) {
                                                agem9++;
                                            } else {
                                                if (age >= 40 && age <= 44) {
                                                    agem10++;
                                                } else {
                                                    if (age >= 45 && age <= 49) {
                                                        agem11++;
                                                    } else {
                                                        if (age >= 50) {
                                                            agem12++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (age < 1) {
                agef1++;
            } else {
                if (age >= 1 && age <= 4) {
                    agef2++;
                } else {
                    if (age >= 5 && age <= 9) {
                        agef3++;
                    } else {
                        if (age >= 10 && age <= 14) {
                            agef4++;
                        } else {
                            if (age >= 15 && age <= 19) {
                                agef5++;
                            } else {
                                if (age >= 20 && age <= 24) {
                                    agef6++;
                                } else {
                                    if (age >= 25 && age <= 29) {
                                        agef7++;
                                    } else {
                                        if (age >= 30 && age <= 34) {
                                            agef8++;
                                        } else {
                                            if (age >= 35 && age <= 39) {
                                                agef9++;
                                            } else {
                                                if (age >= 40 && age <= 44) {
                                                    agef10++;
                                                } else {
                                                    if (age >= 45 && age <= 49) {
                                                        agef11++;
                                                    } else {
                                                        if (age >= 50) {
                                                            agef12++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private ResultSet executeQuery(String query) {
        ResultSet rs = null;
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return rs;
    }

    private void executeUpdate(String query) {
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }

    private boolean found(String query) {
        boolean found = false;
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                found = true;
            }
        } catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return found;
    }

    private ResultSet getLastRefillVisit(long patientId) {
        ResultSet rs = null;
        try {
            query = "SELECT DISTINCT regimentype_id, regimen_id, date_visit, duration FROM pharm WHERE patient_id = " + patientId + " ORDER BY date_visit DESC LIMIT 1";  
            preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
        return rs;        
    }

    private void initVariables() {
        agem1 = 0;
        agem2 = 0;
        agem3 = 0;
        agem4 = 0;
        agem5 = 0;
        agem6 = 0;
        agem7 = 0;
        agem8 = 0;
        agem9 = 0;
        agem10 = 0;
        agem11 = 0;
        agem12 = 0;

        agef1 = 0;
        agef2 = 0;
        agef3 = 0;
        agef4 = 0;
        agef5 = 0;
        agef6 = 0;
        agef7 = 0;
        agef8 = 0;
        agef9 = 0;
        agef10 = 0;
        agef11 = 0;
        agef12 = 0;

        agem13_1 = 0;
        agem14_1 = 0;
        agem15_1 = 0;
        agem13_2 = 0;
        agem14_2 = 0;
        agem15_2 = 0;
        agef13_1 = 0;
        agef14_1 = 0;
        agef15_1 = 0;
        agef13_2 = 0;
        agef14_2 = 0;
        agef15_2 = 0;
        preg = 0;
        feeding = 0;
        tbm = 0;
        tbf = 0;
    }
    
    private void getStateId(){
        System.out.println(".....facility ID:"+facilityId);
       try{ 
          query = "SELECT state_id, lga_id FROM facility  WHERE facility_id = " + facilityId;
          preparedStatement = jdbcUtil.getStatement(query);
           ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
               stateId = rs.getLong("state_id");
               lgaId = rs.getLong("lga_id");
            }
        } catch (Exception exception) {
            resultSet = null;
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }
    }

    private void populateIndicatorValue(int dataElementId, int categoryId, int month, int year, long stateId, long lgaId, long facilityId, int value) {
            Indicatorvalue indicator = new Indicatorvalue();
           
            indicator.setDataElementId(dataElementId);
            indicator.setCategoryId(categoryId);
            indicator.setMonth(month);
            indicator.setYear(year);
            indicator.setStateId(stateId);
            indicator.setLgaId(lgaId);
            indicator.setFacilityId(facilityId);
            indicator.setValue(value);
           
           
            long indicatorId = getIndicatorId(dataElementId, categoryId, facilityId, month, year);
            if(indicatorId == 0) {
                IndicatorDAO.save(indicator);
            }
            else {
                // indicator.setIndicatord(indicatorId);
                IndicatorDAO.update(indicator);
            }
        
        
    }
    
    //Get indicatorvalueId
    private long getIndicatorId(int dataElementId, int categoryId, long facilityId, int month, int year) {
        String query = "SELECT indicatorvalue_id FROM indicatorvalue WHERE data_element_id = " + dataElementId + "  AND category_id = " + categoryId + " AND facility_id = " + facilityId + " AND month = " + month + " AND year = " + year ; 
        long id  = 0;
        try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                id = rs.getLong("indicatorvalue_id");               
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return id;
    }
 
    
    private int getCount(String query) {
       int count  = 0;
       try {
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                count = rs.getInt("count");               
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        return count;
    }
    
}

//create a temporary table of date of the latest status change on or before the last day of reporting month 
//executeUpdate("DROP TABLE IF EXISTS currentstatus");        
//query = "CREATE TEMPORARY TABLE currentstatus AS SELECT DISTINCT patient_id, MAX(date_current_status) AS date_status FROM statushistory WHERE facility_id = " + facilityId + " AND date_current_status <= '" + reportingDateEnd + "' GROUP BY patient_id";
//preparedStatement = jdbcUtil.getStatement(query);
//preparedStatement.executeUpdate();
//query = "SELECT DISTINCT patient.patient_id, patient.gender, DATEDIFF(YEAR, patient.date_birth, '" + reportingDateBegin + "') AS age, patient.date_registration, patient.status_registration, patient.date_started, statushistory.current_status, currentstatus.date_status "
//+ " FROM patient JOIN statushistory ON patient.patient_id = statushistory.patient_id JOIN currentstatus ON patient.patient_id = currentstatus.patient_id WHERE patient.facility_id = " + facilityId + " AND statushistory.facility_id = " + facilityId + " AND statushistory.patient_id = currentstatus.patient_id AND statushistory.date_current_status = currentstatus.date_status"; 
// create a temporary table of date of the latest regimen change on or before the last day of reporting month 
//executeUpdate("DROP TABLE IF EXISTS currentregimen");
//query = "CREATE TEMPORARY TABLE currentregimen AS SELECT DISTINCT patient_id, MAX(date_visit) AS date_visit FROM regimenhistory WHERE facility_id = " + facilityId + " AND date_visit <= '" + reportingDateEnd + "' GROUP BY patient_id";
//preparedStatement = jdbcUtil.getStatement(query);
//preparedStatement.executeUpdate();
//query = "SELECT DISTINCT patient.patient_id, patient.gender, DATEDIFF(YEAR, patient.date_birth, '" + reportingDateBegin + "') AS age, patient.date_registration, patient.date_started, regimenhistory.regimentype, currentregimen.date_visit "
//+ " FROM patient JOIN regimenhistory ON patient.patient_id = regimenhistory.patient_id JOIN currentregimen ON patient.patient_id = currentregimen.patient_id WHERE patient.facility_id = " + facilityId + " AND regimenhistory.facility_id = " + facilityId + " AND regimenhistory.patient_id = currentregimen.patient_id AND regimenhistory.date_visit = currentregimen.date_visit"; 
//preparedStatement = jdbcUtil.getStatement(query);
//query = "SELECT patient_id FROM clinic WHERE facility_id = " + facilityId + " AND date_visit BETWEEN DATEADD('MONTH', -3, '" + reportingDateEnd + "') AND " + reportingDateEnd + " AND pregnant = 1";


//"Pre-ART Transfer Out", "ART Transfer Out", "Stopped Treatment", "Lost to Follow Up", "Known Death", 
    

