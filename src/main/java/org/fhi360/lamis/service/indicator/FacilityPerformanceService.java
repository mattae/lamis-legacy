/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.indicator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.PropertyAccessor;

/**
 *
 * @author user10
 */
public class FacilityPerformanceService {
    private long stateId;
    private long lgaId;
    private long facilityId;
    private int month;
    private int year;

    private JDBCUtil jdbcUtil;
    private String query;
    
    public void analyze() {
        String databaseName = new PropertyAccessor().getDatabaseName();
        try {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        year =  2017; // cal.get(Calendar.YEAR);
        month = 12; //cal.get(Calendar.MONTH);
            
            
            Map<String, Object> map = new PropertyAccessor().getSystemProperties();
            String appInstance = (String) map.get("appInstance");
            if(appInstance.equalsIgnoreCase("server")) {
                //Select all facilities that has uploaded data in the last 3 days and run their performance analysis
                //query = "SELECT DISTINCT facility_id FROM patient WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE()) UNION SELECT DISTINCT facility_id FROM clinic WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE()) UNION SELECT DISTINCT facility_id FROM pharmacy WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE()) UNION SELECT DISTINCT facility_id FROM laboratory WHERE MONTH(time_stamp) = MONTH(CURDATE()) AND YEAR(time_stamp) = YEAR(CURDATE())";
                query = "SELECT DISTINCT facility_id FROM patient";
                //query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -7 DAY) AND CURDATE()";
                //if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATEADD('DAY', -7, CURDATE()) AND CURDATE()";
            }
            else {  
                query = "SELECT DISTINCT facility_id FROM patient";
//                query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 DAY) AND CURDATE()";
                //if(databaseName.equalsIgnoreCase("h2")) query = "SELECT DISTINCT facility_id FROM patient WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM clinic WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM pharmacy WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM laboratory WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE() UNION SELECT DISTINCT facility_id FROM statushistory WHERE time_stamp BETWEEN DATEADD('DAY', -1, CURDATE()) AND CURDATE()";
            }
            jdbcUtil = new JDBCUtil();
            PreparedStatement preparedStatement = jdbcUtil.getStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
           while (resultSet.next()) { 
                System.out.println("Analyzing....."+resultSet.getLong("facility_id"));
                facilityId = resultSet.getLong("facility_id");
                new ArtIndicatorService().process(facilityId, month, year);
            }
            new PropertyAccessor().setDateLastAsyncTask(new Date());
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
    }    
}
