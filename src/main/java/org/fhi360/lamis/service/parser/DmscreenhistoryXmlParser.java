/**
 *
 * @author aalozie
 */
package org.fhi360.lamis.service.parser;

import org.fhi360.lamis.model.Patient;
import org.fhi360.lamis.service.EntityIdentifier;
import org.fhi360.lamis.utility.PatientNumberNormalizer;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.fhi360.lamis.dao.hibernate.DmscreenhistoryDAO;
import org.fhi360.lamis.model.Dmscreenhistory;
import org.fhi360.lamis.utility.DateUtil;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class DmscreenhistoryXmlParser extends DefaultHandler {

    private long facilityId;
    private String hospitalNum;
    private String dateVisit;
    private long idOnServer;
    private boolean populated;
    private Boolean skipRecord;
    private Dmscreenhistory dmscreenhistory;
    private Patient patient = new Patient();
    private EntityIdentifier entityIdentifier = new EntityIdentifier();

    ;

    public void parseXml(String xmlFileName) {
        populated = false;
        skipRecord = false;
        try {
            //obtain and configure a SAX based parser
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

            //obtain object for SAX hadler class
            SAXParser saxParser = saxParserFactory.newSAXParser();

            //default handler for SAX handler class
            // all three methods are written in handler's body
            DefaultHandler defaultHandler = new DefaultHandler() {
                String dmscreenhistoryTag = "close";
                String historyIdTag = "close";
                String patientIdTag = "close";
                String facilityIdTag = "close";
                String hospitalNumTag = "close";
                String dateVisitTag = "close";
                String descriptionTag = "close";
                String timeStampTag = "close";
                String idUUIDTag = "close";

                //this method is called every time the parser gets an open tag '<'
                //identifies which tag is being open at the time by assigning an open flag
                @Override
                public void startElement(String uri, String localName, String element, Attributes attributes) throws SAXException {
                    if (element.equalsIgnoreCase("dmscreenhistory")) {
                        dmscreenhistoryTag = "open";
                        dmscreenhistory = new Dmscreenhistory();
                    }
                    if (element.equalsIgnoreCase("history_id")) {
                        historyIdTag = "open";
                    }
                    if (element.equalsIgnoreCase("patient_id")) {
                        patientIdTag = "open";
                    }
                    if (element.equalsIgnoreCase("facility_id")) {
                        facilityIdTag = "open";
                    }
                    if (element.equalsIgnoreCase("hospital_num")) {
                        hospitalNumTag = "open";
                    }
                    if (element.equalsIgnoreCase("date_visit")) {
                        dateVisitTag = "open";
                    }
                    if (element.equalsIgnoreCase("description")) {
                        descriptionTag = "open";
                    }
                    if (element.equalsIgnoreCase("time_stamp")) {
                        timeStampTag = "open";
                    }
                    if (element.equalsIgnoreCase("id_uuid")) {
                        idUUIDTag = "open";
                    }
                }

                //store data store in between '<' and '>' tags                     
                @Override
                public void characters(char[] chars, int start, int length) throws SAXException {
                    if (facilityIdTag.equals("open")) {
                        facilityId = Long.parseLong(new String(chars, start, length));
                        dmscreenhistory.setFacilityId(facilityId);
                        if (!populated) {
                            entityIdentifier.createEntities(facilityId, "dmscreenhistory");
                            populated = true;
                        }
                    }
                    if (hospitalNumTag.equals("open")) {
                        hospitalNum = new String(chars, start, length);
                    }
                    if (dateVisitTag.equals("open")) {
                        dateVisit = new String(chars, start, length);
                        if (!dateVisit.trim().isEmpty()) {
                            dmscreenhistory.setDateVisit(DateUtil.parseStringToDate(dateVisit, "yyyy-MM-dd"));
                        } else {
                            skipRecord = true;
                        }
                    }
                    if (descriptionTag.equals("open")) {
                        dmscreenhistory.setDescription(new String(chars, start, length));
                    }
                    if (timeStampTag.equals("open")) {
                        dmscreenhistory.setTimeStamp(new java.sql.Date(new java.util.Date().getTime()));
                    }
                    if (idUUIDTag.equals("open")) {
                        dmscreenhistory.setIdUUID(new String(chars, start, length));
                    }
                }

                @Override
                public void endElement(String uri, String localName, String element) throws SAXException {
                    if (element.equalsIgnoreCase("history_id")) {
                        historyIdTag = "close";
                    }
                    if (element.equalsIgnoreCase("patient_id")) {
                        patientIdTag = "close";
                    }
                    if (element.equalsIgnoreCase("facility_id")) {
                        facilityIdTag = "close";
                    }
                    if (element.equalsIgnoreCase("hospital_num")) {
                        hospitalNumTag = "close";
                    }
                    if (element.equalsIgnoreCase("date_visit")) {
                        dateVisitTag = "close";
                    }
                    if (element.equalsIgnoreCase("description")) {
                        descriptionTag = "close";
                    }
                    if (element.equalsIgnoreCase("time_stamp")) {
                        timeStampTag = "close";
                    }
                    if (element.equalsIgnoreCase("id_uuid")) {
                        idUUIDTag = "close";
                    }

                    //if this is the closing tag of a adherehistory element save the record
                    if (element.equalsIgnoreCase("dmscreenhistory")) {
                        dmscreenhistoryTag = "close";
                        if (skipRecord) {
                            System.out.println("....record has a null value: " + hospitalNum);
                        } else {
                            String query = "SELECT id_on_server FROM entity WHERE TRIM(LEADING '0' FROM hospital_num) = '" + PatientNumberNormalizer.unpadNumber(hospitalNum) + "'";
                            idOnServer = entityIdentifier.getIdOnServer(query); //check if patient exist on the server
                            if (idOnServer != 0) {
                                patient.setPatientId(idOnServer);
                                dmscreenhistory.setPatient(patient);

                                query = "SELECT id_on_server FROM dependant WHERE TRIM(LEADING '0' FROM hospital_num) = '" + PatientNumberNormalizer.unpadNumber(hospitalNum) + "' AND date_visit = '" + dateVisit + "'";
                                idOnServer = entityIdentifier.getIdOnServer(query); //check if this record exist on the server
                                if (idOnServer == 0) {
                                    idOnServer = DmscreenhistoryDAO.save(dmscreenhistory);
                                } else {
                                    dmscreenhistory.setHistoryId(idOnServer);
                                    DmscreenhistoryDAO.update(dmscreenhistory);
                                }
                            }

                        }
                    }
                }
            };

            //parse the XML specified in the given path and uses supplied handler to parse the document
            //this calls startElement(), endElement(), and character() methods accordingly
            saxParser.parse(xmlFileName, defaultHandler);
            //new CleanupService().cleanNullFields("dmscreenhistory", facilityId);
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new RuntimeException(exception);
        }
    }

}
