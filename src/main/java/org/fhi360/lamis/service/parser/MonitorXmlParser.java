/**
 *
 * @author aalozie
 */

package org.fhi360.lamis.service.parser;

import org.fhi360.lamis.service.DeleteService;
import org.fhi360.lamis.service.EntityIdentifier;
import org.fhi360.lamis.utility.PatientNumberNormalizer;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.fhi360.lamis.interceptor.updater.PatientHospitalNumberUpdater;
import org.fhi360.lamis.utility.DateUtil;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MonitorXmlParser {
    private String[] entityId;
    private long facilityId;
    private long idOnServer;
    private String tableName;
    private int operationId;
    private boolean populated;
    private EntityIdentifier entityIdentifier = new EntityIdentifier(); 
    private DeleteService deleteService = new DeleteService();
    
    
    public void parseXml(String xmlFileName) {
        populated = false;
        try {
            //obtain and configure a SAX based parser
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

            //obtain object for SAX hadler class
            SAXParser saxParser = saxParserFactory.newSAXParser();

            //default handler for SAX handler class
            // all three methods are written in handler's body
            DefaultHandler defaultHandler = new DefaultHandler() {
                String monitorTag = "close";
                String facilityIdTag = "close";
                String entityIdTag = "close";
                String tableNameTag = "close";
                String operationIdTag = "close";
                
                //this method is called every time the parser gets an open tag '<'
                //identifies which element is being open at the time by assigning an open flag
                @Override
                public void startElement(String uri, String localName, String element, Attributes attributes) throws SAXException {
                    if (element.equalsIgnoreCase("monitor")) {
                        monitorTag = "open";
                    }
                    if (element.equalsIgnoreCase("facility_id")) {
                        facilityIdTag = "open";
                    }
                    if (element.equalsIgnoreCase("entity_id")) {
                        entityIdTag = "open";
                    }
                    if (element.equalsIgnoreCase("table_name")) {
                        tableNameTag = "open";
                    }
                    if (element.equalsIgnoreCase("operation_id")) {
                        operationIdTag = "open";
                    }
                }
                
                //store data store in between '<' and '>' tags                     
                @Override
                public void characters(char[] chars, int start, int length) throws SAXException {
                    if (facilityIdTag.equals("open")) {
                        facilityId = Long.parseLong(new String(chars, start, length));
                        if(!populated) {
                            entityIdentifier.createEntities(facilityId, "patient");
                            populated = true;                                
                        }            
                    }                    
                    if (entityIdTag.equals("open")) {
                        entityId = (new String(chars, start, length)).split("#");   //see Monitor interceptor for format of entity id
                    }
                    if (tableNameTag.equals("open")) {
                        tableName = new String(chars, start, length);
                    }                    
                    if (operationIdTag.equals("open")) {
                        operationId = Integer.parseInt(new String(chars, start, length));
                    }                    
                }
                
                @Override
                public void endElement(String uri, String localName, String element) throws SAXException {
                    if (element.equalsIgnoreCase("monitor")) {
                        monitorTag = "close";
                    }
                    if (element.equalsIgnoreCase("facility_id")) {
                        facilityIdTag = "close";
                    }
                    if (element.equalsIgnoreCase("entity_id")) {
                        entityIdTag = "close";
                    }
                    if (element.equalsIgnoreCase("table_name")) {
                        tableNameTag = "close";
                    }
                    if (element.equalsIgnoreCase("operation_id")) {
                        operationIdTag = "close";
                    }
                    
                    //if this is the closing tag of a monitor element save the record
                    if (element.equalsIgnoreCase("monitor")) {
                        monitorTag = "close";
                        if(entityId.length > 0) {
                            String query = "SELECT id_on_server FROM entity WHERE TRIM(LEADING '0' FROM hospital_num) = '" + PatientNumberNormalizer.unpadNumber(entityId[0]) + "'"; 
                            idOnServer = entityIdentifier.getIdOnServer(query); //check if patient exist on the server                                                    
                            if(idOnServer != 0) {
                                if(operationId == 3) {
                                    if(tableName.equals("patient")) {
                                        deleteService.deletePatient(facilityId, idOnServer);
                                    }
                                    if(tableName.equals("clinic") && entityId[1] != null) {
                                        deleteService.deleteClinic(facilityId, idOnServer, DateUtil.parseStringToDate(entityId[1], "MM/dd/yyyy"));
                                    }
                                    if(tableName.equals("pharmacy") && entityId[1] != null) {
                                        deleteService.deletePharmacy(facilityId, idOnServer, DateUtil.parseStringToDate(entityId[1], "MM/dd/yyyy"));
                                    }                   
                                    if(tableName.equals("laboratory") && entityId[1] != null) {
                                        deleteService.deleteLaboratory(facilityId, idOnServer, DateUtil.parseStringToDate(entityId[1], "MM/dd/yyyy"));
                                    }

                                    if(tableName.equals("statushistory") && entityId[1] != null && entityId[2] != null) {
                                        deleteService.deleteStatus(facilityId, idOnServer, entityId[1], DateUtil.parseStringToDate(entityId[2], "MM/dd/yyyy"));
                                    }                           
                                    if(tableName.equals("chroniccare") && entityId[1] != null) {
                                        deleteService.deleteChroniccare(facilityId, idOnServer, DateUtil.parseStringToDate(entityId[1], "MM/dd/yyyy"));
                                    }                           
                                    if(tableName.equals("anc")) {
                                        deleteService.deleteAnc(facilityId, idOnServer);
                                    }
                                    if(tableName.equals("delivery")) {
                                        deleteService.deleteDelivery(facilityId, idOnServer);
                                    }
                                    if(tableName.equals("child")) {
                                        deleteService.deleteChild(facilityId, idOnServer);
                                    }
                                    if(tableName.equals("childfollowup")) {
                                        deleteService.deleteChildfollowup(facilityId, idOnServer);
                                    }
                                    if(tableName.equals("maternalfollowup")) {
                                        deleteService.deleteMaternalfollowup(facilityId, idOnServer);
                                    }
                                    if(tableName.equals("specimen") && entityId[0] != null) {
                                        deleteService.deleteSpecimen(facilityId, entityId[0]);
                                    }
                                    if(tableName.equals("nigqual") && entityId[0] != null) {
                                        deleteService.deleteNigqual(facilityId, Integer.parseInt(entityId[0]));
                                    }
                                }
                                if(tableName.equals("patient") && operationId == 4) {
                                    if(entityId[0] != null && entityId[1] != null) new PatientHospitalNumberUpdater().changeHospitalNum(entityId[0], entityId[1], facilityId);
                                }
                            }                                    
                            
                        }
                    }                   
                }
            };
            
            //parse the XML specified in the given path and uses supplied handler to parse the document
            //this calls startElement(), endElement(), and character() methods accordingly
            saxParser.parse(xmlFileName, defaultHandler);
        }
        catch(Exception exception) {
            exception.printStackTrace();            
        }
    }   
  
}
