/**
 *
 * @author AALOZIE
 */
package org.fhi360.lamis.service.scheduler;

import org.fhi360.lamis.utility.PropertyAccessor;
import java.util.Map;
import org.fhi360.lamis.service.FacilityPerformanceAnalyzer;
import org.fhi360.lamis.service.SyncAnalyzer;
import org.fhi360.lamis.service.SyncService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class DataSyncJobScheduler extends QuartzJobBean { 
    private final SyncService syncService = new SyncService();
    private final SyncAnalyzer syncAnalyzer = new SyncAnalyzer();
    private final FacilityPerformanceAnalyzer performanceAnalyzer = new FacilityPerformanceAnalyzer();
    
    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        //This job is executed every 3 hours triggered by the syncCronTrigger in the applicationContext-servlet.xml
        //The startTransaction() method loops through all available upload folders, check if the webservice has 
        //transfered xml files into the folder and locked the folder.
        //This job is executed only on the server 
        try {
            Map<String, Object> map = new PropertyAccessor().getSystemProperties();
            String appInstance = (String) map.get("appInstance");
            String enableSync = (String) map.get("enableSync");
            if(appInstance.equalsIgnoreCase("server") && enableSync.equalsIgnoreCase("1")) {
                System.out.println("Started!");
                syncService.startTransaction();
                //Analyze and compute figures for facility performance charts and data sync charts
                syncAnalyzer.analyze();
                performanceAnalyzer.analyze();
            }
        } 
        catch (Exception exception) {
            exception.printStackTrace();
        }
    }

}
