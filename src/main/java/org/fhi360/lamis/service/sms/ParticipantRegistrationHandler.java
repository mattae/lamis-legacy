/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.sms;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author user1
 */
public class ParticipantRegistrationHandler {
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public ParticipantRegistrationHandler() {
    }

    public void savePhone(String phone) {
        executeUpdate("DELETE FROM sequencer WHERE phone = '" + phone + "'"); 
        executeUpdate("INSERT INTO sequencer (phone, active) VALUES('" + phone + "', 1)");        
    }

    public void saveAge(String age) {
        executeUpdate("UPDATE sequencer SET age = " + Integer.parseInt(age));      
    }
    
    public void saveGender(String gender) {
        executeUpdate("UPDATE sequencer SET gender = '" + gender + "'");        
    }
    
    public void saveLocation(String location) {
        executeUpdate("UPDATE sequencer SET location = '" + location + "'");        
    }
    
    public String getNextStepMsg(String phone) {
        String message = "";
        executeQuery("SELECT * FROM sequencer WHERE phone = '" + phone + "'");      
        try {
            if(resultSet.next()) {
                int age = resultSet.getInt("age"); 
                String gender = (resultSet.getString("gender") == null)? "" : resultSet.getString("gender");
                String location = (resultSet.getString("location") == null)? "" : resultSet.getString("location");
                if(age == 0){
                    message = "What is your age?";
                }
                else {
                    System.out.println("gender: " + gender);
                    if(gender.trim().isEmpty()) {
                        message = "What is your gender? M/F";
                    }
                    else {
                        System.out.println("location: " + location);
                        if(location.trim().isEmpty()) {
                            message = "Where is your location?";
                        }
                        else {
                            executeUpdate("INSERT INTO participant(phone, age, gender, location, active) SELECT phone, age, gender, location, active FROM sequencer WHERE phone = '" + phone + "'");
                            executeUpdate("DELETE FROM sequencer WHERE phone = '" + phone + "'"); 
                            message = "Thanks for joining our forum";
                        }
                    }
                }
            }
            else {
                message = "Send JOIN to commence registration";
            }            
        }
        catch (Exception exception) {
            
        }
        return message;
    }
    
    private void executeQuery(String query) {
        try {
            jdbcUtil = new JDBCUtil();        
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }        
    }
    
    private void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();    
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }                
    }
}
