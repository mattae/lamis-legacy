/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.sms;

/**
 *
 * @author user1
 */

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.utility.JDBCUtil;

public class SmsConversationService {
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public SmsConversationService() {
    }
    
    public void saveSms(Sms sms) {
        String message = sms.getText();
        String phone = sms.getFrom();
        long originatorId = sms.getOriginatorId();
        executeUpdate("INSERT INTO conversation (phone, message, date_message, originator_id, time_stamp, unread) VALUES('" + phone + "', '" + message + "', CURDATE()," + originatorId + ", NOW(), 1)");
    }
    
    public void sendSms(Sms sms) {
        String message = sms.getText();
        String phone = sms.getFrom(); 
        long originatorId = sms.getOriginatorId();
        new SmsMessageSender().send(message, phone);
        executeUpdate("INSERT INTO conversation (phone, message, date_message, originator_id, time_stamp, unread) VALUES('" + phone + "', '" + message + "', CURDATE()," + originatorId + ", NOW(), 0)");
    }
    
    public void unreadFlagUpdate(String phone) {
        executeUpdate("UPDATE conversation SET unread = 0 WHERE phone = '" + phone + "'" );
    }

    private void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();        
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }                
    }    
}
