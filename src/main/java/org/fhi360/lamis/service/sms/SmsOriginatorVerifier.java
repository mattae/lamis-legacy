/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.service.sms;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import org.fhi360.lamis.utility.JDBCUtil;

/**
 *
 * @author user1
 */
public class SmsOriginatorVerifier {
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public SmsOriginatorVerifier() {
    }
    
    public boolean isOriginatorRegistered(String phone) {
        String phonePorted = phone;
        if(phone.length() > 12) phonePorted = phone.substring(2);
        String query = "SELECT DISTINCT participant_id FROM participant WHERE phone = '" + phone + "' OR phone = '" + phonePorted + "'";              
        return executeQuery(query);
    }
    
    public boolean isFacilityRegistered(String text) {
        
        return true;
    }
    
    private boolean executeQuery(String query) {
        try {
            jdbcUtil = new JDBCUtil();     
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database 
            return false;
        }        
    }
}
