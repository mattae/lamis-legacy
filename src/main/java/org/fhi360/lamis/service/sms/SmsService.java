/**
 *
 * @author aalozie
 */
package org.fhi360.lamis.service.sms;

import org.fhi360.lamis.utility.PropertyAccessor;
import org.fhi360.lamis.dao.hibernate.SentDAO;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.io.File;

import org.fhi360.lamis.dao.hibernate.UnsentDAO;
import org.fhi360.lamis.model.Sent;
import org.fhi360.lamis.utility.Scrambler;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.time.DateUtils;
import org.fhi360.lamis.utility.JDBCUtil;
import org.fhi360.lamis.utility.StringUtil;

public class SmsService {
    private JDBCUtil jdbcUtil;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    private Scrambler scrambler;
    private SmsMessageSender smsMessageSender;
    private Map<String, Object> map;
    
    public SmsService() {
        this.scrambler = new Scrambler();
        this.smsMessageSender = new SmsMessageSender();
        this.map =  new HashMap<String, Object>();
    }
    
    public void init() {
        //This method is called by the SmsListener each time the application started
        //The method initializes the activity_tracker property file 
        try {
            File file = new File("activity_tracker.properties");
            if (!file.exists()) {
                map.put("dateLastDqa", new SimpleDateFormat("MMM yyyy").format(new Date()));
                map.put("dateLastAsyncTask", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                map.put("dateLastSms", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                map.put("appointmentMessages", "0");
                map.put("dailyMessages", "0");
                map.put("specificMessages", "0");
                new PropertyAccessor().setActivityTrackerProperties(map);
            }
        } 
        catch (Exception exception) {
            exception.printStackTrace();
        }                
    }
        
    public synchronized void runSchedule() {
        //This method is called by the SmsJobScheduler class to start sending sms via the selected gateway (modem or internet)
        try {
            File file = new File("activity_tracker.properties");
            if (!file.exists()) {
                map.put("dateLastDqa", new SimpleDateFormat("MMM yyyy").format(new Date()));
                map.put("dateLastAsyncTask", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                map.put("dateLastSms", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                map.put("appointmentMessages", "0");
                map.put("dailyMessages", "0");
                map.put("specificMessages", "0");
                new PropertyAccessor().setActivityTrackerProperties(map);
            }
            else {
                map = new PropertyAccessor().getActivityTrackerProperties();
                Date dateLastSms = new SimpleDateFormat("yyyy-MM-dd").parse((String) map.get("dateLastSms"));                
                if(!DateUtils.isSameDay(new Date(), dateLastSms)) {
                    map.put("dateLastSms", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                    map.put("appointmentMessages", "0");
                    map.put("dailyMessages", "0");
                    map.put("specificMessages", "0");
                    new PropertyAccessor().setActivityTrackerProperties(map);
                }                                 
            }
            
            file = new File("activity_tracker.properties");
            if (file.exists()) {
                map = new PropertyAccessor().getActivityTrackerProperties();
                String appointmentMessages = (String) map.get("appointmentMessages");
                String dailyMessages = (String) map.get("dailyMessages");
                String specificMessages = (String) map.get("specificMessages");
                
                smsFailedMessages();  // send previous failed messages first
                if(appointmentMessages.equals("0")) smsAppointmentMessages();     // send sms for due appointments
                if(dailyMessages.equals("0")) smsDailyMessages();   // send sms for daily messages
                if(specificMessages.equals("0")) smsSpecificMessages();     // send sms for on specific dates

                map.put("dateLastSms", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                new PropertyAccessor().setActivityTrackerProperties(map);
            } 
            else {
                System.out.println("Activity properties file not found .....");              
            }
        } 
        catch (Exception exception) {
            exception.printStackTrace();
        }                
    }
    
    private void smsAppointmentMessages() {
        //Send clients appointment messages
        String phone = "";
        String message = "";                    
        int due = getDaysToAppointment();
        Date expire = DateUtils.addDays(new Date(), due);
        try {
            ResultSet messages = getMessages("SELECT message1, message2, message3, message4, recipients FROM message WHERE message_type = 1");
            if(messages.next()) {
                //String query = "SELECT DISTINCT phone, send_message FROM patient WHERE send_message != 0";              
                String query = "SELECT DISTINCT phone, send_message FROM patient WHERE (DATEDIFF(DAY, CURDATE(), date_next_clinic) = " + due + " OR DATEDIFF(DAY, CURDATE(), date_next_refill) = " + due + ") AND send_message != 0";              
                System.out.println("Sending messages due in....."+due);
                if(messages.getString("recipients").equalsIgnoreCase("ART Only")) {
                    query = query + " AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In')";
                }
                else {
                    if(messages.getString("recipients").equalsIgnoreCase("Pre-ART Only")) {
                        query = query + " AND current_status IN ('HIV+ non ART', 'Pre-ART Transfer In')";
                    }
                    else {
                        if(messages.getString("recipients").equalsIgnoreCase("Defaulter Only")) {
                            query = query + " AND current_status IN ('Lost to Follow Up')";
                        }
                    }
                }
                executeQuery(query);
                while(resultSet.next()) {
                    phone = scrambler.unscrambleNumbers(resultSet.getString("phone").trim());
                    if(isValidPhone(phone)) {
                        if(resultSet.getInt("send_message") == 2) message = messages.getString("message2");
                        if(resultSet.getInt("send_message") == 3) message = messages.getString("message3");
                        if(resultSet.getInt("send_message") == 4) message = messages.getString("message4");
                        if(message.equals("")) message = messages.getString("message1");
 
                        String messageStatus =  smsMessageSender.send(message, phone);
                        if (messageStatus.equals("SENT")) {
                            logSentSms(scrambler.scrambleNumbers(phone), message);
                        } 
                        else if ("FAILED".equals(messageStatus) || "UNSENT".equals(messageStatus)) {    
                            logUnsentSms(scrambler.scrambleNumbers(phone), message, expire);  
                        }                        
                    }
                }            
                map.put("appointmentMessages", "1");
                resultSet = null;            
            }
        } 
        catch (Exception exception) {
            resultSet = null;
            exception.printStackTrace();
        }
    }
        
    private void smsDailyMessages() {
        //Send daily messages if any has been setup
        String phone = "";
        String message = "";                    
        Date expire = new Date();
        try {
            ResultSet messages = getMessages("SELECT message1, message2, message3, message4, recipients FROM message WHERE message_type = 2");                
            if(messages.next()) {
                String query = "SELECT DISTINCT phone, send_message FROM patient WHERE send_message != 0";        
                if(messages.getString("recipients").equalsIgnoreCase("ART Only")) {
                    query = query + " AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In')";
                }
                else {
                    if(messages.getString("recipients").equalsIgnoreCase("Pre-ART Only")) {
                        query = query + " AND current_status IN ('HIV+ non ART', 'Pre-ART Transfer In')";
                    }
                    else {
                        if(messages.getString("recipients").equalsIgnoreCase("Defaulter Only")) {
                            query = query + " AND current_status IN ('Lost to Follow Up')";
                        }
                    }
                }
                executeQuery(query);
                while(resultSet.next()) {
                    phone = scrambler.unscrambleNumbers(resultSet.getString("phone").trim());
                    if(isValidPhone(phone)) {
                        if(resultSet.getInt("send_message") == 2) message = messages.getString("message2");
                        if(resultSet.getInt("send_message") == 3) message = messages.getString("message3");
                        if(resultSet.getInt("send_message") == 4) message = messages.getString("message4");
                        if(message.equals("")) message = messages.getString("message1");

                        String messageStatus =  smsMessageSender.send(message, phone);
                        if (messageStatus.equals("SENT")) {
                            logSentSms(scrambler.scrambleNumbers(phone), message);
                        } 
                        else if ("FAILED".equals(messageStatus) || "UNSENT".equals(messageStatus)) {
                            logUnsentSms(scrambler.scrambleNumbers(phone), message, expire);
                        }                        
                    }
                }            
                map.put("dailyMessages", "1");
                resultSet = null;
            }
        } 
        catch (Exception exception) {
            resultSet = null;            
            exception.printStackTrace();
        }                
    }    
    
    private void smsSpecificMessages() {
        //Send messages setup to be sent on a specific date
        String phone = "";
        String message = "";                    
        Date expire = new Date();
        try {
            ResultSet messages = getMessages("SELECT message1, message2, message3, message4, recipients FROM message WHERE date_to_send = CURDATE() AND message_type = 3");                
            if(messages.next()) {
                String query = "SELECT DISTINCT phone, send_message FROM patient WHERE send_message != 0";        
                if(messages.getString("recipients").equalsIgnoreCase("ART Only")) {
                    query = query + " AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In')";
                }
                else {
                    if(messages.getString("recipients").equalsIgnoreCase("Pre-ART Only")) {
                        query = query + " AND current_status IN ('HIV+ non ART', 'Pre-ART Transfer In')";
                    }
                    else {
                        if(messages.getString("recipients").equalsIgnoreCase("Defaulter Only")) {
                            query = query + " AND current_status IN ('Lost to Follow Up')";
                        }
                    }
                }
                executeQuery(query);
                while(resultSet.next()) {
                    phone = scrambler.unscrambleNumbers(resultSet.getString("phone").trim());
                    if(isValidPhone(phone)) {
                        if(resultSet.getInt("send_message") == 2) message = messages.getString("message2");
                        if(resultSet.getInt("send_message") == 3) message = messages.getString("message3");
                        if(resultSet.getInt("send_message") == 4) message = messages.getString("message4");
                        if(message.equals("")) message = messages.getString("message1");

                        String messageStatus =  smsMessageSender.send(message, phone);
                        if(messageStatus.equals("SENT")) {
                            logSentSms(scrambler.scrambleNumbers(phone), message);
                        } 
                        else if ("FAILED".equals(messageStatus) || "UNSENT".equals(messageStatus)) {    
                            logUnsentSms(scrambler.scrambleNumbers(phone), message, expire);  
                        }                        
                    }
                }                            
                map.put("specificMessages", "1");
                resultSet = null;            
            }
        } 
        catch (Exception exception) {
            resultSet = null;            
            exception.printStackTrace();
        }                        
    }

    private void smsFailedMessages() { 
        //Check for unsent messages and resend
        String query = "SELECT unsent_id, phone, message FROM unsent WHERE expire >= CURDATE()"; 
        executeQuery(query);
        try {
            while(resultSet.next()) {
                String phone = scrambler.unscrambleNumbers(resultSet.getString("phone").trim());              
                String message = resultSet.getString("message");

                String messageStatus = smsMessageSender.send(message, phone);
                if ("SENT".equals(messageStatus)) {
                    logSentSms(scrambler.scrambleNumbers(phone), message);
                    UnsentDAO.delete(resultSet.getLong("unsent_id"));
                } 
            }            
            resultSet = null;            
        } 
        catch (Exception exception) {
            resultSet = null;            
            exception.printStackTrace();
        }        
    }   

    public void logSentSms(String phone, String message) {
        //Log sms that has been sent
        Sent sent = new Sent();
        sent.setPhone(phone);
        sent.setMessage(message);
        sent.setTimeStamp(new java.sql.Timestamp(new Date().getTime()));
        SentDAO.save(sent);
    }
    
    private void logUnsentSms(String phone, String message, Date expire) {
        //Log unsent sms
        ResultSet rs = null;
        String query = "SELECT unsent_id FROM unsent WHERE phone = '" + phone + "' AND message = '" + message + "' AND expire = '" + expire + "'";
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
            if(!rs.next()) {
                query = "INSERT INTO unsent(phone, message, expire) VALUES('" + phone + "','" + message + "','" + expire + "')";
                executeUpdate(query);
            }
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }        
    }

    private ResultSet getMessages(String query) {
        ResultSet rs = null;
        try {
            preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }        
        return rs;
    }
    
    private int getDaysToAppointment() {
        ResultSet rs = null;
        int days = 0;
        String query = "SELECT days_to_appointment FROM message WHERE message_type = 1";
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            rs = preparedStatement.executeQuery();
            if(rs.next()) days = rs.getInt("days_to_appointment");
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }        
        return days;
    }

    private void executeQuery(String query) {
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            resultSet = preparedStatement.executeQuery();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database            
        }        
    }
    
    private void executeUpdate(String query) {
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
        }                
    }

    private boolean isValidPhone(String phone) {
        if(!StringUtil.isInteger(phone)) return false;
        if(phone.length() != 11) return false;
        return true;
    }
}

//update patient set send_message = 1 where date_started is not null and current_status IN ('HIV+ non ART', 'ART Start', 'ART Restart', 'ART Transfer In', 'Pre-ART Transfer In')
//update patient set send_message = 1 where state in ('Rivers', 'Lagos', 'Akwa Ibom') AND current_status IN ('ART Start', 'ART Restart', 'ART Transfer In') 
