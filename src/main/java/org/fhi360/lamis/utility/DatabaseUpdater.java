/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fhi360.lamis.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import org.fhi360.lamis.dao.hibernate.ApplicationinfoDAO;
import org.fhi360.lamis.model.ApplicationInfo;

/**
 *
 * @author user1
 */
public class DatabaseUpdater {
    private static JDBCUtil jdbcUtil;
    private static PreparedStatement preparedStatement;

    private static String contextPath;
    
    public static void findUpdate(String databaseDate) {
        File file  = null;
        try {
            file = new File("system_setting.properties");
            if (file.exists()) {
                Map<String, Object> map = new PropertyAccessor().getSystemProperties();
                contextPath = (String) map.get("contextPath");
            } 
            else {
                System.out.println("System properties file not found .....");              
            }
            
            //
            if(databaseDate == ""){
                file = new File(contextPath+"version/manifest.txt");
                if (file.exists()) {
                    String line = "";
                    InputStream input = new FileInputStream(file);
                    InputStreamReader reader = new InputStreamReader(input);
                    BufferedReader bufferedReader = new BufferedReader(reader);
                    while ((line = bufferedReader.readLine()) != null) {
                        String split[] = line.split("#");
                        String fileName = split[0].trim();
                        String directory = split[1].trim();
                        if(directory.trim().equalsIgnoreCase("schema/")) update(contextPath+directory+fileName, true);
                    }
                    input.close();
                    reader.close();             
                    bufferedReader.close();
                }
                else {
                    System.out.println("System properties file not found .....");              
                }
            }
            else{
                //search in the schema/updated folder and run all files that have date more than the manifestDate
                file = new File(contextPath+"schema/updated/");
                if(file.exists() && file.isDirectory()){
                    File[] schemaFiles = file.listFiles();
                    for(File schemaFile : Arrays.asList(schemaFiles)){
                        if (schemaFile.exists()) {
                            String[] parts = schemaFile.getName().split(".");
                            String schemaDate = parts[1];
                            if ((DateUtil.parseStringToDate(schemaDate, "yyyy-MM-dd")).after(DateUtil.parseStringToDate(databaseDate, "yyyy-MM-dd"))) {                          
                                update(schemaFile.getName(), false);
                            }
                        }
                    }
                }
            }
        } 
        catch (Exception exception) {
            System.out.println("OI exception ........" + exception.getMessage());
            throw new RuntimeException(exception);
        } 
    }
    
    private static String convertDateToSchemaDate(String date){
        String[] parts = date.split("-");
        String formed = parts[1].concat(parts[0]);
        
        return formed;
    }
    
    private static void update(String fileName, boolean move) {
        InputStream in  = null;
        InputStreamReader reader  = null;
        BufferedReader br  = null;
        File file  = null;
        try {
            file = new File(fileName);
            if (file.exists()) {
                System.out.println("Database update file found .....");              
                in = new FileInputStream(file);
                reader = new InputStreamReader(in);
                br = new BufferedReader(reader);

                String line = "";
                while((line = br.readLine()) != null) {
                    if(!line.contains(";")) break;
                    String query = line.substring(0, line.lastIndexOf(";"));
                    try {
                        if(!query.trim().isEmpty()) executeUpdate(query);
                    }
                    catch (Exception exception) {
                        System.out.println("Error occured while excuting database updates ........" + exception.getMessage());
                        throw new RuntimeException(exception);    
                    }
                }
                if(in != null) in.close();
                if(reader != null) reader.close();
                if(br != null) br.close();
                FileUtil fileUtil = new FileUtil();
                if (file.exists() && move){
                    fileUtil.moveFile(fileName, contextPath+"schema/", contextPath+"schema/updated/");                  
                } 
                //TODO: Update Application Info
                Set<ApplicationInfo> aAppInfo = ApplicationinfoDAO.list();
                 ApplicationInfo info = aAppInfo.iterator().next();
                    if(info != null){
                        info.setDatabaseVersion(info.getApplicationVersion());
                        info.setDateDatabaseUpdate(info.getDateApplicationUpdate());
                        ApplicationinfoDAO.save(info);
                    }
            }
        } 
        catch (Exception exception) {
            System.out.println("OI exception ........" + exception.getMessage());
            throw new RuntimeException(exception);
        } 
    }
    
    private static void executeUpdate (String query) throws Exception{
        try {
            jdbcUtil = new JDBCUtil();
            preparedStatement = jdbcUtil.getStatement(query);
            preparedStatement.executeUpdate();
        }
        catch (Exception exception) {
            jdbcUtil.disconnectFromDatabase();  //disconnect from database
            throw exception;
        }        
    }    
}
