<%-- 
    Document   : casemanager_data
    Created on : Oct 20, 2017, 4:00:54 PM
    Author     : DURUANYANWU IFEANYI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/JavaScript">
            var obj = {};
            var updateRecord = false;
            var lastSelected = -99;
            $(document).ready(function(){
                resetPage();

                $("#grid").jqGrid({
                    url: "Casemanager_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Full Name", "Address", "Phone", "Age", "Sex", "Facility Id"],
                    colModel: [
                        {name: "fullname", index: "fullname", width: "170"},
                        {name: "address", index: "address", width: "230"},
                        {name: "phone", index: "phone", width: "140"},
                        //{name: "religion", index: "religion", width: "100"},                        
                        {name: "age", index: "age", width: "110"},
                        {name: "sex", index: "sex", width: "90"},                        
                        {name: "facilityId", index: "facilityId", width: "0", sortable:false, hidden:true},                        
                    ],
                    rowNum: -1,
                    sortname: "casemanagerId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 300,                    
                    jsonReader: {
                        root: "caseManagerList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "casemanagerId"
                    },
                    onSelectRow: function(id) {
                        var data = $("#grid").getRowData(id)
                        $("#fullname").val(data.fullname);
                        $("#address").val(data.address);
                        $("#phoneNumber").val(data.phone);
                        $("#sex").val(data.sex);
                        $("#age").val(data.age);
                        $("#religion").val(data.religion);
                        $("#casemanagerId").val(id);
                        $("#facilityId").val(data.facilityId);
                        
                        updateRecord = true;
                        lastSelected = id;                        
                        initButtonsForModify();
                    }               
                }); 

                $("#save_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                        //if(validateForm()) {
                            $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');
                            if(updateRecord) {
                                $("#lamisform").attr("action", "Casemanager_update");                
                            } 
                            else {
                                $("#lamisform").attr("action", "Casemanager_save");                
                            }
                            $("#loader").html('');
                            return true;                                                            
                        //} 
                        //else {
                           // return false;
                        //}
                    }
                }); 
                
                $("#delete_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                    }
                    else {
                        $("#lamisform").attr("action", "Casemanager_delete");
                    }
                    return true;
                });    

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Setup_page");
                    return true;
                });
                //checkConnection();
            }); 

            function reset() {
                updateRecord = false;
                lastSelected = -99;
                resetButtons();
            };
  
            
            function validateForm() {
                var validate = true;

                // check for valid input is entered
                if($("#stateId").val().length == 0){
                    $("#stateHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#stateHelp").html("");                    
                }
                
                if($("#lgaId").val().length == 0){
                    $("#lgaHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#lgaHelp").html("");                    
                }
                
                if($("#pharmacy").val().length == 0){
                    $("#nameHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#nameHelp").html("");                    
                }
                return validate;
            }                                         
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_setup.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/settings.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Setup >> Case Manager Setup</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Case Manager Details</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>                        

                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><input name="casemanagerId" type="hidden" id="casemanagerId"/></td>
                                <td width="30%"><input name="facilityId" type="hidden" id="facilityId"/></td>
                                <td width="20%"></td>
                                <td width="30%"></td>
                            </tr>
<!--                            <tr>
                                <td><label>State:</label></td>
                                <td>
                                    <select name="stateId" style="width: 130px;" class="inputboxes" id="stateId">
                                    </select><span id="stateHelp" class="errorspan"></span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>L.G.A.</label></td>
                                <td>
                                    <select name="lgaId" style="width: 130px;" class="inputboxes" id="lgaId">
                                    </select><span id="lgaHelp" class="errorspan"></span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>-->
                            <tr>
                                <td><label>Full Name:</label></td>
                                <td><input name="fullname" type="text" style="width: 300px;" class="inputboxes" id="fullname"/><span id="nameHelp" class="errorspan"></span></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>Address:</label></td>
                                <td><input name="address" type="text" style="width: 350px;" class="inputboxes" id="address"/></td>
                            </tr>
                            <tr>
                                <td><label>Telephone:</label></td>
                                <td><input name="phoneNumber" type="text" style="width: 200px;" class="inputboxes" id="phoneNumber"/></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>Age: </label></td>
                                <td><input name="age" type="text" style="width: 100px;" class="inputboxes" id="age"/></td>
                            </tr>
                            <tr>
                                <td><label>Sex: </label></td>
                                <td><select name="sex" style="width: 150px;" class="inputboxes" id="sex">
                                        <option></option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                    <span id="sexHelp" class="errorspan"></span>
                                </td>
                            </tr>
                            <tr>
                                <!--<td><label>Religion:</label></td>-->
                                <td><input name="religion" value="" type="text" style="width: 200px;" class="inputboxes" id="religion" hidden/></td>
                            </tr>
                        </table>
                        <p></p>
                        <div>
                            <fieldset>  
                                <legend> Case Manager Listing</legend>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>

