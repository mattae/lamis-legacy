<%-- 
    Document   : Data Export
    Created on : Aug 15, 2012, 6:53:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />

        <script type="text/javascript" src="js/lamis/maintenance-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var timer = {}; 
            $(document).ready(function(){
                initialize();
                timer = $.timer(function() { 
                    $.ajax({
                        url: "Processing_status.action",
                        dataType: "json",
                        success: function(status) {
                            if(status == "check1") $("#check1").removeAttr("hidden");
                            if(status == "check2") {
                                $("#check1").removeAttr("hidden");
                                $("#check2").removeAttr("hidden");
                            }
                            if(status == "check3") {
                                $("#check1").removeAttr("hidden");
                                $("#check2").removeAttr("hidden");
                                $("#check3").removeAttr("hidden");
                            }
                            if(status == "check4") {
                                $("#check1").removeAttr("hidden");
                                $("#check2").removeAttr("hidden");
                                $("#check3").removeAttr("hidden");
                                $("#check4").removeAttr("hidden");
                            }
                        }                    
                    }); 
                });
                timer.set({time : 30000, autostart : false});
                
                $("#ok_button").bind("click", function(event){
                    $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                    timer.play();
                    cleanup();
                    return false;
                });
                
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Maintenance_page");
                    return true;
                });                
            }); 

            function cleanup() {
                $("#messageBar").hide(); 
                $("#ok_button").attr("disabled", true);
                $.ajax({
                    url: "Cleanup_data.action",
                    dataType: "json",
                    success: function(status) { 
                        timer.stop();
                        $("#loader").html('');
                        $("#check1").removeAttr("hidden");
                        $("#check2").removeAttr("hidden");
                        $("#check3").removeAttr("hidden");
                        $("#check4").removeAttr("hidden");
                        $("#check5").removeAttr("hidden");
                        $("#messageBar").html("Database Cleanup Completed").slideDown('fast');     
                        $("#ok_button").attr("disabled", false);
                    }                    
                });
            }
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_maintenance.jsp" />  

               <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/tools.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Data Maintenance >> Cleanup Database Records</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Database Cleanup</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="5%"></td>
                                <td width="80%"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span id="check1" hidden><img src="images/checked.png" style="margin-bottom: -5px;" /> &nbsp; &nbsp;</span>                                  
                                    <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.2em;">Cleaning clinic records and updating date of last clinic in patient table</span>
                                </td>
                            </tr>
                            <tr>
                                <td width="5%"></td>
                                <td width="80%"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span id="check2" hidden><img src="images/checked.png" style="margin-bottom: -5px;" /> &nbsp; &nbsp;</span>                                  
                                    <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.2em;">Updating date of last ARV refill in patient table</span>
                                </td>
                            </tr>

                            <tr>
                                <td width="5%"></td>
                                <td width="80%"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span id="check3" hidden><img src="images/checked.png" style="margin-bottom: -5px;" /> &nbsp; &nbsp;</span>                                  
                                    <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.2em;">Updating date of last lab investigation in patient table</span>
                                </td>
                            </tr>

                            <tr>
                                <td width="5%"></td>
                                <td width="80%"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span id="check4" hidden><img src="images/checked.png" style="margin-bottom: -5px;" /> &nbsp; &nbsp;</span>                                  
                                    <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.2em;">Determining due date for viral load test</span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td width="5%"></td>
                                <td width="80%"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span id="check5" hidden><img src="images/checked.png" style="margin-bottom: -5px;" /> &nbsp; &nbsp;</span>                                  
                                    <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.2em;">Updating patient current status</span>
                                </td>
                            </tr>
                        </table>                        
                        <p></p>

                        <div id="buttons" style="width: 200px">
                            <button id="ok_button">Cleanup</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
