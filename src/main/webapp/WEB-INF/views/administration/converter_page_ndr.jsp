<%-- 
    Document   : Data Export
    Created on : Aug 15, 2012, 6:53:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var facilityIds;
            $(document).ready(function(){
                initialize(); 
                
                $("body").bind('ajaxStart', function(event){
                    $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                });

                $("body").bind('ajaxStop', function(event){
                    $("#loader").html('');
                });
                $("#messageBar").hide();
 
                $.ajax({
                    url: "StateId_retrieve.action",
                    dataType: "json",
                    success: function(stateMap) {
                        var options = "<option value = '" + '' + "'>" + '' + "</option>";
                        $.each(stateMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#stateId").html(options);                        
                    }                    
                }); //end of ajax call

                 
                $("#grid").jqGrid({
                    datatype: "json",
                    mtype: "GET", 
                    colNames: ["S/N","Facility","Patient Count", "Active Patients", "Datim Code"],
                    colModel: [
                        {name: "sn", index: "sn", width: "50"}, 
                        {name: "name", index: "name", width: "335"},
                        {name: "count", index: "count", width: "100"},
                        {name: "active", index: "active", width: "100"},
                        {name: "datimId", index: "datimId", width: "150"}
                    ],
                    rowNum: -1,
                    sortname: "facilityId",
                    sortorder: "desc",
                    multiselect: true,
                    viewrecords: true,
                    imgpath: "themes/basic/images", 
                    resizable: false,
                    height: 350,                    
                    jsonReader: {
                        root: "facilityList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "facilityId"
                    }
                }); //end of jqGrid    
                
                
                $("#stateId").change(function(event){
                    $("#messageBar").slideUp('fast'); 
                    $("#grid").setGridParam({url: "Facilitysel_grid.action?q=1&stateId="+$("#stateId").val(), page:1}).trigger("reloadGrid");
                });
                                
                $("#ok_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                        facilityIds = jQuery("#grid").jqGrid('getGridParam','selarrrow');
                        convertData();                                
                        return false;
                    }
                });
               
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Conversion_page");
                    return true;
                });                
            });

            var url = "";
            var x = function wait() {window.open(url);}
 
            function convertData() {
                $("#messageBar").hide();
                $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                $("#ok_button").attr("disabled", true);
                $.ajax({
                    url: "Converter_dispatch_ndr.action",
                    dataType: "json",
                    data: {stateId: $("#stateId").val(), facilityIds : facilityIds.toString()},
                    success: function(fileName) {
                        console.log(fileName);
                        $("#messageBar").html("Conversion Completed").slideDown('slow');
                        $("#ok_button").attr("disabled", false);
                        url = fileName;
                        //window.setTimeout(x, 3000);
                    }, 
                    error: function(e){
                        console.log("Error: "+JSON.stringify(e));
                        alert("There was an error in conversion!");
                        $("#ok_button").attr("disabled", false); 
                    }
                });
            }
            
            function validateUpload() {
                var validate = true;

                // check for valid input is entered
                if($("#facilityId").val().length == 0){
                    $("#facilityHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#facilityHelp").html("");                    
                }                
                return validate;
            }            
            
        </script>
    </head>  

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
               <jsp:include page="/WEB-INF/views/template/nav_conversion.jsp" />  

               <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/report.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Data Conversion >> Generate NDR Files</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Data Conversion</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="15%"><label>State/Facility:</label></td>
                                <td  width="85%">
                                    <select name="stateId" style="width: 300px;" class="inputboxes" id="stateId">
                                    </select><span id="stateHelp" class="errorspan"></span>
                                </td>                                                                
                            </tr>
                        </table>
                        
                        <p></p>
                        <div>
                            <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td width="15%"></td>
                                    <td width="85%">
                                        <table id="grid"></table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <p></p>
                        <hr>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 200px">
                            <button id="ok_button">Convert</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
