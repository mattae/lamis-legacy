<%-- 
    Document   : Data Export
    Created on : Aug 15, 2012, 6:53:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var facilityIds = [];
            $(document).ready(function(){
                initialize();
                reports();

                for(i = new Date().getFullYear(); i > 1900; i--) {
                    $("#cohortYearBegin").append($("<option/>").val(i).html(i));
                    $("#cohortYearEnd").append($("<option/>").val(i).html(i));
                }

                $("body").bind('ajaxStart', function(event){
                    $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                });

                $("body").bind('ajaxStop', function(event){
                    $("#loader").html('');
                });
                $("#messageBar").hide();
                                
                $("#ok_button").bind("click", function(event){
                    if(validateForm()) {
                        if($("#cohortYearEnd").val() - $("#cohortYearBegin").val() < 0){
                            var message = "Cohort end year cannot be eailer than cohort beginning year";
                            $("#messageBar").html(message).slideDown('slow');                                                     
                        }
                        else {
                            convertData();
                        }          
                    }
                    return false;
                });
                
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Conversion_page");
                    return true;
                });               
            });
            
            var url = "";
            var x = function wait() {window.open(url);}
            
            function convertData() { 
                $("#messageBar").hide();
                $("#ok_button").attr("disabled", true);
                $.ajax({
                    url: "Converter_dispatch_retention.action",
                    dataType: "json",
                    data: {cohortMonthBegin: $("#cohortMonthBegin").val(), cohortYearBegin: $("#cohortYearBegin").val(), cohortMonthEnd: $("#cohortMonthEnd").val(), cohortYearEnd: $("#cohortYearEnd").val()},
                    success: function(fileName) {
                        $("#messageBar").html("Analysis Completed").slideDown('fast');
                        url = fileName;
                        window.setTimeout(x, 3000);                        
                        $("#ok_button").attr("disabled", false);   //$("#ok_button").removeAttr("disabled");
                    }                    
                }); 
            }
            
            function validateForm() {
                var validate = true;

                // check for valid input is entered
                if($("#cohortMonthBegin").val().length == 0 || $("#cohortYearBegin").val().length == 0){
                    $("#periodHelp1").html(" *");
                    validate = false;
                }
                else {
                    $("#periodHelp1").html("");                    
                }
                if($("#cohortMonthEnd").val().length == 0 || $("#cohortYearEnd").val().length == 0){
                    $("#periodHelp2").html(" *");
                    validate = false;
                }
                else {
                    $("#periodHelp2").html("");                    
                }
                return validate;
            }                                         
        </script>
    </head>  

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_conversion.jsp" />  

               <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/report.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Data Conversion >> Generate Cohort Analysis File</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">RADET Report</td>
                            </tr>
                        </table>
                        <p></p>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="15%"><label>Cohorts of ART:</label></td>
                                <td width="85%">
                                    <select name="cohortMonthBegin" style="width: 100px;" class="inputboxes" id="cohortMonthBegin"/>
                                       <option></option>
                                       <option>January</option>
                                       <option>February</option>
                                       <option>March</option>
                                       <option>April</option>
                                       <option>May</option>
                                       <option>June</option>
                                       <option>July</option>
                                       <option>August</option>
                                       <option>September</option>
                                       <option>October</option>
                                       <option>November</option>
                                       <option>December</option>
                                    </select>
                                    <select name="cohortYearBegin" style="width: 75px;" class="inputboxes" id="cohortYearBegin"/>
                                       <option></option>
                                    </select><span id="periodHelp1" style="color:red"></span>
                                    &nbsp; to:&nbsp; 
                                    <select name="cohortMonthEnd" style="width: 100px;" class="inputboxes" id="cohortMonthEnd"/>
                                       <option></option>
                                       <option>January</option>
                                       <option>February</option>
                                       <option>March</option>
                                       <option>April</option>
                                       <option>May</option>
                                       <option>June</option>
                                       <option>July</option>
                                       <option>August</option>
                                       <option>September</option>
                                       <option>October</option>
                                       <option>November</option>
                                       <option>December</option>
                                    </select>
                                    <select name="cohortYearEnd" style="width: 75px;" class="inputboxes" id="cohortYearEnd"/>
                                       <option></option>
                                    </select><span id="periodHelp2" style="color:red"></span>
                                </td>
                            </tr>                            
                            <tr><td><p></p></td></tr>
                            <tr>
                                <td width="15%"></td>
                                <td width="85%"><label>Cohorts of ART are defined by the month/year they are started on ART</label></td>
                            </tr>
                            <tr>
                                <td width="15%"></td>
                                <td><label>Treatment outcome for each cohort will be evaluated for after 12 months of initiation</label></td>
                            </tr>
                        </table>
            
                        <p></p>
                        <hr></hr>

                        <div id="buttons" style="width: 200px">
                            <button id="ok_button">Generate</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
