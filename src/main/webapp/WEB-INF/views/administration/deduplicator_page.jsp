<%-- 
    Document   : Contact
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />

        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var enablePadding = true;
            var patientIds;
        $(document).ready(function(){
            $.ajax({
            url: "Padding_status.action",
            dataType: "json",
            success: function(statusMap) {
            enablePadding = statusMap.paddingStatus;
            }
            });
            resetPage();
            initialize();
            reports();

            $("#dialog").dialog({
            title: "Hospital Number Deduplication",
            autoOpen: false,
            width: 500,
            resizable: false,
            buttons: [{text: "Yes", click: removeDuplicates}, 
            {text: "Cancel", click: function(){$(this).dialog("close")}}] 
            });

            var lastSelected = -99;
            
            loadGrid();

            $("#update_button").bind("click", function(event){
            patientIds = [];            
            var ids = $("#grid").getDataIDs();                    
            for(var i = 0; i < ids.length; i++) {
            var data = $("#grid").getRowData(ids[i]);
            if(data.sel == 1) {
            patientIds.push(ids[i]);
            }
            }
            $("#patientIds").val(patientIds);
            if($("#patientIds").val().length == 0 ) {
            $("#messageBar").html("No duplicate hospital number is selected").slideDown('slow');     
            return true; 
            }
            else {
            $("#messageBar").hide();                        
            if($("#userGroup").html() != "Administrator") {
            $("#lamisform").attr("action", "Error_message");
            return true;                        
            }
            else {
            $("#dialog").dialog("open");
            return false;                        
            }
            }

            });

            $("#export_button").bind("click", function(){ 
                $("#messageBar").hide();
                $("#messageBar").html('');
                $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');
               
                $.ajax({
                url: "Export_duplicates.action",
                dataType: "json",
                success: function(fileName) {
                console.log(fileName);
                $("#loader").html('');
                $("#messageBar").html("Export to excel completed").slideDown('slow');
                url = fileName;
                window.setTimeout(x, 3000);
                }, 
                error: function(e){
                console.log("Error: "+JSON.stringify(e));
                $("#loader").html('');
                alert("There was an error in exporting record");
                }
                }); 
            });  

            $("#import_button").bind("click", function(event){
                var validate = true;
                $("#messageBar").hide();
                $("#messageBar").html('');
                
                // check if file name is entered
                if($("#attachment").val().length == 0) {
                    $("#fileHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');
                    $("#fileHelp").html("");
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                        uploadFile();
                    }
                }
                
            });
            
            

            $("#close_button").bind("click", function(event){
            $("#lamisform").attr("action", "Maintenance_page");
            return true;
            });
            
            });
            
            function uploadFile(){
                var form = $('#uploadForm')[0];
                                // Create an FormData object 
                var data = new FormData(form);

                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "uploadDuplicates",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (result) {
                        $("#grid").html();
                        $("#grid").setGridParam({ url: "Duplicate_grid.action"})               
                        $("#grid").trigger('reloadGrid');
                        $("#loader").html('');
                        $("#attachment").val('');
                        $("#messageBar").html("Duplicates remove successfully.").slideDown('slow');

                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        $("#loader").html('');
                        $("#messageBar").html("Error in removing duplicates.").slideDown('slow');
                        $("#attachment").val('');
                    }

                });
            }
            
            function removeDuplicates() {  
            $("#dialog").dialog("close");
            $("#update_button").attr("disabled", true);
            $("#lamisform").attr("action", "Remove_duplicates");
            $("#lamisform").trigger("submit"); 
            $("#update_button").attr("disabled", false);
            }      
           
            function loadGrid(){
                console.log("load grid initiated....");
                $("#grid").jqGrid({
                    url: "Duplicate_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["", "Hospital No","Unique ID", "Name", "Gender", "Date of Birth", "Address", "ART Status", "Encounters"],
                    colModel: [
                    {name: "sel", index: "sel", width: "30", align: "center", formatter:"checkbox", editoptions:{value:"1:0"}, formatoptions:{disabled:false}},                        
                    {name: "hospitalNum", index: "hospitalNum", width: "80"},
                    {name: "uniqueId", index: "uniqueId", width: "80"},
                    {name: "name", index: "name", width: "140"},
                    {name: "gender", index: "gender", width: "50"},
                    {name: "dateBirth", index: "dateBirth", width: "70", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d-M-Y"}},                        
                    {name: "address", index: "address", width: "180"},                        
                    {name: "currentStatus", index: "currentStatus", width: "100"},
                    {name: "count", index: "count", width: "80", align: 'center'}
                    ],
                    pager: $('#pager'),
                    rowNum: -1,
                    sortname: "hospitalNum",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 350,
                    //width: 400,                    
                    jsonReader: {
                    root: "duplicateList",
                    page: "currpage",
                    total: "totalpages",
                    records: "totalrecords",
                    repeatitems: false,
                    id: "patientId"
                    },
                    onSelectRow: function(id) {
                    var data = $(this).getRowData(id);
                    if(data.sel == 1) {
                    $("#grid").jqGrid('setRowData', id, false, {background: 'khaki'});
                    //$("#grid").jqGrid('setRowData', id, false, {color: 'khaki'});
                    }
                    else {
                    //console.log("Selected "+data.sel);
                    $("#grid").jqGrid('setRowData', id, false, {background: 'khaki'});
                    }                        
                    }
                });             
            }
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  

            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_maintenance.jsp" />  

                <div id="rightPanel">

                    <table width="100%" border="0">
                        <tr>
                            <td>
                                <span>
                                    <img src="images/tools.png" style="margin-bottom: -5px;" /> &nbsp;
                                    <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Data Maintenance >> Remove Duplicate Numbers</strong></span>
                                </span>
                                <hr style="line-height: 2px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="topheaders">Remove Duplicate Numbers</td>
                        </tr>
                    </table>
                    <div id="loader"></div>
                    <div id="messageBar"></div>

                    <s:form theme="css_xhtml" action="uploadDuplicates" id="uploadForm" method="post" enctype="multipart/form-data">  
                        <table width="70%">
                            <tr>
                                <td>
                                    <label>File:</label>
                                    <input type="file" id="attachment" name="attachment" />
                                    <span id="fileHelp" style="color:red;"></span>
                                </td>
                                <td>
                                    <div id="buttons">
                                        <button type="button" id="import_button" name="import_button"> Import Data</button>
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="buttons" style="width: 300px">
                                        <a href="#" id="export_button" style="font-size:16px;">Export Duplicate record to Excel</a>
                                    </div> 
                                </td>
                            </tr>
                        </table>
                    </s:form>  

                    <s:form id="lamisform" theme="css_xhtml">  
                        <input name="patientIds" type="hidden" id="patientIds"/>  
                        <div>
                            <fieldset>  
                                <legend> Duplicate Numbers</legend>
                                <table width="99%" height="90" border="0" class="space">
                                    <tr>
                                        <td>
                                            <table id="grid"></table>
                                            <div id="pager" style="text-align:center;"></div>
                                        </td>
                                    </tr>

                                </table>
                                <div id="buttons" style="width: 300px; margin-bottom: 5px;">
                                    <button id="update_button" >Remove</button> &nbsp;<button id="close_button">Close</button>
                                </div>
                            </fieldset> 
                        </div>
                        <p></p>
                        <div id="dialog">
                            <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td><label>Do you want to continue with deletion of duplicate hospital numbers?</label></td>
                                </tr>
                                <tr>
                                    <td width="20%"><label>Click Yes to continue or No to cancel:</label></td>
                                </tr>   
                            </table>
                        </div>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>

                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
