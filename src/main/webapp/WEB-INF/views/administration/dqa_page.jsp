<%-- 
    Document   : Facility
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var lastSelected = -99;
            var facilityIds = [];
            var response = "";
            $(document).ready(function(){
                resetPage();
                                
                $("#grid").jqGrid({
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Data Element", "Total", "id"],
                    colModel: [
                        {name: "element", index: "element", width: "610"},
                        {name: "total", index: "total", width: "110"},
                        {name: "elementId", index: "elementId", width: "0", sortable:false, hidden:true}               
                    ],
                    rowNum: -1,
                    sortname: "elementId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 350,                    
                    loadtext: "Analyzing Data Quality, this may take some minutes...",                    
                    jsonReader: {
                        root: "elementList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "elementId"
                    },
                    afterInsertRow: function(id, data) {
                        if(data.total == "0") {
                            $(this).jqGrid('setRowData', id, false, {background: 'lime'});
                        }                        
                    },                    
                    loadComplete: function() { 
                        $("#loader").html('');     
                        $("#showingData").html(response);
                    },
                    ondblClickRow: function(id) {
                        var data = $("#grid").getRowData(id);
                        if(data.total != 0 && lastSelected != id) {
                            lastSelected = id;                        
                            dqaReport(id);                            
                        }
                    }                    
                }); //end of jqGrid  
                
                $.ajax({
                    url: "StateId_retrieve_custom.action",
                    dataType: "json",
                    success: function(stateMapCustom) {
                        var options = "<option value = '" + 0 + "'>" + '' + "</option>";
                        $.each(stateMapCustom, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#stateId").html(options);                        
                    }                    
                }); //end of ajax call
                               
               $("#stateId").change(function(event){
                   facilityIds = [];
                   $("#ok_button").removeAttr("disabled");
                    $("#messageBar").slideUp('fast');
                    if($("#stateId").val() === "0"){
                        $("#facilityId").html(""); 
                    }else{
                        $.ajax({
                            url: "Facility_retrieve.action?stateId="+$("#stateId").val()+"&active",
                            dataType: "json",
                            success: function(facilityMap) {
                                var options = "<option value = '" + 0 + "'>" + '' + "</option>";
                                $.each(facilityMap, function(key, value) {
                                    facilityIds.push(key);
                                    options += "<option value = '" + key + "'>" + value + "</option>";
                                }) //end each
                                $("#facilityId").html(options);                        
                            }                   
                        }); //end of ajax call
                    }
                });
                
                $("#facilityId").change(function(event){
                    $("#ok_button").removeAttr("disabled");
                });
                  
                $("#ok_button").bind("click", function(event){
                    $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                    dqaAnalysis();
                    return false;
                });
                
                $("#print_button").bind("click", function(event){
                    $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                    printReport();
                    return false;
                });
                
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Maintenance_page");
                    return true;
                });
                
                $('input[type="radio"]').bind("click", function(event){
                    $("#ok_button").removeAttr("disabled");
                    $("#messageBar").slideUp('fast'); 
                    //$('input[type="text"]').val("");
                });
            }); 
 
            function dqaAnalysis() {
                var hasFacility = false; var hasState = false;
                lastSelected = -99;
                $("#showingData").html('');
                var url = "Dqa_grid.action?q=1&analysisOption="+$('[name="analysisOption"]:checked').val();
                if($("#stateId").val() > 0){
                    if($("#facilityId").val() > 0){
                        hasFacility = true;
                        url = "Dqa_grid.action?q=1&analysisOption="+$('[name="analysisOption"]:checked').val()+"&facilityIds="+$("#facilityId").val();
                    }else{
                        hasFacility = false;
                        url = "Dqa_grid.action?q=1&analysisOption="+$('[name="analysisOption"]:checked').val()+"&facilityIds="+facilityIds;
                    }
                    hasState = true;
                }else if($("#stateId").val() == 0){
                    hasFacility = true;
                    hasState = false;
                    url = "Dqa_grid.action?q=1&analysisOption="+$('[name="analysisOption"]:checked').val();
                } 
                $("#grid").setGridParam({url: url, page:1}).trigger("reloadGrid");
                var state = document.getElementById("stateId");
                var facility = document.getElementById("facilityId");
                if(hasState == true){
                    if(hasFacility == false){
                        response = "Showing Data Quality Records Analyzed for all facilities in <b>"+state.options[state.selectedIndex].text+" State</b>";
                    }else{
                        response = "Showing Data Quality Records Analyzed for <b>"+ facility.options[facility.selectedIndex].text +"</b> in <b>"+state.options[state.selectedIndex].text+" State</b>";
                    }
                }else{
                    response = "Showing Data Quality Records Analyzed for your facility <b>"+$("#facilityName").html()+"</b>";
                }
                $("#ok_button").attr("disabled", "disabled");
                $("#print_button").removeAttr("disabled");
            }
            
            function dqaReport(id) {
                if($("#userGroup").html() != "Data Analyst") {
                    url = "Dqa_report.action?elementId="+id;                                                            
                    window.open(url);
                }
            }

            var url = "";
            var x = function wait() {window.open(url);}
            
            function printReport() {
             
                $("#messageBar").hide();
                $("#print_button").attr("disabled", true);
                $.ajax({
                    url: "Converter_dispatch.action",
                    dataType: "json",
                    data: {recordType: 14, exportDetail: response},
                    beforeSend: function(){
                        //console.log($("#recordType").val(), $("#yearId option:selected").text(), $("#stateId option:selected").text(), $("#labtestId").val(), $("#reportingDateBegin").val(), $("#reportingDateEnd").val(), fac_ids.toString());
                    },
                    success: function(fileName) {
                        console.log(fileName);
                        $("#messageBar").html("Export to Excel Completed").slideDown('slow');
                        $("#print_button").attr("disabled", false);
                        url = fileName;
                        window.setTimeout(x, 3000);
                    }, 
                    error: function(e){
                        console.log("Error: "+JSON.stringify(e));
                        alert("There was an error in conversion!");
                        $("print_button").attr("disabled", false);
                    }
                }); 
            }

        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_maintenance.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/tools.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Data Maintenance >> Data Quality Analyzer</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Data Quality Analysis</td>
                            </tr>
                        </table>
                        <div id="loader"></div>                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="15%"><label>Select State:</label></td>
                                <td width="85%">
                                    <select name="stateId" style="width: 300px;" class="inputboxes" id="stateId">                        
                                    </select> &nbsp;
                                </td>
                            </tr>
                            
                            <tr>
                                <td width="15%"><label>Select Facility:</label></td>
                                <td width="85%">
                                    <select name="facilityId" style="width: 300px;" class="inputboxes" id="facilityId">                        
                                    </select> &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"> 
                                    <input type="radio" name="analysisOption" value="1" id="recordsCurrent" checked="checked" /> <label>Current month transaction records</label><span style="margin-left:50px"></span><input type="radio" name="analysisOption" value="2" id="recordsAll" /> <label>All transaction records</label>
                                </td>
                            </tr>
                            <tr><td></td></tr>
                            <tr>
                                <td colspan="2"> 
                                    <label id="showingData"></label>
                                </td>
                            </tr>
                            <tr><td></td></tr>
                        </table>                        
                        <div>
                            <fieldset>  
                                <legend> Analysis Result</legend>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td width="2%"></td>
                                    <td width="98%">
                                        <table id="grid"></table>
                                    </td>
                                </tr>
                                </table>                                    
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="facilityName" style="display: none"><s:property value="#session.facilityName"/></div>
                        <div id="buttons" style="width: 300px">
                            <!--<button id="print_button" disabled>Print</button> &nbsp;&nbsp;--> 
                            <button id="ok_button">Analyze</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
