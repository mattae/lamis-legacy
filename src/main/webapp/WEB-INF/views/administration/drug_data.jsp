<%-- 
    Document   : Facility
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/JavaScript">
            var obj = {};
            var updateRecord = false;
            var lastSelected = -99;
            $(document).ready(function(){
                resetPage();
               
                $.ajax({
                    url: "StateId_retrieve.action",
                    dataType: "json",
                    success: function(stateMap) {
                        var options = "<option value = '" + '' + "'>" + '' + "</option>";
                        $.each(stateMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#stateId").html(options);                        
                    }                    
                }); //end of ajax call

                $("#grid").jqGrid({
                    url: "Communitypharm_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Pharmacy", "Address", "Phone", "Email", "Pin", "StateId", "LgaId"],
                    colModel: [
                        {name: "pharmacy", index: "pharmacy", width: "200"},
                        {name: "address", index: "address", width: "250"},
                        {name: "phone", index: "phone1", width: "150"},
                        {name: "email", index: "email", width: "150"},                        
                        {name: "pin", index: "pin", width: "50"},
                        {name: "stateId", index: "stateId", width: "0", sortable:false, hidden:true},                        
                        {name: "lgaId", index: "lgaId", width: "0", sortable:false, hidden:true},                        
                    ],
                    rowNum: -1,
                    sortname: "communitypharmId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 280,                    
                    jsonReader: {
                        root: "pharmList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "communitypharmId"
                    },
                    onSelectRow: function(id) {
                        var data = $("#grid").getRowData(id)
                        $("#pharmacy").val(data.pharmacy);
                        $("#address").val(data.address)
                        $("#phone").val(data.phone)
                        $("#email").val(data.email)
                        $("#pin").val(data.pin)
                        $("#communitypharmId").val(id);
                        obj.stateId =  data.stateId;
                        obj.lgaId =  data.lgaId;
                        retrieve(obj);
                        
                        updateRecord = true;
                        lastSelected = id;                        
                        initButtonsForModify()
                    }               
                }); 

                $("#stateId").change(function(event){
                    $.ajax({
                        url: "LgaId_retrieve.action",
                        dataType: "json",
                        data: {stateId: $("#stateId").val()},

                        success: function(lgaMap) {
                            var options = "<option value = '" + '' + "'>" + '' + "</option>";
                            $.each(lgaMap, function(key, value) {
                                options += "<option value = '" + key + "'>" + value + "</option>";
                            }) 
                            $("#lgaId").html(options);
                        }                    
                    }); //end of ajax call
                    $("#grid").setGridParam({url: "Communitypharm_grid.action?q=1&stateId="+$("#stateId").val(), page:1}).trigger("reloadGrid");
                    reset();
                }); 
                
                $("#lgaId").change(function(event){
                    $("#grid").setGridParam({url: "Communitypharm_grid.action?q=1&stateId="+$("#stateId").val()+"&lgaId="+$("#lgaId").val(), page:1}).trigger("reloadGrid");   
                    reset();
                });

                $("#save_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                        if(validateForm()) {
                            $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');
                            if(updateRecord) {
                                $("#lamisform").attr("action", "Communitypharm_update");                
                            } 
                            else {
                                $("#lamisform").attr("action", "Communitypharm_save");                
                            }
                            $("#loader").html('');
                            return true;                                                            
                        } 
                        else {
                            return false;
                        }
                    }
                }); 
                
                $("#delete_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                    }
                    else {
                        $("#lamisform").attr("action", "Communitpharm_delete");
                    }
                    return true;
                });    

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Setup_page");
                    return true;
                });
                //checkConnection();
            }); 

            function reset() {
                updateRecord = false;
                lastSelected = -99;
                resetButtons();
            };
  
            function checkConnection() {
               $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
               $.ajax({
                    url: "Internet_connection.action",
                    dataType: "json",
                    error: function(jgXHR, status) {
                        alert(status);
                    },
                    success: function(status) {
                        if(status == 1) {
                            if(updateRecord) {
                                $("#save_button").removeAttr("disabled");
                                $("#delete_button").removeAttr("disabled");
                            } 
                            else {
                                $("#save_button").removeAttr("disabled");
                                $("#delete_button").attr("disabled", "disabled");
                            }
                            $("#messageBar").slideUp('slow');                                                            
                        } 
                        else {
                            $("#save_button").attr("disabled", "disabled");
                            $("#delete_button").attr("disabled", "disabled");
                            $("#messageBar").html("No connection established with the LAMIS server, please connect to the internet before proceeding").slideDown('slow');                                
                        }
                    }                    
                }); 
                $("#loader").html('');
            };
  
            function retrieve(obj) {
                $.ajax({
                    url: "LgaId_retrieve.action",
                    dataType: "json",
                    data: {stateId: obj.stateId},
                    success: function(lgaMap) {
                        var options = "<option value = '" + '' + "'>" + '' + "</option>";
                        $.each(lgaMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#lgaId").html(options); 
                        $("#lgaId").val(obj.lgaId);
                        $("#stateId").val(obj.stateId);
                    }                    
                }); //end of ajax call            
            };

            function validateForm() {
                var validate = true;

                // check for valid input is entered
                if($("#stateId").val().length == 0){
                    $("#stateHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#stateHelp").html("");                    
                }
                
                if($("#lgaId").val().length == 0){
                    $("#lgaHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#lgaHelp").html("");                    
                }
                
                if($("#pharmacy").val().length == 0){
                    $("#nameHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#nameHelp").html("");                    
                }
                return validate;
            }                                         
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_setup.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/settings.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Setup >> Drug Setup</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>                        

                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><input name="communitypharmId" type="hidden" id="communitypharmId"/></td>
                                <td width="30%"></td>
                                <td width="20%"></td>
                                <td width="30%"></td>
                            </tr>
                            <tr>
                                <td><label>State:</label></td>
                                <td>
                                    <select name="stateId" style="width: 130px;" class="inputboxes" id="stateId">
                                    </select><span id="stateHelp" class="errorspan"></span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>L.G.A.</label></td>
                                <td>
                                    <select name="lgaId" style="width: 130px;" class="inputboxes" id="lgaId">
                                    </select><span id="lgaHelp" class="errorspan"></span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>Name of Pharmacy:</label></td>
                                <td><input name="pharmacy" type="text" style="width: 300px;" class="inputboxes" id="pharmacy"/><span id="nameHelp" class="errorspan"></span></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>Address:</label></td>
                                <td><input name="address" type="text" style="width: 300px;" class="inputboxes" id="address"/></td>
                            </tr>
                            <tr>
                                <td><label>Telephone:</label></td>
                                <td><input name="phone" type="text" style="width: 200px;" class="inputboxes" id="phone"/></td>
                                <td><label>Alternate Telephone:</label></td>
                                <td><input name="phone1" type="text" style="width: 200px;" class="inputboxes" id="phone1"/></td>
                            </tr>
                            <tr>
                                <td><label>Email:</label></td>
                                <td><input name="email" type="text" style="width: 200px;" class="inputboxes" id="email"/></td>
                            </tr>
                            <tr>
                                <td><label>Activation Code:</label></td>
                                <td><input name="pin" type="text" style="width: 100px;" class="inputboxes" id="pin"/></td>
                            </tr>
                        </table>
                        <p></p>
                        <div>
                            <fieldset>  
                                <legend> Search Result</legend>
                                <table width="99%" height="85" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
