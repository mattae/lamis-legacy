<%-- 
    Document   : Data Export
    Created on : Aug 15, 2012, 6:53:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />

        <script type="text/javascript" src="js/lamis/maintenance-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var timer = {}; 
            $(document).ready(function(){
                initialize();
                timer = $.timer(function() {
                    $.ajax({
                        url: "Processing_status.action",
                        dataType: "json", 
                        success: function(status) {
                            if(status == "terminated") {
                                timer.stop();
                                $("#loader").html('');
                                $("#messageBar").html("Error occured while exporting data, please retry").slideDown('fast');                                 
                            }
                            else {
                                if(status == "completed") {
                                    timer.stop();
                                    $("#loader").html('');
                                    $("#messageBar").html("Export Completed").slideDown('fast');
                                }
                                else {
                                    processingStatusNotifier("Exporting " + status + " table, please wait...");
                                }
                            }
                        }                    
                    }); 
                });
                timer.set({time : 30000, autostart : false});
                
                $("#export_button").bind("click", function(event){
                    timer.play();
                    exportData();
                    return false;
                });
                
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Maintenance_page");
                    return true;
                });
                $('input[type="radio"]').bind("click", function(event){
                    $("#info").slideUp('fast'); 
                    $('input[type="text"]').val("");
                });
            });

            var url = "";
            var x = function wait() {window.open(url);}
 
            function exportData() {
                $("#messageBar").hide(); 
                $("#export_button").attr("disabled", true);
                $.ajax({
                    url: "Export_data.action",
                    dataType: "json",
                    data: {recordsAll : $("#recordsAll").prop("checked")},
                    success: function(fileName) { 
                        timer.stop();
                        $("#loader").html('');
                        $("#messageBar").html("Export Completed").slideDown('fast');     
                        url = fileName;
                        window.setTimeout(x, 3000);
                        $("#export_button").attr("disabled", false);
                    }                    
                });
            }

        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_maintenance.jsp" />  

               <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/tools.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Data Maintenance >> Export Data</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Data Export</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        
                        <table width="99%" height="80" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="5%"></td>
                                <td width="60%">
                                    <p><label><strong>Newly created and modified</strong> records since the last export operation will be &nbsp;included in the export file</label></p>                                   
                                </td>
                                <td width="35%"></td>   
                            </tr>

                            <tr> 
                                <td width="5%"></td>
                                <td width="60%">
                                    <span ><input name="recordsAll" type="checkbox" id="recordsAll"/>&nbsp;<label>Export all transaction records to the export file</label></span>
                                </td>                                    
                            </tr>
                        </table>

                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                        </table>
                        <hr></hr>

                        <div id="buttons" style="width: 200px">
                            <button id="export_button">Export</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>



<!--                        <table width="99%" height="120" border="0" class="space" cellpadding="3">
                            <tr>
                                <td> 
                                    <input type="radio" name="exportOption" value="1" id="recordsNewOrMod" checked="checked" /> <label>Export only new/modified records</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="radio" name="exportOption" value="2" id="recordsAll" /> <label>Export all transaction records</label>
                                </td>
                            </tr>
                        </table>-->