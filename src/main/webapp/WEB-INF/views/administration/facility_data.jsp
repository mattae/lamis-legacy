<%-- 
    Document   : Facility
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/JavaScript">
            var obj = {};
            var updateRecord = false;
            var lastSelected = -99;
            $(document).ready(function(){
                resetPage();
                for(i = 1; i < 31; i++) {
                    $("#dayDqa").append($("<option/>").val(i).html(i));
                }
                $("#dayDqa").val("15");
  
  
                $.ajax({
                    url: "StateId_retrieve.action",
                    dataType: "json",
                    success: function(stateMap) {
                        var options = "<option value = '" + '' + "'>" + '' + "</option>";
                        $.each(stateMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#stateId").html(options);                        
                    }                    
                }); //end of ajax call

                $("#grid").jqGrid({
                    url: "Facility_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Facility", "Address", "Phone1", "Phone2", "Email", "Address1", "Address2", "FacilityType", "StateId", "LgaId", "PadHospitalNum", "DayDqa"],
                    colModel: [
                        {name: "name", index: "name", width: "200"},
                        {name: "address", index: "address", width: "250"},
                        {name: "phone1", index: "phone1", width: "150"},
                        {name: "phone2", index: "phone2", width: "150"},
                        {name: "email", index: "email", width: "150", sortable:false, hidden:true},                        
                        {name: "address1", index: "address1", width: "0", sortable:false, hidden:true},
                        {name: "address2", index: "address2", width: "0", sortable:false, hidden:true},
                        {name: "facilityType", index: "facilityType", width: "0", sortable:false, hidden:true},                        
                        {name: "stateId", index: "stateId", width: "0", sortable:false, hidden:true},                        
                        {name: "lgaId", index: "lgaId", width: "0", sortable:false, hidden:true},                        
                        {name: "padHospitalNum", index: "padHospitalNum", width: "0", sortable:false, hidden:true},                        
                        {name: "dayDqa", index: "dayDqa", width: "0", sortable:false, hidden:true},                        
                    ],
                    rowNum: -1,
                    sortname: "facilityId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 220,                    
                    jsonReader: {
                        root: "facilityList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "facilityId"
                    },
                    onSelectRow: function(id) {
                        var data = $("#grid").getRowData(id)
                        $("#name").val(data.name);
                        $("#address1").val(data.address1)
                        $("#address2").val(data.address2)
                        $("#phone1").val(data.phone1)
                        $("#phone2").val(data.phone2)
                        $("#email").val(data.email)
                        $("#dayDqa").val(data.dayDqa)
                        $("#facilityType").val(data.facilityType)
                        if(data.padHospitalNum == "1") {
                           $("#padHospitalNum").attr("checked", "checked"); 
                        }
                        else {
                           $("#padHospitalNum").removeAttr("checked");                             
                        }
                        $("#facilityId").val(id);
                        obj.stateId =  data.stateId;
                        obj.lgaId =  data.lgaId;
                        retrieve(obj);
                        
                        updateRecord = true;
                        lastSelected = id;                        
                        initButtonsForModify()
                    }               
                }); //end of jqGrid 

                $("#stateId").change(function(event){
                    $.ajax({
                        url: "LgaId_retrieve.action",
                        dataType: "json",
                        data: {stateId: $("#stateId").val()},

                        success: function(lgaMap) {
                            var options = "<option value = '" + '' + "'>" + '' + "</option>";
                            $.each(lgaMap, function(key, value) {
                                options += "<option value = '" + key + "'>" + value + "</option>";
                            }) //end each
                            $("#lgaId").html(options);
                        }                    
                    }); //end of ajax call
                    $("#grid").setGridParam({url: "Facility_grid.action?q=1&stateId="+$("#stateId").val(), page:1}).trigger("reloadGrid");
                    reset();
                }); 
                
                $("#lgaId").change(function(event){
                    $("#grid").setGridParam({url: "Facility_grid.action?q=1&stateId="+$("#stateId").val()+"&lgaId="+$("#lgaId").val(), page:1}).trigger("reloadGrid");   
                    reset();
                });

                $("#save_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                        if(validateForm()) {
                            $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');
                            if(updateRecord) {
                                $("#lamisform").attr("action", "Facility_update");                
                            } 
                            else {
                                $("#lamisform").attr("action", "Facility_save");                
                            }
                            $("#loader").html('');
                            return true;                                                            
                        } 
                        else {
                            return false;
                        }
                    }
                });      
                $("#delete_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                    }
                    else {
                        $("#lamisform").attr("action", "Facility_delete");
                    }
                    return true;
                });    

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Setup_page");
                    return true;
                });
                //checkConnection();
            }); 

            function reset() {
                updateRecord = false;
                lastSelected = -99;
                resetButtons();
            };
  
            function checkConnection() {
               $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
               $.ajax({
                    url: "Internet_connection.action",
                    dataType: "json",
                    error: function(jgXHR, status) {
                        alert(status);
                    },
                    success: function(status) {
                        if(status == 1) {
                            if(updateRecord) {
                                $("#save_button").removeAttr("disabled");
                                $("#delete_button").removeAttr("disabled");
                            } 
                            else {
                                $("#save_button").removeAttr("disabled");
                                $("#delete_button").attr("disabled", "disabled");
                            }
                            $("#messageBar").slideUp('slow');                                                            
                        } 
                        else {
                            $("#save_button").attr("disabled", "disabled");
                            $("#delete_button").attr("disabled", "disabled");
                            $("#messageBar").html("No connection established with the LAMIS server, please connect to the internet before proceeding").slideDown('slow');                                
                        }
                    }                    
                }); 
                $("#loader").html('');
            };
  
            function retrieve(obj) {
                $.ajax({
                    url: "LgaId_retrieve.action",
                    dataType: "json",
                    data: {stateId: obj.stateId},
                    success: function(lgaMap) {
                        var options = "<option value = '" + '' + "'>" + '' + "</option>";
                        $.each(lgaMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#lgaId").html(options); 
                        $("#lgaId").val(obj.lgaId);
                        $("#stateId").val(obj.stateId);
                    }                    
                }); //end of ajax call            
            };

            function validateForm() {
                var validate = true;

                // check for valid input is entered
                if($("#stateId").val().length == 0){
                    $("#stateHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#stateHelp").html("");                    
                }
                
                if($("#lgaId").val().length == 0){
                    $("#lgaHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#lgaHelp").html("");                    
                }
                
                if($("#name").val().length == 0){
                    $("#nameHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#nameHelp").html("");                    
                }
                return validate;
            }                                         
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_setup.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/settings.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Setup >> Facility Setup</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>                        

                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><input name="facilityId" type="hidden" id="facilityId"/></td>
                                <td width="30%"></td>
                                <td width="20%"></td>
                                <td width="30%"></td>
                            </tr>
                            <tr>
                                <td><label>State:</label></td>
                                <td>
                                    <select name="stateId" style="width: 130px;" class="inputboxes" id="stateId">
                                    </select><span id="stateHelp" class="errorspan"></span>
                                </td>
                                <td><label>L.G.A.</label></td>
                                <td>
                                    <select name="lgaId" style="width: 130px;" class="inputboxes" id="lgaId">
                                    </select><span id="lgaHelp" class="errorspan"></span>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Facility Name:</label></td>
                                <td><input name="name" type="text" style="width: 248px;" class="inputboxes" id="name"/><span id="nameHelp" class="errorspan"></span></td>
                                <td><label>Type of Facility:</label></td>
                                <td>
                                    <select name="facilityType" style="width: 130px;" class="inputboxes" id="facilityType"/>
                                       <option></option>
                                       <option>Primary</option>
                                       <option>Secondary</option>
                                       <option>Tertiary</option>
                                       <option>Private</option>
                                    </select><span id="typeHelp" class="errorspan"></span>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Address:</label></td>
                                <td><input name="address1" type="text" style="width: 200px;" class="inputboxes" id="address1"/></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input name="address2" type="text" style="width: 200px;" class="inputboxes" id="address2"/></td>
                            </tr>
                            <tr>
                                <td><label>Telephone:</label></td>
                                <td><input name="phone1" type="text" style="width: 200px;" class="inputboxes" id="phone1"/></td>
                                <td><label>SMS Printer Phone:</label></td>
                                <td><input name="phone2" type="text" style="width: 200px;" class="inputboxes" id="phone2"/></td>
                            </tr>
                            <tr>
                                <td><label>Email:</label></td>
                                <td><input name="email" type="text" style="width: 200px;" class="inputboxes" id="email"/></td>
                            </tr>
                        </table>
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Settings</td>
                            </tr>
                        </table>
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="18%"><label>Day for DQA:</label></td>
                                <td width="30%">
                                    <select name="dayDqa" style="width: 50px;" class="inputboxes" id="dayDqa"/>
                                       <option></option>
                                    </select>
                                </td>
                                <td width="20%"></td>
                                <td width="30%"></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input name="padHospitalNum" type="checkbox" value="0" id="padHospitalNum"/>&nbsp;<label>Enable padding on hospital number</label></td>                                
                                <td colspan="2"><input name="defaultFacility" type="checkbox" value="0" id="defaultFacility"/>&nbsp;<label>Use as Default Facility?</label></td>                                
                            </tr>
                        </table>
                        
                        <p></p>
                        <div>
                            <fieldset>  
                                <legend> Search Result</legend>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
