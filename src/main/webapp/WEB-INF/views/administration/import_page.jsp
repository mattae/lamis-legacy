<%-- 
    Document   : Data Export
    Created on : Aug 15, 2012, 6:53:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />

        <script type="text/javascript" src="js/lamis/maintenance-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var timer = {};
            $(document).ready(function(){
                initialize();
           
                timer = $.timer(function() {
                    $.ajax({
                        url: "Processing_status.action",
                        dataType: "json",
                        success: function(status) {
                            if(status == "terminated") {
                                timer.stop();
                                $("#loader").html('');
                                $("#messageBar").html("Error occured while importing data, please retry").slideDown('fast');                                 
                            }
                            else {
                                if(status == "completed") {
                                    timer.stop();
                                    $("#loader").html('');
                                    $("#messageBar").html("Import Completed").slideDown('fast');
                                }
                                else {
                                    processingStatusNotifier("Importing " + status + " table, please wait...");
                                }

                            }
                        }                    
                    }); 
                });
                timer.set({time : 60000, autostart : false});

                $("#import_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                        if(validateUpload()) {
                            $("#lamisform").attr("method", "post");
                            $("#lamisform").attr("enctype", "multipart/form-data");
                            $("#lamisform").attr("action", "Upload_file");
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                });
                
                if($("#fileUploaded").html() == 1) {
                    timer.play();
                    importData();                        
                }

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Maintenance_page");
                    return true;
                });
            });
            
            function importData() {  
                $("#messageBar").hide();
                $("#import_button").attr("disabled", true);
                $("#attachment").attr("disabled", true);
                $.ajax({
                    url: "Import_data.action",
                    dataType: "json",
                    success: function(status) {
                        timer.stop();
                        $("#loader").html('');
                        $("#messageBar").html("Import Completed").slideDown('fast');     
                        $("#import_button").attr("disabled", false);
                        $("#attachment").attr("disabled", false);
                    }                    
                });
            }

            function validateUpload() {
                var validate = true;

                // check if file name is entered
                if($("#attachment").val().length == 0) {
                    $("#fileHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#fileHelp").html("");
                }
                return validate;
            }            
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_maintenance.jsp" />  

                <div id="rightPanel">
                <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/tools.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Data Maintenance >> Import Data</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Data Import</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td></td>
                                <td colspan="3"><label>File Name: </label><input type="file" name="attachment" class="inputboxes" id="attachment"/><span id="fileHelp" class="errorspan"></span></td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                        </table> 
                        <hr></hr>
                        
                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="fileUploaded" style="display: none"><s:property value="#session.fileUploaded"/></div>
                        <div id="buttons" style="width: 200px">
                            <button id="import_button">Import</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
