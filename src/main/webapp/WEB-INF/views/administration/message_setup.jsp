<%-- 
    Document   : SMS Service
    Created on : Aug 15, 2012, 6:53:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />

        <script type="text/javascript" src="js/lamis/maintenance-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var updateRecord = false;
            $(document).ready(function(){
                initialize();
                
                $("#dialog").dialog({
                    title: "Modem setting",
                    autoOpen: false,
                    width: 500,
                    resizable: false,
                    buttons: [{text: "Save", click: saveModem}, 
                        {text: "Close", click: function(){$(this).dialog("close")}}]
                });
  
                $("#dialog1").dialog({
                    title: "Send Message",
                    autoOpen: false,
                    width: 500,
                    resizable: false,
                    buttons: [{text: "Send", click: sendMessage}, 
                        {text: "Close", click: function(){$(this).dialog("close")}}]
                });
        
                $("#grid").jqGrid({
                    url: "Message_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Message Type", "Date to Send", "Message", "", "", "", "", "", "", ""],
                    colModel: [
                        {name: "msgType", index: "msgType", width: "200"},
                        {name: "dateToSend", index: "date", width: "100", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},
                        {name: "recipients", index: "recipients", width: "100"},
                        {name: "message1", index: "message1", width: "350"},
                        {name: "message2", index: "message2", width: "0", hidden:true},
                        {name: "message3", index: "message3", width: "0", hidden:true},
                        {name: "message4", index: "message4", width: "0", hidden:true},
                        {name: "messageType", index: "messageType", width: "0", hidden: true},
                        {name: "daysToAppointment", index: "daysToAppointment", width: "0", hidden:true},
                        {name: "dateToSend", index: "dateToSend", width: "0",  hidden: true},
                    ],
                    rowNum: -1,
                    sortname: "messageId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 200,                    
                    jsonReader: {
                        root: "messageList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "messageId"
                    },
                    onSelectRow: function(id) {
                        var data = $("#grid").getRowData(id)
                        $("#messageType").val(data.messageType);
                        $("#daysToAppointment").val(data.daysToAppointment);
                        var date = data.dateToSend;
                        if(date != "" && date.length != 0) {
                            $("#date").val(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));                            
                        }
                        else {
                            $("#date").val("");
                        }
                        $("#dateToSend").val(data.dateToSend);
                        $("#recipients").val(data.recipients);
                        $("#message1").val(data.message1);
                        $("#message2").val(data.message2);
                        $("#message3").val(data.message3);
                        $("#message4").val(data.message4);
                        $("#messageId").val(id);
                        
                        updateRecord = true;
                        lastSelected = id;
                        initButtonsForModify();
                        resetMsg();
                    }               
                }); //end of jqGrid 
                
                $.ajax({
                    url: "Modem_retrieve.action",
                    dataType: "json",                    
                    success: function(modemList) {
                        populateForm(modemList);
                    }                    
                }); 
                
                $("#date").attr("disabled", "disabled");                                   
                $("#messageType").bind("change", function() {
                    resetMsg();
                });
                
                $("#show_dialog").bind("click", function() {
                    $("#dialog").dialog("open");
                    return false;
                });
                $("#show_dialog1").bind("click", function() {
                    $("#dialog1").dialog("open");
                    return false;
                });
                                
                $("#save_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                        if(validateMessageForm()) {
                            if(updateRecord) {
                                $("#lamisform").attr("action", "Message_update");                
                            } 
                            else {
                                $("#lamisform").attr("action", "Message_save");                
                            }
                            return true;                        
                        } 
                        else {
                            return false;
                        }
                    }
                });      
                $("#delete_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                    }
                    else {
                        $("#lamisform").attr("action", "Message_delete");
                    }
                    return true;
                });    
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Setup_page");
                    return true;
                });                
            });
            
            function saveModem() {
                if(validateModemForm()) {
                    $.ajax({
                        url: "Modem_save.action",
                        dataType: "json",                    
                        data: {
                            comPort: $("#comPort").val(),
                            baudRate: $("#baudRate").val(),
                            manufacturer: $("#manufacturer").val(),
                            model: $("#model").val(),
                            countryCode: $("#countryCode").val()
                        }
                    }); 
                    $("#dialog").dialog("close");
                } 
            }
            
            function sendMessage() {
                $.ajax({
                    url: "TestMessage_send.action",
                    dataType: "json",                    
                    data: {
                        phone: $("#phone").val(),
                        message: $("#message").val(),
                    }
                }); 
                $("#dialog1").dialog("close");
            }
            
            function resetMsg() {
                var msg = $("#messageType").val();
                if(msg == 1) {
                    $("#daysToAppointment").removeAttr("disabled");
                    $("#date").attr("disabled", "disabled");
                    $("#date").val("");
                }
                else {
                    if(msg == 3) {
                        $("#date").removeAttr("disabled");                                                    
                        $("#daysToAppointment").attr("disabled", "disabled");
                        $("#daysToAppointment").val("");
                    }
                    else {
                        $("#daysToAppointment").attr("disabled", "disabled");
                        $("#date").attr("disabled", "disabled");                                                                            
                        $("#daysToAppointment").val("");
                        $("#date").val("");
                    }
                }                                  
            }
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_setup.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/settings.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Setup >> SMS Setup</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Message Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td width="20%"><label>Message Type:</label></td>
                                    <td width="80%">
                                       <select name="messageType" style="width: 160px;" id="messageType">
                                            <option value=1>Appointment messages</option>
                                            <option value=2>Daily messages</option>
                                            <option value=3>Scheduled messages</option>
                                       </select>
                                        
                                       <select name="daysToAppointment" style="width: 50px;" id="daysToAppointment">
                                            <option></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select><span id="daysToAppointmentHelp" style="color:red"></span><label> days to appointment date </label>                                                                                  
                                    </td>
                                    <td><input name="messageId" type="hidden" class="inputboxes" id="messageId"/></td>                              
                                </tr>
                                <tr>
                                    <td><label>Date to Send:</label></td>
                                    <td><input name="date" type="text" style="width: 100px;" class="inputboxes" id="date"/><input name="dateToSend" type="hidden" id="dateToSend"/><span id="dateHelp" style="color:red"></span></td>
                                </tr>
                                <tr>
                                    <td><label>Message Recipients:</label></td>
                                    <td>
                                        <select name="recipients" style="width: 130px;" id="recipients">
                                            <option>All</option>
                                            <option>ART Only</option>
                                            <option>Non ART Only</option>
                                            <option>Defaulter Only</option>
                                        </select>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Message (English):</label></td>
                                    <td><input name="message1" type="text" style="width: 300px;" class="inputboxes" id="message1"/><span id="messageHelp" style="color:red"></span></td>
                                </tr>
                                <tr>
                                    <td><label>Message (Hausa):</label></td>
                                    <td><input name="message2" type="text" style="width: 300px;" class="inputboxes" id="message2"/></td>
                                </tr>
                                <tr>
                                    <td><label>Message (Igbo):</label></td>
                                    <td><input name="message3" type="text" style="width: 300px;" class="inputboxes" id="message3"/></td>
                                </tr>
                                <tr>
                                    <td><label>Message (Yoruba):</label></td>
                                    <td><input name="message4" type="text" style="width: 300px;" class="inputboxes" id="message4"/></td>
                                </tr>
                                <tr>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                        </table>
                        <div>
                            <button id="show_dialog">Modem settings...</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button id="show_dialog1">Send Message...</button>
                            <fieldset>  
                                <legend> Message List </legend>
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                         <table id="grid"></table>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <div id="dialog">
                            <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td><label>COM Port:</label></td>
                                    <td>
                                        <select name="comPort" style="width: 130px;" id="comPort">
                                            <option></option>
                                            <option>COM1</option>
                                            <option>COM2</option>
                                            <option>COM3</option>
                                            <option>COM4</option>
                                            <option>COM5</option>
                                            <option>COM6</option>
                                            <option>COM7</option>
                                            <option>COM8</option>
                                            <option>COM9</option>
                                            <option>COM10</option>

                                            <option>COM11</option>
                                            <option>COM12</option>
                                            <option>COM13</option>
                                            <option>COM14</option>
                                            <option>COM15</option>
                                            <option>COM16</option>
                                            <option>COM17</option>
                                            <option>COM18</option>
                                            <option>COM19</option>
                                            <option>COM20</option>

                                            <option>COM21</option>
                                            <option>COM22</option>
                                            <option>COM23</option>
                                            <option>COM24</option>
                                            <option>COM25</option>
                                            <option>COM26</option>
                                            <option>COM27</option>
                                            <option>COM28</option>
                                            <option>COM29</option>
                                            <option>COM30</option>

                                            <option>COM31</option>
                                            <option>COM32</option>
                                            <option>COM33</option>
                                            <option>COM34</option>
                                            <option>COM35</option>
                                            <option>COM36</option>
                                            <option>COM37</option>
                                            <option>COM38</option>
                                            <option>COM39</option>
                                            <option>COM40</option>

                                            <option>COM41</option>
                                            <option>COM42</option>
                                            <option>COM43</option>
                                            <option>COM44</option>
                                            <option>COM45</option>
                                            <option>COM46</option>
                                            <option>COM47</option>
                                            <option>COM48</option>
                                            <option>COM49</option>
                                            <option>COM50</option>
                                        </select>
                                        <span id="comPortHelp" style="color:red"></span>
                                    </td>
                                    <td><label>Baud Rate:</label></td>
                                    <td><select name="baudRate" style="width: 125px;" id="baudRate">
                                            <option></option>
                                            <option>9600</option>
                                            <option>19200</option>
                                            <option>38400</option>
                                            <option>56000</option>
                                            <option>57600</option>
                                            <option>115200</option>
                                            <option>230400</option>
                                            <option>460800</option>
                                        </select><span id="baudRateHelp" style="color:red"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Manufacturer:</label></td>
                                    <td>
                                        <select name="manufacturer" style="width: 130px;" id="manufacturer">
                                            <option></option>
                                            <option>Billionton</option>
                                            <option>EagleTec</option>
                                            <option>ITengo</option>
                                            <option>Huawei</option>
                                            <option>ZTE</option>
                                            <option>Janus</option>
                                            <option>Nokia</option>
                                            <option>Multitech</option>
                                            <option>Sharp</option>
                                            <option>Siemens</option>

                                            <option>SIMCOM</option>
                                            <option>Sony Ericsson</option>
                                            <option>Ubinetics</option>
                                            <option>Wavecom</option>
                                            <option>Motorola</option>
                                            <option>Teltonika</option>
                                            <option>Samsung</option>
                                            <option>Samba</option>
                                            <option>Rogers</option>
                                            <option>Falcom</option>

                                            <option>Fargo Maestro 20</option>
                                            <option>BandLuxe</option>
                                            <option>SIM548C GSM module</option>
                                            <option>Karbonn</option>
                                            <option>D-Link</option>
                                        </select><span id="manufacturerHelp" style="color:red"></span>
                                    </td>
                                    <td><label>Model:</label></td>
                                    <td><input name="model" type="text" class="inputboxes" id="model"/><span id="modelHelp" style="color:red"></span></td>
                                </tr>
                                <tr>
                                    <td width="20%"><label>Country Code:</label></td>
                                    <td><input name="countryCode" type="text" class="inputboxes" id="countryCode"/><span id="countryCodeHelp" style="color:red"></span></td>
                                </tr>   
                            </table>
                        </div>
                        <div id="dialog1">
                             <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td><label>Message:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="message" type="text" style="width: 200px;" class="inputboxes" id="message"/></td>
                                </tr>
                                <tr>
                                    <td><label>Phone Number:</label>&nbsp;&nbsp;&nbsp;<input name="phone" type="text" style="width: 100px;" class="inputboxes" id="phone"/></td>
                                </tr>
                             </table>
                        </div>
                        <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
