<%-- 
    Document   : validation_page
    Created on : Aug 7, 2017, 10:38:24 AM
    Author     : DURUANYANWU IFEANYI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">            
            var url = "";
            var entity_global;            
            $(document).ready(function(){ 
                                $("body").bind('ajaxStart', function(event){
                    $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                });

                $("body").bind('ajaxStop', function(event){
                    $("#loader").html('');
                });
                $("#messageBar").hide();


                $("#grid").jqGrid({
                    url: "Enrolled_nonTX.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["", "Entity", "Count"],
                    colModel: [
                        {name: "id", index: "id", hidden: true, width: "1"},
                        {name: "entity", index: "entity", width: "550"},
                        {name: "entityCount", index: "entityCount", width: "190"}
                    ],  
                    sortname: "id",
                    sortorder: "asc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,                
                    height: 150,                    
                    jsonReader: {
                        root: "notificationList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "id"
                    },
                    onSelectRow: function(id) {
                        $("#messageBar").hide();
                        var selectedRow = $("#grid").getGridParam('selrow');
                        entity_global = null;
                        if(selectedRow != null) {
                            var data = $("#grid").getRowData(selectedRow);
                            var entity = data.id;
                            entity_global = entity;
                            //console.log("The ID is: "+id);
                            //if(data.entityCount > 0){ 
                                $("#detail").jqGrid("clearGridData", true).setGridParam({url: "Enrolled_nonTXDetail.action?entity="+entity, page:1}).trigger("reloadGrid");
                                $("#generate_button").attr("disabled", false);
                            //}
                        }
                        
                    }               
                }); //end of master jqGrid                 
                
                //Start for the details page...
                $("#detail").jqGrid({
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["", "Registration Date", "Hospital Number", "Name", "Status"],
                    colModel: [
                        {name: "patientId", index: "patientId", hidden: true, width: "30"},
                        {name: "dateRegistration", index: "dateRegistration", width: "195", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},
                        {name: "hospitalNum", index: "hospitalNum", width: "170"},
                        {name: "name", index: "name", width: "250"},
                        {name: "statusRegistration", index: "statusRegistration", width: "120"}
                    ],
                    pager: $('#pager'),
                    rowNum: -1,
                    sortname: "patientId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    loadtext: "Wait, while record loads...", 
                    resizable: false,   
//                    multiselect: true,
                    height: 160,                    
                    jsonReader: {
                        root: "notificationListReport",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "patientId"
                    }
                    //,      
//                    onSelectRow: function(id) {
//                        var data = $(this).getRowData(id);
//                        if(data.sel == 1) {
//                            $(this).jqGrid('setRowData', id, true, {color: 'black'});
//                        }
//                        else {
//                            $(this).jqGrid('setRowData', id, false, {color: 'black'});                            
//                        }
//                        
//                    }
                }); //end of detail jqGrid                 
                
                $("#generate_button").bind("click", function(event){
                    //formatDateFields();
                    event.preventDefault();
                    event.stopPropagation();
                    //console.log("Global Entity is: "+entity_global);
                    url = "";
                    url += "entity="+entity_global;
                    if($('[name="reportFormat"]:checked').val() == "pdf") {
                        url = "Patient_Notification_list.action?"+url;                            
                        window.open(url);
                        return false;                        
                    }
                    else {
                        url += "&recordType=1&entity="+entity_global;
                        url = "Converter_dispatch.action?"+url;                            
                        convertData();
                    }
                });
                
                $("#close_button").bind("click", function(event){
                      window.location.href = "Home_page.action";
                });
            });           
            
            var x = function wait() {window.open(url);}
            
            function convertData() {                
                $("#messageBar").hide();
                $("#generate_button").attr("disabled", true);
                $.ajax({
                    url: url,
                    dataType: "json",
                    success: function(fileName) {
                        $("#messageBar").html("Conversion Completed").slideDown('fast');
                        url = fileName;
                        window.setTimeout(x, 3000);
                        //$("#messageBar").html("Conversion Completed").fadeOut('slow');
                    }                    
                }); 
                $("#generate_button").attr("disabled", false);
            }
            
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_home.jsp" />  

                <div id="rightPanel">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/notification.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Home >> Notifications</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        <p></p>
                        
                        <div>
                            <fieldset> 
                            <legend>Records Listing</legend>                            
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>                                       
                                        <p></p>
                                        <table id="detail"></table>
                                        <div id="pager" style="text-align:center;"></div>
                                    </td>
                                  </tr>                              
                                </table>
                            </fieldset>  
                        </div>
                        <p></p>
                        <div style="margin-right: 10px;">
                            <fieldset> 
                                <legend> Report format </legend>                            
                                <p></p>                                                                                                                                                                                                                            
                                <table width="99%" border="0" class="space" cellpadding="3">
                                    <tr>
                                        <td colspan="2"><input type="radio" name="reportFormat" value="pdf" checked/><label>Generate report in PDF format </label></td>
                                        <td colspan="2"><input type="radio" name="reportFormat" value="cvs"/><label>Generate report and convert report to MS Excel </label></td>
                                    </tr>                            
                                    <tr>
                                        <td colspan="2"></td>
                                        <td colspan="2">                                        
                                            <span style="margin-left:20px"><label>This report format contains more client information than PDF </label></span>
                                        </td>
                                    </tr>
                                    <tr> </tr>
                                </table>
                            </fieldset>  
                        </div>								
                        <p></p> 
                        
                        <div id="buttons" style="width: 200px">
                            <button id="generate_button" disabled="true">Generate</button>&nbsp;<button id="close_button">Close</button>
                        </div> 
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
