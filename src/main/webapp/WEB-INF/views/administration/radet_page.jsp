<%-- 
    Document   : Facility
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/updater-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/json.min.js"></script>   		
        <script type="text/JavaScript">
            var obj = {};
            var url = "";
            $(document).ready(function(){
                resetPage();
                initialize();

                $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	

                $("#grid").jqGrid({
                    url: "Client_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Hospital No.", "Unique ID", "Name", "Gender", "ART Start", "Last Pickup", "Regimen (ART Start)", "Current Status", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
                    colModel: [
                        {name: "hospitalNum", index: "hospitalNum", width: "70"},
                        {name: "uniqueId", index: "uniqueId", width: "70"},
                        {name: "name", index: "name", width: "140"},
                        {name: "gender", index: "gender", width: "50"},
                        {name: "dateStarted", index: "date1", width: "70", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},
                        {name: "dateLastRefill", index: "date2", width: "70", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},
                        {name: "regimenStart", index: "regimenStart", width: "155"},
                        {name: "currentStatus", index: "currentStatus", width: "100"},
                        {name: "patientId", index: "patientId", hidden: true},
                        {name: "dateRegistration", index: "dateRegistration", hidden: true},
                        {name: "regimentypeStart", index: "regimentypeStart", hidden: true},
                        {name: "regimentype", index: "regimentype", hidden: true},
                        {name: "regimen", index: "regimen", hidden: true},
                        {name: "regimenId", index: "regimenStart", hidden: true},
                        {name: "regimentypeId", index: "regimentypeStart", hidden: true},
                        {name: "duration", index: "duration", hidden: true},
                        {name: "surname", index: "surname", hidden: true},
                        {name: "otherNames", index: "otherNames", hidden: true},
                        {name: "dateBirth", index: "dateBirth", hidden: true},
                        {name: "age", index: "age", hidden: true},
                        {name: "ageUnit", index: "ageUnit", hidden: true},
                        {name: "dateCurrentStatus", index: "dateCurrentStatus", hidden: true},
                        {name: "clinicStage", index: "clinicStage", hidden: true},
                        {name: "funcStatus", index: "funcStatus", hidden: true},
                        {name: "cd4", index: "cd4", hidden: true},
                        {name: "cd4p", index: "cd4p", hidden: true},
                        {name: "statusRegistration", index: "statusRegistration", hidden: true},
                        {name: "dateStarted", index: "dateStarted", hidden: true},
                        {name: "dateLastRefill", index: "dateLastRefill", hidden: true},
                        {name: "enrollmentSetting", index: "enrollmentSetting", hidden: true},
                        {name: "dateCurrentViralLoad", index: "dateCurrentViralLoad", hidden: true},
                        {name: "dateCollected", index: "dateCollected", hidden: true},                        
                        {name: "viralLoad", index: "viralLoad", hidden: true},
                        {name: "viralLoadIndication", index: "viralLoadIndication", hidden: true},
                        {name: "category", index: "category", hidden: true},
                    ],
                    rowNum: -1,
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,                    
                    height: 220,
                    loadtext: "Retrieving records, please wait...",                    
                    jsonReader: {
                        root: "clientList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false
                    },
                    afterInsertRow: function(id, data) {
                        if(data.category == "1") {
                            $(this).jqGrid('setRowData', id, false, {background: 'khaki'});
                        }                        
                        if(data.category == "2") {
                            $(this).jqGrid('setRowData', id, false, {background: 'lightblue'});
                        }                        
                        if(data.category == "3") {
                            $(this).jqGrid('setRowData', id, false, {background: 'lime'});
                        }                        
                        if(data.category == "4") {
                            $(this).jqGrid('setRowData', id, false, {background: 'yellow'});
                        }                        
                    },
                    loadComplete: function() {
                        $("#save_button").attr("disabled", true);
                        $("#loader").html('');                                
                    },
                    onSelectRow: function(id) {
                        $("#save_button").attr("disabled", false);
                        $("#messageBar").hide();
                        var data = $("#grid").getRowData(id);
                        $("#patientId").val(data.patientId);
                        $("#hospitalNum").val(data.hospitalNum);
                        $("#uniqueId").val(data.uniqueId);
                        $("#currentStatus").val(data.currentStatus);
                        $("#surname").val(data.surname);
                        $("#otherNames").val(data.otherNames);
                        $("#age").val(data.age);
                        $("#ageUnit").val(data.ageUnit);
                        $("#gender").val(data.gender);
                        $("#statusRegistration").val(data.statusRegistration);
                        $("#statusRegistration").val(data.statusRegistration);
                        $("#enrollmentSetting").val(data.enrollmentSetting);                        
                        $("#clinicStage").val(data.clinicStage);
                        $("#funcStatus").val(data.funcStatus);
                        $("#clinicStage").val(data.clinicStage);
                        $("#cd4").val(data.cd4);
                        $("#cd4p").val(data.cd4p);
                        $("#dateBirth").val(data.dateBirth);
                        $("#date1").val(dateSlice(data.dateBirth));
                        $("#dateRegistration").val(data.dateRegistration);
                        $("#date2").val(dateSlice(data.dateRegistration));
                        $("#dateCurrentStatus").val(data.dateCurrentStatus);
                        $("#date3").val(dateSlice(data.dateCurrentStatus));
                        $("#dateStarted").val(data.dateStarted);
                        $("#date4").val(dateSlice(data.dateStarted));
                        $("#dateLastRefill").val(data.dateLastRefill);
                        $("#date5").val(dateSlice(data.dateLastRefill));
                        $("#duration").val(data.duration);
                        $("#dateCurrentViralLoad").val(data.dateCurrentViralLoad);
                        $("#date6").val(data.dateCurrentViralLoad);
                        $("#dateCollected").val(data.dateCollected);
                        $("#date7").val(data.dateCollected);
                        $("#viralLoad").val(data.viralLoad);
                        $("#viralLoadIndication").val(data.viralLoadIndication);
                        retrieveRegimen(data);
                        retrieveRegimenId(data);
                    }               
                }); //end of jqGrid 
                 
                $("#radet_link1").bind("click", function(event){               
                     var url = "Radet_report.action?reportType=1";
                     event.preventDefault();
                     event.stopPropagation();
                     window.open(url);
                     return false;
                });              
                $("#radet_link2").bind("click", function(event){
                     var url = "Radet_report.action?reportType=2";
                     event.preventDefault();
                     event.stopPropagation();
                     window.open(url);
                     return false;
                });              
                $("#radet_link3").bind("click", function(event){ 
                     var url = "Radet_report.action?reportType=3";
                     event.preventDefault();
                     event.stopPropagation();
                     window.open(url);
                     return false;
                });              
                $("#radet_link4").bind("click", function(event){
                     var url = "Radet_report.action?reportType=4";
                     event.preventDefault();
                     event.stopPropagation();
                     window.open(url);
                     return false;
                });                              
            });
            
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_maintenance.jsp" />  

               <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                             <tr>
                                <td>
                                    <span>
                                        <img src="images/tools.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Data Maintenance >> RADET Analyzer & Data Update</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">RADET Analysis</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        <!-- <span style="margin-left:650px"><button id="print_button" disabled="true">Convert to Excel</button></span>-->

                        <div>
                            <table width="99%" height="90" border="0" class="space">
                                <tr></tr>
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                    </td>
                                </tr>
                                <tr></tr>
                            </table>
                            <p></p>
                            <div id="legend" style="height:20px"><span style="margin-left:8px"></span><span id="khaki" style="height:10px; background-color: khaki">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span id="radet_link1" style="color:blue"> No ARV Pickup</span> &nbsp;&nbsp;&nbsp;<span id="lightblue" style="height:10px; background-color: lightblue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span id="radet_link2" style="color:blue"> No Regimen at ART Start</span> &nbsp;&nbsp;&nbsp;&nbsp;<span id="lime" style="height:10px; background-color: lime">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span id="radet_link3" style="color:blue"> Lost to Follow Up Unconfirmed</span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="yellow" style="height:10px; background-color: yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span id="radet_link4" style="color:blue"> Due for Viral Load Test</span>&nbsp;&nbsp;&nbsp;</div>
                            <p></p>
                        </div>
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Patient Data Update</td>
                            </tr>
                        </table>
                        <div id="section">
                            <div id="container" style="height: 280px; overflow-y: scroll; margin-top: 10px">
                                <div style="border: 1px solid #ccc">
                                    <table width="99%" border="0" class="space" cellpadding="3">
                                        <tr>
                                            <td width="20%"><label>Hospital No:</label></td>
                                            <td width="30%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/></td>
                                            <td width="20%"><label>Unique ID:</label></td>
                                            <td width="30%"><input name="uniqueId" type="text" class="inputboxes" id="uniqueId"/></td>
                                        </tr>
                                        <tr>
                                            <td><label>Surname:</label></td>
                                            <td><input name="surname" type="text" class="inputboxes" id="surname"/><span id="surnameHelp" class="errorspan"></span></td>
                                            <td><label>Other Names:</label></td>
                                            <td><input name="otherNames" type="text" class="inputboxes" id="otherNames"/></td>
                                        </tr>
                                        <tr>
                                            <td><label>Date of Birth:</label></td>
                                            <td><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateBirth" type="hidden" id="dateBirth"/></td>
                                            <td><label>Age at Registration:</label></td>
                                            <td><input name="age" type="text" style="width: 50px;" class="inputboxes" id="age"/>
                                                <select name="ageUnit" style="width: 75px;" class="inputboxes" id="ageUnit">
                                                    <option></option>
                                                    <option>year(s)</option>
                                                    <option>month(s)</option>
                                                    <option>day(s)</option>
                                                </select><span id="ageHelp" class="errorspan"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Gender:</label></td>
                                            <td>
                                                <select name="gender" style="width: 130px;" class="inputboxes" id="gender">
                                                    <option></option>
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                </select><span id="genderHelp" class="errorspan"></span>
                                            </td>                            
                                            <td><label>HIV Status at registration:</label></td>
                                            <td>
                                                <select name="statusRegistration" style="width: 200px;" class="inputboxes" id="statusRegistration"/>
                                                   <option></option>
                                                   <option>HIV exposed status unknown</option>
                                                   <option>HIV+ non ART</option>
                                                   <option>ART Transfer In</option>
                                                   <option>Pre-ART Transfer In</option>
                                                </select><span id="statusregHelp" class="errorspan"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Date of registration/ &nbsp;Transfer-in:</label></td>
                                            <td><input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/><input name="dateRegistration" type="hidden" id="dateRegistration"/><span id="dateregHelp" class="errorspan"></span></td>
                                            <td><label>ART enrollment setting:</label></td>
                                            <td>
                                                <select name="enrollmentSetting" style="width: 130px;" class="inputboxes" id="enrollmentSetting">
                                                    <option></option>
                                                    <option>Facility</option>
                                                    <option>Community</option>
                                                </select>
                                            </td>                            
                                        </tr>                                                                                     
                                    </table>                                  
                                </div>
                                <p></p>
                                <div style="border: 1px solid #ccc">
                                    <table width="99%" border="0" class="space" cellpadding="3">
                                        <tr>
                                            <td width="20%"><label>ART Start Date:</label></td>
                                            <td width="30%"><input name="date4" type="text" style="width: 100px;" class="inputboxes" id="date4"/><input name="dateStarted" type="hidden" id="dateStarted"/><span id="dateHelp" class="errorspan"></span></td>
                                            <td width="20%"><label>CD4 at start of ART:</label></td>
                                            <td colspan="3"><input name="cd4" type="text" style="width: 50px;" class="inputboxes" id="cd4"/>&nbsp;<input name="cd4p" type="text" style="width: 50px;" class="inputboxes" id="cd4p"/>&nbsp;CD4%</td>
                                        </tr>                            
                                        <tr>
                                            <td><label>Clinical Stage:</label></td>
                                            <td>
                                                <select name="clinicStage" style="width: 100px;" class="inputboxes" id="clinicStage">
                                                    <option></option>
                                                    <option>Stage I</option>
                                                    <option>Stage II</option>
                                                    <option>Stage III</option>
                                                    <option>Stage IV</option>
                                                </select>
                                            </td>
                                            <td><label>Functional Status:</label></td>
                                            <td>
                                                <select name="funcStatus" style="width: 100px;" class="inputboxes" id="funcStatus">
                                                    <option></option>
                                                    <option>Working</option>
                                                    <option>Ambulatory</option>
                                                    <option>Bedridden</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Regimen Line (first):</label></td>
                                            <td>
                                                <select name="regimentype" style="width: 200px;" class="inputboxes" id="regimentype">
                                                    <option></option>
                                                </select>
                                            </td>
                                            <td><label>Regimen (first):</label></td>
                                            <td>
                                                <select name="regimen" style="width: 200px;" class="inputboxes" id="regimen">
                                                    <option></option>
                                                </select>                                    
                                            </td>
                                        </tr>
                                        
                                    </table>
                                </div>
                                <p></p>
                                <div style="border: 1px solid #ccc; background-color: ghostwhite">
                                    <table width="99%" border="0" class="space" cellpadding="3">
                                        <tr>
                                            <td width="20%"><label>Date of last refill:</label></td>
                                            <td width="30%"><input name="date5" type="text" style="width: 100px;" class="inputboxes" id="date5"/><input name="dateLastRefill" type="hidden" id="dateLastRefill"/><span id="dateHelp" class="errorspan"></span></td>
                                            <td width="20%"><label>Refill Period (days):</label></td>
                                            <td width="30%"><input name="duration" type="text" style="width: 50px;" class="inputboxes" id="duration"/><span id="refillHelp" style="color:red"></span></td>
                                        </tr>
                                        <tr>
                                            <td><label>Regimen Line (current):</label></td>
                                            <td>
                                                <select name="regimentypeId" style="width: 200px;" class="inputboxes" id="regimentypeId">
                                                    <option></option>
                                                </select>
                                            </td>
                                            <td><label>Regimen (current):</label></td>
                                            <td>
                                                <select name="regimenId" style="width: 200px;" class="inputboxes" id="regimenId">
                                                    <option></option>
                                                </select><span id="regimenHelp" class="errorspan"></span>                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label>Current Status:</label></td>
                                            <td>
                                                <select name="currentStatus" style="width: 200px;" class="inputboxes" id="currentStatus"/>
                                                   <option></option>
                                                   <option>HIV+ non ART</option>
                                                   <option>ART Start</option>
                                                   <option>ART Restart</option>
                                                   <option>ART Transfer In</option>
                                                   <option>Pre-ART Transfer In</option>
                                                   <option>ART Transfer Out</option>
                                                   <option>Pre-ART Transfer Out</option>
                                                   <option>Lost to Follow Up</option>
                                                   <option>Stopped Treatment</option>
                                                   <option>Known Death</option>
                                                </select><span id="statusregHelp" class="errorspan"></span>
                                            </td>
                                            <td><label>Date of Current Status:</label></td>
                                            <td><input name="date3" type="text" style="width: 100px;" class="inputboxes" id="date3"/><input name="dateCurrentStatus" type="hidden" id="dateCurrentStatus"/><span id="dateHelp" class="errorspan"></span></td>
                                        </tr>                            
                                    </table>
                                </div>
                                <p></p>
                                <div style="border: 1px solid #ccc; background-color: ghostwhite">
                                    <table width="99%" border="0" class="space" cellpadding="3">
                                        <tr>
                                            <td width="20%"><label>Date of Current Viral Load:</label></td>
                                            <td width="30%"><input name="date6" type="text" style="width: 100px;" class="inputboxes" id="date6"/><input name="dateCurrentViralLoad" type="hidden" id="dateCurrentViralLoad"/></td>
                                            <td width="20%"><label>Date of Sample Collected:</label></td>
                                            <td width="30%"><input name="date7" type="text" style="width: 100px;" class="inputboxes" id="date7"/><input name="dateCollected" type="hidden" id="dateCollected"/></td>
                                        </tr>
                                        <tr>
                                            <td><label>Current Viral Load (c/ml):</label></td>
                                            <td><input name="viralLoad" type="text" style="width: 70px;" class="inputboxes" id="viralLoad"/><span id="vlHelp" style="color:red"></span></td>
                                            <td><label>Viral Load Indication:</label></td>
                                            <td>
                                                <select name="viralLoadIndication" style="width: 100px;" class="inputboxes" id="viralLoadIndication">
                                                    <option></option>
                                                    <option>Routine Monitoring</option>
                                                    <option>Targeted Monitoring</option>
                                                </select>
                                            </td>                            
                                        </tr>
                                        <tr>
                                            <td><input name="patientId" type="hidden" class="inputboxes" id="patientId"/></td>
                                        </tr>
                                    </table>
                                </div>
                            <p></p>
                            <div>
                                <span style="margin-left:625px"><button id="save_button" style="width: 100px;">Save</button></span>                                    
                            </div>    
                       </div>
                    </div>
                </s:form>
            </div>
            </div>
        </div>
                
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
