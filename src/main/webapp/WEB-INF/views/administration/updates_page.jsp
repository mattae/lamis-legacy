<%-- 
    Document   : Data Export
    Created on : Aug 15, 2012, 6:53:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />

        <script type="text/javascript" src="js/lamis/maintenance-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var timer = {};
            $(document).ready(function(){
                initialize(); 
                timer = $.timer(function() {
                    $.ajax({
                        url: "Processing_status.action",
                        dataType: "json",
                        success: function(status) {
                            if(status == "terminated") {
                                timer.stop();
                                $("#loader").html('');
                                $("#messageBar").html("Error occured while downloading updates, please retry").slideDown('fast');                                 
                            }
                            else {
                                if(status == "completed") {
                                    timer.stop();
                                    $("#loader").html('');
                                    $("#messageBar").html("Download completed, updates will be applied when LAMIS restarts").slideDown('fast');
                                }
                                else {
                                    processingStatusNotifier("Downloading " + status + " file, please wait...");
                                }
                            }
                        }                    
                    }); 
                });
                timer.set({time : 60000, autostart : false});
                
                $("#updates_button").bind("click", function(event){
                    $("#messageBar").hide();
                    checkUpdates();
                    return false;                        
                });
                
                $("#test_button").bind("click", function(event){
                    $("#messageBar").hide();
                    checkConnection();
                    return false;                        
                });

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Maintenance_page");
                    return true;
                });                
            });
            
            function checkUpdates() {  
                timer.play();
                $("#messageBar").hide();
                $("#updates_button").attr("disabled", true);
                $("#test_button").attr("disabled", true);                       
                $.ajax({
                    url: "Check_updates.action",
                    dataType: "json",
                    success: function(status) {
                        timer.stop();
                        $("#loader").html('');
                        $("#messageBar").html(status).slideDown('slow');     
                        $("#updates_button").attr("disabled", false);
                        $("#test_button").attr("disabled", false);                       
                    }                    
                }); 
            }                

            function checkConnection() {
               $.ajax({
                    url: "Internet_connection.action",
                    dataType: "json",
                    error: function(jgXHR, status) {
                        alert(status);
                    },
                    success: function(status) {
                        if(status == 1) {
                            $("#messageBar").html("Connection successfuly established with the LAMIS server").slideDown('slow');                                
                        } 
                        else {
                            $("#messageBar").html("No connection established with the LAMIS server, please connect to the internet before proceeding").slideDown('slow');                                
                        }
                    }                    
                }); 
            }
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_maintenance.jsp" />  

               <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/tools.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Data Maintenance >> Check & Download Updates</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Check & Download Updates</td>
                            </tr>
                        </table>                   
                        <div id="loader"></div>
                        <div id="messageBar"></div>

                        <table width="99%" height="80" border="0" class="space" cellpadding="3">
                            <tr>
                                <td></td>
                                <td>
                                    <p><label>Available updates will be downloaded and applied when LAMIS is restarted</label></p>                                   
                                </td>
                                <td></td>   
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><button id="test_button">Test Connection</button></td>                                 
                            </tr>
                        </table>
                        
                        <hr></hr>
                        <div id="buttons" style="width: 200px">
                            <button id="updates_button">Updates</button> &nbsp;<button id="close_button">Cancel</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>