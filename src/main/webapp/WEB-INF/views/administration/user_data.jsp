<%-- 
    Document   : User
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var updateRecord = false;
			var selected_ids, loaded_state_ids = "";
           
            $(document).ready(function(){
                resetPage();

                $("#grid").jqGrid({
                    url: "User_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["User Name", "Full Name", "User Group", "Facility Accessed", "Time Login", "State", "", ""],
                    colModel: [
                        {name: "username", index: "username", width: "100"},
                        {name: "fullName", index: "fullName", width: "150"},
                        {name: "userGroup", index: "userGroup", width: "100"},
                        {name: "name", index: "name", width: "300"},
                        {name: "timeLogin", index: "timeLogin", width: "100"},
						{name: "stateIds", index: "stateIds", width: "0", sortable:false, hidden:true},
                        {name: "password", index: "password", width: "0", sortable:false, hidden:true},
                        {name: "viewIdentifier", index: "viewIdentifier", width: "0", sortable:false, hidden:true},
                    ],
                    rowNum: -1,
                    sortname: "userId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 300,                    
                    jsonReader: {
                        root: "userList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "userId"
                    },
                    onSelectRow: function(id) {
						$("#states").hide();
                        var data = $("#grid").getRowData(id);
//                        console.log(data);				
                        $("#fullname").val(data.fullName);
                        $("#username").val(data.username);
                        $("#password").val(data.password);
                        $("#passwordConfirm").val(data.password);
                        $("#userGroup").val(data.userGroup);
						if(data.userGroup === "State Administrator"){
                            $("#states").show();
                        }
                        $("#state_ids").val(data.stateIds);
                        loaded_state_ids = data.stateIds;							 
                        $("#viewIdentifier").val(data.viewIdentifier);
                        if(data.viewIdentifier == "1") {
                           $("#viewIdentifier").attr("checked", "checked"); 
                        }else{
                            $("#viewIdentifier").removeAttr("checked"); 
                        }
                        $("#userId").val(id);
                        
						//Reload the states Grid and set selected
                        $("#statesgrid").setGridParam({url: "State_user_retrieve.action", page:1}).trigger("reloadGrid");
                        
                        updateRecord = true;
                        lastSelected = id;
                        initButtonsForModify()
                    }               
                }); //end of jqGrid 
                 
				$("#statesgrid").jqGrid({ 
                    url: "State_user_retrieve.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["", "State Name"],
                    colModel: [
                        {name: "stateId", index: "stateId", width: "5", hidden: true},
                        {name: "name", index: "name", width: "170"}                      
                    ],
                    sortname: "stateId",
                    sortorder: "desc",
                    viewrecords: true,
                    rowNum: 100,
                    imgpath: "themes/basic/images", 
                    multiselect: true,
                    resizable: false,
                    height: 100,                    
                    jsonReader: {
                        root: "statesList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false, 
                        id: "stateId"
                    },
                    loadComplete: function(data) {
                        if(loaded_state_ids.length === 1){
                            jQuery('#statesgrid').jqGrid('setSelection', loaded_state_ids);
                        }else if(loaded_state_ids.length > 1){
                            var s_data = loaded_state_ids.split(",");
                            for(var i = 0; i< s_data.length; i++){
                                jQuery('#statesgrid').jqGrid('setSelection', s_data[i]);
                            }
                        }                      
                    },
                    onSelectRow: function(id) { 
                       
                    }                                   
                });
                $("#save_button").bind("click", function(event){
                    //Pull staes selected...
                    selected_ids = jQuery("#statesgrid").jqGrid('getGridParam','selarrrow'); 
                    $("#stateIds").val(selected_ids)							 
                    if($("#userGroupLogin").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                        if(validateForm()) {
                            if(updateRecord) {
                                $("#lamisform").attr("action", "User_update");                
                            } 
                            else {
                                $("#lamisform").attr("action", "User_save");                
                            }
                            return true;                        
                        } 
                        else {
                            return false;
                        } 
                    }
                });
                
                $("#delete_button").bind("click", function(event){
                    if($("#userGroupLogin").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                    }
                    else {
                        $("#lamisform").attr("action", "User_delete");
                    }
                    return true;
                });
                
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Setup_page");
                    return true;
                });
				var showing = false;
                $("#userGroup").change(function(event){
                    var userGroup = $("#userGroup").val();
                    if(userGroup === "State Administrator"){
                        $("#states").show();
                    }else{
                        $("#states").hide();
                    }
                }); 
            });
            
            function validateForm() {
                var validate = true;

                // check for valid input is entered
                //For the full namw
                if($("#fullname").val().length == 0){
                    $("#nameHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#nameHelp").html("");                    
                }
                
                //For the Username
                if($("#username").val().length == 0){
                    $("#userHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#userHelp").html("");                    
                } 
                
                //For the Password
                if($("#password").val().length == 0){
                    $("#passHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#passHelp").html("");                    
                }
                
                if($("#passwordConfirm").val().length == 0 || $("#password").val() != $("#passwordConfirm").val()){
                    $("#passHelp1").html(" *");
                    validate = false;
                }
                else {
                    $("#passHelp1").html("");                    
                }
                
                if($("#userGroup").val().length == 0){
                    $("#userGroupHelp").html(" *");
                    validate = false;
                }
                else {
                    $("#userGroupHelp").html("");                    
                }
                return validate;
            }                                         
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_setup.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/settings.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Setup >> User Setup</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><input name="userId" type="hidden" class="inputboxes" id="userId"/></td>
                                <td width="30%"></td>
                                <td width="20%"></td>
                                <td width="30%"></td>
                            </tr>
                             <tr>
                                <td><label>Full Name:</label></td>
                                <td><input name="fullname" style="width: 200px;" type="text" class="inputboxes" id="fullname"/><span id="nameHelp" class="errorspan"></span></td>                               
                            </tr>
                            <tr>
                                <td><label>User Name:</label></td>
                                <td><input name="username" type="text" class="inputboxes" id="username"/><span id="userHelp" class="errorspan"></span></td>                               
                            </tr>
                            <tr>
                                <td><label>Password:</label></td>
                                <td><input name="password" type="password" class="inputboxes" id="password"/><span id="passHelp" class="errorspan"></span></td>                               
                            </tr>
                            <tr>
                                <td><label>Confirm Password:</label></td>
                                <td><input name="passwordConfirm" type="password" class="inputboxes" id="passwordConfirm"/><span id="passHelp1" class="errorspan"></span></td>                               
                            </tr>
                            <tr></tr>
                            <tr>
                                <td><label>User Group:</label></td>
                                <td>
                                    <select name="userGroup" style="width: 200px;" class="inputboxes" id="userGroup">
                                        <option></option>
					<option value="Administrator">Administrator</option>
                                        <option value="State Administrator">State Administrator</option>
                                        <option value="Clinician">Clinician</option>
                                        <option value="Pharmacist">Pharmacist</option>
                                        <option value="Laboratory Scientist">Laboratory Scientist</option>
                                        <option value="Data Analyst">Data Analyst</option>
                                        <option value="Data Clerk">Data Clerk</option>
                                    </select><span id="userGroupHelp" class="errorspan"></span>
                                </td> 
                                <td colspan="2"><input name="viewIdentifier" type="checkbox" value="1" id="viewIdentifier"/>&nbsp;<label>View patient identifiers</label></td>
                            </tr>
							<tr id="states" style="display: none">
                                <td><label>Assign State:</label></td>
                                <td colspan="2">
                                   <div id="statestable">
                                       <table id="statesgrid"></table> 
                                       <input name="stateIds" type="text"  hidden id="stateIds"/>
                                   </div>                                   
                               </td>
                            </tr> 
                        </table>
                        
                        <p></p>
                        <div>
                            <fieldset>  
                                <legend> Search Result</legend>
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                         <table id="grid"></table>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="userGroupLogin" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
