<%-- 
    Document   : Clini
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var gridNum = 4;
            var enablePadding = true;
            $(document).ready(function(){
                $.ajax({
                    url: "Padding_status.action",
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                });
                resetPage();
                initialize(); 
                reports();

                $("#grid").jqGrid({
                    url: "Patient_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Hospital No", "Name", "Gender", "ART Status", "Address", ""],
                    colModel: [
                        {name: "hospitalNum", index: "hospitalNum", width: "100"},
                        {name: "name", index: "name", width: "150"},
                        {name: "gender", index: "gender", width: "50"},
                        {name: "currentStatus", index: "currentStatus", width: "130"},
                        {name: "address", index: "address", width: "300"},                        
                        {name: "dateCurrentStatus", index: "dateCurrentStatus", width: "100", hidden: true},
                    ],
                    pager: $('#pager'),
                    rowNum: 100,
                    sortname: "Name",
                    sortorder: "asc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 200,                    
                    jsonReader: {
                        root: "patientList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "patientId"
                    },
                    onSelectRow: function(id) {
                        if(id == null) {
                            id = 0;
                            if ($("#detail").getRecords() > 0) {
                                $("#detail").setGridParam({url: "Chroniccare_grid.action?q=1&patientId="+id, page:1}).trigger("reloadGrid");
                            }
                        } 
                        else {
                            $("#detail").setGridParam({url: "Chroniccare_grid.action?q=1&patientId="+id, page:1}).trigger("reloadGrid");
                        }
                        $("#patientId").val(id);
                        var data = $("#grid").getRowData(id)
                        $("#hospitalNum").val(data.hospitalNum);
                        $("#name").val(data.name);
                        $("#currentStatus").val(data.currentStatus);
                        $("#dateCurrentStatus").val(data.dateCurrentStatus);
                        $("#new_button").removeAttr("disabled");
                        $("#new_button").html("New");													 
                    },               
                    ondblClickRow: function(id) {
                        $("#lamisform").attr("action", "Chroniccare_new");
                        $("#lamisform").submit();
                    }
                }); //end of master jqGrid                 
                
                $("#detail").jqGrid({
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Date of Visit", "ART Status", "Clinical Stage",  "Last CD4 Result", "Last Viral Load Result", ""],
                    colModel: [
                        {name: "dateVisit", index: "date1", width: "100", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},
                        {name: "currentStatus", index: "currentStatus", width: "100"},
                        {name: "clinicStage", index: "clinicStage", width: "130"},
                        {name: "lastCd4", index: "lastCd4", width: "200"},
                        {name: "lastViralLoad", index: "lastViralLoad", width: "200"},
                        {name: "dateVisit", index: "dateVisit", width: "100", hidden: true},
                    ],
                    rowNum: -1,
                    sortname: "chroniccareId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,                    
                    height: 150,                    
                    jsonReader: {
                        root: "chroniccareList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "chroniccareId",
                    },    
                    onSelectRow: function(id) {
                        if(id != null) {
                            var selectedRow = $("#detail").getGridParam('selrow');
                            if(selectedRow != null) {
                                var data = $("#detail").getRowData(selectedRow);
                                $("#dateVisit").val(data.dateVisit);
                            }
                            $("#chroniccareId").val(id);
                            $("#new_button").html("View");
                        }
                    },                   
                    ondblClickRow: function(id) {
                        var selectedRow = $("#detail").getGridParam('selrow');
                        if(selectedRow != null) {
                            var data = $("#detail").getRowData(selectedRow);
                            $("#dateVisit").val(data.dateVisit);
                        }
                        $("#chroniccareId").val(id);
                        $("#lamisform").attr("action", "Chroniccare_find");
                        $("#lamisform").submit();
                    }
                }); //end of detail jqGrid                 

                $.ajax({
                    url: "Patient_retrieve_detail.action",
                    dataType: "json",
                    success: function(patientMap) {
                        if(!$.isEmptyObject(patientMap)) {
                            $("#patientId").val(patientMap.patientId);
                            $("#hospitalNum").val(patientMap.hospitalNum);
                            $("#name").val(patientMap.name);
                            $("#new_button").removeAttr("disabled");                            
                        }
                    },
                    complete: function() {
                        $("#detail").setGridParam({url: "Chroniccare_grid.action?q=1&patientId="+$("#patientId").val(), page:1}).trigger("reloadGrid");                        
                    }
                });
                
                $("#new_button").bind("click", function(event){
                    if($("#new_button").html() === "New"){
                        $("#lamisform").attr("action", "Chroniccare_new");
                        return true;  
                    }
                    else if($("#new_button").html() === "View"){
                        $("#lamisform").attr("action", "Chroniccare_find");
                        $("#lamisform").submit();
                        return true;
                    }   										  
                });
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Clinic_page");
                    return true;
                });
            });
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_clinic.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/search.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> ART Clinic >> Care & Support Assessment</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">
                                    <table width="97%" border="0">
                                        <tr>
                                            <td width="12%">Hospital No:</td>
                                            <td><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" /></td>
                                            <td><input name="chroniccareId" type="hidden" id="chroniccareId" /><input name="patientId" type="hidden" id="patientId" /></td>
                                            <td><input name="currentStatus" type="hidden" id="currentStatus"/><input name="dateCurrentStatus" type="hidden" id="dateCurrentStatus"/></td>
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td width="18%"><input name="name" type="text" class="inputboxes" id="name" /></td>
                                            <td><input name="dateVisit" type="hidden" id="dateVisit"/></td>
                                        </tr>
                                    </table>                               
                                </td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        <p></p>
                        
                        <div>
                            <fieldset> 
                            <legend> Patient List</legend>                            
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                        <div id="pager" style="text-align:center;"></div>
                                        <p></p>
                                        <table id="detail"></table>
                                    </td>
                                  </tr>                              
                                </table>
                            </fieldset>  
                        </div>
                        <p></p>
                                                  
                        <div id="buttons" style="width: 200px">
                            <button id="new_button" disabled="true">New</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
