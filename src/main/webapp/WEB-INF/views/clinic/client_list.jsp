<%-- 
    Document   : case_management_report
    Created on : Oct 27, 2017, 11:37:12 AM
    Author     : user10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/JavaScript">
            var updateRecord = false; 
            $(document).ready(function(){
                resetPage();
                reports();
                
                $.ajax({
                    url: "Casemanager_retrieve.action",
                    dataType: "json",
                    success: function(caseManagerMap) {
                        var options = "<option value = '" + '0' + "'>" + '' + "</option>";
                        $.each(caseManagerMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#casemanagerId").html(options);                        
                    }                    
                }); //end of ajax call
                
                $("#casemanagerId").bind("change", function(event){
                    var casemanagerId = $("#casemanagerId").val();
                    if(casemanagerId != "0"){ 
                        $("#ok_button").prop("disabled", false);
                    }
                    else if(casemanagerId == "0"){
                        $("#ok_button").prop("disabled", true);
                    }
                });

                $("#ok_button").bind("click", function(event){
                    var casemanagerId = $("#casemanagerId").val();
                    event.preventDefault();
                    event.stopPropagation();
                    url = "";
                    url += "casemanagerId="+casemanagerId;
                    if($('[name="reportFormat"]:checked').val() == "pdf") {
                        url = "Client_list_report.action?"+url;                            
                        window.open(url);
                        return false;                        
                    }
                    else {
                        url += "&recordType=1&viewIdentifier="+$("#viewIdentifier").prop("checked");
                        url = "Converter_dispatch.action?"+url;                             
                        convertData();
                    }
                });
                
                $("#cancel_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Casemanagement_page");
                    return true;
                });
                
            }); 
            
            var x = function wait() {window.open(url);}
            
            
            function convertData() {                
                $("#messageBar").hide();
                $("#ok_button").attr("disabled", true);
                $.ajax({
                    url: url,
                    dataType: "json",
                    success: function(fileName) {
                        $("#messageBar").html("Conversion Completed").slideDown('fast');
                        url = fileName;
                        window.setTimeout(x, 3000);                        
                    }                    
                }); 
                $("#ok_button").attr("disabled", false);
            }
                                    
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_casemanagement.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/report.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> Case Management >> Case Manager Clients List</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Case Manager Detail</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        <p></p>
                            
                        <div style="margin-right: 10px;">
                            <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td width="20%"><label>Facility Case Manager:</label></td>
                                    <td>
                                        <select name="casemanagerId" style="width: 250px;" class="inputboxes" id="casemanagerId">
                                                <option value='0'></option>
                                        </select><span id="caseManagerHelp" class="errorspan"></span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table> 
                        </div>								
                        <p></p>                        
                        <div style="margin-right: 10px;">
                            <fieldset> 
                                <legend> Report format </legend>                            
                                <p></p>
                                <table width="99%" border="0" class="space" cellpadding="3">
                                    <tr>
                                        <td colspan="2"><input type="radio" name="reportFormat" value="pdf" checked/><label>Generate report in PDF format </label></td>
                                        <td colspan="2"><input type="radio" name="reportFormat" value="cvs"/><label>Generate report and convert report to MS Excel </label></td>
                                    </tr>                            
                                    <tr> 
                                        <td colspan="2"></td>
                                        <td colspan="2">                                        
                                            <span style="margin-left:20px"><input name="viewIdentifier" type="checkbox" id="viewIdentifier"/>&nbsp;<label>Unscramble patient identifiers like names, addresses and phone numbers</label></span>
                                        </td>                                    
                                    </tr>
                                    <tr> </tr>
                                </table>
                            </fieldset>  
                        </div>								
                        <p></p>
                        
                        <div id="buttons" style="width: 200px">
                            <button id="ok_button" disabled>Ok</button> &nbsp;<button id="cancel_button">Cancel</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
