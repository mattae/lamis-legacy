<%-- 
    Document   : Clinic
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/commence-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/javascript" src="js/date.js"></script>       
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var obj = {};
            var oiIds = "", adrIds = "", adhereIds = "", date = "", lastSelectDate = "";
            var updateRecord = false;
            $(document).ready(function(){ 
                resetPage();
                initialize();
                reports();
                 
                $.ajax({ 
                    url: "Clinic_retrieve.action",
                    dataType: "json",                    
                    success: function(clinicList) {
                        populateForm(clinicList);                                    
                    }                    
                }); //end of ajax call

                var lastSelected = -99;
                $("#adrgrid").jqGrid({
                    url: "Adr_grid_clinic.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Description", "Severity"],
                    colModel: [
                        {name: "description", index: "description", width: "250"},
                        {name: "severity", index: "severity", width: "70", sortable:false, editable:true, edittype:"select", editoptions:{value:" : ;Grade 1:Grade 1;Grade 2:Grade 2;Grade 3:Grade 3;Grade 4:Grade 4"}},                        
                    ],
                    sortname: "adrId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 100,                    
                    jsonReader: {
                        root: "adrList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "adrId"
                    },
                    onSelectRow: function(id) { 
                        if(id && id!=lastSelected) {
                            $("#adrgrid").jqGrid('saveRow', lastSelected,
                            {
                                successfunc: function(response) {
                                    return true;
                                },
                                url: "Adr_update.action"
                            })
                            lastSelected = id;
                        }
                        $("#adrgrid").jqGrid('editRow',id);
                        $("#adrId").val(id);
                        var data = $("#adrgrid").getRowData(id)
                        $("#description").val(data.description);
                    }, //end of onSelectRow                     
                    loadComplete: function(data) {
                        for(i = 0; i < adrIds.length; i++) {
                            var values = adrIds[i].split(",");                          
                            $("#adrgrid").jqGrid('setCell', values[0], 'severity', 'Grade '+values[1]);
                        }
                    }                                     
                }); //end of jqGrid  
                
                $("#oigrid").jqGrid({
                    url: "Oi_grid.action", 
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Description"],
                    colModel: [
                        {name: "description", index: "description", width: "250"},
                    ],
                    sortname: "oiId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    multiselect: true,
                    height: 85,                     
                    jsonReader: {
                        root: "oiList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "oiId"
                    },
                    loadComplete: function(data) {
                        $.each(oiIds, function(_, rowId){
                            $("#oigrid").setSelection(rowId, true);
                        })
                    }                    
                }); //end of jqGrid 
                 
                $("#adheregrid").jqGrid({
                    url: "Adhere_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Description"],
                    colModel: [
                        {name: "description", index: "description", width: "250"},
                    ],
                    sortname: "adhereId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    multiselect: true,
                    height: 85,                    
                    jsonReader: {
                        root: "adhereList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "adhereId"
                    },
                    loadComplete: function(data) {
                        $.each(adhereIds, function(_, rowId){
                            $("#adheregrid").setSelection(rowId, true);
                        })
                    }
                }); //end of jqGrid 
                 
                $("#adrScreened").bind("click", function() {
                    if($("#adrScreened").val() == "Yes") {
                        $("#adr_button").removeAttr("disabled");                        
                    }
                    else {
                        $("#adr_button").attr("disabled", "disabled");                                                
                    }
                });
                 
                $("#oiScreened").bind("click", function() {
                    if($("#oiScreened").val() == "Yes") {
                        $("#oi_button").removeAttr("disabled");                        
                    }
                    else {
                        $("#oi_button").attr("disabled", "disabled");                                                
                    }
                });                
                $("#adherenceLevel").bind("click", function() {
                    if($("#adherenceLevel").val() == "Fair" || $("#adherenceLevel").val() == "Poor") {
                        $("#adhere_button").removeAttr("disabled");                        
                    }
                    else {
                        $("#adhere_button").attr("disabled", "disabled");                                                
                    }
                });
                
                $("#adr_button").bind("click", function(event){
                   $("#adrtable").toggle("slow");
                   return false;
                }); //show and hide grid

                $("#oi_button").bind("click", function(event){
                   $("#oitable").toggle("slow");
                   return false;
                }); //show and hide grid
                
                $("#adhere_button").bind("click", function(event){
                   $("#adheretable").toggle("slow");
                   return false;
                }); //show and hide grid
                
                $("#date1").bind("change", function(event){
                    checkDate();                
                });
                $("#date3").bind("change", function(event){
                    checkDate();
                });

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Commence_search");
                    return true;
                });
            });
            
            function checkDate() {
                if($("#date1").val().length != 0 && $("#date3").val().length != 0 ) {
                   if(parseInt(compare($("#date1").val(), $("#date3").val())) == -1) {
                       var message = "Date of next appointment cannot be ealier than date of visit";
                       $("#messageBar").html(message).slideDown('slow');                                                     
                    } 
                    else {
                       $("#messageBar").slideUp('slow');                 
                    }
                } 
            }            
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_clinic.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/clinic.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> ART Clinic >> ART Commencement</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>Hospital No:</label></td>
                                <td width="20%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/></td>
                                <td width="25%"><span id="patientInfor"></span></td>
                                <td width="20%"><input name="patientId" type="hidden" id="patientId"/><input name="clinicId" type="hidden" id="clinicId"/></td>
                            </tr>
                            <tr>
                                <td><label>ART Start Date:</label></td>
                                <td><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateVisit" type="hidden" id="dateVisit"/><span id="dateHelp" class="errorspan"></span></td>
                                <td><label>CD4 at start of ART:</label></td>
                                <td colspan="3"><input name="cd4" type="text" style="width: 50px;" class="inputboxes" id="cd4"/>&nbsp;<input name="cd4p" type="text" style="width: 50px;" class="inputboxes" id="cd4p"/>&nbsp;CD4% &nbsp;&nbsp;&nbsp;<span id="cd4Help" class="errorspan"></span></td>
                            </tr>                            
                            <tr>
                                <td><label>Original Regimen Line:</label></td>
                                <td>
                                    <select name="regimentype" style="width: 200px;" class="inputboxes" id="regimentype">
                                    </select>
                                </td>
                                <td><label>Original Regimen:</label></td>
                                <td>
                                    <select name="regimen" style="width: 200px;" class="inputboxes" id="regimen">
                                    </select>                                    
                                </td>
                            </tr>
                            <tr>
                                <td><label>Clinical Stage:</label></td>
                                <td>
                                    <select name="clinicStage" style="width: 100px;" class="inputboxes" id="clinicStage">
                                        <option></option>
                                        <option>Stage I</option>
                                        <option>Stage II</option>
                                        <option>Stage III</option>
                                        <option>Stage IV</option>
                                    </select>
                                </td>
                                <td><label>Functional Status:</label></td>
                                <td>
                                    <select name="funcStatus" style="width: 100px;" class="inputboxes" id="funcStatus">
                                        <option></option>
                                        <option>Working</option>
                                        <option>Ambulatory</option>
                                        <option>Bedridden</option>
                                    </select>
                                </td>
                            </tr>
                             <tr>
                                <td><label>Body Weight(kg):</label></td>
                                <td><input name="bodyWeight" type="text" style="width: 50px;" class="inputboxes" id="bodyWeight"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Height(cm):</label> <input name="height" type="text" style="width: 50px;" class="inputboxes" id="height"/><span id="bmi" style="color:red"></span></td>
                                <td><label>Blood Pressure(mmHg):</label></td>
                                <td><input name="bp1" type="text" style="width: 25px;" class="inputboxes" id="bp1"/> <input name="bp2" type="text" style="width: 25px;" class="inputboxes" id="bp2"/><input name="bp" type="hidden" style="width: 50px;" class="inputboxes" id="bp"/></td>
                            </tr>
                           <tr id="maternal">
                                <td><label>Maternal Status at ART Start:</label></td>
                                <td>
                                    <select name="maternalStatusArt" style="width: 200px;" class="inputboxes" id="maternalStatusArt">
                                       <option></option>
                                       <option value="1">Not Pregnant</option>
                                       <option value="2">Pregnant</option>
                                       <option value="3">Breastfeeding</option>
<!--                                       <option value="4">Post Partum (Breastfeeding)</option>
                                       <option value="5">Post Partum (Not breastfeeding)</option>-->
                                    </select>
                                </td><span id="pregHelp" class="errorspan"></span>
                                <td id="gestationalAge1"><label>Gestational Age (weeks):</label></td>
                                <td>
                                    <select name="gestationalAge" style="width: 100px;" class="inputboxes" id="gestationalAge">
                                        <option></option>
                                        <option>&lt;=36weeks</option>
                                        <option>&gt;36weeks</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>TB Status:</label></td>
                                <td>
                                    <select name="tbStatus" style="width: 200px;" class="inputboxes" id="tbStatus">
                                        <option></option>
                                        <option>No sign or symptoms of TB</option>
                                        <option>TB suspected and referred for evaluation</option>
                                        <option>Currently on INH prophylaxis</option>
                                        <option>Currently on TB treatment</option>
                                        <option>TB positive not on TB drugs</option>
                                    </select>
                                </td>
                                <td><label>Date of Next Appointment:</label></td>
                                <td><input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/><input name="nextAppointment" value="" type="hidden" id="nextAppointment"/><span id="nextHelp" style="color:red"></span></td>
                           </tr>
                           <tr>
                               <td></td>     
                               <td><input name="oiIds" type="hidden" id="oiIds"/><input name="adrId" type="hidden" id="adrId"/><input name="adhereIds" type="hidden" id="adhereIds"/></td>
                               <td><input name="description" type="hidden" id="description"/><input name="screener" type="hidden" value="1" id="screener"/></td>
                           </tr>
<!--                           <tr>
                                <td><label>Notes:</label></td>
                            </tr>
-->
                           <tr>
                                <td></td>
                                <!--<td colspan="3"><textarea name="notes" rows="7" cols="65" id="notes"></textarea></td>  -->                             
                               <td><input name="notes" type="hidden" id="notes"/></td>
                           </tr>

                            <tr>
                                <td><input name="gender" type="hidden" id="gender"/><input name="dateBirth" type="hidden" id="dateBirth"/></td>                               
                                <td><input name="currentStatus" type="hidden" id="currentStatus"/><input name="dateCurrentStatus" type="hidden" id="dateCurrentStatus"/></td>                               
                                <td><input name="dateLastCd4" type="hidden" id="dateLastCd4"/><input name="dateLastClinic" type="hidden" id="dateLastClinic"/></td>
                                <td><input name="dateNextClinic" type="hidden" id="dateNextClinic"/><input name="commence" type="hidden" value="1" id="commence"/></td>
                            </tr>
                        </table>
                        <hr>                        
                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
