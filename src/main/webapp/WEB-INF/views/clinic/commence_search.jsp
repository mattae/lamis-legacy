<%-- 
    Document   : Commence
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var gridNum = 3;
            var enablePadding = true;
            $(document).ready(function(){
                $.ajax({
                    url: "Padding_status.action",
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                });
                resetPage();
                initialize();
                reports();

                $("#grid").jqGrid({
                    url: "Patient_grid.action",
                    datatype: "json", 
                    mtype: "GET",
                    colNames: ["Hospital No", "Name", "Gender", "ART Status", "Current CD4", "Last Clinic Stage", "ART Start Date", "", ""],
                    colModel: [
                        {name: "hospitalNum", index: "hospitalNum", width: "100"},
                        {name: "name", index: "name", width: "150"},
                        {name: "gender", index: "gender", width: "50"},
                        {name: "currentStatus", index: "currentStatus", width: "130"},
                        {name: "lastCd4", index: "lastCd4", width: "100"},                        
                        {name: "lastClinicStage", index: "lastClinicStage", width: "100"},                        
                        {name: "dateStarted", index: "date1", width: "100", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},                        
                        {name: "dateStarted", index: "dateStarted", width: "100", hidden: true},
                        {name: "dateCurrentStatus", index: "dateCurrentStatus", width: "100", hidden: true},
                    ],
                    pager: $('#pager'),
                    rowNum: 100,
                    sortname: "patientId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 350,                    
                    jsonReader: {
                        root: "patientList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "patientId"
                    },  
                    afterInsertRow: function(id, data) {
                        //$(this).jqGrid('setRowData', id, false, {fontFamily: 'Arial, Helvetica, sans-serif'});
                        if(data.dateStarted != "") {
                            $(this).jqGrid('setRowData', id, false, {color: 'red'});
                        }                        
                    },
                    onSelectRow: function(id) {
                        $("#patientId").val(id);
                        var data = $("#grid").getRowData(id);
                        $("#hospitalNum").val(data.hospitalNum);
                        $("#name").val(data.name);
                        $("#currentStatus").val(data.currentStatus);
                        $("#dateCurrentStatus").val(data.dateCurrentStatus);
                        //if ART start date is empty enable new button
                        if(data.dateStarted == "") {
                            $("#new_button").removeAttr("disabled");
                        } else {
                            $("#new_button").attr("disabled", "disabled");
                        }
                    },               
                    ondblClickRow: function(id) {
                        $("#patientId").val(id);
                        var data = $("#grid").getRowData(id);
                        $("#hospitalNum").val(data.hospitalNum);
                        $("#name").val(data.name);
                        $("#currentStatus").val(data.currentStatus);
                        $("#dateCurrentStatus").val(data.dateCurrentStatus);
                        //if ART start date is empty open a new commencement page, else find clinic visit with date of ART start
                        if(data.dateStarted == "") {
                            $("#lamisform").attr("action", "Commence_new");                            
                        } else {
                            $("#dateVisit").val(data.dateStarted);
                            $("#lamisform").attr("action", "Commence_find");                            
                        }
                        $("#lamisform").submit();
                    }
                }); //end of jqGrid                 
                
                $("#new_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Commence_new");
                    return true;
                });
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Clinic_page");
                    return true;
                });
            });
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_clinic.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/search.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> ART Clinic >> ART Commencement</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">
                                    <table width="97%" border="0">
                                        <tr>
                                            <td width="12%">Hospital No:</td>
                                            <td><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" /></td>
                                            <td><input name="clinicId" type="hidden" id="clinicId" /><input name="patientId" type="hidden" id="patientId" /></td>
                                            <td><input name="currentStatus" type="hidden" id="currentStatus"/><input name="dateCurrentStatus" type="hidden" id="dateCurrentStatus"/></td>
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td width="18%"><input name="name" type="text" class="inputboxes" id="name" /></td>
                                            <td><input name="dateVisit" type="hidden" id="dateVisit" /><input name="commence" type="hidden" value="1" id="commence"/></td>
                                        </tr>
                                    </table>                               
                                </td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        <p></p>

                        <div>
                            <fieldset>  
                                <legend> Patient List</legend>
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                        <div id="pager" style="text-align:center;"></div>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="buttons" style="width: 200px">
                            <button id="new_button" disabled="true">New</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
