<%-- 
    Document   : Appointment
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/eac-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/date.js"></script>       
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var obj = {};
            var lastSelectDate = "", date = ""; 
            var updateRecord = false;
            $(document).ready(function(){
                resetPage();
                initialize();
                reports();
                
                $.ajax({
                    url: "Eac_retrieve.action",
                    dataType: "json",                    
                    success: function(eacList) {
                        populateForm(eacList);
                    }                    
                }); //end of ajax call

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Eac_search");
                    return true;
                });                
            });          
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_clinic.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/report.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> ART Clinic >> Unsuppressed Client Monitoring/ Enhanced Adherence Counseling</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>Hospital No:</label></td>
                                <td width="20%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/></td>
                                <td width="20%"><span id="patientInfor"></span><input name="name" type="hidden" id="name"/></td>
                                <td width="25%"><input name="patientId" type="hidden" id="patientId"/><input name="eacId" type="hidden" id="eacId"/></td>
                            </tr>
                            <tr></tr>
                            <tr>
                                <td><label>Date of 1st EAC Session:</label></td>
                                <td><input name="date1" type="text" style="width: 100px;"  class="inputboxes" id="date1"/><input name="dateEac1" type="hidden" id="dateEac1"/><span id="dateHelp1" class="errorspan"></span></td>
                                <td colspan="2"><label>Last Viral Load: </label><span id="lastViralLoad_view" style="color:blue"></span></td>                                
                            </tr>                            
                            <tr>
                                <td><label>Date of 2ndt EAC Session:</label></td>
                                <td><input name="date2" type="text" style="width: 100px;"  class="inputboxes" id="date2"/><input name="dateEac2" type="hidden" id="dateEac2"/></td>
                                <td colspan="2"><label>Date of Last Last Viral Lod: </label><span id="dateLastViralLoad_view" style="color:blue"></span></td>
                            </tr>                            
                            <tr>
                                <td><label>Date of 3rd EAC Session:</label></td>
                                <td><input name="date3" type="text" style="width: 100px;"  class="inputboxes" id="date3"/><input name="dateEac3" type="hidden" id="dateEac3"/></td>
                                <td colspan="2"></td>                                
                            </tr>                            
                            <tr>
                                <td><label>Date of Repeat VL Sample Collection:</label></td>
                                <td><input name="date4" type="text" style="width: 100px;"  class="inputboxes" id="date4"/><input name="dateSampleCollected" type="hidden" id="dateSampleCollected"/></td>
                                <td colspan="2"></td>                                
                            </tr>                            
                           <tr>
                               <td><input name="lastViralLoad" type="hidden" id="lastViralLoad"/><input name="dateLastViralLoad" type="hidden" id="dateLastViralLoad"/></td>                               
                           </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </table>
                        <hr></hr>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
