<%-- 
    Document   : reassign_client
    Created on : Oct 20, 2017, 3:30:16 PM
    Author     : user10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var enablePadding = true;
            var patientIds = [];
            var selectedIds;
            var caseManagerId;
            var done = false;
            $(document).ready(function(){
                resetPage();
                initializeClients();
                reports();
                $("#detailstd").hide();
                $("#detailstdRe").hide();
                
                
                $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');
                
                $.ajax({
                    url: "Init_client.action",
                    dataType: "json",
                    success: function(clientsMap) {
                        $("#grid").jqGrid("clearGridData", true).setGridParam({url: "Casemanagerclients_grid.action?casemanagerId="+$("#casemanagerId").val(), page:1}).trigger("reloadGrid");
                        $("#loader").html(''); 
                        done = true;
                    }                    
                }); 
 
                $("#dialog").dialog({
                    title: "Client to Case Manager Re-Assignment",
                    autoOpen: false,
                    width: 500,
                    resizable: false,
                    buttons: [{text: "Yes", click: reAssignClients}, 
                        {text: "No", click: function(){$(this).dialog("close")}}]
                });
                
                $.ajax({
                    url: "Casemanager_retrieve.action",
                    dataType: "json",
                    success: function(caseManagerMap) {
                        var options = "<option value = '" + '0' + "'>" + '' + "</option>";
                        $.each(caseManagerMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#casemanagerId").html(options);                        
                    }                    
                }); //end of ajax call

                $("#casemanagerId").change(function(event){
                    var casemanagerId = $("#casemanagerId").val();
                    $("#reassign_button").prop("disabled", true);
                    if(casemanagerId != 0){
                        //First Ajax Call...
                        $.ajax({ //retrieves other case managers...
                            url: "Casemanagerassign_retrieve.action",
                            dataType: "json",
                            data: {casemanagerId: casemanagerId},
                            success: function(caseManagerExceptMap) {
                                var options = "<option value = '" + '0' + "'>" + '' + "</option>";
                                $.each(caseManagerExceptMap, function(key, value) {
                                    options += "<option value = '" + key + "'>" + value + "</option>";
                                }) //end each
                                $("#newcasemanagerId").html(options);                       
                            }                    
                        }); //end of ajax call
                        
                        //Populate the clients for the selected case manager...             
                        $("#grid").jqGrid("clearGridData", true).setGridParam({url: "Casemanagerclients_grid.action?casemanagerId="+casemanagerId, page:1}).trigger("reloadGrid");     
                        
                        getCaseManagerDetails(casemanagerId, "assign");
                        $("#detailstd").show();   
                        $("#detailstdRe").hide();
                    }else{
                        //Re-opulate the clients for the selected case manager...             
                        $("#grid").jqGrid("clearGridData", true).setGridParam({url: "Casemanagerclients_grid.action?casemanagerId="+casemanagerId, page:1}).trigger("reloadGrid");
                        $("#newcasemanagerId").empty();
                        $("#detailstd").hide();
                        $("#detailstdRe").hide();
                    }
                });
                
                $("#newcasemanagerId").bind("change", function(event){
                    var casemanagerId = $("#newcasemanagerId").val();
                    if(casemanagerId != "0"){ 
                        $("#reassign_button").prop("disabled", false);
                        //console.log("Has "+$("#caseManagerAssignments").html());
                        if($("#caseManagerAssignments").html() > 0){                          
                            //$("#reassign_button").prop("disabled", false);
                        }
                        getCaseManagerDetails(casemanagerId, "re-assign");
                        $("#detailstdRe").show();
                    }
                    else if(casemanagerId == "0"){
                        //console.log("Has not "+$("#caseManagerAssignments").html())
                        $("#reassign_button").prop("disabled", true);
                        $("#detailstdRe").hide();
                    }
                });

                var lastSelected = -99;
                $("#grid").jqGrid({
                    //url: "Assign_CaseManager_grid.action",
                    url: "Casemanagerclients_grid.action?casemanagerId="+$("#casemanagerId").val(),
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Hospital No", "Name", "Gender", "Date of Birth", "Address", "ART Status"],
                    colModel: [
                        //{name: "sel", index: "sel", width: "50", align: "center", formatter:"checkbox", editoptions:{value:"1:0"}, formatoptions:{disabled:false}},                        
                        {name: "hospitalNum", index: "hospitalNum", width: "100"},
                        {name: "name", index: "name", width: "150"},
                        {name: "gender", index: "gender", width: "60"},
                        {name: "dateBirth", index: "dateBirth", width: "90", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d-M-Y"}},                        
                        {name: "address", index: "address", width: "210"},                        
                        {name: "currentStatus", index: "currentStatus", width: "100"}
                    ],
                    pager: $('#pager'),
                    rowNum: 100,
                    sortname: "hospitalNum",
                    sortorder: "desc",
                    viewrecords: true,
                    multiselect: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 250,                    
                    jsonReader: {
                        root: "caseManagerClientsList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "patientId"
                    }, 
                    onSelectRow: function(id) {
                        var data = $(this).getRowData(id);
                        if(data.sel == 1) {
                            $("#grid").jqGrid('setRowData', id, false, {background: 'khaki'});
                        }
                        else {
                            $("#grid").jqGrid('setRowData', id, false, {background: 'khaki'});
                        }                        
                    }
                });
                
                $("#reassign_button").bind("click", function(event){
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                        var casemanagerId = $("#newcasemanagerId").val();
                        selectedIds = jQuery("#grid").jqGrid('getGridParam','selarrrow');                       
                        if(selectedIds == ""){
                            alert("Please mark clients to de-assign");
                        }
                        else{
                            patientIds = selectedIds;
                            caseManagerId = casemanagerId;
                            $("#dialog").dialog("open");
                        }
                        event.preventDefault();
                        return false;                        
                    }
                });

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Casemanagement_page");
                    return true;
                });
            });
            
            function getCaseManagerDetails(casemanagerId, option) {
                $.ajax({
                    url: "Casemanagerdetails_retrieve.action?casemanagerId="+casemanagerId,
                    dataType: "json",
                    success: function(caseManagerDetailsMap) {
                        console.log(caseManagerDetailsMap)
                        if(option.includes("re")){
                            //$("#caseManagerReligionRe").html(caseManagerDetailsMap.religion);
                            $("#caseManagerGenderRe").html(caseManagerDetailsMap.sex.toUpperCase());                
                            $("#caseManagerAssignmentsRe").html(caseManagerDetailsMap.clientCount);
                        }else{
                            //$("#caseManagerReligion").html(caseManagerDetailsMap.religion);
                            $("#caseManagerGender").html(caseManagerDetailsMap.sex.toUpperCase());     
                            $("#caseManagerAssignments").html(caseManagerDetailsMap.clientCount);
                        }
                        
                    }                    
                }); //end of ajax call
            }
            
            function reAssignClients() {  
                $("#dialog").dialog("close");
                var data = {"patient_ids" : selectedIds.toString(), "casemanagerId" : caseManagerId.toString()};
                $.ajax({
                    url: "Casemanager_reassign.action",
                    data: data,
                    dataType: "json",
                    success: function(assignmentMap) {
                       if(assignmentMap.response.includes("success")){
                           $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');
                           $("#grid").setGridParam({url: "Casemanagerclients_grid.action?casemanagerId="+$("#casemanagerId").val(), page:1}).trigger("reloadGrid");
                           $("#loader").html('');
                           $("#casemanagerId").val('');
                           $("#newcasemanagerId").val('');
                           $("#detailstd").hide();
                           $("#detailstdRe").hide();
                        }
                    }
                });
            }                
           
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_casemanagement.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/tools.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> Case Management >> Re-Assign Case Managers</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Assign Clients Case Managers</td>
                            </tr>                                                                                 
                            <tr><td>
                            <td><input name="patientIds" type="hidden" id="patientIds"/></td>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>De-Assign From:</label></td>
                                <td>
                                    <select name="casemanagerId" style="width: 200px;" class="inputboxes" id="casemanagerId">
                                            <option></option>
                                    </select><span id="caseManagerHelp" class="errorspan"></span>
                                </td>
                                <td width="10%"><label>Assign To:</label></td>
                                <td>
                                    <select name="newcasemanagerId" style="width: 200px;" class="inputboxes" id="newcasemanagerId">
                                    </select><span id="newcaseManagerHelp" class="errorspan"></span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td id="detailstd" style="display: none">
                                    <!--<label><strong id="caseManagerReligion">Christianity</strong> : </label>-->  
                                    <label>Gender: <strong id="caseManagerGender"></strong> : </label>
                                    <label><strong id="caseManagerAssignments">0</strong> Clients(s) Assigned </label>
                                </td>
                                <td></td>
                                <td id="detailstdRe" style="display: none">
                                    <!--<label><strong id="caseManagerReligionRe">Christianity</strong> : </label>-->  
                                    <label>Gender: <strong id="caseManagerGenderRe"></strong> : </label>
                                    <label><strong id="caseManagerAssignmentsRe">0</strong> Clients(s) Assigned </label>
                                </td>
                            </tr>
                        </table>					
                        <p></p>                            
                        <div>
                            <fieldset>  
                                <legend> Case Manager Clients' List</legend>
                                <table width="99%" height="90" border="0" class="space">
                                    <tr>
                                        <td>
                                            <table id="grid"></table>
                                            <div id="pager" style="text-align:center;"></div>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>
                        
                        <div id="dialog">
                            <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td><label>Do you want to continue with case manager assignment for select clients?</label></td>
                                </tr>
                                <tr>
                                    <td width="20%"><label>Click Yes to continue or No to cancel:</label></td>
                                </tr>   
                            </table>
                        </div>
                        
                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 200px">
                            <button id="reassign_button" disabled>Re-Assign</button> &nbsp;<button id="close_button">Close</button>
                        </div>                        
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
