<%-- 
    Document   : viral_load_report
    Created on : Sep 15, 2017, 9:59:45 AM
    Author     : user10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/JavaScript">
            var updateRecord = false; 
            $(document).ready(function(){
                resetPage();
                reports();
                                 
                $("#date1").mask("99/99/9999"); 
                $("#date1").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    constrainInput: true,
                    buttonImageOnly: true,
                    buttonImage: "/images/calendar.gif"
                });

                $("#date2").mask("99/99/9999");
                $("#date2").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    constrainInput: true,
                    buttonImageOnly: true,
                    buttonImage: "/images/calendar.gif"
                });

                $("#ok_button").bind("click", function(event){
                    //if(validateForm()) {
                    event.preventDefault();
                    event.stopPropagation();
                    var reportType = $("input[name=reportType]:checked").val();

                    if(reportType == "1") {
                        url = "Baselinevl_due.action";
                    } 
                    else if(reportType == "2"){
                        url = "Secondvl_due.action";
                    }
                    else if(reportType == "3"){
                        url = "Routinevl_due.action";
                    }
                    else if(reportType == "4"){
                        url = "Repeatvl_due.action";
                    }
                    window.open(url);
                    //}
                    return false;
                });              
                $("#cancel_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Clinic_page");
                    return true;
                });
            }); 

            function validateForm() {
                var validate = true; 
                // check for valid input is entered
                if($("#date1").val().length == 0){
                    $("#dateHelp1").html(" *");
                    validate = false;
                }
                else {
                    $("#date1").datepicker("option", "altField", "#reportingDateBegin");    
                    $("#date1").datepicker("option", "altFormat", "mm/dd/yy");    
                    $("#dateHelp1").html("");                    
                }
                if($("#date2").val().length == 0){
                    $("#dateHelp2").html(" *");
                    validate = false;
                }
                else {
                    $("#date2").datepicker("option", "altField", "#reportingDateEnd");    
                    $("#date2").datepicker("option", "altFormat", "mm/dd/yy");    
                    $("#dateHelp2").html("");                    
                }
                return validate; 
            }                                         
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_clinic.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/report.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> ART Clinic >> Patients Due For Viral Load Test</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Viral Load Reporting</td>
                            </tr>
                        </table>
                        <p></p>
                            
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td> </td>
                                <td colspan="3"><input type="radio" name="reportType" value="1" checked/><label>Patients due for Baseline Viral Load Test</label></td>
                            </tr>                            
                            <tr> </tr>
                            <tr>
                                <td> </td>
                                <td colspan="3"><input type="radio" name="reportType" value="2"/><label>Patients due for Second Viral Load Test</label></td>
                            </tr>       
                            <tr>
                                <td> </td>
                                <td colspan="3"><input type="radio" name="reportType" value="3"/><label>Patients due for Routine Viral Load Test (Suppressed)</label></td>
                            </tr> 
                            <tr>
                                <td> </td>
                                <td colspan="4"><input type="radio" name="reportType" value="4"/><label>Patients due for Repeat Viral Load Test (Un-suppressed)</label></td>
                            </tr> 
                            <tr> </tr>
                            <p></p>                         
                        </table>
                        <p></p>
                        
                        <div id="buttons" style="width: 200px">
                            <button id="ok_button">Ok</button> &nbsp;<button id="cancel_button">Cancel</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
