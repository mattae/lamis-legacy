<%-- 
    Document   : Data Export
    Created on : Aug 15, 2012, 6:53:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/json.min.js"></script>
        <script type="text/javascript" src="js/highcharts.js"></script>               
        <script type="text/javascript" src="js/modules/exporting.js"></script>
        <s:url id="fileDownload" namespace="/" action="sync_download" ></s:url>
        <script type="text/JavaScript">
            var facilityIds = [];
            $(document).ready(function(){
                initialize();
                reports();
                             
                $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                
                $.ajax({
                    url: "Sync_summary.action",
                    dataType: "json",
                    success: function(obj) {
                        $("#facilitySyncing").html(obj.syncMap["facilitySyncing"]); 
                        $("#facilityExpected").html(obj.syncMap["facilityExpected"]); 
                    }                    
                }); //end of ajax call
                 
                $("#grid").jqGrid({
                    url: "Sync_analyzer.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["State", "Facility", "Last Sync", "", "", ""],
                    colModel: [
                        {name: "state", index: "state", width: "150"},
                        {name: "facility", index: "facility", width: "440"},
                        {name: "dateLastSync", index: "dateLastSync", width: "100", align: 'center'},
                        {name: "status", index: "status", width: "50"},
                        {name: "sync", index: "sync", width: "0", hidden: true},
                        {name: "facilityId", index: "facilityId", width: "100", hidden: true},
                    ],
                    rowNum: -1,
                    viewrecords: true, 
                    imgpath: "themes/basic/images",
                    resizable: false,                    
                    height: 180,
                    loadtext: "Analyzing data, this may take some minutes...",                    
                    jsonReader: {
                        root: "syncList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false
                    },
                    loadComplete: function() {
                        var gridIds = $("#grid").getDataIDs();
                        for(i = 0; i < gridIds.length; i++) {
                            var data = $("#grid").getRowData(gridIds[i]);
                            if(data.sync == "0") {
                                $("#grid").jqGrid('setCell', i+1, 'status', '', {background: '#ff9933'});
                            }
                            else {
                                $("#grid").jqGrid('setCell', i+1, 'status', '', {background: '#ccff33'});                                
                            }                        
                        }
                        $("#loader").html('');                                
                    },
                    onSelectRow: function(id) {
                        var data = $("#grid").getRowData(id)
                        $("#facilityId").val(data.facilityId);
                        getAudit();
                        getChart();
                    }               
                }); //end of jqGrid                 
            });
            
            function getAudit() {
                $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');					                             
                $.getJSON("Sync_audit.action", {facilityId: $('#facilityId').val()}, function(auditList) {
                    $("#newRecPatient").html(auditList[0].newRecPatient);
                    $("#newRecClinic").html(auditList[0].newRecClinic);
                    $("#newRecPharm").html(auditList[0].newRecPharm);
                    $("#newRecLab").html(auditList[0].newRecLab);
                    $("#newEnrolled").html(auditList[0].newEnrolled);
                    $("#everEnrolled").html(auditList[0].everEnrolled);
                    $("#currentCare").html(auditList[0].currentCare);
                    $("#currentART").html(auditList[0].currentART);
                });            
            }

            function getChart() {
                $.getJSON("SyncPatient_chart.action", {facilityId: $('#facilityId').val()}, function(json) {
                    $("#container1").css({width: '100%', height: '100%', position: "absolute", margin: '0 auto'});
                    setChart1(json);    			
                });
                $.getJSON("SyncClinic_chart.action", {facilityId: $('#facilityId').val()}, function(json) {
                    $("#container2").css({width: '100%', height: '100%', position: "absolute", margin: '0 auto'});
                    setChart2(json);    			
                });
                $.getJSON("SyncPharm_chart.action", {facilityId: $('#facilityId').val()}, function(json) {
                    $("#container3").css({height: '100%', width: '100%', position: "absolute", margin: '0 auto'});
                    setChart3(json);    			
                });   
                $.getJSON("SyncLab_chart.action", {facilityId: $('#facilityId').val()}, function(json) {
                    $("#container4").css({width: '100%', height: '100%', position: "absolute", margin: '0 auto'});
                    setChart4(json);    			
                });                   
            }
            
            function setChart1(json) {
                $('#container1').highcharts({
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: json.title
                    },
                    subtitle: {
                        text: json.subtitle
                    },
                    xAxis: {
                        categories: json.categories
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: json.titleForYAxis
                        }
                    },
                    colors: ['#FF0000'],
                    tooltip: {
                        headerFormat: '<span style="font-size:9px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: json.series
                });
            }

            function setChart2(json) {
               $('#container2').highcharts({
                   chart: {
                        type: 'line'
                    },
                    title: {
                        text: json.title
                    },
                    subtitle: {
                        text: json.subtitle
                    },
                    xAxis: {
                        categories: json.categories
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: json.titleForYAxis
                        }
                    },
                    colors: ['#3399FF'],
                    tooltip: {
                        headerFormat: '<span style="font-size:9px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            colorByPoint: true,
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: json.series
                });
            }

            function setChart3(json) {
                $('#container3').highcharts({
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: json.title
                    },
                    subtitle: {
                        text: json.subtitle
                    },
                    xAxis: {
                        categories: json.categories
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: json.titleForYAxis
                        }
                    },
                    colors: ['#FFCC00'],
                    tooltip: {
                        headerFormat: '<span style="font-size:9px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            colorByPoint: true,
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: json.series
                });
            }

            function setChart4(json) {
               $('#container4').highcharts({
                   chart: {
                        type: 'line'
                    },
                    title: {
                        text: json.title
                    },
                    subtitle: {
                        text: json.subtitle
                    },
                    xAxis: {
                        categories: json.categories
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: json.titleForYAxis
                        }
                    },
                    colors: ['#009933'],
                    tooltip: {
                        headerFormat: '<span style="font-size:9px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            colorByPoint: true,
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: json.series
                });
                $("#loader").html('');                                
            }
        </script>
    </head>  

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">               
                <jsp:include page="/WEB-INF/views/template/nav_event.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                 <span>
                                    <img src="images/monitor.png" style="margin-bottom: -5px;" /> &nbsp;
                                    <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Events Monitor >> Sync Analyzer</strong></span>
                                </span>
                                <hr style="line-height: 2px"/>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div>
                            <table width="99%"  border="0">
                                <tr>
                                    <td width="30%"><label style="font-size:14px">No. of facilities Syncing: </label></td>
                                    <td width="30%"><span id="facilitySyncing" style="color:blue;font-size:14px"></span></td>
                                    <td width="15%"></td>
                                    <td width="45%"></td>                                                                    
                                </tr>
                                <tr>
                                    <td><label style="font-size:14px">No. of facilities Expected: </label></td>
                                    <td><span id="facilityExpected" style="color:blue;font-size:14px"></span></td>                                                                    
                                </tr>
                                <tr>
                                    <td colspan="5"><hr></hr></td>
                                </tr>
                                <tr>
                                    <td width="30%">
                                        <s:a href="%{fileDownload}">Export Sync to Excel</s:a>
                                </tr>
                                <tr>
                                    <td colspan="5"><hr></hr></td>
                                </tr>
                            </table>
                        </div>
                        
                        <div>
                            <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <p></p>
                        
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Facility Activity Summary</td>
                            </tr>
                        </table>
                        <div>
                            <table width="99%"  border="0">
                                <tr>
                                    <td width="30%"><label style="font-size:14px">No. of registrations entered: </label></td>
                                    <td width="30%"><span id="newRecPatient" style="color:blue;font-size:14px"></span></td>
                                    <td width="15%"><label style="font-size:14px">Newly enrolled: </label></td>
                                    <td width="45%"><span id="newEnrolled" style="color:blue;font-size:14px"></span></td>
                                </tr>
                                <tr>
                                    <td><label style="font-size:14px">No. of clinic records entered: </label></td>
                                    <td><span id="newRecClinic" style="color:blue;font-size:14px"></span></td>
                                    <td><label style="font-size:14px">Ever enrolled: </label></td>
                                    <td><span id="everEnrolled" style="color:blue;font-size:14px"></span></td>
                                </tr>
                                <tr>
                                    <td><label style="font-size:14px">No. of pharmacy records entered: </label></td>
                                    <td><span id="newRecPharm" style="color:blue;font-size:14px"></span></td>
                                    <td><label style="font-size:14px">Current on care: </label></td>
                                    <td><span id="currentCare" style="color:blue;font-size:14px"></span></td>
                                </tr>
                                <tr>
                                    <td><label style="font-size:14px">No. of lab records entered: </label></td>
                                    <td><span id="newRecLab" style="color:blue;font-size:14px"></span></td>
                                    <td><label style="font-size:14px">Current on ART: </label></td>
                                    <td><span id="currentART" style="color:blue;font-size:14px"></span></td>
                                </tr>
                                <tr>                                
                                    <td width="20%"><input name="facilityId" type="hidden" id="facilityId"/></td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Facility Activity Trend</td>
                            </tr>
                        </table>
                        <div id="chartPanelScroll">
                            <div id="chartSync1"> 
                                <div id="container1"></div>
                            </div>
                            <div id="chartSync2"> 
                                <div id="container2"></div>
                            </div>
                            <div id="chartSync3"> 
                                <div id="container3"></div>
                            </div>
                            <div id="chartSync4"> 
                                <div id="container4"></div>
                            </div>                    
                        </div>
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>

<%--
    $.getJSON("Sync_chart.action", {facilityId: $('#facilityId').val()}, function(json) {
        $("#container").css({minWidth: '400px', height: '270px', margin: '0 auto'});
        setChart(json);    			
    });






$.post("yoururlhere", function(JSONData) {
    var obj = $.parseJSON(JSONData);
    if (obj.status.toLowerCase() === "success") {
        for (var key in obj.result) {
            if (obj.result.hasOwnProperty(key)) {
               console.log(key + ': ' + obj.result[key]);
            }
        }
    }
});
--%>
