<%-- 
    Document   : Facility
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/JavaScript">
            var obj = {};
            $(document).ready(function(){
                resetPage();
                
                $.ajax({
                    url: "StateId_retrieve.action",
                    dataType: "json",
                    success: function(stateMap) {
                        var options = "<option value = '" + '' + "'>" + '' + "</option>";
                        $.each(stateMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#stateId").html(options);                        
                    }                    
                }); //end of ajax call

                $("#grid").jqGrid({
                    url: "Encounter_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Hospital No.", "Name", "Regimen Dispensed", "Quantity", "Date of Refill", "Community Pharmacy"],
                    colModel: [
                        {name: "hospitalNum", index: "hospitalNum", width: "80"},
                        {name: "name", index: "name", width: "120"},
                        {name: "regimen1", index: "regimen1", width: "200"},
                        {name: "duration1", index: "duration1", width: "50", align: "center"},
                        {name: "dateVisit", index: "dateVisit", width: "80", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},                                        
                        {name: "pharmacy", index: "pharmacy", width: "200"},
                    ],
                    rowNum: -1,
                    sortname: "encounterId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 450,                    
                    jsonReader: {
                        root: "encounterList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "encounterId"
                    }
                }); 

                $("#stateId").change(function(event){
                    $.ajax({
                        url: "LgaId_retrieve.action",
                        dataType: "json",
                        data: {stateId: $("#stateId").val()},

                        success: function(lgaMap) {
                            var options = "<option value = '" + '' + "'>" + '' + "</option>";
                            $.each(lgaMap, function(key, value) {
                                options += "<option value = '" + key + "'>" + value + "</option>";
                            }) //end each
                            $("#lgaId").html(options);                        
                        }                    
                    }); //end of ajax call
                });     
                $("#lgaId").change(function(event){
                    $.ajax({
                        url: "Facility_retrieve.action",
                        dataType: "json",
                        data: {stateId: $("#stateId").val(), lgaId: $("#lgaId").val()},

                        success: function(facilityMap) {
                            var options = "<option value = '" + '' + "'>" + '' + "</option>";
                            $.each(facilityMap, function(key, value) {
                                options += "<option value = '" + key + "'>" + value + "</option>";
                            }) //end each
                            $("#facilityId").html(options);                        
                        }                    
                    }); //end of ajax call
                }); 
                 
                $("#facilityId").change(function(event){
                    var name = $("select[name='facilityId'] option:selected").text();
                    $("#facilityName").val(name);
                });
                
                $("#ok_button").bind("click", function(event){
                    if(validateForm()) {
                        $("#lamisform").attr("action", "Verify_group");
                        return true;
                    } 
                    else {
                        return false;
                    }
                });
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Event_page");
                    return true;
                });
            });
            
            function refreshGrid() {
            }                                         

            var timer = $.timer(function() {
                $("#grid").setGridParam({url: "Encounter_grid.action?q=1", page:1}).trigger("reloadGrid");
            });
            timer.set({time : 120000, autostart : true});

        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_event.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/monitor.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Events Monitor >> Community Pharmacy Events</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Community ARV Refill Monitor</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><input name="facilityName" type="hidden" id="facilityName"/></td>
                                <td width="30%"></td>
                                <td width="20%"></td>
                                <td width="30%"></td>
                            </tr>
                            <tr>
                                <td><label>State:</label></td>
                                <td>
                                    <select name="stateId" style="width: 130px;" class="inputboxes" id="stateId">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>L.G.A.</label></td>
                                <td>
                                    <select name="lgaId" style="width: 130px;" class="inputboxes" id="lgaId">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Facility:</label></td>
                                <td>
                                    <select name="facilityId" style="width: 250px;" class="inputboxes" id="facilityId">
                                    </select><span id="facilityHelp" class="errorspan"></span>
                                </td>
                            </tr>
                        </table>
                        <div>
                            <fieldset>  
                                <legend> Refill Events</legend>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
