<%-- 
    Document   : treatment_tracker_page
    Created on : Dec 5, 2018, 5:20:30 PM
    Author     : user10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>    
        <script type="text/javascript" src="js/lamis/report-common.js"></script> 
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        
        <script type="text/JavaScript">
            var updateRecord = false; 
            $(document).ready(function(){
                resetPage();
                reports();
                
                $("body").bind('ajaxStart', function(event){
                    $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                });

                $("body").bind('ajaxStop', function(event){
                    $("#loader").html('');
                });
                $("#messageBar").hide();
                
                $.ajax({
//                    url: "StateId_retrieve.action",
                    url: "StateId_retrieve_custom.action",
                    dataType: "json",
                    success: function(stateMap) {
                        var options = "<option value = '" + '' + "'>" + '' + "</option>";
                        $.each(stateMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#stateId").html(options);                        
                    }                    
                }); //end of ajax call
                
                $("#stateId").change(function(event){
                    $("#messageBar").slideUp('fast'); 
                    $("#ok_button").attr("disabled", false);
                    
                });
                
                $("#date1").mask("99/99/9999");
                $("#date1").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    constrainInput: true,
                    buttonImageOnly: true,
                    buttonImage: "/images/calendar.gif" 
                });
                
                $("#ok_button").bind("click", function(event){
                    if(validateForm()) {
                       convertTrackerData(15);
                    }
                    return false;  
                });
                
                $("#cancel_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Event_page");
                    return true;
                });
                
            }); 
            
            var url = "";
            var x = function wait() {window.open(url);}
            function convertTrackerData(recordType) { 
                $("#messageBar").hide();
                $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                $("#ok_button").attr("disabled", true);
                $.ajax({
                    url: "Converter_dispatch.action",
                    dataType: "json",
                    data: {recordType: recordType, reportingDateBegin: $("#reportingDateBegin").val(), stateId: $("#stateId").val()},
                    beforeSend: function(){
                        console.log("About to generate report...");
                    },
                    success: function(fileName) {
                        $("#messageBar").html("Report generated successfully!").slideDown('slow');
                        $("#ok_button").attr("disabled", false); 
                        console.log(fileName);
                        url = fileName;
                        window.setTimeout(x, 3000);
                    }, 
                    error: function(e){
                        alert("There was an error in report generation!");
                        console.log("Error: "+JSON.stringify(e));
                        $("#ok_button").attr("disabled", false); 
                    }
                }); 
            }
            
            function validateForm() {
                var validate = true; 

                // check for valid input is entered
                if($("#date1").val().length == 0){
                    $("#dateHelp1").html(" *");
                    validate = false;
                }
                else {
                    $("#date1").datepicker("option", "altField", "#reportingDateBegin");    
                    $("#date1").datepicker("option", "altFormat", "mm/dd/yy");    
                    $("#dateHelp1").html("");                    
                }
                return validate;
            }
                                   
        </script>
    </head>
    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_event.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/monitor.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Events Monitor >> Treatment Tracker</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Treatment Tracker</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        <p></p>
                        
                        <div style="margin-right: 10px;">
                            <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td width="20%"><label>Reporting Period: </label></td>
                                    <td colspan="3"> <span style="margin-left:0px"></span><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="reportingDateBegin" type="hidden" id="reportingDateBegin"/><span id="dateHelp1" style="color:red"></span></td>                           
                                </tr>
                                <tr>
                                    <td width="15%"><label>Select state:</label></td>
                                    <td  width="85%">
                                        <select name="stateId" style="width: 300px;" class="inputboxes" id="stateId">
                                        </select><span id="stateHelp" class="errorspan"></span>
                                    </td>                                                                
                                </tr>
                            </table> 
                        </div>								
                        <p></p>                        
                        <div id="buttons" style="width: 200px">
                            <button id="ok_button">Continue</button> &nbsp;<button id="cancel_button">Cancel</button>
                        </div> 
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
