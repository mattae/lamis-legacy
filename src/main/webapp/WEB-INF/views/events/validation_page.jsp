<%-- 
    Document   : validation_page
    Created on : Aug 7, 2017, 10:38:24 AM
    Author     : DURUANYANWU IFEANYI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var selected_ids;
            var recordIds = [];
            var creators = [];
            var patientIds = [];
            var gridNum = 4;
            var enablePadding = true;
            $(document).ready(function(){
                //console.log("ready");
                $.ajax({
                    url: "Padding_status.action",
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                });
                resetPage();
                initialize();
                reports();
                
                for(i = new Date().getFullYear(); i > 1900; i--) {
                    $("#validationYear").append($("<option/>").val(i).html(i));
                }
                
                $("#validationMonth").val("08");
                $("#validationYear").val("2016");
                

                $("#grid").jqGrid({
                    url: "validation_grid.action?year=2016&month=08",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["ID", "Entity", "Count"],
                    colModel: [
                        {name: "id", index: "id", width: "50"},
                        {name: "entity", index: "entity", width: "500"},
                        {name: "entityCount", index: "entityCount", width: "185"}
                    ],  
                    sortname: "id",
                    sortorder: "asc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,                
                    height: 150,                    
                    jsonReader: {
                        root: "validationList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "id"
                    },
                    onSelectRow: function(id) {
                        var selectedRow = $("#grid").getGridParam('selrow');
                        if(selectedRow != null) {
                            var data = $("#grid").getRowData(selectedRow);
                            var entity = data.id;
                            //console.log("Entity is: "+entity);
                            $("#detail").jqGrid("clearGridData", true).setGridParam({url: "entity_grid.action?entity="+entity, page:1}).trigger("reloadGrid");
                            $("#validate_button").attr("disabled", false);
                        }
                        
                    }               
                }); //end of master jqGrid                 
                
                $("#detail").jqGrid({
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["","S/N", "Patient ID ", "Date", "Hospital Number", "Name", "Status", "", ""],
                    colModel: [
                        {name: "sel", index: "sel", width: "30", align: "center", formatter:"checkbox", editoptions:{value:"1:0"}, formatoptions:{disabled:false}},
                        {name: "sn", index: "sn", width: "40"},
                        {name: "patientId", index: "patientId", width: "85"},
                        {name: "dateRegistration", index: "dateRegistration", width: "100", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},
                        {name: "hospitalNum", index: "hospitalNum", width: "150"},
                        {name: "name", index: "name", width: "250"},
                        {name: "statusRegistration", index: "statusRegistration", width: "80"},
                        {name: "fullName", index: "fullName", width: "5", hidden: true},
                        {name: "validated", index: "validated", width: "5", hidden: true}
                    ],
                    pager: $('#pager'),
                    rowNum: -1,
                    sortname: "patientId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    loadtext: "Wait, while record loads...", 
                    resizable: false,   
//                    multiselect: true,
                    height: 250,                    
                    jsonReader: {
                        root: "entityList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "patientId"
                    },      
                    afterInsertRow: function(id, data) {
                        if(data.validated == "true") {
                            $(this).jqGrid('setRowData', id, false, {background: 'lime'});
                        }                        
                    },
                    onSelectRow: function(id) {
                        var data = $(this).getRowData(id);
                        if(data.sel == 1) {
                            $(this).jqGrid('setRowData', id, true, {color: 'black'});
                        }
                        else {
                            $(this).jqGrid('setRowData', id, false, {color: 'black'});                            
                        }
                        
                    }
                }); //end of detail jqGrid                 
                
                $("#validate_button").bind("click", function(event){
                    //selected_ids = jQuery("#detail").jqGrid('getGridParam','selarrrow'); 
                    recordIds = [];
                    creators = [];
                    patientIds = [];
                    var ids = $("#detail").getDataIDs();
                        for(var i = 0; i < ids.length; i++) {
                            var data = $("#detail").getRowData(ids[i]);
                            if(data.sel == 1) {
                                var hospNum = data.hospitalNum;
                                var report_date = data.dateRegistration;
                                var entity_id = hospNum+"#"+report_date;
                                recordIds.push(entity_id);
                                creators.push(data.fullName);
                                patientIds.push(data.patientId);
                            }
                        }
                    if(recordIds == ""){
                        alert("Please mark records to validate");
                    }
                    else{
                        submitValidation(recordIds, creators, patientIds);
                    }
                    event.preventdefault();
                });
                $("#close_button").bind("click", function(event){
                  $("#lamisform").attr("action", "Event_page");
                   return true;
                });
                $("#profile_button").bind("click", function(event){
                    //console.log("Profiler Clicked");
                    buildProfiler();
                    event.preventDefault();
                });
                $("#fetch_records").bind("click", function(event){
                    //console.log(" Record Fetcher Clicked");                  
                    retrieveResult();
                    event.preventDefault();
                });
            });           
            
            function retrieveResult() {
                
                var url = "validation_grid.action?year=" + $("#validationYear").val()+"&month=" + $("#validationMonth").val(); 
                $("#detail").jqGrid("clearGridData", true);
                $("#validate_button").attr("disabled", true);
                $("#validationMonth").val($("#validationMonth").val());
                $("#validationYear").val($("#validationYear").val());
                $("#grid").jqGrid("clearGridData", true).setGridParam({url: url, page:1}).trigger("reloadGrid");   
            }
            
            var url = "";
            var x = function wait() {window.open(url);}
            
            function buildProfiler(){
                $("#messageBar").hide();
                //$("#ok_button").attr("disabled", true);
                $.ajax({
                    url: "Profiler_download.action",
                    dataType: "json",
                    data: {recordType: 1, year: $("#validationYear").val(), month: $("#validationMonth").val()},
                    beforeSend: function(){
                        //console.log($("#recordType").val(), $("#yearId option:selected").text(), $("#stateId option:selected").text(), $("#labtestId").val(), $("#reportingDateBegin").val(), $("#reportingDateEnd").val(), fac_ids.toString());
                    },
                    success: function(fileName) {
                        console.log(fileName);
                        $("#messageBar").html("Profiling Report Generated Successfully").slideDown('slow');
                        //$("#ok_button").attr("disabled", false);
                        url = fileName;
                        window.setTimeout(x, 3000);
                    }, 
                    error: function(e){
                        console.log("Error: "+JSON.stringify(e));
                        alert("There was an error in conversion!");
                        //$("#ok_button").attr("disabled", false);
                    }
                }); 
            }
           
            function submitValidation(recordIds, creators, patientIds){
                
                var data = {"records_ids" : recordIds.toString(), "creators" : creators.toString(), "patient_ids": patientIds.toString()};
                console.log(data);
                $.ajax({
                    url: "save_validation.action",
                    data: data,
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                });
            }
          
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_event.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                     <span>
                                        <img src="images/monitor.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Administration >> Events Monitor >> Data Profiling and Validation</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">
                                    <table width="97%" border="0">
                                        <tr>
                                            <td width="15%"><label>Validation Period</label></td>
                                            <td width="26%">
                                                    <select name="validationMonth" style="width: 100px;" class="inputboxes" id="validationMonth"/>
                                                       <option></option>
                                                       <option value="01">January</option>
                                                       <option value="02">February</option>
                                                       <option value="03">March</option>
                                                       <option value="04">April</option>
                                                       <option value="05">May</option>
                                                       <option value="06">June</option>
                                                       <option value="07">July</option>
                                                       <option value="08">August</option>
                                                       <option value="09">September</option>
                                                       <option value="10">October</option>
                                                       <option value="11">November</option>
                                                       <option value="12">December</option>
                                                    </select>
                                                    <select name="validationYear" style="width: 75px;" class="inputboxes" id="validationYear"/>
                                                       <option></option>
                                                    </select>
                                            </td>
                                        <td width="15%"><button id="fetch_records">Fetch Records</button></td>
                                           <td><input name="clinicId" type="hidden" id="clinicId" /><input name="patientId" type="hidden" id="patientId" /></td>
                                                        </tr>
                                    </table>                               
                                        </td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        <p></p>
                        
                        <div>
                            <fieldset> 
                            <legend>Records Listing</legend>                            
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>                                       
                                        <p></p>
                                        <table id="detail"></table>
                                        <div id="pager" style="text-align:center;"></div>
                                    </td>
                                  </tr>                              
                                </table>
                            </fieldset>  
                        </div>
                        <p></p>
                        
                        <div id="buttons" style="width: 200px">
                            <button id="validate_button" disabled="true">Validate</button>&nbsp;<button id="close_button">Close</button>
                        </div> 
                        <div style="align: left">
                            <button id="profile_button">Profile Data</button>
                        </div> 
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
