<%-- 
    Document   : eid2
    Created on : Oct 30, 2014, 2:53:09 PM
    Author     : user1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<table width="100%" border="0">
    <tr>
        <td class="topheaders">Sender Details</td>
    </tr>
</table>
<table width="99%" border="0" class="space" cellpadding="3">
    <tr>
        <td width="20%"><label>Name:</label></td>
        <td width="30%"><input name="senderName" type="text" style="width: 200px;" class="inputboxes" id="senderName"/></td>
        <td width="20%"><label>Designation:</label></td>
        <td width="30%"><input name="senderDesignation" type="text" style="width: 150px;" class="inputboxes" id="senderDesignation"/></td>
    </tr>
    <tr>
        <td><label>Address:</label></td>
        <td><input name="senderAddress" type="text" style="width: 200px;" class="inputboxes" id="senderAddress"/></td>
    </tr>
    <tr>
        <td><label>Phone:</label></td>
        <td><input name="senderPhone" type="text" class="inputboxes" id="senderPhone"/></td>
    </tr>
</table>

<table width="100%" border="0">
    <tr>
        <td class="topheaders">Clinical Information</td>
    </tr>
</table>
<table width="99%" border="0" class="space" cellpadding="3">
    <tr>
        <td width="18%"><label>Reason for PCR:</label></td>
        <td width="27%">
            <select name="reasonPcr" style="width: 200px;" class="inputboxes" id="reasonPcr">
                <option></option>
                <option>1st test for health exposed baby</option>
                <option>1st test for sick baby</option>
                <option>Repeat test after cessation of breastfeeding</option>
                <option>Repeat because of problem with 1st test</option>
                <option>Repeat to confirm 1st result</option>
            </select>
        </td>
        <td width="18%"><label>Rapid Test Done?:</label></td>
        <td width="27%">
            <select name="rapidTestDone" style="width: 100px;" class="inputboxes" id="rapidTestDone">
                <option></option>
                <option>Yes</option>
                <option>No</option>
            </select>                                    
        </td>
    </tr>
    <tr>
        <td><label>Date Rapid Test Done:</label></td>
        <td><input name="date4" type="text" style="width: 100px;" class="inputboxes" id="date4"/><input name="dateRapidTest" type="hidden" id="dateRapidTest"/></td>
        <td><label>Result of Rapid Test:</label></td>
        <td>
            <select name="rapidTestResult" style="width: 100px;" class="inputboxes" id="rapidTestResult">
                <option></option>
                <option>Negative</option>
                <option>Positive</option>
                <option>Indeterminate</option>
            </select>                                                                        
        </td>
    </tr>
</table>

<table width="100%" border="0">
    <tr>
        <td class="topheaders">Intervention received by Mother during Pregnancy</td>
    </tr>
</table>

<table width="99%" border="0" class="space" cellpadding="3">
    <tr>
        <td width="20%"><label>ART Received?:</label></td>
        <td width="30%">
            <select name="motherArtReceived" style="width: 200px;" class="inputboxes" id="motherArtReceived">
                <option></option>
                <option>No</option>
                <option>ART started during pregnancy</option>
                <option>ART started before pregnancy</option>
            </select>
        </td>
        <td width="20%"></td>
        <td width="30%"></td>
    </tr>
    <tr>
        <td><label>ARV Prophylaxis &nbsp;received by mother:</label></td>
        <td>
            <select name="motherProphylaxReceived" style="width: 150px;" class="inputboxes" id="motherProphylaxReceived">
                <option></option>
                <option>No</option>
                <option>AZT + 3TC + sdNVP in labour</option>
                <option>AZT + sdNVP in labour</option>
                <option>Single dose NVP in labour</option>
                <option>Triple Regimen</option>
                <option>Unknown</option>
            </select>                                    
        </td>
    </tr>
    <tr>
        <td><label>Next Clinic Appointment</label></td>
        <td><input name="date5" type="text" style="width: 100px;" class="inputboxes" id="date5"/><input name="nextAppointment" type="hidden" id="nextAppointment"/></td>
        <td></td>
        <td></td>
    </tr>
</table>                     