<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />

        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/laboratory-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var labtestIds = [];
            var date = "";
            var lastSelectDate = "";
            var updateRecord = false;
            $(document).ready(function(){
                resetPage();
                initialize();
                reports();
                
                $.ajax({
                    url: "Laboratory_retrieve.action",
                    dataType: "json",                    
                    success: function(laboratoryList) {
                        populateForm(laboratoryList);
                    }                     
                }); //end of ajax call           


                var lastSelected = -99;
                $("#grid").jqGrid({
                    url: "Labresult_grid_retrieve.action",
                    datatype: "json", 
                    mtype: "GET",
                    colNames: ["Test Description", "Absolute", "Unit", "Relative", "Unit", "Comment", "Indication"],
                    colModel: [
                        {name: "description", index: "description", width: "200"},
                        {name: "resultab", index: "resultab", width: "90", sortable:false, editable:true, edittype:"text"},
                        {name: "measureab", index: "measureab", width: "60"},
                        {name: "resultpc", index: "resultpc", width: "90", sortable:false, editable:true, edittype:"text"},
                        {name: "measurepc", index: "measurepc", width: "60"},
                        {name: "comment", index: "comment", width: "220", sortable:false, editable:true, edittype:"text"},
                        {name: "indication", index: "indication", width: "220", hidden:true, sortable:false, editable:true, edittype:"select", editoptions:{value:" : ;Routine Monitoring:Routine Monitoring;Targeted Monitoring:Targeted Monitoring"}},                        
                    ],
                    sortname: "labtestId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 200,                    
                    jsonReader: {
                        root: "labresultList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "labtestId"
                    },
                    onSelectRow: function(id) { 
                        if(id && id!=lastSelected) {
                            if(id == 16) {
                                $("#grid").hideCol("comment");
                                $("#grid").showCol("indication");
                            }
                            else {
                                $("#grid").showCol("comment");
                                $("#grid").hideCol("indication");                                
                            }                              
                            $("#grid").jqGrid('saveRow', lastSelected,
                            {
                                successfunc: function(response) {
                                    return true;
                                },
                                url: "Labresult_update.action"
                            })
                            lastSelected = id;
                        }
                        $("#testId").val(id);
                        $("#grid").jqGrid('editRow',id);
                    } //end of onSelectRow                    
                }); //end of jqGrid                                
                $("#labtestgrid").jqGrid({
                    url: "Labtest_retrieve.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["", "", "Description"],
                    colModel: [
                        {name: "labtestId", index: "labtestId", width: "5", hidden: true},
                        {name: "selectedLabTest", index: "selectedLabTest", width: "5", hidden: true},
                        {name: "description", index: "description", width: "275"}                       
                    ],
                    sortname: "labtestId",
                    sortorder: "desc",
                    viewrecords: true,
                    rowNum: 100,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 100,                    
                    jsonReader: {
                        root: "labtestList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false, 
                        id: "labtestId"
                    },
                    loadComplete: function(data) {
                        console.log(data);
//                        if(data.regimenPharmList.length > 0){
//                            for(var i = 0; i< data.regimenPharmList.length; i++){
//                                if(data.regimenPharmList[i].regimenId === data.regimenPharmList[i].selectedRegimen){
//                                    var id = data.regimenPharmList[i].selectedRegimen;
//                                    showDrugComposition(id);
//                                    $("#regimengrid").jqGrid('setSelection', id);
//                                }                      
//                            }     
//                        }
                    },
                    onSelectRow: function(id) { 
                       getLabTestComposition(id);
                    }                                   
                });

                $("#date1").bind("change", function(event){
                    checkDate();                
                });
                $("#date2").bind("change", function(event){
                    checkDate();
                });

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Laboratory_search");
                    return true;
                });
            });
            
            function checkDate() {
                if($("#date1").val().length != 0 && $("#date2").val().length != 0 ) {
                   if(parseInt(compare($("#date1").val(), $("#date2").val())) == -1) {
                       var message = "Date of result reported cannot be ealier than date of sample collection";
                       $("#messageBar").html(message).slideDown('slow');                                                     
                    } 
                    else {
                       $("#messageBar").slideUp('slow');                 
                    }
                }
            }            
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_laboratory.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/clinic.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Laboratory >> General Lab >> Laboratory Test</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>Hospital No:</label></td>
                                <td width="20%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/></td>
                                <td width="25%"><span id="patientInfor"></span><input name="name" type="hidden" id="name"/></td>
                                <td width="20%"><input name="patientId" type="hidden" id="patientId"/><input name="testId" type="hidden" id="testId"/></td>
                            </tr>
                            <tr>
                                <td><label>Date of sample collection:</label></td>
                                <td><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateCollected" type="hidden" id="dateCollected"/></span></td>
                            </tr>
                            <tr>
                                <td><label>Date of result reported:</label></td>
                                <td><input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/><input name="dateReported" type="hidden" id="dateReported"/><span id="dateHelp" class="errorspan"></span></td>                                
                            </tr>
                            <tr>
                                <td><label>Laboratory No.:</label></td>
                                <td><input name="labno" type="text" class="inputboxes" id="labno"/></td>
                            </tr>
                            <tr>
                                <td></td>                                
                                <td colspan="2">
                                   <div id="labtesttable">
                                       <table id="labtestgrid"></table> 
                                   </div>                                   
                               </td>
                            </tr>
                            <tr>
                                <td><input name="dateLastCd4" type="hidden" id="dateLastCd4"/><input name="dateLastViralLoad" type="hidden" id="dateLastViralLoad"/></td>
                                <td><input name="gender" type="hidden" id="gender"/><input name="dateBirth" type="hidden" id="dateBirth"/></td>                               
                                <td><input name="currentStatus" type="hidden" id="currentStatus"/><input name="dateCurrentStatus" type="hidden" id="dateCurrentStatus"/></td>
                            </tr>
                        </table>
                        <div>
                            <fieldset>  
                                <legend> Selected Test</legend>
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                         <table id="grid"></table>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
						<div id="fromLabTest" style="display: none"><s:property value="#session.fromLabTest"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>

<%-- 
onSelectRow: function(id){ 
        var data = jQuery(#grid).jqGrid('getRowData',id);
        if (data.measureab != '' && data.measureab != '') {
            jQuery(#grid).setColProp('resultab',{editable:true});
            jQuery(#grid).setColProp('resultpc',{editable:true});
        }
        else {
            if (data.measureab == '' && data.measureab != '') {
                jQuery(#grid).setColProp('resultab',{editable:false});
                jQuery(#grid).setColProp('resultpc',{editable:true});
            }
            else {
                jQuery(#grid).setColProp('resultab',{editable:true});
                jQuery(#grid).setColProp('resultpc',{editable:false});            
            }
        }
--%>