<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
		<link type="text/css" rel="stylesheet" href="css/toastr.min.css" />
        <style type="text/css">
            #mainBackground{
                height: 628px;
                width: 773px;
                margin-left: 226px;
                background-image: url(images/background.jpg);
            }
        </style>   
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
		<script type="text/javascript" src="js/toastr.min.js"></script>
        
        <script type="text/JavaScript">
            $(document).ready(function(){
                resetPage();
                reports();
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "30000",
                    "extendedTimeOut": "3000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut",
                    "onclick": function () { 
                        window.location.href = "Labtest_prescription_search.action";
                    } 
                  }
                  
                  $.ajax({
                    url: "Labtests_prescribed.action",
                    dataType: "json",
                    method: "POST",
                    beforeSend : function(){
                        //console.log("here!");
                    },
                    success: function(response) {
                        labtests = response.notificationListCount[0].labtests;
                        
                        var warning = "";
                        var i = 0;
                        var tracked = false; 
                        
                        if(typeof labtests != "undefined" && labtests != 0){
                            warning +="<strong>"+labtests+"</strong> clients with laboratory test order <br/>";
                            tracked = true;
                        }
                        if(tracked == true){
                            toastr.warning(warning, "LabTest Order Notifications"); 
                        }   
                    }
                });
                
            });
        </script>
    </head>

    <body>
        <div id="page">
            
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_laboratory.jsp" />  

                <div id="mainBackground"></div>
            </div>
        </div>
    </body>
    <div id="footer">
        <jsp:include page="/WEB-INF/views/template/footer.jsp" />
    </div>
</html>
