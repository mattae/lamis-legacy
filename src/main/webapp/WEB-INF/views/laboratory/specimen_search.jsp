<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/specimen-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var gridNum = 1;
            var enablePadding = true;
            $(document).ready(function(){
                resetPage();
                initialize();
                reports();
                
                $("#grid").jqGrid({
                    url: "Specimen_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Lab No", "Hospital No", "Name", "Date Received" , "Sample", "Result", "Facility"],
                    colModel: [
                        {name: "labno", index: "labno", width: "100"},
                        {name: "hospitalNum", index: "hospitalNum", width: "80"},
                        {name: "name", index: "name", width: "130"},
                        {name: "dateReceived", index: "dateReceived", width: "80", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d-M-Y"}},                        
                        {name: "specimenType", index: "specimenType", width: "50"},
                        {name: "result", index: "result", width: "80"},
                        {name: "facilityName", index: "facilityName", width: "200"},
                    ],
                    pager: $('#pager'),
                    rowNum: 100,
                    sortname: "specimenId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 350,                    
                    jsonReader: {
                        root: "specimenList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "specimenId"
                    },
                    onSelectRow: function(id) {
                        $("#specimenId").val(id);
                        var data = $("#grid").getRowData(id);
                        $("#labno").val(data.labno);
                    },               
                    ondblClickRow: function(id) {
                        $("#specimenId").val(id);
                        var data = $("#grid").getRowData(id);
                        $("#labno").val(data.labno);
                        $("#lamisform").attr("action", "Specimen_find");
                        $("#lamisform").submit();
                    }
                }); //end of jqGrid                 
                
                $("#new_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Specimen_new");                        
                    return true;
                });                
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Specimen_page");
                    return true;
                });
            });
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_specimen.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/search.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Laboratory >> Auto Lab (PCR) >> Sample Registration</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">
                                    <table width="97%" border="0">
                                        <tr>
                                            <td width="12%">Labno/Barcode:</td>
                                            <td><input name="labno" type="text" class="inputboxes" id="labno" /></td>
                                            <td><input name="specimenId" type="hidden" id="specimenId" /></td>
                                            <td></td>
                                        </tr>
                                    </table>                               
                                </td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>                        
                        <p></p>
                        
                        <div>
                            <fieldset>  
                                <legend> Specimen List</legend>
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid" ></table>
                                        <div id="pager" style="text-align:center;"></div>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="buttons" style="width: 200px">
                            <button id="new_button">New</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
