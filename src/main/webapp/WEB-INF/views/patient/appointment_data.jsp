<%-- 
    Document   : Appointment
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/appointment-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/date.js"></script>       
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            $(document).ready(function(){
                resetPage();
                initialize();
                reports();
                
                
                $.ajax({
                    url: "Patient_retrieve.action",
                    dataType: "json",                    
                    success: function(patientList) {
                        // set patient id and number for which infor is to be entered
                        $("#patientId").val(patientList[0].patientId);
                        $("#hospitalNum").val(patientList[0].hospitalNum);
                        $("#dateNextClinic").val(patientList[0].dateNextClinic);
                        if((patientList[0].dateLastClinic).length != 0) {
                            date = patientList[0].dateLastClinic;
                            $("#dateLastClinic").html(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));                            
                        }
                        $("#dateNextRefill").val(patientList[0].dateNextRefill);
                        if((patientList[0].dateLastRefill).length != 0) {
                            date = patientList[0].dateLastRefill;
                            $("#dateLastRefill").html(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));
                        }
                        if((patientList[0].dateLastCd4).length != 0) {
                            date = patientList[0].dateLastCd4;
                            $("#dateLastCd4").html(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));
                        }
                        $("#sendMessage").val(patientList[0].sendMessage);
                        if(patientList[0].sendMessage != 0) {
                            $("#sendMessage").attr("disabled", false);                                   
                            $("#enableSms").attr("checked", true); 
                        } else {
                            $("#sendMessage").attr("disabled", true);                                   
                            $("#enableSms").attr("checked", false);                             
                        }
                        $("#patientInfor").html(patientList[0].surname + " " + patientList[0].otherNames);                        
                        if((patientList[0].dateLastCd4).length != 0) {
                            checkCd4(patientList[0].dateLastCd4);
                        }
                        if((patientList[0].dateNextClinic).length != 0) {
                            date = patientList[0].dateNextClinic;
                            var dateNext = new Date(date.slice(6), date.slice(0,2), date.slice(3,5));
                            if(Date.today().isBefore(dateNext)){
                                $("#date1").val(patientList[0].dateNextClinic);
                            }                            
                        }
                        if((patientList[0].dateNextRefill).length != 0) {
                            date = patientList[0].dateNextRefill;
                            var dateNext = new Date(date.slice(6), date.slice(0,2), date.slice(3,5));
                            if(Date.today().isBefore(dateNext)){
                                $("#date2").val(patientList[0].dateNextRefill);
                            }                            
                        }
                   }                    
                }); //end of ajax call

                $("#enableSms").bind("click", function(event){
                    if($("#enableSms").attr("checked")) {
                        $("#sendMessage").attr("disabled", false);                                    
                    } 
                    else {
                        $("#sendMessage").attr("disabled", true);                                   
                    }
                });                

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Appointment_search");
                    return true;
                });                
            });          
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_home.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/report.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Home >> Appointment Scheduling</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>Hospital No:</label></td>
                                <td width="20%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/></td>
                                <td width="25%"><span id="patientInfor"</span></td>
                                <td width="20%"><input name="patientId" type="hidden" class="inputboxes" id="patientId"/></td>
                            </tr>
                            <tr></tr>
                            <tr>
                                <td><label>Next Clinic Appointment:</label></td>
                                <td><input name="date1" type="text" style="width: 100px;"  class="inputboxes" id="date1"/><input name="dateNextClinic" type="hidden" id="dateNextClinic"/><span id="dateHelp1" class="errorspan"></span></td>
                                <td colspan="2"><label>Last clinic visit: </label><span id="dateLastClinic" style="color:blue"></span></td>                                
                            </tr>                            
                            <tr>
                                <td><label>Next Refill Appointment:</label></td>
                                <td><input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/><input name="dateNextRefill" type="hidden" id="dateNextRefill"/><span id="dateHelp2" class="errorspan"></span></td>
                                <td colspan="2"><label>Last refill visit: </label><span id="dateLastRefill" style="color:blue"></span></td>
                            </tr>                            
                            <tr>
                                <td><input name="enableSms" type="checkbox" id="enableSms"/><label>Send SMS reminder:</label></td>                                
                                <td>
                                   <select name="sendMessage" style="width: 70px;" id="sendMessage">
                                        <option value=1>English</option>
                                        <option value=2>Hausa</option>
                                        <option value=3>Igbo</option>
                                        <option value=4>Yoruba</option>
                                    </select>
                                </td>                            
                                <td colspan="2"><label>Last CD4 test: </label><span id="dateLastCd4" style="color:blue"></span></td>                                
                            </tr>
                            <tr>
                                <td><input name="gender" type="hidden" id="gender"/><input name="dateBirth" type="hidden" id="dateBirth"/></td>                               
                                <td><input name="currentStatus" type="hidden" id="currentStatus"/><input name="dateCurrentStatus" type="hidden" id="dateCurrentStatus"/></td>                                                               
                            </tr>                            
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </table>
                        <hr></hr>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 200px">
                            <button id="save_button">Save</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
