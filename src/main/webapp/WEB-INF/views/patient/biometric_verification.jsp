<%-- 
    Document   : biometric_verification
    Created on : Mar 16, 2019, 3:01:11 PM
    Author     : User10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />

        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />

        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>   
        <script type="text/javascript" src="js/zebra.js"></script> 
        <script type="text/javascript" src="js/fingerprint.js"></script>
        <script>
            console.log('Biometric verification');
            $(document).ready(function () {
                $.ajax({
                    url: "Patient_biometric_status.action",
                    dataType: "json",
                    success: function (res) {
                        if (!!res && res[0] && res[0][1]) {
                            var url = "http://" + window.location.host + "/LAMIS2/Patient_search.action";
                            window.location.replace(url);
                        } else {
                            var url = "http://" + window.location.host + "/LAMIS2/Patient_find?validate_biometric=true";
                            window.location.replace(url);
                        }
                    }
                });
            });
        </script>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
