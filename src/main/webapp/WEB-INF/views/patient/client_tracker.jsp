<%-- 
    Document   : Contact
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/JavaScript">
            var enablePadding = true;
            $(document).ready(function(){
                $.ajax({
                    url: "Padding_status.action",
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                });
                resetPage();
                initialize(); 
                reports();
 
                $("#dialog").dialog({
                    title: "Client Status Update",
                    autoOpen: false,
                    width: 500,
                    resizable: false,
                    buttons: [{text: "Yes", click: updateStatus}, 
                        {text: "Cancel", click: function(){$(this).dialog("close")}}]
                });

                var lastSelected = -99;
                $("#grid").jqGrid({
                    url: "Defaulter_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Hospital No", "Name", "Phone", "Date Tracked", "Outcome", "Agreed Date"],
                    colModel: [
                        {name: "hospitalNum", index: "hospitalNum", width: "100"},
                        {name: "name", index: "name", width: "160"},
                        {name: "phone", index: "phone", width: "100"},
                        {name: "dateTracked", index: "dateTracked", width: "115", sortable:false, editable:true, editoptions:{dataInit:function(date_tracked){$(date_tracked).datepicker({dateFormat:'mm/dd/yy'})}}},                        
                        {name: "outcome", index: "outcome", width: "140", sortable:false, editable:true, edittype:"select", editoptions:{value:" : ;ART Transfer Out:ART Transfer Out;Pre-ART Transfer Out:Pre-ART Transfer Out;Lost to Follow Up:Lost to Follow Up;Stopped Treatment:Stopped Treatment;Known Death:Known Death"}},                        
                        {name: "agreedDate", index: "agreedDate", width: "115", sortable:false, editable:true, editoptions:{dataInit:function(agreed_date){$(agreed_date).datepicker({dateFormat:'mm/dd/yy'})}}}                        
                    ],
                    pager: $('#pager'),
                    rowNum: 100,
                    sortname: "hospitalNum",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 350,                    
                    jsonReader: {
                        root: "defaulterList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "patientId"
                    },
                    onSelectRow: function(id) { 
                        if(id && id != lastSelected) {
                            $("#grid").jqGrid('saveRow', lastSelected,
                            {
                                successfunc: function(response) {
                                    return true;
                                },
                                url: "Defaulter_update.action"
                            })
                            $("#update_button").attr("disabled", false);
                            lastSelected = id;
                        }
                        $("#patientId").val(id);
                        $("#grid").jqGrid('editRow', id);
                    } //end of onSelectRow
                }); //end of jqGrid                 

                $("#update_button").bind("click", function(event){
                    $("#messageBar").hide();
                    if($("#userGroup").html() != "Administrator") {
                        $("#lamisform").attr("action", "Error_message");
                        return true;                        
                    }
                    else {
                       $("#dialog").dialog("open");
                       return false;                        
                    }
                });

                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Home_page");
                    return true;
                });
            });
            
            function updateStatus() {  
                $("#dialog").dialog("close");
                $("#update_button").attr("disabled", true);
                $("#lamisform").attr("action", "Status_update_defaulter");
                $("#lamisform").trigger("submit"); 
                $("#update_button").attr("disabled", false);
            }                
            
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_home.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/appointment.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Home >> Client Tracking</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">
                                    <table width="97%" border="0">
                                        <tr>
                                            <td width="12%">Hospital No:</td>
                                            <td><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" /></td>
                                            <td><input name="patientId" type="hidden" id="patientId" /></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td width="18%"><input name="name" type="text" class="inputboxes" id="name" /></td>
                                        </tr>
                                    </table>                               
                                </td>
                            </tr>                                                                                 
                            <tr><td></td></tr>
                        </table>
                        <div id="messageBar"></div>

                        <div>
                            <fieldset>  
                                <legend> Defaulters </legend>
                                <table width="99%" height="90" border="0" class="space">
                                    <tr>
                                        <td>
                                            <table id="grid"></table>
                                            <div id="pager" style="text-align:center;"></div>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>
                        <div id="dialog">
                            <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td><label>Do you want to continue with client status update?</label></td>
                                </tr>
                                <tr>
                                    <td width="20%"><label>Click Yes to continue or No to cancel:</label></td>
                                </tr>   
                            </table>
                        </div>
                        
                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 200px">
                            <button id="update_button" disabled="true">Update</button> &nbsp;<button id="close_button">Close</button>
                        </div>                        
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
