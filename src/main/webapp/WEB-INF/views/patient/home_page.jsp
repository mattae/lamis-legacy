<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/toastr.min.css" />
        
        <style type="text/css">
            #mainBackground{
                height: 628px;
                width: 773px;
                margin-left: 226px;
                background-image: url(images/background.jpg);
            }
        </style>   
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>   
        <script type="text/javascript" src="js/toastr.min.js"></script>
        <script type="text/JavaScript">
            
            var enrolled = 0, unconfirmed = 0, vl = 0, unsupressed = 0; 
			$(window).load(function(){
                var userGroup = $("#user_group").html();
                //console.log(userGroup);
                if(userGroup === "Clinician"){
                    window.location.href = "Clinic_page.action";
                }else if(userGroup === "Laboratory Scientist"){
                    window.location.href = "Laboratory_page.action";
                }else if(userGroup === "Pharmacist"){
                    window.location.href = "Pharmacy_page.action";
                }                                                      
            });
            $(document).ready(function(){
                resetPage();
                reports();
                
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "30000",
                    "extendedTimeOut": "3000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut",
                    "onclick": function () { 
                        window.location.href = "Notifications_page.action";
                    } 
                  }
                  

                $.ajax({
                    url: "Enrolled_nonTX.action",
                    dataType: "json",
                    method: "POST",
                    beforeSend : function(){
                        //console.log("here!");
                    },
                    success: function(response) {
                        //var i = 0;
                        //console.log(response.notificationListCount);
                        //if()
                        enrolled = response.notificationListCount[0].enrolled;
                        unconfirmed = response.notificationListCount[1].lostUnconfirmed;
                        //arv = response.notificationListCount[2].txNoArv;
                        vl = response.notificationListCount[2].txNoVl;
                        unsupressed = response.notificationListCount[3].vlUnsupressed; 
                        
                        var warning = "";
                        var i = 0;
                        var tracked = false; 
                        
                        if(typeof enrolled != "undefined" && enrolled != 0){
                            i++;
                            warning +="<strong>"+enrolled+"</strong> clients enrolled but not on treatment <br/>";
                            tracked = true;
                        }
                        if(typeof unconfirmed != "undefined" && unconfirmed != 0){
                            i++;
                            warning +="<strong>"+unconfirmed+"</strong> clients who are lost to follow-up unconfirmed<br/>";
                            tracked = true;
                        }
//                        if(typeof arv != "undefined" && arv != 0){
//                            i++;
//                            warning +="<strong>"+arv+"</strong> clients on treatment but no first ARV dispensed<br/>";
//                            tracked = true;
//                        }     
                        if(typeof vl != "undefined" && vl != 0){
                            i++;
                            warning +="<strong>"+vl+"</strong> clients on treatment who are due for viral load test<br/>";
                            tracked = true;
                        }
                        if(typeof unsupressed != "undefined" && unsupressed != 0){
                            i++;
                            warning +="<strong>"+unsupressed+"</strong> clients on treatment with viral load un-suppressed<br/>";
                            tracked = true;
                        }     
                        
                        if(tracked == true){
                            toastr.warning(warning, "Notifications"); 
                        }   
                    }
                });
                
            });
        </script>
    </head>

    <body>
        <div id="page">            
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">                
                <jsp:include page="/WEB-INF/views/template/nav_home.jsp" />                 
                <div id="mainBackground"></div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
