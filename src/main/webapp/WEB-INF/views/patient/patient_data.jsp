how<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra.css" />

        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script> 
        <script type="text/javascript" src="js/zebra.js"></script> 
        <script type="text/javascript" src="js/fingerprint.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script> 
        <script type="text/javascript" src="js/zebra_dialog.js"></script>
        <script type="text/javascript" src="js/moment-with-locales.js"></script> 
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>  
        <script type="text/javascript" src="js/lamis/patient-common.js"></script> 
        <script type="text/javascript" src="js/lamis/report-common.js"></script>    

        <script type="text/JavaScript">
            var obj = {};
            var date = ""; 
            var updateRecord = false;
            var enablePadding = true;
            $(document).ready(function(){
                $('#delete_button1').bind('click', function() {
                    console.log('Deleting', $("#patientId").val());
                    $.ajax({
                        url: "Patient_delete.action",
                        data: {patientId: $("#patientId").val(), hospitalNum: $("#hospitalNum").val()},
                        dataType: "json",
                    }); 
                });
                $("#enrol_button").bind("click", function(event){
                    var cmp = new FingerprintComponent();
                    var result = function(res) {
                        if(!!res && res.patientId) {
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    cmp.enrol($("#patientId").val(), result);
                    return false;
                });
                $("#dup_button").bind("click", function(event){
                    var cmp = new FingerprintComponent();
                    cmp.recordDuplicate($("#patientId").val());
                    return false;
                }).hide();
                
                $("#dialog").dialog({
                    title: "Change hospital number",
                    autoOpen: false,
                    width: 400,
                    resizable: false,
                    buttons: [{text: "Change", click: changeNumber}, 
                        {text: "Cancel", click: function(){$(this).dialog("close")}}]
                }); 
                
                $.ajax({
                    url: "Padding_status.action",
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                }); 
                resetPage();
                initialize();
                reports();
                
                $.ajax({
                    url: "Patient_retrieve.action",
                    dataType: "json",                    
                    success: function(patientList) {
                        populateForm(patientList);
                        if(!$("#patientId").val()) {
                            $("#enrol_button").hide(); 
                            $("#dup_button").hide();
                        }
                        var cmp = new FingerprintComponent();
                        cmp.enrolled($("#patientId").val(), function(res){
                           if(res) {
                               $("#dup_button").show();
                           } 
                        });
                    }                    
                }); //end of ajax call
                
                $("#timeHivDiagnosis").change(function(event){
                    var timeHivDiagnosis = $("#timeHivDiagnosis").val();
                    if(timeHivDiagnosis === "Labour") {
                        $("#lm").slideUp('fast');
                        $("#gravid").slideUp('fast');
                        $("#tdAncNo").slideUp('fast');    
                        $("#tdAncNo1").slideUp('fast');
                        nullifyValues();
                    }
                    else if(timeHivDiagnosis.includes("Post Partum")) {
                        $("#lm").slideUp('fast');
                        $("#gravid").slideUp('fast');
                        $("#tdAncNo").slideUp('fast');  
                        $("#tdAncNo1").slideUp('fast'); 
                        nullifyValues();
                    }else{
                        $("#lm").slideDown('fast');
                        $("#gravid").slideDown('fast');
                        $("#tdAncNo").slideDown('fast');  
                        $("#tdAncNo1").slideDown('fast');
                    }
                }); 
                
                function nullifyValues(){
                    $("#lmp").val("");
                    $("#edd").val("");
                    $("#gravida").val("");
                    $("#ancNum").val("");
                    $("#date5").val("");
                    $("#date6").val("");
                    $("#parity").val("");
                    $("#gestationalAge").val("");
                }
                
                function changeNumber() {
                    if($("#newHospitalNum").val().length !== 0) {
                        var newHospitalNum = zerorize($("#newHospitalNum").val());

                        $.ajax({
                            url: "Update_number.action",
                            dataType: "json",
                            data: {patientId: $("#patientId").val(), hospitalNum: $("#hospitalNum").val(), newHospitalNum: newHospitalNum},
                            success: function(messageList) {
                                if(messageList[0].message === "Updated") {
                                    $("#messageBar").html("Hospital number changed").slideDown('slow');
                                    $("#hospitalNum").val(newHospitalNum);                                
                                }
                                else {
                                    $("#messageBar").html("Hospital number already exist").slideDown('slow');                                
                                }
                            }                    
                        }); //end of ajax call 
                        $("#dialog").dialog("close");
                    } 
                }   
            });
            
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  

            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_home.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/patients.png" style="margin-bottom: -5px; width: 20px; height: 20px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 0.8em;"><strong>Home >> Patient Registration</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Patient Details <b>Fields marked with ** are required</b></td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>

                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>Hospital No **</label></td>
                                <td width="30%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum"/><span id="numHelp" class="errorspan"></span></td>
                                <td width="20%"><input name="patientId" type="hidden" class="inputboxes" id="patientId"/></td>
                                <td width="30%"><div id="hyperlink" ><span id="changeNumberLink" style="color:blue"></span></div></td>
                            </tr>
                            <tr><td></td><td></td><td><button id="dup_button">Record Duplicate</button></td><td><button id="enrol_button">Enrol</button></td></tr>																																	  
                            <tr>
                                <td><label>Unique ID</label></td>
                                <td><input name="uniqueId" type="text" class="inputboxes" id="uniqueId"/></td>                         
                            </tr>
                            <tr>
                                <td><label>Surname **</label></td>
                                <td><input name="surname" type="text" class="inputboxes" id="surname"/><span id="surnameHelp" class="errorspan"></span></td>
                                <td><label>Other Names:</label></td>
                                <td><input name="otherNames" type="text" class="inputboxes" id="otherNames"/></td>
                            </tr>
                            <tr>
                                <td><label>Date of Birth</label></td>
                                <td><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateBirth" type="hidden" id="dateBirth"/></td>
                                <td><label>Age at Registration **</label></td>
                                <td><input name="age" type="text" style="width: 50px;" class="inputboxes" id="age"/>
                                    <select name="ageUnit" style="width: 75px;" class="inputboxes" id="ageUnit">
                                        <option></option>
                                        <option>year(s)</option>
                                        <option>month(s)</option>
                                        <option>day(s)</option>
                                    </select><span id="ageHelp" class="errorspan"></span>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Gender **</label></td>
                                <td>
                                    <select name="gender" style="width: 130px;" class="inputboxes" id="gender">
                                        <option></option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select><span id="genderHelp" class="errorspan"></span>
                                </td>                            
                                <td><label>Marital Status</label></td>
                                <td>
                                    <select name="maritalStatus" style="width: 130px;" class="inputboxes" id="maritalStatus">
                                        <option></option>
                                        <option>Single</option>
                                        <option>Married</option>
                                        <option>Widowed</option>
                                        <option>Separated</option>
                                        <option>Divorced</option>
                                    </select>
                                </td>                                
                            </tr>
                            <tr>
                                <td><label>Job/Occupation Status</label></td>
                                <td>
                                    <select name="occupation" style="width: 130px;" class="inputboxes" id="occupation">
                                        <option></option>
                                        <option>Unemployed</option>
                                        <option>Employed</option>
                                        <option>Student</option>
                                        <option>Retired</option>
                                    </select>
                                </td>
                                <td><label>Educational Level</label></td>
                                <td>
                                    <select name="education" style="width: 130px;" class="inputboxes" id="education">
                                        <option></option>
                                        <option>None</option>
                                        <option>Primary</option>
                                        <option>Senior Secondary</option>
                                        <option>Quranic</option>
                                        <option>Junior Secondary</option>
                                        <option>Post Secondary</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>State of residence</label></td>
                                <td>
                                    <select name="state" style="width: 130px;" class="inputboxes" id="state">
                                    </select>
                                </td>
                                <td><label>LGA of residence</label></td>
                                <td>
                                    <select name="lga" style="width: 130px;" class="inputboxes" id="lga">
                                        <option></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Address</label></td>
                                <td><input name="address" type="text" style="width: 200px;" class="inputboxes" id="address"/></td>
                                <td><label>Telephone</label></td>
                                <td><input name="phone" type="text" class="inputboxes" id="phone"/></td>
                            </tr>
                            <tr>
                                <td><label>Care Entry Point **</label></td>
                                <td>
                                    <select name="entryPoint" style="width: 200px;" class="inputboxes" id="entryPoint">
                                        <option></option>
                                        <option>OPD</option>
                                        <option>In patients</option>
                                        <option>HCT</option>
                                        <option>TB DOTS</option>
                                        <option>STI Clinic</option>
                                        <option>PMTCT</option>
                                        <option>Transfer-in</option>
                                        <option>Outreaches</option>
                                        <option>CBO</option>
                                        <option>Others</option>
                                    </select><span id="entryHelp" class="errorspan"></span>
                                </td>
                                <td><label>Date of Confirmed HIV Test</label></td>
                                <td><input name="date3" type="text" style="width: 100px;" class="inputboxes" id="date3"/><input name="dateConfirmedHiv" type="hidden" id="dateConfirmedHiv"/><span id="confirmedHelp" class="errorspan"></span></td>
                            </tr>
                            <tr id="ancNo">
                                <td><label>Time of HIV Diagnosis:</label></td>
                                <td>
                                    <select name="timeHivDiagnosis" style="width: 210px;" class="inputboxes" id="timeHivDiagnosis">
                                        <option></option>
                                        <option>Newly Tested HIV+ (ANC)</option>
                                        <option>Previously known HIV+ (ANC)</option>
                                        <option>Newly Tested HIV+ (L&AMP;D)</option>
                                        <option>Previously known HIV+ (L&AMP;D)</option>
                                        <option>Newly Tested HIV+ (PP &lt;=72hrs)</option>
                                        <option>Previously konwn HIV+ (PP &lt;=72hrs)</option>
                                        <option>Newly Tested HIV+ (PP &gt;72hrs)</option>
                                        <option>Previously konwn HIV+ (PP &gt;72hrs)</option>
                                    </select>
                                </td>
                                <td><label>Date enrolled into PMTCT:</label></td>
                                <td><input name="date4" type="text" style="width: 100px;" class="inputboxes" id="date4"/><input name="dateEnrolledPmtct" type="hidden" class="inputboxes" id="dateEnrolledPmtct"/><span id="datepmtctHelp" class="errorspan"></span></td>

                                <!--                                <td id="tdAncNo"><label>ANC No:</label></td>
                                                                <td id="tdAncNo1"><input name="ancNum" type="text" class="inputboxes" id="ancNum"/><span id="numHelp" class="errorspan"></span></td>                               -->
                            </tr>						
                            <tr id="pmtct">
                                <td><label>Source of Referral:</label></td>
                                <td>
                                    <select name="sourceReferral" style="width: 180px;" class="inputboxes" id="sourceReferral">
                                        <option></option>
                                        <option>PMTCT outreach</option>
                                        <option>Sex worker outreach</option>
                                        <option>Medical outpatient</option>
                                        <option>Youth/Adolescent outreach</option>
                                        <option>Private/Commercial Health facility</option>
                                        <option>Under-fives/Immunization clinic</option>
                                        <option>External HCT centre</option>
                                        <option>CBO</option>
                                        <option>In-patients</option>
                                        <option>Self-referral</option>
                                    </select>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>						
                            <!--                            <tr id="lm">
                                                            <td><label>L.M.P:</label></td>
                                                            <td><input name="date5" type="text" style="width: 100px;" class="inputboxes" id="date5"/><input name="lmp" type="hidden" class="inputboxes" id="lmp"/><span id="lmpHelp" class="errorspan"></span></td>
                                                            <td colspan="2"><label>E.D.D:</label>&nbsp;&nbsp;<input name="date6" type="text" style="width: 65px;" class="inputboxes" id="date6"/><input name="edd" type="hidden" class="inputboxes" id="edd"/>&nbsp;
                                                            <label>Gestational Age (weeks):</label>&nbsp;&nbsp;<input name="gestationalAge" type="text" style="width: 25px;" class="inputboxes" id="gestationalAge"/></td>
                                                        </tr>
                                                        <tr id="gravid">
                                                            <td><label>Gravida:</label></td>
                                                            <td><input name="gravida" type="text" style="width: 50px;" class="inputboxes" id="gravida"/></td>
                                                            <td><label>Parity:</label></td>
                                                            <td><input name="parity" type="text" style="width: 50px;" class="inputboxes" id="parity"/></td>
                                                        </tr>-->
                            <tr>
                                <td><label>HIV Status at registration **</label></td>
                                <td>
                                    <select name="statusRegistration" style="width: 200px;" class="inputboxes" id="statusRegistration">
                                        <option></option>
                                        <option>HIV exposed status unknown</option>
                                        <option>HIV+ non ART</option>
                                        <option>ART Transfer In</option>
                                        <option>Pre-ART Transfer In</option>
                                    </select><span id="statusregHelp" class="errorspan"></span>
                                </td>
                                <td><label>Date of registration/ &nbsp;Transfer-in **</label></td>
                                <td><input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/><input name="dateRegistration" type="hidden" id="dateRegistration"/><span id="dateregHelp" class="errorspan"></span></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>TB Status</label></td>
                                <td>
                                    <select name="tbStatus" style="width: 200px;" class="inputboxes" id="tbStatus">
                                        <option></option>
                                        <option>No sign or symptoms of TB</option>
                                        <option>TB suspected and referred for evaluation</option>
                                        <option>Currently on INH prophylaxis</option>
                                        <option>Currently on TB treatment</option>
                                        <option>TB positive not on TB drugs</option>
                                    </select>
                                </td>
                                <td><label>Pregnancy Status</label></td>
                                <td>
                                    <select name="pregnancyStatus" style="width: 200px;" class="inputboxes" id="pregnancyStatus">
                                        <option></option>
                                        <option value="1">Not Pregnant</option>
                                        <option value="2">Pregnant</option>
                                        <option value="3">Breastfeeding</option>
                                    </select>
                                </td><span id="pregHelp" class="errorspan"></span>
                            </tr>
                            <tr>
                                <td><input name="currentStatus" type="hidden" id="currentStatus"/><input name="dateCurrentStatus" type="hidden" id="dateCurrentStatus"/></td>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </table>
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Next of Kin/Treatment Supporter</td>
                            </tr>
                        </table>
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>Name</label></td>
                                <td width="30%"><input name="nextKin" type="text" class="inputboxes" id="nextKin"/></td>
                                <td width="20%"><label>Relationship</label></td>
                                <td width="30%">
                                    <select name="relationKin" style="width: 130px;" class="inputboxes" id="relationKin">
                                        <option></option>
                                        <option>Aunt</option>
                                        <option>Brother</option>
                                        <option>Cousin</option>
                                        <option>Daughter</option>
                                        <option>Father</option>
                                        <option>Friend</option>
                                        <option>Grand parent</option>
                                        <option>Mother</option>
                                        <option>Son</option>
                                        <option>Spouse</option>
                                        <option>Sister</option>
                                        <option>Treatment supporter</option>
                                        <option>Uncle</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Address</label></td>
                                <td><input name="addressKin" type="text" style="width: 200px;"  class="inputboxes" id="addressKin"/></td>
                                <td><label>Telephone</label></td>
                                <td><input name="phoneKin" type="text" class="inputboxes" id="phoneKin"/></td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </table>
                        <div id="dialog">
                            <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td><label>New Hospital No</label>&nbsp; <input name="newHospitalNum" type="text" class="inputboxes" id="newHospitalNum"/><span id="newHelp" style="color:red"></span></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        <hr></hr>
                        <p></p>
                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button1"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
