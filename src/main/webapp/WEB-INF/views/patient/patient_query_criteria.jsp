<%-- 
    Document   : Facility
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/JavaScript">
            var url = "";
            var updateRecord = false;
            $(document).ready(function(){
                resetPage();
                reports();
                
                $("body").bind('ajaxStart', function(event){
                    $("#loader").html('<img id="loader_image" src="images/loader_small.gif" />');	
                });

                $("body").bind('ajaxStop', function(event){
                    $("#loader").html('');
                });
                $("#messageBar").hide();

                $("#date1").mask("99/99/9999");
                $("#date1").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-70:+0",
                    constrainInput: true,
                    buttonImageOnly: true,
                    buttonImage: "/images/calendar.gif"
                });
                $("#date2").mask("99/99/9999");
                $("#date2").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-70:+0",
                    constrainInput: true,
                    buttonImageOnly: true,
                    buttonImage: "/images/calendar.gif"
                });
                
                $("#date3").mask("99/99/9999");
                $("#date3").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-70:+0",
                    constrainInput: true,
                    buttonImageOnly: true,
                    buttonImage: "/images/calendar.gif"
                });
                $("#date4").mask("99/99/9999");
                $("#date4").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-70:+0",
                    constrainInput: true,
                    buttonImageOnly: true,
                    buttonImage: "/images/calendar.gif"
                });
                 
                $("#date5").mask("99/99/9999");
                $("#date5").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-70:+0",
                    constrainInput: true,
                    buttonImageOnly: true,
                    buttonImage: "/images/calendar.gif"
                });
                $("#date6").mask("99/99/9999");
                $("#date6").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-70:+0",
                    constrainInput: true,
                    buttonImageOnly: true,
                    buttonImage: "/images/calendar.gif"
                });
                
                $("#ageBegin").mask("9?99", {placeholder: " "});
                $("#ageEnd").mask("9?99", {placeholder: " "});

                $("#cd4Begin").keypress(function(key) {
                    if((key.charCode < 48 || key.charCode > 57) && (key.charCode != 0) && (key.charCode != 8) && (key.charCode != 9) && (key.charCode != 46)) {
                        return false;
                    }
                    return true;
                })
                $("#cd4End").keypress(function(key) {
                    if((key.charCode < 48 || key.charCode > 57) && (key.charCode != 0) && (key.charCode != 8) && (key.charCode != 9) && (key.charCode != 46)) {
                        return false;
                    }
                    return true;
                })

                $("#viralloadBegin").keypress(function(key) {
                    if((key.charCode < 48 || key.charCode > 57) && (key.charCode != 0) && (key.charCode != 8) && (key.charCode != 9) && (key.charCode != 46)) {
                        return false;
                    }
                    return true;
                })
                $("#viralloadEnd").keypress(function(key) {
                    if((key.charCode < 48 || key.charCode > 57) && (key.charCode != 0) && (key.charCode != 8) && (key.charCode != 9) && (key.charCode != 46)) {
                        return false;
                    }
                    return true;
                })
 
                $.ajax({
                    url: "RegimenType_retrieve_name.action?commence",
                    dataType: "json",

                    success: function(regimenTypeMap) {
                        var options = "<option value = '" + '' + "'>" + '' + "</option>";
                        $.each(regimenTypeMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#regimentype").html(options);                        

                    }                    
                }); //end of ajax call
                
                $.ajax({
                    url: "State_retrieve.action",
                    dataType: "json",
                    success: function(stateMap) {
                        var options = "<option value = '" + '' + "'>" + '' + "</option>";
                        $.each(stateMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#state").html(options);                        
                    }                    
                }); //end of ajax call

                $("#state").change(function(event){
                    $.ajax({
                        url: "Lga_retrieve.action",
                        dataType: "json",
                        data: {state: $("#state").val()},
                        success: function(lgaMap) {
                            var options = "<option value = '" + '' + "'>" + '' + "</option>";
                            $.each(lgaMap, function(key, value) {
                                options += "<option value = '" + key + "'>" + value + "</option>";
                            }) //end each
                            $("#lga").html(options);                        
                        }                    
                    }); //end of ajax call
                }); 

                $("#currentStatus").bind("change", function() {
                    var currentStatus = $("#currentStatus").val();
                    if(currentStatus == "HIV negative" || currentStatus == "HIV+ non ART" || currentStatus == "Pre-ART Transfer In" || currentStatus == "Pre-ART Transfer Out") {
                        $("#regimentype").val("");
                        $("#regimentype").attr("disabled", true);
                        $("#date3").val("");
                        $("#date3").attr("disabled", true);
                        $("#date4").val(""); 
                        $("#date4").attr("disabled", true);
                    }
                    else {
                        $("#regimentype").attr("disabled", false);
                        $("#date3").attr("disabled", false);
                        $("#date4").attr("disabled", false);
                    }
                });

                $("#ok_button").bind("click", function(event){
                    formatDateFields();
                    event.preventDefault();
                    event.stopPropagation();
                    url = "";
                    url += "gender="+$("#gender").val();
                    url += "&ageBegin="+$("#ageBegin").val();
                    url += "&ageEnd="+$("#ageEnd").val();
                    url += "&state="+$("#state").val();
                    url += "&lga="+$("#lga").val();
                    url += "&currentStatus="+$("#currentStatus").val();
                    url += "&dateCurrentStatusBegin="+$("#dateCurrentStatusBegin").val();
                    url += "&dateCurrentStatusEnd="+$("#dateCurrentStatusEnd").val();
                    url += "&regimentype="+$("#regimentype").val();
                    url += "&dateRegistrationBegin="+$("#dateRegistrationBegin").val();
                    url += "&dateRegistrationEnd="+$("#dateRegistrationEnd").val();
                    url += "&artStartDateBegin="+$("#artStartDateBegin").val();
                    url += "&artStartDateEnd="+$("#artStartDateEnd").val();
                    url += "&clinicStage="+$("#clinicStage").val();
                    url += "&cd4Begin="+$("#cd4Begin").val();
                    url += "&cd4End="+$("#cd4End").val();
                    url += "&viralloadBegin="+$("#viralloadBegin").val();
                    url += "&viralloadEnd="+$("#viralloadEnd").val();
                    //url += "&tbStatus="+$("#tbStatus").val();
                    if($('[name="reportFormat"]:checked').val() == "pdf") {
                        url = "Patient_list.action?"+url;                            
                        window.open(url);
                        return false;                        
                    }
                    else {
                        url += "&recordType=1&viewIdentifier="+$("#viewIdentifier").prop("checked");
                        url = "Converter_dispatch.action?"+url;                            
                        convertData();
                    }
                }); 
                
                $("#cancel_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Home_page");
                    return true;
                });
            }); 

            var x = function wait() {window.open(url);}
            
            
            function convertData() {                
                $("#messageBar").hide();
                $("#ok_button").attr("disabled", true);
                $.ajax({
                    url: url,
                    dataType: "json",
                    success: function(fileName) {
                        $("#messageBar").html("Conversion Completed").slideDown('fast');
                        url = fileName;
                        window.setTimeout(x, 3000);                        
                    }                    
                }); 
                $("#ok_button").attr("disabled", false);
            }            
 
            function formatDateFields() {
                $("#date1").datepicker("option", "altField", "#dateRegistrationBegin");    
                $("#date1").datepicker("option", "altFormat", "mm/dd/yy");    
                $("#date2").datepicker("option", "altField", "#dateRegistrationEnd");    
                $("#date2").datepicker("option", "altFormat", "mm/dd/yy");    
                
                $("#date3").datepicker("option", "altField", "#artStartDateBegin");    
                $("#date3").datepicker("option", "altFormat", "mm/dd/yy");                    
                $("#date4").datepicker("option", "altField", "#artStartDateEnd");    
                $("#date4").datepicker("option", "altFormat", "mm/dd/yy");    

                $("#date5").datepicker("option", "altField", "#dateCurrentStatusBegin");    
                $("#date5").datepicker("option", "altFormat", "mm/dd/yy");    
                $("#date6").datepicker("option", "altField", "#dateCurrentStatusEnd");    
                $("#date6").datepicker("option", "altFormat", "mm/dd/yy");    
            }                                          
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_home.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/report.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Home >> Patient Information Query</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Additional Query Filters</td>
                            </tr>
                        </table>
                        <div id="loader"></div>
                        <div id="messageBar"></div>
                        
                        <div style="margin-right: 10px;">
                            <fieldset> 
                                <legend> Demographic filters </legend>                            
                                <p></p>
                                <table width="99%" border="0" class="space" cellpadding="3">
                                    <tr>
                                        <td width="20%"><label>Gender:</label></td>
                                        <td> 
                                            <select name="gender" style="width: 130px;" class="inputboxes" id="gender">
                                               <option>--All--</option>
                                               <option>Male</option>
                                               <option>Female</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label>Age:</label></td>
                                        <td><input name="ageBegin" type="text" style="width: 50px;" class="inputboxes" id="ageBegin"/> &nbsp;to:&nbsp;<input name="ageEnd" type="text" style="width: 50px;" class="inputboxes" id="ageEnd"/></td>
                                    </tr>
                                    <tr>
                                        <td><label>State/LGA of residence:</label></td>
                                        <td>
                                            <select name="state" style="width: 130px;" class="inputboxes" id="state">
                                            </select>&nbsp;&nbsp;
                                            <select name="lga" style="width: 130px;" class="inputboxes" id="lga">
                                            <option></option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>  
                        </div>								
                        <p></p>                            
                        
                        <div style="margin-right: 10px;">
                            <fieldset> 
                                <legend> Clinical filters </legend>                            
                                <p></p>
                                <table width="99%" border="0" class="space" cellpadding="3">
                                <tr>
                                    <td width="20%"><label>Current Status:</label></td>
                                    <td> 
                                        <select name="currentStatus" style="width: 230px;" class="inputboxes" id="currentStatus">
                                           <option>--All--</option>
                                           <option>HIV&plus; non ART</option>
                                           <option>ART Start</option>
                                           <option>ART Restart</option>
                                           <option>ART Transfer In</option>
                                           <option>ART Transfer Out</option>
                                           <option>Pre-ART Transfer In</option>
                                           <option>Pre-ART Transfer Out</option>
                                           <option>Lost to Follow Up</option>
                                           <option>Stopped Treatment</option>
                                           <option>Known Death</option>
                                           <option>Currently Active</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Date of Current Status:</label></td>
                                    <td><input name="date5" type="text" style="width: 100px;" class="inputboxes" id="date5"/><input name="dateCurrentStatusBegin" type="hidden" id="dateCurrentStatusBegin"/> &nbsp;to:&nbsp;<input name="date6" type="text" style="width: 100px;" class="inputboxes" id="date6"/><input name="dateCurrentStatusEnd" type="hidden" id="dateCurrentStatusEnd"/></td>
                                </tr>
                                <tr>
                                    <td><label>Regimen Line:</label></td>
                                    <td> 
                                        <select name="regimentype" style="width: 230px;" class="inputboxes" id="regimentype"/>
                                            <option></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Date of Registration:</label></td>
                                    <td><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateRegistrationBegin" type="hidden" id="dateRegistrationBegin"/> &nbsp;to:&nbsp;<input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/><input name="dateRegistrationEnd" type="hidden" id="dateRegistrationEnd"/></td>
                                </tr>
                                <tr>
                                    <td><label>ART Start Date:</label></td>
                                    <td><input name="date3" type="text" style="width: 100px;" class="inputboxes" id="date3"/><input name="artStartDateBegin" type="hidden" id="artStartDateBegin"/> &nbsp;to:&nbsp;<input name="date4" type="text" style="width: 100px;" class="inputboxes" id="date4"/><input name="artStartDateEnd" type="hidden" id="artStartDateEnd"/></td>
                                </tr>
                                <tr>
                                    <td><label>Current Clinical Stage:</label></td>
                                    <td> 
                                        <select name="clinicStage" style="width: 170px;" class="inputboxes" id="clinicStage">
                                            <option></option>
                                            <option>Stage I</option>
                                            <option>Stage II</option>
                                            <option>Stage III</option>
                                            <option>Stage IV</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Current CD4 Count:</label></td>
                                    <td><input name="cd4Begin" type="text" style="width: 70px;" class="inputboxes" id="cd4Begin"/> &nbsp;to:&nbsp;<input name="cd4End" type="text" style="width: 70px;" class="inputboxes" id="cd4End"/></td>
                                </tr>
                                <tr>
                                    <td><label>Current Viral Load:</label></td>
                                    <td><input name="viralloadBegin" type="text" style="width: 70px;" class="inputboxes" id="viralloadBegin"/> &nbsp;to:&nbsp;<input name="viralloadEnd" type="text" style="width: 70px;" class="inputboxes" id="viralloadEnd"/></td>
                                </tr>
                                <tr>
                                    <!--<td><label>TB Status:</label></td>
                                    <td> 
                                        <select name="tbStatus" style="width: 170px;" class="inputboxes" id="tbStatus">
                                            <option></option>
                                            <option>No sign or symptoms of TB</option>
                                            <option>TB suspected and referred for evaluation</option>
                                            <option>Currently on INH prophylaxis</option>
                                            <option>Currently on TB treatment</option>
                                            <option>TB positive not on TB drugs</option>
                                        </select>
                                    </td>-->
                                </tr>
                                </table>
                            </fieldset>  
                        </div>								
                        <div style="margin-right: 10px;">
                            <fieldset> 
                                <legend> Report format </legend>                            
                                <p></p>
                                <table width="99%" border="0" class="space" cellpadding="3">
                                    <tr>
                                        <td colspan="2"><input type="radio" name="reportFormat" value="pdf" checked/><label>Generate report in PDF format </label></td>
                                        <td colspan="2"><input type="radio" name="reportFormat" value="cvs"/><label>Generate report and convert report to MS Excel </label></td>
                                    </tr>                            
                                    <tr> 
                                        <td colspan="2"></td>
                                        <td colspan="2">                                        
                                            <span style="margin-left:20px"><input name="viewIdentifier" type="checkbox" id="viewIdentifier"/>&nbsp;<label>Unscramble patient identifiers like names, addresses and phone numbers</label></span>
                                        </td>                                    
                                    </tr>
                                    <tr> </tr>
                                </table>
                            </fieldset>  
                        </div>								
                        <p></p>                            
 
                        <div id="viewIdentifiers" style="display: none"><s:property value="#session.viewIdentifier"/></div>
                        <div id="buttons" style="width: 200px">
                            <button id="ok_button">Ok</button> &nbsp;<button id="cancel_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
