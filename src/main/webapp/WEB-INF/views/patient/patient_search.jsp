<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
		<link type="text/css" rel="stylesheet" href="css/zebra.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />

        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />

        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>    
		<script type="text/javascript" src="js/zebra.js"></script> 
        <script type="text/javascript" src="js/fingerprint.js"></script>		

        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var gridNum = 1;  
            var enablePadding = true;
            $(document).ready(function(){
                $.ajax({
                    url: "Padding_status.action",
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                });
				function imageFormat( value, options, rowObject ){
					if(value === 'true') {
						return '<div style="text-align: center;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACqUlEQVQ4T6WTW0iTYRjH/99pc9MN576yltoBYenMXJgijZT0opslZCDqnRddhBRosItCRlAwSCGJIKPu1IIKrCspOuBEDI2gVMwzzXma25rbt9O374tvh3INuumF5+Z9nv/vfZ8Tgf88RIZ+iNWBIi5CxBkQKIz7RfwAgTHExJdocTn3atIBz1hzFiPv1Ov0xYfZwrwceU6WCCAQ9odWtlfdc+vzC+FouBfNrtcpyB/AIGtWK9TWOsPZEhqUguf5tM/RNI2wEAl+nBmd9Yf8VrQmIAnAEKuTgxloqKivjoVjCkEQMjIbMT7F+S8tECgh+OHb6ESUiLZJ6SQAT7QdZcdKLfnZ+QWRSCRDfK/sNozqsvj9uU9NcHgcjnnnog3tO/cTgH7NYK2xrjG4yylT6mg0Cj/nh7XoOmo0lZBSuPH1Dka23kOlUXGLW4vDuOxpTQAe5NrrTzfUeL1ecrLhDYqen0QgGMApdTn6y3vi4scrA+hbfhQPVyqUwprLMY4rXlMC0Jdrr62qq2lmL5AmrhIUReHa6k08PH4XDM3gncuOru/dEMhEbWS0TNh0bIzjagrQox40VJxoXP+5oRzVvwLDMPFXJZvnlnBpph2Q7SlNVOQ8SzvD6PIlU7CpO7RHWAuZTxfwOzymjG9/AwxTJiA7va4hh98RXA/ZYPEli2hT6iiCGWCrDlRHs3iFuCViutIOw6QJ2J8uFvyxoO+ze0IUY22wcMk2SjHdSjOtpK0qk7aEUJEKcU0EDv0l9vHB3TH3rMAJVtzi9gxSKq5JbiaOEp3ykuximV6ZR2noLMkV8/ChyBznDs8GFsRlsRcvwhmjTAPQAmChow+iVDBhn2gATeribF5wYpuYxgxph5PfBLANwCV5UrtASu1NmgT714lJ+wWAAyD8AhbJDCA03+U6AAAAAElFTkSuQmCC"/></div>';
					}
						return '<div style="text-align: center;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACrUlEQVQ4T21TbUhTYRR+3nv9aG7dud1NM6fDmZBKMjXsh30RLjKKICLICILAMCSlUgTpV0ahEFriIAoEQUiwAgVFJczqh5jOPrCiqYxp2dyW2/xgc943dmVXNzu/zjnPeZ7znvc9L0GU9es4NVmn5QAtAlAIgKXAOCjGBQbm0wvLi9spZHvQlyx/EKuQ16WcKAaXqYciIx2EIfDN2rE8a8f80Duse3xNpY6V2jBPEuhNVjzh87Irc66XgbhtEDyLELxOsY7ZzYPhNIA2A9/bu+EYmzSfWVi+EcJEgV4t18AX5tbnlJ1E4PMwBjqnYLq4L2K4wS4rTJeyEX/gKH50j+DPqKXprMNXS3p4WSrDcXNFd64g+O09Jq0KnOobRH+pCcZMrygyOc1F5GJzj2Cs5QWCTpeOvNRyDYZzJfWauAXQdT90Tz9KnUMiIQsJhm2u/CBITCyc/j2Y6Xlzn3RpOUvB5RIj654WaxLyjyGpuiX6ccTY0VyFVctb0Re0WRjvGJginRrOf+i8MU7wuiSSvOA4UmpaI0R+N1ViZWJYyjGKRIy+/hIgHRqlL//wXgVDBAm0raVFHDsEhMbRy+xSDSUMJkZ+BUi7RmnJzuONsvhNbJ4adpDDrJBIKpkRw0AA+PrJZSXPeGWDzsDXJ3FBEcgfskldZu5eE33DvedSzlKiF32Xj4Vt+m8rMfN8qkwWM7c/nQXDAInFJmQ9bMfPuqtY+rB5+9E5SoEpWxD+1WCuuEhtalWjimNr9Ensf28/Omlf3IDbE2ypcC9VS6v8mFe2qROYijSeBRPxQ7boAgXm3QKcKxvmmy7P1iqHS5rVqsY4ltRo5BTKXUB8zCYS2AA8a4BzlSAQFB5VuZduhzk7ejWrVOkAKihwAYD4ISgwSyh9RQXBfMvrtW4f6R9Xl/9Jy6U2GQAAAABJRU5ErkJggg=="/></div>';
				}
                resetPage();
                initialize(); 
                reports();
                                            
                $("#grid").jqGrid({
                    url: "Patient_grid_search.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Hospital No","Biometric", "Name", "Gender", "ART Status", "Address", "Date Modified"],
                    colModel: [
                        {name: "hospitalNum", index: "hospitalNum", width: "80"},
						{name: "biometric", index: "biometric", width: "50", formatter: imageFormat},	
                        {name: "name", index: "name", width: "150"},
                        {name: "gender", index: "gender", width: "50"},
                        {name: "currentStatus", index: "currentStatus", width: "100"},
                        {name: "address", index: "address", width: "250"},                        
                        {name: "timeStamp", index: "timeStamp", width: "100", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d-M-Y"}}                        
                    ],
                    pager: $('#pager'),
                    rowNum: 100,
                    sortname: "timeStamp", 
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 350,                    
                    jsonReader: {
                        root: "patientList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "patientId"
                    },
//                    loadComplete: function() {
//                        var grid = $("#grid"),
//                        ids = grid.getDataIDs();
//                        for (var i = 0; i < ids.length; i++) {
//                            grid.setRowData(ids[i], false, { height : 35});
//                        }
//                        // grid.setGridHeight('auto');
//                    },
                    onSelectRow: function(id) {
                        $("#patientId").val(id);
                        var data = $("#grid").getRowData(id);
                        $("#hospitalNum").val(data.hospitalNum);
                        $("#name").val(data.name);
                        $("#new_button").html("View");
                    },               
                    ondblClickRow: function(id) {
                        $("#patientId").val(id);
                        var data = $("#grid").getRowData(id);
                        $("#hospitalNum").val(data.hospitalNum);
                        $("#name").val(data.name); 
                        $("#lamisform").attr("action", "Patient_find");
                        $("#lamisform").submit();
                    }
                }); //end of jqGrid                 
                
				$("#new_button").bind("click", function(event){
                    if($("#new_button").html() === "New"){
                        $("#lamisform").attr("action", "Patient_new");  
                        return true; 
                    }else if($("#new_button").html() === "View"){
                        $("#lamisform").attr("action", "Patient_find");
                        $("#lamisform").submit();
                        return true;
                    }		
                });                
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Home_page");
                    return true;
                });
				$("#identify_button").bind("click", function(event){
                    var cmp = new FingerprintComponent();
                    var result = function(res) {
                        if(!!res && res.patientId) {
                            if(res.inFacility){
                                $("#patientId").val(res.patientId);
                                $("#lamisform").attr("action", "Patient_find");
                                $("#lamisform").submit();
                                return true;
                            }else {
                                $('#messageBar').text(res.name + ' not in this facility!').show();
                                return false;
                            }
                        }
                        else {
                            return true;
                        }
                    }
                    cmp.identify(result);
                    return false;
                });
            });
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  

            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_home.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/search.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Home >> Patient Registration</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">
                                    <table width="97%" border="0">
                                        <tr>
                                            <td width="12%">Hospital No:</td>
                                            <td><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" /></td>
                                            <td><input name="patientId" type="hidden" id="patientId" /></td>
											<td><button id="identify_button">Identify</button></td>	
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td width="18%"><input name="name" type="text" class="inputboxes" id="name" /></td>
                                        </tr>
                                    </table>                               
                                </td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>                        
                        <p></p>

                        <div>
                            <fieldset>  
                                <legend> Patient List</legend>
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                    <tr>
                                        <td>
                                            <table id="grid" ></table>
                                            <div id="pager" style="text-align:center;"></div>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="buttons" style="width: 200px">
                            <button id="new_button">New</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
