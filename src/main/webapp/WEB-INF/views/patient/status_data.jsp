<%-- 
    Document   : Status
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        <link type="text/css" rel="stylesheet" href="css/zebra_dialog.css" />

        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/status-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/zebra_dialog.js"></script>        
        <script type="text/JavaScript"> 
            var lastSelectDate = "";
            var updateRecord = false;
            $(document).ready(function(){
                resetPage();
                initialize();
                reports();
                      
                $.ajax({
                    url: "Patient_retrieve.action",
                    dataType: "json",                    
                    success: function(patientList) {
                        // set patient id and number for which infor is to be entered
                        $("#patientId").val(patientList[0].patientId);
                        $("#hospitalNum").val(patientList[0].hospitalNum);
                        $("#previousStatus").html(patientList[0].currentStatus);
                        $("#currentStatus").html(patientList[0].currentStatus);
                        $("#outcome").val(patientList[0].currentStatus);
                        
                        date = patientList[0].dateCurrentStatus;
                        $("#datePreviousStatus").html(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));
                        $("#lastStatusDate").val(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));
                        $("#date").val(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));
                        $("#dateCurrentStatus").html(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));
                        $("#patientInfor").html(patientList[0].surname + " " + patientList[0].otherNames);
                   }                    
                }); //end of ajax call
 
                $.ajax({
                    url: "Status_retrieve.action",
                    dataType: "json",   
                    data: {historyId: sessionStorage.getItem("historyId"), dateCurrentStatus: sessionStorage.getItem("dateCurrentStatus")},
                    success: function(statusList) {
                        console.log("Log status dataww: ", statusList[0]);
                        populateForm(statusList); 
                    },
                    error: function(err){
                        console.log(err);
                    }
                }); //end of ajax call
        }); 
        
        function populateForm(statusList){
            $("#outcome").val(statusList[0].currentStatus);
            if(statusList[0].deletable === '1'){
                $("#currentStatus").val(statusList[0].currentStatus);
                $("#dateCurrentStatus").val(statusList[0].dateCurrentStatus); 
            }
            
            $("#outcomeDate").val(statusList[0].dateCurrentStatus); 
            $("#historyId").val(statusList[0].historyId);                        
            date = statusList[0].dateCurrentStatus;
            console.log("Populate form: ", date);
            $("#date").val(date);
            updateRecord = true;
            lastSelected = statusList[0].historyId;
            initButtonsForModify()
            if(statusList[0].deletable == "1") {
                $("#delete_button").removeAttr("disabled");
                $("#save_button").removeAttr("disabled");
            } else {
                $("#delete_button").attr("disabled", "disabled");
                $("#save_button").attr("disabled", "disabled");
               // $("#outcome").val("");
                //$("#outcomeDate").val("");                        
               // $("#date").val("");
            }
        }
        </script>
    </head>
 
    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_home.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/appointment.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> ART Clinic >> Client Status Update</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>                        
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>Hospital No:</label></td>
                                <td width="20%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/></td>
                                <td width="25%"><span id="patientInfor"</span></td>
                                <td width="20%"><input name="patientId" type="hidden" id="patientId"/><input name="historyId" type="hidden" id="historyId"/></td>
                            </tr>
                            <tr></tr>
                            <tr>
                                <td><label>New Status:</label></td>
                                <td>
                                    <select name="outcome" style="width: 230px;" class="inputboxes" id="outcome">
                                       <option ></option>
                                       <option>ART Restart</option>
                                       <option>ART Transfer Out</option>
                                       <option>Pre-ART Transfer Out</option>
                                       <option>Lost to Follow Up</option>
                                       <option>Stopped Treatment</option> 
                                       <option>Died (Confirmed)</option>
                                       <option>Previously Undocumented Patient Transfer (Confirmed)</option>
                                       <option>Traced Patient (Unable to locate)</option>
                                       <option>Did Not Attempt to Trace Patient</option>
                                    </select><span id="statusregHelp" class="errorspan"></span>
                                </td>
                                <td colspan="2"><label>Current Status: </label><span id="previousStatus" style="color:blue"></span></td>
                            </tr>                            
                            <tr id="statusDateTr">
                                <td><label>Date of New Status:</label></td>
                                <td><input name="date" type="text" style="width: 100px;" class="inputboxes" id="date"/>
                                    <input name="outcomeDate" type="hidden" id="outcomeDate"/><span id="dateHelp" class="errorspan"></span></td>
                                <td colspan="2"><label>Date of Current Status: </label><span id="datePreviousStatus" style="color:blue"></span></td>
                            </tr>
                            <tr id="interruptTr" style="display: none">
                                <td><label>Reason for Interruption:</label></td>
                                <td>
                                    <select name="reasonInterrupt" style="width: 230px;" class="inputboxes" id="reasonInterrupt">
                                        <option></option>
                                        <option>Toxicity/side effect</option>
                                        <option>Pregnancy</option>
                                        <option>Treatment failure</option>
                                        <option>Poor adherence</option>
                                        <option>Illness, hospitalization</option>
                                        <option>Drug out of stock</option>
                                        <option>Patient lacks finances</option>
                                        <option>Other patient decision</option>
                                        <option>Planned Rx interruption</option>
                                        <option>Other</option>
                                    </select>
                                </td>
                            </tr>
                            <tr id="deathTr" style="display: none">
                                <td><label>Cause of Death:</label></td>
                                <td>
                                    <select name="causeDeath" style="width: 230px;" class="inputboxes" id="causeDeath">
                                        <option></option>
                                        <option>HIV disease resulting in TB</option>
                                        <option>HIV disease resulting in cancer</option>
                                        <option>HIV disease resulting in other infectious and parasitic disease</option>
                                        <option>Other HIV disease resulting in other disease or conditions leading to death</option>
                                        <option>Other natural causes</option>
                                        <option>Non-natural causes</option>
                                        <option>Unknown cause</option>
                                    </select>
                                </td>
                            </tr>
                            <tr id="trackedDateTr" style="display: none">
                                <td><label>Date of Tracked:</label></td>
                                <td>
                                    <input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/>
                                    <input name="dateTracked" type="hidden" id="dateTracked"/></span>
                                </td>
                            </tr>
                            <tr id="agreedDateTr" style="display: none">
                                <td><label>Date Agreed to Return:</label></td>
                                <td>
                                    <input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/>
                                    <input name="agreedDate" type="hidden" id="agreedDate"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input name="currentStatus" type="hidden" id="currentStatus"/>
                                    <input name="dateCurrentStatus" type="hidden" id="dateCurrentStatus"/>
                                    <input name="lastStatusDate" type="hidden" id="lastStatusDate"/>
                                </td>                               
                                <td>
                                    <input name="gender" type="hidden" id="gender"/>
                                    <input name="dateBirth" type="hidden" id="dateBirth"/>
                                </td>                               
                            </tr>
                        </table>
                        <p></p>
                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;
                            <button id="delete_button" disabled="true"/>Delete</button> &nbsp;
                            <button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
