<%-- 
    Document   : Appointment
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var gridNum = 7;
            var enablePadding = true;
            $(document).ready(function(){
                $.ajax({
                    url: "Padding_status.action",
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                });
                resetPage();
                initialize();
                reports();
                
                $("#grid").jqGrid({
                    url: "Patient_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Hospital No", "Name", "Gender", "ART Status", "Address"],
                    colModel: [
                        {name: "hospitalNum", index: "hospitalNum", width: "100"},
                        {name: "name", index: "name", width: "150"},
                        {name: "gender", index: "gender", width: "50"},
                        {name: "currentStatus", index: "currentStatus", width: "130"},
                        {name: "address", index: "address", width: "300"}                        
                    ],
                    pager: $('#pager'),
                    rowNum: 100,
                    sortname: "patientId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 200,                    
                    jsonReader: {
                        root: "patientList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "patientId"
                    },
                    onSelectRow: function(id) {
                        if(id == null) {
                            id = 0;
                            if ($("#detail").getRecords() > 0) {
                                $("#detail").setGridParam({url: "Status_grid.action?q=1&patientId="+id, page:1}).trigger("reloadGrid");
                            }
                        } 
                        else {
                            $("#detail").setGridParam({url: "Status_grid.action?q=1&patientId="+id, page:1}).trigger("reloadGrid");
                        }
                        
                        $("#patientId").val(id);
                        var data = $("#grid").getRowData(id);
                        $("#hospitalNum").val(data.hospitalNum);
                        $("#name").val(data.name);
                        $("#new_button").html("New");
                        $("#new_button").removeAttr("disabled");   
                         console.log($("#patientId").val());
                         console.log(data);
                    },               
                    ondblClickRow: function(id) {
                        $("#patientId").val(id);
                        console.log($("#patientId").val());
                        var data = $("#grid").getRowData(id)
                        $("#hospitalNum").val(data.hospitalNum);
                        $("#name").val(data.name)
                        $("#lamisform").attr("action", "Status_new");
                        $("#lamisform").submit();
                    }
                }); //end of jqGrid 
                
                    // status history
                $("#detail").jqGrid({
                    url: "Status_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["ART Status", "Date of Status", "", "", "", ""],
                    colModel: [
                        {name: "currentStatus", index: "currentStatus", width: "500"},
                        {name: "dateCurrentStatus", index: "date", width: "160", align: 'center', formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},                        
                        {name: "status", index: "status", width: "70"},
                        {name: "dateCurrentStatus", index: "dateCurrentStatus", width: "100", hidden: true},
                        {name: "historyId", index: "historyId", width: "100", hidden: true},
                        {name: "deletable", index: "deletable", width: "5", hidden: true},
                    ],
                    rowNum: -1,
                    sortname: "dateCurrentStatus",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 100,                    
                    jsonReader: {
                        root: "statusList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "historyId"
                    },
                    loadComplete: function() {
                        var gridIds = $("#detail").getDataIDs();
                        
                        for(i = 0; i < gridIds.length; i++) {
                            var data = $("#detail").getRowData(gridIds[i]);
                            
                            if(data.deletable == "0") {
                                $("#detail").jqGrid('setCell', i+1, 'status', '', {background: '#ff9933'});
                            }
                            else {
                                $("#detail").jqGrid('setCell', i+1, 'status', '', {background: '#ccff33'});   
                            }  
                        } 
                    },
                    onSelectRow: function(id) {
                        var data = $("#detail").getRowData(id);
                        if(id != null) {
                            var selectedRow = $("#detail").getGridParam('selrow');
                            if(selectedRow != null) {
                                var data = $("#detail").getRowData(selectedRow);
                                $("#dateCurrentStatus").val(data.dateCurrentStatus);
                            }
                            $("#historyId").val(id);
                            $("#new_button").html("View");
                            console.log("History Id: ", $("#historyId").val());
                            console.log("PatientId2: ", $("#patientId").val());
                            sessionStorage.setItem("historyId", $("#historyId").val());
                            sessionStorage.setItem("dateCurrentStatus", $("#dateCurrentStatus").val());
                        }
                    },
                    ondblClickRow: function(id) {
                        $("#historyId").val(id);
                        
                        var selectedRow = $("#detail").getGridParam('selrow');
                        if(selectedRow != null) {
                            var data = $("#detail").getRowData(selectedRow);
                            $("#dateCurrentStatus").val(data.dateCurrentStatus);
                        }
                        
                        sessionStorage.setItem("historyId", $("#historyId").val());
                        sessionStorage.setItem("dateCurrentStatus", $("#dateCurrentStatus").val());
                        
                        $("#lamisform").attr("action", "Status_find");
                        $("#lamisform").submit();
                    }
                }); //end of jqGrid  
                
                $("#new_button").bind("click", function(event){
                    if ($("#new_button").html == 'View'){
                        $("#lamisform").attr("action", "Status_find");
                        return true;
                    } else {
                        $("#lamisform").attr("action", "Status_new");
                        return true;
                    }
                });
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Clinic_page");
                    return true;
                });
            });
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_home.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/search.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> ART Clinic >> Client Status Update</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">
                                    <table width="97%" border="0">
                                        <tr>
                                            <td width="12%">Hospital No:</td>
                                            <td><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" /></span></td>
                                            <td><input name="patientId" type="hidden" id="patientId" /></td>
                                            <td><input name="dateCurrentStatus" type="hidden" id="dateCurrentStatus" />
                                            <input name="historyId" type="hidden" id="historyId" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td width="18%"><input name="name" type="text" class="inputboxes" id="name" /></td>
                                        </tr>
                                    </table>                               
                                </td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>                        
                        <p></p>

                        <div>
                            <fieldset>  
                                <legend> Patient List</legend>
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                        <div id="pager" style="text-align:center;"></div>
                                    </td>
                                </tr>
                                </table>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="detail"></table>
                                    </td>
                                </tr>
                                </table>
                            </fieldset> 
                        </div>
                        <p></p>

                        <div id="buttons" style="width: 200px">
                            <button id="new_button" disabled="true">New</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
