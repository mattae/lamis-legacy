<%-- 
    Document   : devolve_data
    Created on : Feb 3, 2017, 2:12:29 PM
    Author     : user1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/devolve-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/jquery.timer.js"></script>
        <script type="text/javascript" src="js/date.js"></script>       
        <script type="text/JavaScript">
            var obj = {};
            var date = "", lastSelectDate = "";
            var updateRecord = false;
            $(document).ready(function(){
                resetPage();
                initialize();
                reports();
               
                $.ajax({
                    url: "Devolve_retrieve.action",
                    dataType: "json",                    
                    success: function(devolveList) {
                        populateForm(devolveList);
                    }                    
                });   
 
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Devolve_search");
                    return true;
                });                                
            }); 
            //date.slice(6), date.slice(0,2), date.slice(3,5)
            function checkDate() {
                if($("#date1").val().length != 0 ) {
                   var date = $("#date1").val(); 
                   var dateLastCd4 = new Date();
                   var revat = dateLastCd4.setMonth(6);
                   $("#date5").val(formatDate(revat));
                }
            }
            
            function formatDate(date) {
              var monthNames = [
                "January", "February", "March",
                "April", "May", "June", "July",
                "August", "September", "October",
                "November", "December"
              ]; 

              var day = date.getDate();
              var monthIndex = date.getMonth();
              var year = date.getFullYear();

              return day + ' ' + monthNames[monthIndex] + ' ' + year;
            }
            
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_pharmacy.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/clinic.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Pharmacy >> General Pharmacy >> Differentiated Care Service</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>Hospital No:</label></td>
                                <td  width="20%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/></td>
                                <td width="20%"><span id="patientInfor"></span><input name="name" type="hidden" id="name"/></td>
                                <td  width="20%"><input name="patientId" type="hidden" id="patientId"/><input name="devolveId" type="hidden" id="devolveId"/></td>
                            </tr>
                            <tr>
                                <td><label>Date of DMOC:</label></td>
                                <td><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateDevolved" type="hidden" id="dateDevolved"/><span id="dateDevolveHelp" class="errorspan"></span></td>
                                <td></td><td></td>
                            </tr>
                            <tr>
                                <td><label>Type of DMOC:</label></td>
                                <td>
                                    <select name="typeDmoc" style="width: 100px;" class="inputboxes" id="typeDmoc">
                                        <option></option>
                                        <option>CPARP</option>
                                        <option>CARC</option>
                                        <option>MMS</option>
                                        <option>MMD</option>
                                    </select>
                                <span id="typeHelp" class="errorspan"></span></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>Viral Load Assessment done?:</label></td>
                                <td>
                                    <select name="viralLoadAssessed" style="width: 100px;" class="inputboxes" id="viralLoadAssessed">
                                        <option></option>
                                        <option>No</option>
                                        <option>Yes</option>
                                    </select>
                                </td>
                                <td><label>CD4 count Assessment done?:</label></td>
                                <td>
                                    <select name="cd4Assessed" style="width: 100px;" class="inputboxes" id="cd4Assessed">
                                        <option></option>
                                        <option>No</option>
                                        <option>Yes</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Current Viral Load:</label></td>
                                <td width="20%"><input name="lastViralLoad" type="text" style="width: 100px;" id="lastViralLoad" /></td>
                                <td><label>Date of Viral Load:</label></td>
                                <td width="20%"><input name="date2" type="text" style="width: 100px;" id="date2" /><input name="dateLastViralLoad" type="hidden" id="dateLastViralLoad" /></td>
                            </tr>
                            <tr>
                                <td><label>Current CD4:</label></td>
                                <td width="20%"><input name="lastCd4" type="text" style="width: 100px;" id="lastCd4" /></td>
                                <td><label>Date of CD4:</label></td>
                                <td width="20%"><input name="date3" type="text" style="width: 100px;" id="date3" /><input name="dateLastCd4" type="hidden" id="dateLastCd4" /></td>
                            </tr>                            
                            <tr>
                                <td><label>Current Clinical Stage:</label></td>
                                <td width="20%"><input name="lastClinicStage" type="text" style="width: 100px;" id="lastClinicStage" /></td>
                                <td><label>Date of Clinical Stage:</label></td>
                                <td width="20%"><input name="date4" type="text" style="width: 100px;" id="date4" /><input name="dateLastClinicStage" type="hidden" id="dateLastClinicStage" /></td>
                            </tr>
                            <tr>
                                <td><input name="regimentype" type="hidden" id="regimentype"/></td>
                            </tr>
                        </table>
                        <div id="dmocForm" style="display:">

                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td><label>ARV Regimen:</label></td>
                                <td>
                                    <select name="regimen" style="width: 200px;" class="inputboxes" id="regimen">
                                    </select>
                                </td>
                                <td><label>Two months ARV refill dispensed?:</label></td>
                                <td>
                                    <select name="arvDispensed" style="width: 100px;" class="inputboxes" id="arvDispensed">
                                        <option></option>
                                        <option>No</option>
                                        <option>Yes</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td  width="20%"><label>Date of next Clinic/Lab re-evaluation:</label></td>
                                <td width="20%"><input name="date5" type="text" style="width: 100px;" class="inputboxes" id="date5"/><input name="dateNextClinic" type="hidden" id="dateNextClinic"/><span id="dateAppointHelp" class="errorspan"></span></td>
                                <td  width="20%"><label>Date of next ARV Refill:</label></td>
                                <td width="20%"><input name="date6" type="text" style="width: 100px;" class="inputboxes" id="date6"/><input name="dateNextRefill" type="hidden" id="dateNextRefill"/><span id="dateRefillHelp" class="errorspan"></span></td>
                            </tr>                            
                        </table>
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Details of Community Pharmacy/Club Providing ARV</td>
                            </tr>
                        </table>
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="20%"><label>State:</label></td>
                                <td width="20%">
                                    <select name="stateId" style="width: 130px;" class="inputboxes" id="stateId">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><label>LGA:</label></td>
                                <td width="20%">
                                    <select name="lgaId" style="width: 130px;" class="inputboxes" id="lgaId">
                                    <option></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Community Pharmacy/Club:</label></td>
                                <td width="20%">
                                    <select name="communitypharmId" style="width: 250px;" class="inputboxes" id="communitypharmId">
                                    </select><span id="pharmHelp" class="errorspan"></span>
                                </td>
                                <td</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>Address:</label></td>
                                <td width="20%"><input name="address" style="width: 350px;" type="text" class="inputboxes" id="address" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td><label>Phone:</label></td>
                                <td width="20%"><input name="phone" style="width: 350px;" type="text" class="inputboxes" id="phone" readonly="readonly"/></td>
                            </tr>
                        </table>        
                        </div>
                        <hr></hr> 
                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
