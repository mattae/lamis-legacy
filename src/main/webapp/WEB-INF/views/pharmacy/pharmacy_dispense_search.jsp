<%-- 
    Document   : pharmacy_dispense_search
    Created on : Jan 19, 2018, 5:02:32 PM
    Author     : user10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        <link type="text/css" rel="stylesheet" href="css/toastr.min.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>    
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/javascript" src="js/toastr.min.js"></script>
        <script type="text/javascript" src="js/lamis/prescribed_drugs.js"></script>
        <script type="text/JavaScript">
            var gridNum = 5;
            var enablePadding = true;
            $(document).ready(function(){
                $.ajax({
                    url: "Padding_status.action",
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                });
                resetPage();
                initialize();
                reports();
                getPrescribedDrugs();
 
                $("#grid").jqGrid({
                    url: "Patient_drug_dispense_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Hospital No", "Name", "Gender", "ART Status", "Address"],
                    colModel: [
                        {name: "hospitalNum", index: "hospitalNum", width: "100"},
                        {name: "name", index: "name", width: "150"},
                        {name: "gender", index: "gender", width: "50"},
                        {name: "currentStatus", index: "currentStatus", width: "130"},
                        {name: "address", index: "address", width: "300"}                        
                    ],
                    pager: $('#pager'),
                    rowNum: 100,
                    sortname: "patientId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,                    
                    height: 200,                    
                    jsonReader: {
                        root: "patientList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "patientId" 
                    },onSelectRow: function(id) {
                        $("#patientId").val(id);
                        var data = $("#grid").getRowData(id); 
                        $("#hospitalNum").val(data.hospitalNum);
                        $("#name").val(data.name);
                        $("#new_button").removeAttr("disabled");
                        $("#new_button").attr("src", "images/new_button.jpg");
                    },                        
                    ondblClickRow: function(id) {
                        $("#lamisform").attr("action", "Pharmacy_prescription_new"); 
                        $("#lamisform").submit(); 
                    }
                }); //end of master jqGrid  
                
                $.ajax({  
                    url: "Patient_retrieve_detail.action",
                    dataType: "json",
                    success: function(patientMap) {
//                        console.log(patientMap);
                        if(!$.isEmptyObject(patientMap)) {
                            $("#patientId").val(patientMap.patientId);
                            $("#hospitalNum").val(patientMap.hospitalNum);
                            $("#name").val(patientMap.name);
                            $("#new_button").removeAttr("disabled");                            
                        }
                    },
                    complete: function() {
                        //$("#detail").setGridParam({url: "Pharmacy_grid.action?q=1&patientId="+$("#patientId").val(), page:1}).trigger("reloadGrid");                        
                    }                   
                });
                $("#new_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Pharmacy_prescription_new");
                    return true;                        
                });
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Pharmacy_page");
                    return true;
                });
            });
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_pharmacy.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/search.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Pharmacy >> General Pharmacy >> Drug Prescriptions</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">
                                    <table width="97%" border="0">
                                        <tr>
                                            <td width="12%">Hospital No:</td>
                                            <td><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" /></span></td>
                                            <td></td>
                                            <td><input name="pharmacyId" type="hidden" id="pharmacyId" /><input name="patientId" type="hidden" id="patientId" /></td>
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td width="18%"><input name="name" type="text" class="inputboxes" id="name" /></td>
                                            <td><input name="dateVisit" type="hidden" id="dateVisit" /></td>
                                        </tr>
                                    </table>                               
                                </td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>                        
                        <p></p>
                        
                        <div>
                            <fieldset> 
                            <legend> Patient List</legend>                            
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                        <div id="pager" style="text-align:center;"></div>
                                        <p></p>
                                        <table id="detail"></table>
                                    </td>
                                  </tr>                              
                                </table>
                            </fieldset>  
                        </div>
                        <p></p>
                                                  
                        <div id="buttons" style="width: 200px">
                            <button id="new_button" disabled="true">New</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
