<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/anc-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>     
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/JavaScript">
           var date = "";
           var updateRecord = false;
            $(document).ready(function(){
                resetPage();
                initialize();
                reports();             
              
		$.ajax({
                    url: "Anc_retrieve_last.action",
                    dataType: "json",                    
                    success: function(ancLast) {
                        console.log(ancLast);
                        populateForm(ancLast);
                    },
                    error: function(error){
                        console.log("There was an error! "+error.getMessage());
                    }
                }); //end of ajax call

            });	
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_pmtct.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/clinic.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> PMTCT >>First ANC Visit</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details: <b>Fields marked with ** are required</b></td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="25%"><label>Hospital No:</label></td>
                                <td width="30%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/><span id="numHelp" style="color:red"></span></td>
                                <td width="45%" colspan="2"><span id="patientInfor"></span><input name="patientId" type="hidden" class="inputboxes" id="patientId"/><input name="ancId" type="hidden" id="ancId"/><input name="partnerinformationId" type="hidden" id="partnerinformationId"/></td>
                                <input name="uniqueId" type="hidden" id="uniqueId"/>
                            </tr>
                            <tr>
                                <td><label>ANC No:</label></td>
                                <td><input name="ancNum" type="text" class="inputboxes" id="ancNum"/><span id="numHelp" class="errorspan"></span></td>
                                <td width="25%"><label>Date of Visit:</label></td>
                                <td width="30%"><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateVisit" type="hidden" class="inputboxes" id="dateVisit"/><span id="datevisitHelp" class="errorspan"></span></td>
                            </tr>
                            <tr>
                                <td><label>Gravida:</label></td>
                                <td><input name="gravida" type="text" style="width: 50px;" class="inputboxes" id="gravida"/></td>
                                <td width="25%"><label>Parity:</label></td>
                                <td width="35%"><input name="parity" type="text" style="width: 50px;" class="inputboxes" id="parity"/></td>
                            </tr>
                            <tr>
                                <td><label>L.M.P:</label></td>
                                <td><input name="date3" type="text" style="width: 100px;" class="inputboxes" id="date3"/><input name="lmp" type="hidden" class="inputboxes" id="lmp"/><span id="lmpHelp" class="errorspan"></span></td>
                                <td colspan="2"><label>E.D.D:</label>&nbsp;&nbsp;<input name="date4" type="text" style="width: 65px;" class="inputboxes" id="date4"/><input name="edd" type="hidden" class="inputboxes" id="edd"/>&nbsp;
                                <label>Gestational Age (weeks):</label>&nbsp;&nbsp;<input name="gestationalAge" type="text" style="width: 25px;" class="inputboxes" id="gestationalAge"/></td>
                            </tr>
                            <tr>
                                <td><label>Time of HIV Diagnosis:</label></td>
                                <td>
                                    <select name="timeHivDiagnosis" style="width: 170px;" class="inputboxes" id="timeHivDiagnosis">
                                        <option></option>
                                       <option>Newly Tested HIV+ (ANC)</option>
                                       <option>Previously known HIV+ (ANC)</option>
<!--                                        <option>Labour</option>
                                        <option>Post Partum &lt;72hrs</option>-->
                                    </select>
                                </td>
                                <td><label>Source of Referral:</label></td>
                                <td>
                                    <select name="sourceReferral" style="width: 180px;" class="inputboxes" id="sourceReferral">
                                        <option></option>
                                        <option>PMTCT outreach</option>
                                        <option>Sex worker outreach</option>
                                        <option>Medical outpatient</option>
                                        <option>Youth/Adolescent outreach</option>
                                        <option>Private/Commercial Health facility</option>
                                        <option>Under-fives/Immunization clinic</option>
                                        <option>External HCT centre</option>
                                        <option>CBO</option>
                                        <option>In-patients</option>
                                        <option>Self-referral</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%"><label>Date of Next Appointment:</label></td>
                                <td width="30%"><input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/><input name="dateNextAppointment" type="hidden" class="inputboxes" id="dateNextAppointment"/><span id="dateNextAppointmentHelp" class="errorspan"></span></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><input name="date5" type="hidden" style="width: 100px;" class="inputboxes" id="date5"/><input name="dateConfirmedHiv" type="hidden" class="inputboxes" id="dateConfirmedHiv"/></td>
                                <td><input name="date6" type="hidden" style="width: 100px;" class="inputboxes" id="date6"/><input name="dateArvRegimenCurrent" type="hidden" class="inputboxes" id="dateArvRegimenCurrent"/></td>                               
                            </tr>
                        </table>
                        <p></p>
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Syphilis Information</td>
                            </tr>
                        </table>
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="25%"><label>Tested:</label></td>
                                <td width="28%">
                                    <select name="syphilisTested" style="width: 80px;" class="inputboxes" id="syphilisTested">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
                                <td width="20%"><label>Test Result:</label></td>
                                <td width="27%">
                                    <select name="syphilisTestResult" style="width: 130px;" class="inputboxes" id="syphilisTestResult">
                                        <option></option>
                                        <option>P - Positive</option>
                                        <option>N - Negative</option>
                                        <option>I - Indeterminate</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Treated:</label></td>
                                <td>
                                    <select name="syphilisTreated" style="width: 80px;" class="inputboxes" id="syphilisTreated">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                        <p></p>
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Hepatitis Information</td>
                            </tr>
                        </table>
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="25%"><label>Tested for Hepatitis B:</label></td>
                                <td width="28%">
                                    <select name="hepatitisBTested" style="width: 80px;" class="inputboxes" id="hepatitisBTested">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
                                <td width="20%"><label>Hepatitis B Test Result:</label></td>
                                <td width="27%">
                                    <select name="hepatitisBTestResult" style="width: 130px;" class="inputboxes" id="hepatitisBTestResult">
                                        <option></option>
                                        <option>P - Positive</option>
                                        <option>N - Negative</option>
                                        <option>I - Indeterminate</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%"><label>Tested for Hepatitis C:</label></td>
                                <td width="28%">
                                    <select name="hepatitisCTested" style="width: 80px;" class="inputboxes" id="hepatitisCTested">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
                                <td width="20%"><label>Hepatitis C Test Result:</label></td>
                                <td width="27%">
                                    <select name="hepatitisCTestResult" style="width: 130px;" class="inputboxes" id="hepatitisCTestResult">
                                        <option></option>
                                        <option>P - Positive</option>
                                        <option>N - Negative</option>
                                        <option>I - Indeterminate</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <p></p>
                    <table width="100%" border="0">
                        <tr>
                            <td class="topheaders">Partner Information</td>
                        </tr>
                    </table>
                    <table width="99%" border="0" class="space" cellpadding="3">
                        <tr>
                            <td width="25%"><label>Agreed to Partner Notification:</label></td>
                            <td width="30%">
                                <select name="partnerNotification" style="width: 80px;" class="inputboxes" id="partnerNotification">
                                    <option></option>
                                    <option>Yes</option>
                                    <option>No</option>
                                </select>
                            </td>
                            <td><label>Partner's HIV Status:</label></td>
                            <td>
                                <select name="partnerHivStatus" style="width: 130px;" class="inputboxes" id="partnerHivStatus">
                                    <option></option>
                                    <option>Positive</option>
                                    <option>Negative</option>
                                    <option>Unknown</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Partner referred to:</label></td>
                            <td>
                                <input name="fp" type="checkbox" value="1" id="fp"/>&nbsp;<label>FP</label>&nbsp;&nbsp;
                                <input name="art" type="checkbox" value="1" id="art"/>&nbsp;<label>ART</label>&nbsp;&nbsp;
                                <input name="others" type="checkbox" value="1" id="others"/>&nbsp;<label>Others</label>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>							
                    </table>
                    <hr></hr>
                    <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
