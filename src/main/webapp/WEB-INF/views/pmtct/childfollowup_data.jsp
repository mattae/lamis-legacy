<%--
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>    
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/childfollowup-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
          
        <script type="text/JavaScript">
           var date = "";
           var updateRecord = false;
            $(document).ready(function(){ 
                resetPage();
                initialize();
                reports();
				
		$.ajax({
                    url: "Childfollowup_retrieve.action",
                    dataType: "json",                    
                    success: function(childfollowupList) {
                        populateForm(childfollowupList);
                    }                    
                }); //end of ajax call
            });		
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_pmtct.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/clinic.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> PMTCT >> Child Follow-Up</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td><label>Child's Hospital No:</label></td>
                                <td><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/><span id="numHelp" style="color:red"></span></td>
                                <td><span id="patientInfor"></span><input name="patientId" type="hidden" class="inputboxes" id="patientId"/><input name="motherId" type="hidden" class="inputboxes" id="motherId"/></td>
                                <td><span id="childInfor"></span><input name="childId" type="hidden" class="inputboxes" id="childId"/><input name="childfollowupId" type="hidden" id="childfollowupId"/><input name="dateBirth" type="hidden" id="dateBirth"/></td>
                            </tr>
                            <tr>
                                <td width="25%"><label>Date of Visit:</label></td>
                                <td width="30%"><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateVisit" type="hidden" class="inputboxes" id="dateVisit"/><span id="dateVisitHelp"  class="errorspan"></span></td>
                                <td width="30%"><label>Age (weeks):</label></td>
                                <td width="15%"><input name="ageVisit" type="text" style="width: 50px;" class="inputboxes" id="ageVisit"/></td>
                            </tr>
                            <tr>
                                <td><label>Covered with ARV Prophylaxis:</label></td>
                                <td>
                                    <select name="arv" style="width: 80px;" class="inputboxes" id="arv">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
                                <td><label>Type ARV Prophylaxis:</label></td>
                                <td>
                                    <select name="arvType" style="width: 80px;" class="inputboxes" id="arvType">
                                        <option></option>
                                        <option>NVP</option>
                                        <option>NVP+AZT</option>
                                        <option>Others</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Timing of ARV Prophylaxis:</label></td>
                                <td>
                                    <select name="arvTiming" style="width: 230px;" class="inputboxes" id="arvTiming">
                                        <option></option>
                                        <option>Within 72 hrs - Facility Delivery</option>
                                        <option>Within 72 hrs - Delivered Outside Facility</option>
                                        <option>After 72 hrs - Facility Delivery</option>
                                        <option>After 72 hrs - Delivered Outside Facility</option>
                                        <option>Others</option> 
                                    </select>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr id="arvInit">
                                <td><label>ARV Initiation Date:</label></td>
                                <td><input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/><input name="dateNvpInitiated" type="hidden" class="inputboxes" id="dateNvpInitiated"/></td>
                                <td><label>Age at ARV Initiation (weeks):</label></td>
                                <td><input name="ageNvpInitiated" type="text" style="width: 50px;" class="inputboxes" id="ageNvpInitiated"/></td>                           
                            </tr>
                            <tr>
                                <td><label>On Cotrimoxazole:</label></td>
                                <td>
                                    <select name="cotrim" style="width: 80px;" class="inputboxes" id="cotrim">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr id="cotrimInit">
                                <td><label>CTX Initiation Date:</label></td>
                                <td><input name="date3" type="text" style="width: 100px;" class="inputboxes" id="date3"/><input name="dateCotrimInitiated" type="hidden" class="inputboxes" id="dateCotrimInitiated"/></td>
                                <td><label>Age at CTX Initiation (weeks):</label></td>
                                <td><input name="ageCotrimInitiated" type="text" style="width: 50px;" class="inputboxes" id="ageCotrimInitiated"/></td>
                            </tr>
                                <tr>
                                    <td><label>Body Weight (kg):</label></td>
                                    <td><input name="bodyWeight" type="text" style="width: 50px;" class="inputboxes" id="bodyWeight"/></td>
                                    <td><label>Height (cm):</label></td>
                                    <td><input name="height" type="text" style="width: 50px;" class="inputboxes" id="height"/></td>
                                </tr>
                                <tr>
                                    <td><label>Feeding Type:</label></td>
                                    <td>
                                    <select name="feeding" style="width: 230px;" class="inputboxes" id="feeding"/>
                                       <option></option>
                                       <option>1 - Exclusive Breast Feeding (EBF)</option>
                                       <option>2 - Commercial Infant Formula/Exclusive Replacement Feeding</option>
                                       <option>3 - Mixed Feeding (MF)</option>
                                    </select>
                                </td>
				</tr>
                                <tr>
                                    <td><label>Date of PCR Sample Collection:</label></td>
                                    <td><input name="date4" type="text" style="width: 100px;" class="inputboxes" id="date4"/><input name="dateSampleCollected" type="hidden" class="inputboxes" id="dateSampleCollected"/></td>
                                </tr> 
				<tr>
                                    <td><label>Reason for PCR:</label></td>
                                    <td>
                                        <select name="reasonPcr" style="width: 230px;" class="inputboxes" id="reasonPcr"/>
                                           <option></option>
                                           <option>0 - 1st PCR</option>
                                           <option>1 - Confirm Positive PCR</option>
                                           <option>2 - Infant with Signs and Symptoms of HIV</option>
                                           <option>3 - Follow-Up for Breastfed Child</option>
                                           <option>4 - Previous Indeterminate Test</option>
                                           <option>5 - Test +ve at 9 mths with rapid test kit</option>
                                           <option>6 - 2nd PCR</option>
                                        </select>
                                    </td>
                                    <td><label>Date PCR Sample Sent:</label></td>
                                    <td><input name="date5" type="text" style="width: 100px;" class="inputboxes" id="date5"/><input name="dateSampleSent" type="hidden" class="inputboxes" id="dateSampleSent"/></td>
				</tr> 
				<tr>
                                    <td><label>Date PCR Result Received:</label></td>
                                    <td><input name="date6" type="text" style="width: 100px;" class="inputboxes" id="date6"/><input name="datePcrResult" type="hidden" class="inputboxes" id="datePcrResult"/></td>
                                    <td><label>PCR Result:</label></td>
                                    <td>
                                        <select name="pcrResult" style="width: 130px;" class="inputboxes" id="pcrResult">
                                            <option></option>
                                            <option>P - Positive</option>
                                            <option>N - Negative</option>
                                            <option>I - Indeterminate</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Rapid Antibody Test Done at 18 Months:</label></td>
                                    <td>
                                        <select name="rapidTest" style="width: 80px;" class="inputboxes" id="rapidTest">
                                            <option></option>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>
                                    </td>	
                                    <td><label>Date Rapid Antibody Test Was Done:</label></td>
                                    <td><input name="date12" type="text" style="width: 100px;" class="inputboxes" id="date12"/><input name="dateRapidTest" type="hidden" class="inputboxes" id="dateRapidTest"/></td>
                                </tr>
                                <tr>
                                    <td><label>Rapid Test Result at 18 Months:</label></td>
                                    <td>
                                        <select name="rapidTestResult" style="width: 130px;" class="inputboxes" id="rapidTestResult">
                                            <option></option>
                                            <option>Positive</option>
                                            <option>Negative</option>
                                        </select>
                                    </td>
                                    <td colspan="2"></td>
				</tr>
                                <tr>
                                    <td><label>Care Giver Received Result:</label></td>
                                        <td>
                                            <select name="caregiverGivenResult" style="width: 80px;" class="inputboxes" id="caregiverGivenResult">
                                                <option></option>
                                                <option>Yes</option>
                                                <option>No</option>
                                            </select>
                                        </td>
                                    <td><label>Visit Status</label></td>
                                    <td>
                                        <select name="childOutcome" style="width: 130px;" class="inputboxes" id="childOutcome">
                                            <option></option>
                                            <option>1 - Active</option>
                                            <option>2 - Transfered In</option>
                                            <option>3 - Transfered Out</option>
                                            <option>4 - Linked to ART</option>
                                            <option>5 - Discharged from PMTCT</option>
                                            <option>6 - Missed Appointment/Defaulted</option>
                                            <option>7 - Lost to Follow Up</option>
                                            <option>8 - Dead</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Infant Outcome at 18 months</label></td>
                                    <td>
                                        <select name="infantOutcome" style="width: 240px;" class="inputboxes" id="infantOutcome">
                                            <option></option>
                                            <option>1 - HIV Positive Linked to ART</option>
                                            <option>2 - HIV Positive Not Linked to ART</option>
                                            <option>3 - HIV Negative No Longer Breastfeeding</option>
                                            <option>4 - HIV Negative Still Breastfeeding</option>
                                            <option>5 - HIV Status Unknown Still breastfeeding</option>
                                            <option>6 - HIV Status Unknown No Longer breastfeeding</option>
                                            <option>7 - HIV Status Unknown Died</option>
                                            <option>8 - HIV Status Unknown Lost to Follow Up</option>
                                            <option>9 - HIV Status Unknown Transfer Out</option>
                                        </select>
                                    </td>
                                    <td><label>Date Linked to ART:</label></td>
                                    <td><input name="date11" type="text" style="width: 100px;" class="inputboxes" id="date11"/><input name="dateLinkedToArt" type="hidden" class="inputboxes" id="dateLinkedToArt"/></td>                                    
                                </tr>
                            <tr>
                            <td><label>Referred to:</label></td>
                                <td>
                                   <select name="referred" style="width: 130px;" class="inputboxes" id="referred">
                                        <option></option>
                                        <option>ART</option>
                                        <option>Nutrition</option>
                                        <option>Other</option>
                                    </select>
                                </td>
				<td><label>Next Appointment:</label></td>
                                <td><input name="date7" type="text" style="width: 100px;" class="inputboxes" id="date7"/><input name="dateNextVisit" type="hidden" class="inputboxes" id="dateNextVisit"/><span id="dateNextVisitHelp" class="errorspan"></span></td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>							
                        </table>
			<hr></hr>
                        <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
