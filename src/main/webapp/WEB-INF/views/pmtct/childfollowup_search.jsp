<%-- 
    Document   : Clinic
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script> 
        <script type="text/javascript" src="js/json.min.js"></script>       
        <script type="text/javascript" src="js/popper.min.js"></script>               
        <script type="text/javascript" src="js/tippy.min.js"></script>
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>      
        <script type="text/javascript" src="js/lamis/childfollowup-common.js"></script>
        <script type="text/javascript" src="js/lamis/search-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script> 
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script>
        <script type="text/JavaScript">
            var gridNum = 10;
            var enablePadding = true;
            var lastSelected = -99;
            var updateRecord = false;
            $(document).ready(function(){
                $.ajax({
                    url: "Padding_status.action",
                    dataType: "json",
                    success: function(statusMap) {
                       enablePadding = statusMap.paddingStatus;
                    }
                });
                resetPage();
                initialize();
                reports();
                
                $("#dialog").dialog({
                    title: "Register New Child",
                    autoOpen: false,
                    width: 850,
                    resizable: false,
                    buttons: [{text: "Save", click: createChild}, 
                        {text: "Cancel", click: function(){$(this).dialog("close")}}]
                });

                $("#grid").jqGrid({
                    url: "Child_grid.action",
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Child's Hospital No", "Mother's Name", "Child's Name", "Child's Gender", "Mother In Facility", ""],
                    colModel: [
                        {name: "hospitalNumber", index: "hospitalNumber", width: "130"},
                        {name: "nameMother", index: "nameMother", width: "170"},
                        {name: "childName", index: "childName", width: "160"},
                        {name: "gender", index: "gender", width: "130"},
                        {name: "inFacility", index: "inFacility", width: "140"},
                        {name: "childId", index: "childId", width: "0", hidden: true}						
                    ],
                    pager: $('#pager'),
                    rowNum: 100,
                    sortname: "childId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,
                    height: 200,                    
                    jsonReader: {
                        root: "childList",
                        page: "currpage",
                        total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "childId"
                    },
                    onSelectRow: function(id) {
                        if(id == null) {
                            id = 0;
                            if ($("#detail").getRecords() > 0) {
                                $("#detail").setGridParam({url: "Childfollowup_grid.action?q=1&childId="+id, page:1}).trigger("reloadGrid");
                            }
                        } 
                        else {
                            $("#detail").setGridParam({url: "Childfollowup_grid.action?q=1&childId="+id, page:1}).trigger("reloadGrid");
                        }
						
                        $("#childId").val(id);
                        var data = $("#grid").getRowData(id)
                        $("#cHospitalNum").val(data.hospitalNumber);
                        $("#nameMother").val(data.nameMother);
                        $("#patientId").val(data.patientId);
                        $("#new_button").removeAttr("disabled");
                        $("#new_button").html("New");													 
                    },
                    ondblClickRow: function(id) {
                        $("#lamisform").attr("action", "Childfollowup_new");
                        $("#lamisform").submit();
                    }						
                }); //end of master jqGrid                 
                
                $("#detail").jqGrid({
                    datatype: "json",
                    mtype: "GET",
                    colNames: ["Date of Visit", "Feeding at Present", "Outcome", "Rapid Test Result", "On Cotrim"],
                    colModel: [
                        {name: "dateVisit", index: "dateVisit", width: "150", formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},
                        {name: "feeding", index: "feeding", width: "200"},
                        {name: "childOutcome", index: "childOutcome", width: "150"},
                        {name: "rapidTestResult", index: "rapidTestResult", width: "127"},
			{name: "cotrim", index: "cotrim", width: "100"}
                    ],
                    rowNum: -1,
                    sortname: "childfollowupId",
                    sortorder: "desc",
                    viewrecords: true,
                    imgpath: "themes/basic/images",
                    resizable: false,                    
                    height: 150,                    
                    jsonReader: {
                        root: "childfollowupList",
                        page: "currpage",
                        //total: "totalpages",
                        records: "totalrecords",
                        repeatitems: false,
                        id: "childfollowupId"
                    }, 
                    afterInsertRow: function(id) {
                        var content = $("#detail").getCell(id, "feeding");
                        $("#detail").setCell(id, "feeding", content.substring(content.indexOf("-") + 2), '', '');

                        content = $("#detail").getCell(id, "childOutcome");
                        $("#detail").setCell(id, "childOutcome", content.substring(content.indexOf("-") + 2), '', '');  
                    },
                    onSelectRow: function(id) {
                        if(id != null) {
                            var selectedRow = $("#detail").getGridParam('selrow');
                            if(selectedRow != null) {
                                var data = $("#detail").getRowData(selectedRow);
                                var date = data.dateVisit;
                                $("#dateVisit").val(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));
                            }
                            $("#childfollowupId").val(id);
                            $("#new_button").html("View");
                        }
                    },							
                    ondblClickRow: function(id) {
                        var selectedRow = $("#detail").getGridParam('selrow');
                        if(selectedRow != null) {
                            var data = $("#detail").getRowData(selectedRow);
                            var date = data.dateVisit;
                            $("#dateVisit").val(date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6));
                        }
                        $("#childfollowupId").val(id);
                        $("#lamisform").attr("action", "Childfollowup_find");
                        $("#lamisform").submit();
                    }
                }); //end of detail jqGrid                 

                $.ajax({
                    url: "Patient_retrieve_detail.action",
                    dataType: "json",
                    success: function(patientMap) {
                        if(!$.isEmptyObject(patientMap)) {
                            $("#patientId").val(patientMap.patientId);
                            $("#hospitalNum").val(patientMap.hospitalNum);
                            $("#name").val(patientMap.name);
                            $("#new_button").removeAttr("disabled");                            
                        }
                    },
                    complete: function() {
                        $("#detail").setGridParam({url: "Childfollowup_grid.action?q=1&patientId="+$("#patientId").val(), page:1}).trigger("reloadGrid");                        
                    }                   
                });

                $("#new_button").bind("click", function(event){
					if($("#new_button").html() === "New"){
                        $("#lamisform").attr("action", "Childfollowup_new");
                        return true;  
                    }else if($("#new_button").html() === "View"){
                        $("#lamisform").attr("action", "Childfollowup_find");
                        $("#lamisform").submit();
                        return true;
                    } 										   
                });
                $("#new_patient").bind("click", function(event){
                    $("#dialog").dialog("open");
                    return false;      									
                });
                $("#close_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Pmtct_page");
                    return true;
                });
            });
            
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">
                
                <jsp:include page="/WEB-INF/views/template/nav_pmtct.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform"> 
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                    <img src="images/search.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> PMTCT >> Child Follow-Up</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">
                                    <table width="97%" border="0">
                                        <tr>
                                            <td width="18%">Child's Hospital No:</td>
                                            <td><input name="cHospitalNum" type="text" class="inputboxes" id="cHospitalNum" /></td>
                                            <td><input name="childfollowupId" type="hidden" id="childfollowupId" /><input name="childId" type="hidden" id="childId" /><input name="patientId" type="hidden" id="patientId" /><input name="motherId" type="hidden" id="motherId" /></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Mother's Name:</td>
                                            <td width="18%"><input name="nameMother" type="text" class="inputboxes" id="nameMother" /></td>
                                            <td><input name="dateVisit" type="hidden" id="dateVisit"/></td>
                                        </tr>
                                    </table>                               
                                </td>
                            </tr>
                        </table>
                        <div id="buttons" style="width: 300px">
                            <label>Child born outside this facility?</label> &nbsp; &nbsp;<button style="width:100px; font-size: 13px;" id="new_patient"><strog>Register</strog></button>
                        </div>
                        <p></p>
                        
                        <div>
                            <fieldset> 
                            <legend> Child List</legend>                            
                                <p></p>
                                <table width="99%" height="90" border="0" class="space">
                                <tr>
                                    <td>
                                        <table id="grid"></table>
                                        <div id="pager" style="text-align:center;"></div>
                                        <p></p>
                                        <table id="detail"></table>
                                    </td>
                                  </tr>                              
                                </table>
                            </fieldset>  
                        </div>
                        <p></p>
                        <!--Create the child-->
                        <div id="dialog">
                            <table width="98%" border="0" class="space" cellpadding="3">
                                <div style="width: 400px;" id="messageBar"></div>
                                <tr colspan='3'>
                                    <td colspan='6' style="text-align: center"><label><strong>Baby Information</strong></label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="childInfoGrid"></table>
                                        <div id="childInfoPager" style="text-align:center;"></div>
                                    </td>
                                </tr> 
                            </table>
                            <p></p>
                            <table width="98%" border="0" class="space" cellpadding="3">                                
                                <tr colspan='3'>
                                    <td colspan='6' style="text-align: center"><label><strong>Mother/Caregiver Information</strong></label></td>
                                </tr>
                            </table>
                            <table width="60%" border="0" class="space" cellpadding="3"> 
                                <tr>
                                    <td>
                                        <label style="width:150px;">Mother taking treatment in this facility? 
                                            <span>
                                                <button style='color:blue' id="tip" data-tippy-content="This is used to determine if the woman is taking treatment 
                                                   in the facility!">?
                                                </button>
                                            </span>
                                        </label>
                                    </td>
                                    <td>
                                        <select name="willing" style="width: 70px;" class="inputboxes" id="willing">
                                            <option></option>
                                            <option>No</option>
                                            <option>Yes</option>
                                        </select>
                                        <span id="willingHelp" style="color:red"></span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                            <table width="80%" border="0" class="space" cellpadding="3">
                                <tr id="outfac"> 
                                    <td><label>Hospital Number:</label></td>
                                    <td><input style="width: 120px;" name="motherUniqueId" type="text" class="inputboxes" id="motherUniqueId"/></td>
                                    <td><label>Date Confirmed HIV:</label></td>
                                    <td><input name="date9" type="text" style="width: 100px;" class="inputboxes" id="date9"/><input name="dateConfirmedHiv" type="hidden" class="inputboxes" id="dateConfirmedHiv"/></td>                               
                                </tr>
                                <tr id="outfac1">
                                    <td><label>Mother's Surname:</label></td>
                                    <td><input style="width: 120px;" name="motherSurname" type="text" class="inputboxes" id="motherSurname"/>
                                    <span id="motherSurnameHelp" class="errorspan"></span></td>
                                    <td><label>Mother's Other Names:</label></td>
                                    <td><input style="width: 120px;" name="motherOtherNames" type="text" class="inputboxes" id="motherOtherNames"/></td>                                  
                                </tr>
                                <tr id="outfac2">
                                    <td><label>Date Started ARV/ART:</label></td>
                                    <td><input name="date10" type="text" style="width: 100px;" class="inputboxes" id="date10"/><input name="dateStarted" type="hidden" class="inputboxes" id="dateStarted"/></span></td>                                                              
                                    <td><label>Mother's Address:</label></td>
                                    <td><input style="width: 180px;" name="address" type="text" class="inputboxes" id="address"/></td>                                                                 
                                </tr>
                                <tr id="outfac3">
                                    <td><label>Mother's Phone No.:</label></td>
                                    <td><input style="width: 120px;" name="phone" type="text" class="inputboxes" id="phone"/></td>                                                              
                                    <td></td>  
                                    <td></td>  
                                </tr>
                                <tr id="infac">
                                    <td><label>Mother's Hospital Number</label></td>
                                    <td><input style="width: 120px;" name="hospNumber" type="text" class="inputboxes" id="hospNumber"/>
                                        <span><button style='color:blue' id="search">Search</button></span>
                                    </td>   
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr id="infac2">
                                    <td><label>Mother's Name:</label></td>
                                    <td><input style="width: 190px;" name="motherName" type="text" class="inputboxes" id="motherName" readonly="readonly"/></td>                                  
                                    <td><input name="motherPatientId" type="hidden" id="motherPatientId"/>
                                    <span id="motherPatientIdHelp" class="errorspan"></span>
                                    </td> 
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                            <script>
                                
//                                var lastSelected = -99; 
                                $("#search").bind("click", function(event){
                                    $("#motherName").val("");
                                    $("#motherPatientId").val("");
                                    $.ajax({
                                        url: "Patient_find_number.action",
                                        dataType: "json",
                                        data: {hospitalNum: $("#hospNumber").val()},

                                        success: function(patientList) {
                                            if($.isEmptyObject(patientList)) { // check if the return json object is empty ie no patient found
                                               var message = "Hospital number does not exist!";
                                               $("#messageBar").html(message).slideDown('slow');                                                    
                                            }
                                            else {
                                                if(patientList[0].gender !== "Female"){
                                                    var message = "This patient is not a female!";
                                                    $("#messageBar").html(message).slideDown('slow');
                                                }else{
                                                    $("#messageBar").slideUp('slow');
                                                    $("#motherName").val(patientList[0].name);
                                                    id = patientList[0].patientId;
                                                    $("#motherPatientId").val(id);
                                                    console.log("Mother PatientId is: ", $("#motherPatientId").val());
                                                }
                                            }          
                                        }                    
                                    }); //end of ajax call   
                                });
                                
                                $("#motherUniqueId").bind("mouseleave", function (event){
                                    var hospNumber = $("#motherUniqueId").val();
                                    if(hospNumber !== "")
                                        $("#motherUniqueId").val(zerorize(hospNumber));
                                });
                                
                                    //create the child grid
                                    createChildGrid();
                                    tippy('#tip');
                                    $("#infac").attr("hidden", "hidden");
                                    $("#infac2").attr("hidden", "hidden");
                                    $("#date8").mask("99/99/9999");
                                    $("#date8").datepicker({
                                        dateFormat: "dd/mm/yy",
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "-100:+0",
                                        constrainInput: true,
                                        buttonImageOnly: true,
                                        buttonImage: "/images/calendar.gif"
                                    });
                                    
                                    $("#date8").bind("change", function(event){ 
                                        //TODO: Check
                                        var dated = $("#date8").val();
                                        if(dated.length !== 0){
                                            var todayFormatted = formatDate(new Date());
                                            if(parseInt(compare($("#date8").val(), todayFormatted)) === -1) {
                                                var message = "Date of birth cannot be later than today please correct!";
                                                $("#messageBar").html(message).slideDown('slow'); 
                                                $("#date8").val("");
                                            }else{
                                                $("#messageBar").slideUp('slow');
                                            }  
                                        }              
                                    });
                                    
                                    $("#date9").mask("99/99/9999");
                                    $("#date9").datepicker({
                                        dateFormat: "dd/mm/yy",
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "-100:+0",
                                        constrainInput: true,
                                        buttonImageOnly: true,
                                        buttonImage: "/images/calendar.gif"
                                    });
                                    
                                    $("#date9").bind("change", function(event){ 
                                        //TODO: Check
                                        var dated = $("#date9").val();
                                        if(dated.length !== 0){
                                            var todayFormatted = formatDate(new Date());
                                            if(parseInt(compare($("#date9").val(), todayFormatted)) === -1) {
                                                var message = "Date confirmed HIV cannot be later than today please correct!";
                                                $("#messageBar").html(message).slideDown('slow'); 
                                                $("#date9").val("");
                                            }else{
                                                $("#messageBar").slideUp('slow');
                                            }  
                                        }              
                                    });
                                    
                                    $("#date10").mask("99/99/9999");
                                    $("#date10").datepicker({
                                        dateFormat: "dd/mm/yy",
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "-100:+0",
                                        constrainInput: true,
                                        buttonImageOnly: true,
                                        buttonImage: "/images/calendar.gif"
                                    });
                                    
                                    $("#date10").bind("change", function(event){ 
                                        //TODO: Check
                                        var dated = $("#date10").val();
                                        if(dated.length !== 0){
                                            var todayFormatted = formatDate(new Date());
                                            if(parseInt(compare($("#date10").val(), todayFormatted)) === -1) {
                                                var message = "Date ARV Started cannot be later than today please correct!";
                                                $("#messageBar").html(message).slideDown('slow'); 
                                                $("#date10").val("");
                                            }else{
                                                $("#messageBar").slideUp('slow');
                                            }  
                                        }              
                                    });
                                    
                                     $("#willing").change(function(event) {
                                        if ($("#willing").val() === "Yes") {
                                            $("#outfac").attr("hidden", "hidden");
                                            $("#outfac1").attr("hidden", "hidden");
                                            $("#outfac2").attr("hidden", "hidden");
                                            $("#outfac3").attr("hidden", "hidden");
                                            $("#infac").removeAttr("hidden"); 
                                            $("#infac2").removeAttr("hidden"); 
                                            $("#messageBar").slideUp('slow');
                                            $("#motherName").val("");
                                            $("#patientId").val("");
//                                            createChild();
                                        }else{
                                            $("#outfac").removeAttr("hidden");
                                            $("#outfac1").removeAttr("hidden");
                                            $("#outfac2").removeAttr("hidden");
                                            $("#outfac3").removeAttr("hidden");
                                            $("#infac").attr("hidden", "hidden");
                                            $("#infac2").attr("hidden", "hidden");
                                            $("#messageBar").slideUp('slow');
                                            $("#motherName").val("");
                                            $("#patientId").val("");
                                        }
                                    });
                                    
                                    function createChildGrid() {
                                        //var date = $("#date1").val();
                                        //date = date.slice(3,5)+"/"+date.slice(0,2)+"/"+date.slice(6); 

                                        var postData = {deliveryId: 0};

                                        $("#childInfoGrid").jqGrid('GridUnload');
                                        $("#childInfoGrid").jqGrid({
                                            url: "Child_grid.action",
                                            datatype: "json",
                                            mtype: "GET",           
                                            colNames: ["Hospital No", "Surname", "Other Names", "Gender", "Weight (kg)", "Date of Birth", "Status At Registration", "", "", ""],
                                            colModel: [
                                                {name: "hospitalNumber", index: "hospitalNumber", width: "110", editable:true},
                                                {name: "surname", index: "surname", width: "100", editable:true},
                                                {name: "otherNames", index: "otherNames", width: "100", editable:true},
                                                {name: "gender", index: "gender", width: "110", editable:true, edittype:'select', editoptions:{value:"-- select --:-- select --;Male:Male;Female:Female"}}, 
                                                {name: "bodyWeight", index: "bodyWeight", width: "110", editable:true},
                                                {name: "dateBirth", index: "dateBirth", width: "120", editable:true, formatter: "date", formatoptions: {srcformat: "m/d/Y", newformat: "d/m/Y"}},
                                                {name: "registrationStatus", index: "registrationStatus", width: "170", editable:true, edittype:'select', editoptions:{value:"-- select --:-- select --;Transfer In:Transfer In;New:New"}},
                                                {name: "motherId", index: "motherId", width: "0", sortable:false, hidden:true},
                                                {name: "childId", index: "childId", width: "0", sortable:false, hidden:true},
                                                {name: "deliveryId", index: "deliveryId", width: "0", sortable:false, hidden:true}
                                            ],
                                            pager: $("#childInfoPager"),
                                            rowNum: 10,
                                            rowList: [10, 20],
                                            sortname: "childId",
                                            sortorder: "desc",
                                            viewrecords: true,
                                            imgpath: "themes/basic/images",
                                            resizable: false,
                                            height: 80,
                                            postData: postData,
                                            jsonReader: {
                                                root: "childList",
                                                page: "currpage",
                                                total: "totalpages",
                                                records: "totalrecords",
                                                repeatitems: false,
                                                id: "childId"
                                            },
//                                            gridComplete: function()
//                                            {
//                                                $('#childInfoGrid').jqGrid('setGridWidth', '1000'); // max width for grid
//                                            },
                                            onSelectRow: function(id){ 
                                                if(id === null)
                                                    id = 0;
                                                if (id && id !== lastSelected){
                                                    $("#childInfoGrid").saveRow(lastSelected, false, 'clientArray');
                                                    $("#childInfoGrid").editRow(id, true); 
                                                    lastSelected = id; 
                                                    console.log("Selected Id: ", id);

                                                    // add a datepicker to the textfields on the cells
                                                    addDatepicker("#" + id + "_dateBirth");						
                                                }
                                            } //end of onSelectRow 
                                        }); //end of jqGrid

                                        // construct the navigation          
                                        $("#childInfoGrid").navGrid('#childInfoPager',{
                                            edit:false,add:false,del:false,search:false 

                                            })
                                            .navButtonAdd('#childInfoPager',{
                                                caption:"Add Child", buttonimg:"", title:"Add New Child", onClickButton: function(){				
                                                    // retrieve last selected child's id
                                                    var selid = $("#childInfoGrid").getGridParam("selrow");
                                                    $("#childInfoGrid").saveRow(selid, false, 'clientArray'); 

                                                    var datarow = {hospitalNumber: "", surname: "", otherNames: "", gender: "", bodyWeight: "", dateBirth: "", registrationStatus: "", motherId: "", childId: "", deliveryId: ""};

                                                    var row_id = parseInt($("#deliveryId").attr("data-row-id")) - 1;
                                                    if(isNaN(row_id))
                                                        row_id = -1;
                                                    $("#deliveryId").attr("data-row-id", row_id);

                                                    var su = $("#childInfoGrid").addRowData("" + row_id + "",datarow,"first");
                                                    if(su) { 
                                                        $("#childInfoGrid").editRow("" + row_id + "", true);
                                                        // add a datepicker to the textfields on the cells
                                                        addDatepicker("#" + row_id + "_dateBirth");						
                                                    };
                                                    $("#childInfoGrid").setGridParam({selrow:"" + row_id + ""});  
                                                    
                                                    $("#" + row_id + "_hospitalNumber").bind("mouseleave", function (event){
                                                        var hospNumber = $("#" + row_id + "_hospitalNumber").val();
                                                        if(hospNumber !== "")
                                                            $("#" + row_id + "_hospitalNumber").val(zerorize(hospNumber));
                                                    });

                                                }, position:"last"
                                            })
                                            .navButtonAdd('#childInfoPager',{
                                                caption:"Remove Child", buttonimg:"", title:"Delete Child", onClickButton: function(id){
                                                // retrieve selected child's id
                                                var selid = $("#childInfoGrid").getGridParam("selrow");

                                                // a new entry
                                                if (parseInt(selid) < 0) {                                
                                                    $("#childInfoGrid").delRowData(selid);

                                                    // an existing child
                                                } 
                                                else {
                                                    $("#childInfoGrid").restoreRow(selid); // undo changes
                                                    var child = $("#childInfoGrid").getRowData(selid);
                                                    if (child !== null) {                    
                                                        $.ajax({
                                                            url: "Child_delete.action?childId=" + child.childId,
                                                            dataType: "json",                    
                                                            success: function(data) {
                                                                $("#childInfoGrid").delRowData(selid);	
                                                            }                    
                                                        }); //end of ajax call 	      
                                                    }
                                                } 
                                            }, position:"last" 
                                        });
                                    }
                                </script>
                        </div>
                        <!--End Create Child-->
                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>                       
                        <div id="buttons" style="width: 200px">
                            <button id="new_button" disabled="true">New</button> &nbsp;<button id="close_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
