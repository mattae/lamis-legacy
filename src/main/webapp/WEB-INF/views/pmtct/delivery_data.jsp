<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        <link type="text/css" rel="stylesheet" href="css/ui.jqgrid.css" />
        <link type="text/css" rel="stylesheet" href="themes/basic/grid.css" />
        <link type="text/css" rel="stylesheet" href="themes/jqModal.css" />
        
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>               
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script> 
        <script type="text/javascript" src="js/json.min.js"></script>   
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/delivery-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/grid.locale-en.js"></script>
        <script type="text/javascript" src="js/jquery.jqGrid.src.js"></script>
        <script type="text/javascript" src="js/jqDnR.js"></script>
        <script type="text/javascript" src="js/jqModal.js"></script> 
        <script type="text/javascript" src="js/grid.celledit.js"></script>		
        <script type="text/JavaScript">
	   var date = "";
           var updateRecord = false;
	   var lastSelected = -99;
            $(document).ready(function(){
                resetPage();
                initialize();
                reports();
		  
		$.ajax({
                    url: "Delivery_retrieve.action",
                    dataType: "json",                    
                    success: function(deliveryList) {
                        populateForm(deliveryList);
                    }                    
                }); //end of ajax call	
            });	
               
        </script>
    </head> 

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_pmtct.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/clinic.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> PMTCT >> Labour/Delivery</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>
                        
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="25%"><label>Hospital No:</label></td>
                                <td width="30%"><input name="hospitalNumMother" type="text" class="inputboxes" id="hospitalNumMother" readonly="readonly"/><span id="numHelp" style="color:red"></span></td>
                                <td width="45%" colspan="2"><span id="patientInfor"></span><input name="patientId" type="hidden" class="inputboxes" id="patientId"/><input name="deliveryId" type="hidden" id="deliveryId"/><input name="ancId" type="hidden" id="ancId"/><input name="partnerinformationId" type="hidden" id="partnerinformationId"/></td>
                            </tr>
				<tr>
                                <td><label>Booking Status:</label></td>
                                <td>
                                    <select name="bookingStatus" style="width: 130px;" class="inputboxes" id="bookingStatus">
                                        <option></option>
                                        <option value="1">Booked</option>
                                        <option value="0">Unbooked</option>
                                    </select>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="15%"><label>Date of Delivery:</label></td>
                                <td width="35%"><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateDelivery" type="hidden" class="inputboxes" id="dateDelivery"/></td>
				<td width="25%"><label>ROM to Delivery Interval:</label></td>
                                <td width="25%">
                                    <select name="romDeliveryInterval" style="width: 80px;" class="inputboxes" id="romDeliveryInterval">
                                        <option></option>
                                        <option>&lt;4hrs</option>
                                        <option>&gt;4hrs</option>
                                    </select>
                                </td>
                            </tr>						
                            <tr>
                                <td><label>Mode of Delivery:</label></td>
                                <td>
                                    <select name="modeDelivery" style="width: 130px;" class="inputboxes" id="modeDelivery">
                                        <option></option>
                                        <option>Vaginal</option>
                                        <option>Elective CS</option>
                                        <option>Emergency CS</option>
					<option>Other</option>
                                    </select>
                                </td>
                                <td><label>Episiotomy:</label></td>
                                <td>
                                    <select name="episiotomy" style="width: 80px;" class="inputboxes" id="episiotomy">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
                            </tr>
			    <tr>
                                <td><label>Vaginal Tear:</label></td>
                                <td>
                                    <select name="vaginalTear" style="width: 80px;" class="inputboxes" id="vaginalTear">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
				<td><label>Maternal Outcome:</label></td>
                                <td>
                                    <select name="maternalOutcome" style="width: 80px;" class="inputboxes" id="maternalOutcome">
                                        <option></option>
                                        <option>Alive</option>
                                        <option>Dead</option>
                                    </select>
                                </td>
			    </tr>
			    <tr>
                                <td><label>Time of HIV Diagnosis:</label></td>
                                <td>
                                    <select name="timeHivDiagnosis" style="width: 170px;" class="inputboxes" id="timeHivDiagnosis">
                                        <option></option>
                                        <option>Newly Tested HIV+ (L&amp;D)</option>
                                        <option>Previously known HIV+ (L&amp;D)</option>
                                    </select>
                                </td>
                                <td><label>Source of Referral:</label></td>
                                <td>
                                    <select name="sourceReferral" style="width: 180px;" class="inputboxes" id="sourceReferral">
                                        <option></option>
                                        <option>PMTCT outreach</option>
                                        <option>Sex worker outreach</option>
                                        <option>Medical outpatient</option>
                                        <option>Youth/Adolescent outreach</option>
                                        <option>Private/Commercial Health facility</option>
                                        <option>Under-fives/Immunization clinic</option>
                                        <option>External HCT centre</option>
                                        <option>CBO</option>
                                        <option>In-patients</option>
                                        <option>Self-referral</option>
                                    </select>
                                </td>
                            </tr>
                          <tr>
                            <td>
                                <label>Gestational Age at Delivery:</label>
                            </td>
                            <td>
                                <input name="gestationalAge" type="text" style="width: 25px;" class="inputboxes" id="gestationalAge"/>
                            </td>    
                            <td colspan="2">
                                <!--<input name="screenPostPartum" type="checkbox" value="1" id="screenPostPartum"/>&nbsp;<label>Tested for HIV Post Partum (&le;72hrs)-->
                            </td>
                           </tr>
                           <tr>
                               <td><label>Hepatitis B Status:</label></td>
                                <td>
                                    <select name="hepatitisBStatus" style="width: 180px;" class="inputboxes" id="hepatitisBStatus">
                                        <option></option>
                                        <option>Positive</option>
                                        <option>Negative</option>
                                    </select>
                                </td>
                                <td><label>Hepatitis C Status:</label></td>
                                <td>
                                    <select name="hepatitisCStatus" style="width: 180px;" class="inputboxes" id="hepatitisCStatus">
                                        <option></option>
                                        <option>Positive</option>
                                        <option>Negative</option>
                                    </select>
                                </td>
                           </tr>
<!--                           <tr>
				<td><label>ARV Regimen Past:</label></td>
                                <td>
                                    <select name="arvRegimenPast" style="width: 200px;" class="inputboxes" id="arvRegimenPast">
                                        <option></option>
                                        <option>ART</option>
                                        <option>Prophylaxis (Triple)</option>
                                        <option>Prophylaxis (Double)</option>
					<option>Prophylaxis (ZDV)</option>
					<option>Prophylaxis (sdNVP)</option>
                                    </select>
                                </td>
				<td><label>ARV Regimen Current:</label></td>								<td>
                                    <select name="arvRegimenCurrent" style="width: 200px;" class="inputboxes" id="arvRegimenCurrent">
                                        <option></option>
                                        <option>ART</option>
                                        <option>Prophylaxis (Triple)</option>
                                    </select>
                                </td>
				</tr>
				<tr>
                                    <td><label>Date ARV started:</label></td> -->
                                    <td><input name="date3" type="hidden" style="width: 100px;" class="inputboxes" id="date3"/></td>
                                    <!--<td></td>
                                    <td></td>
				</tr>
				<tr>
                                <td><label>Clinical Stage:</label></td>
                                <td>
                                    <select name="clinicStage" style="width: 100px;" class="inputboxes" id="clinicStage">
                                        <option></option>
                                        <option>Stage I</option>
                                        <option>Stage II</option>
                                        <option>Stage III</option>
                                        <option>Stage IV</option>
                                    </select>
                                </td>
                                <td><label>CD4 Ordered:</label></td>
                                <td>								    
                                    <select name="cd4Ordered" style="width: 50px;" class="inputboxes" id="cd4Ordered">
                                    <option></option>
                                    <option>Yes</option>
                                    <option>No</option>
                                    </select>
                                    <span style="margin-left:5px">
                                    <label>CD4 Count:</label>
                                    <input name="cd4" type="text" style="width: 50px;" class="inputboxes" id="cd4"/>
                                    </span>
				</td>
                            </tr>-->
		</table>
		<table width="100%" border="0">
                    <tr>
                        <td class="topheaders">Child's Information</td>
                    </tr>
                </table>
		<table width="99%" height="80" border="0" class="space">
                     <tr>
                        <td>
                            <table id="grid"></table>
                            <div id="pager" style="text-align:center;"></div>
                        </td>
                      </tr>                                
                 </table>
	         <table width="100%" border="0">
                   <tr>
                      <td class="topheaders">Partner Information</td>
                   </tr>
                </table>
		<table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="25%"><label>Agreed to Partner Notification:</label></td>
                                <td width="30%">
                                    <select name="partnerNotification" style="width: 80px;" class="inputboxes" id="partnerNotification">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
				<td><label>Partner's HIV Status:</label></td>
                                <td>
				<select name="partnerHivStatus" style="width: 130px;" class="inputboxes" id="partnerHivStatus">
                                        <option></option>
                                        <option>Positive</option>
                                        <option>Negative</option>
                                        <option>Unknown</option>
                                </select>
				</td>
                           </tr>
                           <tr>
                               <td><label>Partner referred to:</label></td>
                                <td>
                                    <input name="fp" type="checkbox" value="1" id="fp"/>&nbsp;<label>FP</label>&nbsp;&nbsp;
                                    <input name="art" type="checkbox" value="1" id="art"/>&nbsp;<label>ART</label>&nbsp;&nbsp;
                                    <input name="others" type="checkbox" value="1" id="others"/>&nbsp;<label>Others</label>
                                </td>
                                <td></td>
                                <td></td>
                           </tr>
                           <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>							
                        </table>
                        <hr></hr>
                        <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
