<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />

        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/maternalfollowup-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/JavaScript">
            var date = "";
            var updateRecord = false;
            $(document).ready(function(){
                resetPage();
                initialize();
                reports();
				
                $.ajax({
                    url: "Maternalfollowup_retrieve.action",
                    dataType: "json",                    
                    success: function(maternalfollowupList) {
                        populateForm(maternalfollowupList);
                    }                    
                }); //end of ajax call

            });	
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  

            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_pmtct.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/clinic.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong>Clinic >> PMTCT >> Follow-up Visit</strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders">Details</td>
                            </tr>
                        </table>
                        <div id="messageBar"></div>

                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="30%"><label>Hospital No:</label></td>
                                <td width="25%"><input name="hospitalNum" type="text" class="inputboxes" id="hospitalNum" readonly="readonly"/><span id="numHelp" style="color:red"></span></td>
                                <td width="45%" colspan="2"><span id="patientInfor"></span><input name="patientId" type="hidden" class="inputboxes" id="patientId"/><input name="maternalfollowupId" type="hidden" id="maternalfollowupId"/><input name="ancId" type="hidden" id="ancId"/><input name="partnerinformationId" type="hidden" id="partnerinformationId"/></td>
                            </tr>
                            <tr>
                                <td width="30%"><label>Date of Visit:</label></td>
                                <td width="25%"><input name="date1" type="text" style="width: 100px;" class="inputboxes" id="date1"/><input name="dateVisit" type="hidden" class="inputboxes" id="dateVisit"/></td>
                                <td width="25%"><label>Body Weight (kg):</label></td>
                                <td width="20%"><input name="bodyWeight" type="text" style="width: 50px;" class="inputboxes" id="bodyWeight"/></td>
                            </tr>
                            <tr>
                                <td><label>Blood Pressure (mmHg):</label></td>
                                <td><input name="bp1" type="text" style="width: 25px;" class="inputboxes" id="bp1"/> <input name="bp2" type="text" style="width: 25px;" class="inputboxes" id="bp2"/><input name="bp" type="hidden" style="width: 50px;" class="inputboxes" id="bp"/></td>
                                <td><label>Fundal Height (cm):</label></td>
                                <td><input name="fundalHeight" type="text" style="width: 50px;" class="inputboxes" id="fundalHeight"/></td>
                            </tr>
                            <tr>
                                <td><label>Fetal Presentation:</label></td>
                                <td>
                                    <select name="fetalPresentation" style="width: 130px;" class="inputboxes" id="fetalPresentation">
                                        <option></option>
                                        <option>Cephalic</option>
                                        <option>Breech</option>
                                        <option>Face</option>
                                        <option>Brow</option>
                                        <option>Footling</option>
                                    </select>
                                </td>
                                <td><label>Gestational Age (weeks):</label></td>
                                <td>
                                    <input name="gestationalAge" type="text" style="width: 25px;" class="inputboxes" id="gestationalAge" readOnly="readOnly"/>                          
                                </td>
                            </tr>
                            <tr>
                                <td><label>Type of visit:</label></td>
                                <td>
                                    <select name="typeOfVisit" style="width: 100px;" class="inputboxes" id="typeOfVisit">
                                        <option></option>
                                        <option>ANC</option>
                                        <option>PNC</option>
                                    </select>
                                </td>
                                 <td><label>Visit Status:</label></td>
                                <td>
                                    <select name="visitStatus" style="width: 200px;" class="inputboxes" id="visitStatus">
                                        <option></option>
                                        <option>Active in PMTCT cohort</option>
                                        <option>Transfer In</option>
                                        <option>Transfer Out</option>
                                        <option>Transfered to another PMTCT cohort (New Pregnancy)</option>
                                        <option>Transitioned to ART clinic</option>
                                        <option>Missed Appointment/Defaulted</option>
                                        <option>Lost to Follow Up</option>
                                        <option>Dead</option>
                                    </select>
                                </td>
                            </tr>   
                            <tr>
                                <td><label>Time of HIV Diagnosis:</label></td>
                                <td>
                                    <select name="timeHivDiagnosis" style="width: 170px;" class="inputboxes" id="timeHivDiagnosis">
                                        <option></option>
                                        <option>Newly Tested HIV+ (PP &lt;=72hrs)</option>
                                        <option>Previously known HIV+ (PP &lt;=72hrs)</option>
                                    </select>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label>Viral Load sample collected:</label></td>
                                <td>
                                    <select name="viralLoadCollected" style="width: 200px;" class="inputboxes" id="viralLoadCollected">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
                                <td width="30%"><label>Date sample collected:</label></td>
                                <td width="25%"><input name="date2" type="text" style="width: 100px;" class="inputboxes" id="date2"/><input name="dateSampleCollected" type="hidden" class="inputboxes" id="dateSampleCollected"/></td>                               
                            </tr> 
                            <td><input name="date3" type="hidden" style="width: 100px;" class="inputboxes" id="date3"/></td>
                            <tr>
                                <td><label>TB Status</label></td>
                                <td>
                                    <select name="tbStatus" style="width: 200px;" class="inputboxes" id="tbStatus">
                                        <option></option>
                                        <option>No sign or symptoms of TB</option>
                                        <option>TB suspected and referred for evaluation</option>
                                        <option>Currently on INH prophylaxis</option>
                                        <option>Currently on TB treatment</option>
                                        <option>TB positive not on TB drugs</option>
                                    </select>
                                </td>
                                <td><label>Date of Next Appointment:</label></td>
                                <td><input name="date4" type="text" style="width: 100px;" class="inputboxes" id="date4"/><input name="dateNextVisit" type="hidden" class="inputboxes" id="dateNextVisit"/></td>                                
                            </tr>
                        </table>
                        
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Counseling / Others</td>
                            </tr>
                        </table>
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="25%">&nbsp;</td>
                                <td width="28%"><input name="counselNutrition" type="checkbox" value="1" id="counselNutrition"/>&nbsp;<label>Nutritional Support</label></td>
                                <td width="20%"><input name="counselFeeding" type="checkbox" value="1" id="counselFeeding"/>&nbsp;<label>Infant Feeding</label></td>
                                <td width="27%"><input name="counselFamilyPlanning" type="checkbox" value="1" id="counselFamilyPlanning"/>&nbsp;<label>Family Planning</label></td>
                            </tr>
                            <tr>
                                <td><label>Family Planning Method Used:</label></td>
                                <td>
                                    <select name="familyPlanningMethod" style="width: 130px;" class="inputboxes" id="familyPlanningMethod">
                                        <option></option>
                                        <option>1 - No FP Method</option>
                                        <option>2 - Condom</option>
                                        <option>3 - Oral Pills</option>
                                        <option>4 - Injectable</option>
                                        <option>5 - Implant</option>
                                        <option>6 - IUD</option>
                                        <option>7 - Other</option>
                                    </select>
                                </td>
                                <td><label>Referred to:</label></td>
                                <td>
                                    <select name="referred" style="width: 130px;" class="inputboxes" id="referred">
                                        <option></option>
                                        <option>ART</option>
                                        <option>Pap Smear</option>
                                        <option>Other</option>
                                    </select>
                                </td>
                            </tr>
<!--                            <tr>                                                             
                                <td></td>
                                <td></td>
                            </tr>-->
                        </table>
                        <table width="100%" border="0">
                            <tr>
                                <td class="topheaders">Partner Information</td>
                            </tr>
                        </table>
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td width="25%"><label>Agreed to Partner Notification:</label></td>
                                <td width="28%">
                                    <select name="partnerNotification" style="width: 80px;" class="inputboxes" id="partnerNotification">
                                        <option></option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                </td>
                                <td><label>Partner's HIV Status:</label></td>
                                <td>
                                    <select name="partnerHivStatus" style="width: 130px;" class="inputboxes" id="partnerHivStatus">
                                        <option></option>
                                        <option>Positive</option>
                                        <option>Negative</option>
                                        <option>Unknown</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Partner referred to:</label></td>
                                <td>
                                    <input name="fp" type="checkbox" value="1" id="fp"/>&nbsp;<label>FP</label>&nbsp;&nbsp;
                                    <input name="art" type="checkbox" value="1" id="art"/>&nbsp;<label>ART</label>&nbsp;&nbsp;
                                    <input name="others" type="checkbox" value="1" id="others"/>&nbsp;<label>Others</label>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <!--<tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>-->							
                        </table>
                        <hr></hr>
                        <p></p>

                        <div id="userGroup" style="display: none"><s:property value="#session.userGroup"/></div>
                        <div id="buttons" style="width: 300px">
                            <button id="save_button">Save</button> &nbsp;<button id="delete_button" disabled="true"/>Delete</button> &nbsp;<button id="close_button"/>Close</button>
                        </div>                         
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
