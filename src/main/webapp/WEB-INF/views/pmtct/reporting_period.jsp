<%-- 
    Document   : Facility
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/JavaScript">
            var updateRecord = false;
            $(document).ready(function(){
                resetPage();
                reports();
                                
                if($("#formId").html() == 1) {
                    $("#title").html("Facility Care &amp; Support Monthly Summary");
                    $("#crumb").html("Clinic >> Ante-Natal Clinic >> Facility Care &amp; Support Monthly Summary");
                }
                else {
                }
                $("#ok_button").bind("click", function(event){
                    if(validateForm()) {
                        event.preventDefault();
                        event.stopPropagation();
                        if($("#formId").html() == 1) {
                            url = "Nigeriaqual_indicators.action?reportingMonthBegin="+$("#reportingMonthBegin").val()+"&reportingYearBegin="+$("#reportingYearBegin").val()+"&reportingMonthEnd="+$("#reportingMonthEnd").val()+"&reportingYearEnd="+$("#reportingYearEnd").val();
                        }
                        else {
                        }
                        window.open(url);
                    }
                    return false;
                });              
                $("#cancel_button").bind("click", function(event){
                    $("#lamisform").attr("action", "Pmtct_page");
                    return true;
                });
            }); 

            function validateForm() {
                var validate = true;

                // check for valid input is entered
                if($("#reportingMonthBegin").val().length == 0 || $("#reportingYearBegin").val().length == 0){
                    $("#periodHelp1").html(" *");
                    validate = false;
                }
                else {
                    $("#periodHelp1").html("");                    
                }
                if($("#reportingMonthEnd").val().length == 0 || $("#reportingYearEnd").val().length == 0){
                    $("#periodHelp2").html(" *");
                    validate = false;
                }
                else {
                    $("#periodHelp2").html("");                    
                }
                return validate;
            }                                         
        </script>
    </head>

    <body>
        <div id="page">
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
                      
            <div id="mainPanel">
                <jsp:include page="/WEB-INF/views/template/nav_pmtct.jsp" />  

                <div id="rightPanel">
                    <s:form id="lamisform">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <span>
                                        <img src="images/report.png" style="margin-bottom: -5px;" /> &nbsp;
                                        <span class="top" style="margin-bottom: 1px; font-family: sans-serif; font-size: 1.1em;"><strong id="crumb"></strong></span>
                                    </span>
                                    <hr style="line-height: 2px"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="topheaders" id ="title"></td>
                            </tr>
                        </table>
                        <p></p>
                        <table width="99%" border="0" class="space" cellpadding="3">
                            <tr>
                                <td></td>
                                <td>From:&nbsp; 
                                    <select name="reportingMonthBegin" style="width: 100px;" class="inputboxes" id="reportingMonthBegin"/>
                                       <option></option>
                                       <option>January</option>
                                       <option>February</option>
                                       <option>March</option>
                                       <option>April</option>
                                       <option>May</option>
                                       <option>June</option>
                                       <option>July</option>
                                       <option>August</option>
                                       <option>September</option>
                                       <option>October</option>
                                       <option>November</option>
                                       <option>December</option>
                                    </select>
                                    <select name="reportingYearBegin" style="width: 75px;" class="inputboxes" id="reportingYearBegin"/>
                                       <option></option>
                                    </select><span id="periodHelp1" style="color:red"></span>
                                    &nbsp; to:&nbsp; 
                                    <select name="reportingMonthEnd" style="width: 100px;" class="inputboxes" id="reportingMonthEnd"/>
                                       <option></option>
                                       <option>January</option>
                                       <option>February</option>
                                       <option>March</option>
                                       <option>April</option>
                                       <option>May</option>
                                       <option>June</option>
                                       <option>July</option>
                                       <option>August</option>
                                       <option>September</option>
                                       <option>October</option>
                                       <option>November</option>
                                       <option>December</option>
                                    </select>
                                    <select name="reportingYearEnd" style="width: 75px;" class="inputboxes" id="reportingYearEnd"/>
                                       <option></option>
                                    </select><span id="periodHelp2" style="color:red"></span>
                                </td>
                            </tr>
                        </table>
                        <p></p>
                                        
                        <div id="formId" style="display: none"><s:property value="#session.formId"/></div>
                        <div id="buttons" style="width: 200px">
                            <button id="ok_button">Ok</button> &nbsp;<button id="cancel_button">Close</button>
                        </div>                                               
                    </s:form>
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
</html>
