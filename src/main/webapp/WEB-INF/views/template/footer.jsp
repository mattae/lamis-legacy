<%-- 
    Document   : footer
    Created on : Mar 29, 2014, 7:03:06 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>

<div id="foot">
   
    <div id="footerMessage"><s:property value="#session.facilityName"/> Database</div> 
    
    <div id="footerMessage2">Version 2.8.2.v1</div>
    
</div>