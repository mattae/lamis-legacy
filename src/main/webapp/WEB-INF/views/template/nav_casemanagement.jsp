<%-- 
    Document   : nav_casemanagement
    Created on : Oct 20, 2017, 3:32:01 PM
    Author     : user10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">D a t a &nbsp; E n t r y</div>
            <div style="margin-bottom: 20px;">
               <s:a action="Assign_client">Assign Case Managers</s:a>
               <s:a action="Reassign_client">Re-Assign Case Managers</s:a>
               <%--<s:a action="viral_load_monitor">Viral Load Monitoring</s:a>--%>
            </div>
            <div class="navTitle">R e p o r t s</div>
            <div>
                <s:a action="Client_list">Case Manager Clients List</s:a>
                <s:a id="Unassigned_list">List of Clients not assigned to a Case Manager</s:a>
                <s:a action="Client_appointment">Case Manager Client Appointment List</s:a>
                <s:a action="Client_defaulter">Case Manager Client Defaulter List</s:a>
                <s:a action="Client_cd4">Case Manager Clients due for CD4 Count Test</s:a>
                <s:a action="Client_viral_load">Case Manager Clients due for Viral Load Test</s:a>  
            </div>
        </div>
    </div>
</div>
