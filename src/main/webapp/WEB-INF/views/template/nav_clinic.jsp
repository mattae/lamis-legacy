<%-- 
    Document   : nav_clinic
    Created on : Mar 29, 2014, 7:11:41 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">D a t a &nbsp; E n t r y</div>
            <div style="margin-bottom: 20px;">
               <s:a action="Clinic_search">Clinic Visit</s:a>                                
               <s:a action="Commence_search">ART Commencement</s:a> 
               <s:a action="Chroniccare_search">Care &amp; Support Assessment</s:a> 
	       <s:a action="Status_search">Client Status Update</s:a>													 
                <s:a action="Eac_search">Unsuppressed Client Monitoring/ Enhanced Adherence Counseling</s:a>
               <%--<s:a action="viral_load_monitor">Viral Load Monitoring</s:a>--%>
            </div>
            <div class="navTitle">R e p o r t s</div>
            <div>
                <%--<s:a id="eligibleart">Patients Eligible for ARV Initiation</s:a>--%>
                <s:a id="cd4due">Patients due for CD4 Count Test</s:a>
                <s:a id="cd4baseline">Patients with current CD4 Count &le; baseline value</s:a>
                <s:a action="Viral_load">Patients due for Viral Load Test</s:a>
                <s:a id="viralloadsupressed">Patients with current Viral Load &lt; 1000 copies/ml</s:a>
                <s:a id="viralloadunsupressed">Patients with current Viral Load &ge; 1000 copies/ml</s:a>                
		<s:a action="Reporting_period_care.action?formId=1">Care &amp; Support Monthly Summary</s:a>
                <%--<s:a action="Adr_search">Patients Reporting Adverse Drug Reactions (ADRs)</s:a>
                <s:a id="coinfected">TB-HIV co-infected Patients</s:a>--%>
            </div>
        </div>
    </div>
</div>
