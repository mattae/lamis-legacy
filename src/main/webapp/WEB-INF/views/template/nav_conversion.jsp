<%-- 
    Document   : nav_conversion
    Created on : Sep 17, 2017, 4:55:51 PM
    Author     : user10
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">C o n v e r s i o n</div>
            <div>
                 <s:a action="Converter_page">Convert Data to Excel</s:a> 
                 <s:a action="Converter_page_radet">Generate RADET File </s:a> 
                 <s:a action="Converter_page_retention">Generate Cohort Analysis File </s:a> 
                 <s:a action="Converter_page_nigqual">Generate NIGQUAL Files</s:a>
                 <s:a action="Converter_page_ndr">Generate NDR Files</s:a>
                 <%--<s:a action="Barcode_page">Generate QR Code</s:a>
                 s:a action="Converter_page_dhis">Convert Data to DHIS format</s:a>--%>
            </div>
        </div>
    </div>
</div>
