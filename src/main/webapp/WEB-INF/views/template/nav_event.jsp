<%-- 
    Document   : nav_chart
    Created on : Mar 29, 2014, 7:41:49 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">E v e n t &nbsp; M o n i t o r</div>
            <div style="margin-bottom: 20px;">
                <s:a action="Event_pharmacy_page">Community Pharmacy Events</s:a>
                <s:a action="Analyzer_page">Data Synchronization Events</s:a>  
                <s:a action="Validate_page">Data Profiling and Validation</s:a>
                <s:a action="Retention_tracker_page">Retention Tracker Report</s:a>
                <%--<s:a id="treatment_tracker">Treatment Tracker Report</s:a>--%>
                <s:a action="Treatment_tracker_page">Treatment Tracker Report</s:a>
                <s:a action="biometric_report_page">Biometric Report</s:a>
                <s:a action="facility_sync_report">Facility Sync Report</s:a>
            </div>
        </div>
    </div>
</div>
