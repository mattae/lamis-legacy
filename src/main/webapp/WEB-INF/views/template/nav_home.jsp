<%-- 
    Document   : newjsp
    Created on : Mar 29, 2014, 4:23:30 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">D a t a &nbsp; E n t r y</div>
            <div style="margin-bottom: 20px;">
                <s:a action="Patient_search">Patient Registration</s:a> 
                <s:a action="Status_search">Client Status Update</s:a>
                <%-- <s:a action="Client_tracker">Client Tracking</s:a> --%>
                <s:a action="Appointment_search">Appointment Scheduling</s:a>
            </div>
            <div class="navTitle">R e p o r t s</div>
            <div>
                <s:a action="Patient_query_criteria.action">Patient Information Query</s:a>
                <s:a id="currentcare">Currently on Care (ART &amp; Pre-ART)</s:a>
                <s:a id="currenttreatment">Currently on Treatment (ART)</s:a>
                <s:a id="lostUnconfirmedPepfar">Lost to Follow Up Unconfirmed (PEPFAR)</s:a>
                <s:a id="lostUnconfirmedGon">Lost to Follow Up Unconfirmed (National)</s:a>
                <s:a action="Appointment_option">Clinic &amp; Refill Appointments/Visits</s:a>
                <s:a id="defaulterRefill">Defaulters for ARV Refill Appointment</s:a>
                <s:a action="Txml_query.action">Tracking Outcome (TX-ML)</s:a>
                <s:a action="Reporting_month_patient.action?formId=1">ART Monthly Summary</s:a>
                <%-- <s:a action="Reporting_period_patient.action?formId=1">Service Quality Indicators (National)</s:a>
                <s:a action="Reporting_period_patient.action?formId=2">Service Quality Indicators (PEPFAR)</s:a>
                <s:a action="Reporting_period_patient.action?formId=3">Cohort Analysis</s:a>
                <s:a action="Reporting_month_patient.action?formId=3">Performance Indicators</s:a> --%>
            </div>
        </div>
    </div>
</div>
