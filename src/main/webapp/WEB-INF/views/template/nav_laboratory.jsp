<%-- 
    Document   : nav_laboratory
    Created on : Mar 29, 2014, 7:39:27 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">D a t a &nbsp; E n t r y</div>
            <div style="margin-bottom: 20px;">
                <s:a action="Laboratory_search">Laboratory Test</s:a> 
                <s:a action="Labtest_prescription_search">Laboratory Test Orders</s:a>																					  
            </div>
            <div class="navTitle">R e p o r t s</div>
            <div>
                <s:a action="Lab_query_option.action?formId=1">Laboratory Result Query</s:a>
                <s:a id="cd4due">Patients due for CD4 Count Test</s:a>
                <s:a id="cd4baseline">Patients with current CD4 Count &le; baseline value</s:a>
                <s:a action="Viral_load">Patients due for Viral Load Test</s:a>
                <s:a id="viralloadsupressed">Patients with current Viral Load &lt; 1000 copies/ml</s:a>
                <s:a id="viralloadunsupressed">Patients with current Viral Load &ge; 1000 copies/ml</s:a>
                <s:a action="Reporting_month_lab.action?formId=1">LAB Monthly Summary</s:a>
            </div>
        </div>
    </div>
</div>
