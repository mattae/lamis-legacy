<%-- 
    Document   : nav_maintenance
    Created on : Mar 29, 2014, 7:40:19 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">M a i n t e n a n c e</div>
            <div>
                 <s:a action="Export_page">Export Data</s:a> 
                 <s:a action="Import_page">Import Data</s:a> 
                 <s:a action="Upload_page">Upload Data to the Server</s:a> 
                 <s:a action="Sync_page">Download &amp; Synchronize Data</s:a> 
                 <s:a action="Updates_page">Check &amp; Download Updates</s:a> 
                 <s:a action="Radet_page">RADET Analyzer &amp; Data Update</s:a> 
                 <s:a action="Dqa_page">Data Quality Analyzer</s:a> 
                 <s:a action="Cleanup_page">Cleanup Database Records</s:a> 
                 <s:a action="Deduplicator_page">Remove Duplicate Numbers</s:a> 
                 <s:a action="Facility_switch">Switch Facility</s:a> 
                 <%-- <s:a action="Conversation_page">Conversation (SMS)</s:a>
                 <s:a action="Analyzer_page">Data Sync Analyzer</s:a> --%> 
                 <s:a action="Misc_service">Perform some task</s:a>  
            </div>
        </div>
    </div>
</div>
