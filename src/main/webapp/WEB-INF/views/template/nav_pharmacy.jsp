<%-- 
    Document   : nav_pharmacy
    Created on : Mar 29, 2014, 7:38:42 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">D a t a &nbsp; E n t r y</div>
            <div style="margin-bottom: 20px;">
                <s:a action="Pharmacy_search">Drug Dispensing</s:a> 
                <s:a action="Devolve_search">Differentiated Care Service</s:a> 
                <s:a action="Prescription_search">Drug Prescriptions</s:a> 																		   
            </div>
            <div class="navTitle">R e p o r t s</div>
            <div>
                <s:a id="firstline">Patients on First line Regimen</s:a>
                <s:a id="secondline">Patients on Second line Regimen</s:a>
                <s:a id="thirdline">Patients on Third Line Regimen (Salvage)</s:a>
                <s:a id="regimensummary">Patient Per Regimen Report</s:a>
                <s:a action="Reporting_month_pharmacy.action?formId=1">Summary of Drugs Dispensed</s:a>
                <s:a id="devolvedsummary">List of Patients Receiving DMOC Services</s:a>
                <%--<s:a action="Reporting_month_community.action?formId=1">Community Pharmacy Monthly Summary</s:a>--%>
            </div>
        </div>
    </div>
</div>
