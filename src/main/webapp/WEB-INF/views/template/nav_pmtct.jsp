<%-- 
    Document   : nav_clinic
    Created on : Mar 29, 2014, 7:11:41 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">D a t a &nbsp; E n t r y</div>
            <div style="margin-bottom: 20px;">
               <s:a action="Anc_search">First ANC Visit</s:a>                                
               <s:a action="Delivery_search">Labour/Delivery</s:a>
               <s:a action="Maternalfollowup_search">Maternal Follow-Up Visit</s:a>
               <s:a action="Childfollowup_search">Child Follow-Up Visit</s:a>
            </div>
            <div class="navTitle">R e p o r t s</div>
            <div>
                <s:a action="Reporting_month_pmtct.action?formId=1">PMTCT Monthly Summary</s:a>
                <s:a action="Reporting_month_pmtct.action?formId=2">PMTCT Addendum Monthly Summary</s:a>
                <%-- <s:a action="Reporting_period_pmtct.action?formId=1">NIGERIAQUAL Indicators</s:a> --%>                
            </div>
        </div>
    </div>
</div>
