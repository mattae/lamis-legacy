<%-- 
    Document   : nav_setup
    Created on : Mar 29, 2014, 7:40:52 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">S e t u p</div>
            <div style="margin-bottom: 20px;">
                <s:a action="User_new">User Setup</s:a>
                <s:a action="Facility_new">Facility Setup</s:a> 
                <s:a action="Communitypharm_new">Community Pharmacy Setup</s:a>
                <s:a action="Casemanager_new">Case Manager Setup</s:a>
                <s:a action="Drug_new">Drug Setup</s:a> 
                <s:a action="Message_setup">Message/Modem Settings</s:a> 
                
            </div>
        </div>
    </div>
</div>
