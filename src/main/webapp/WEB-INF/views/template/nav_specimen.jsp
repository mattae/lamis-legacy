<%-- 
    Document   : nav_laboratory
    Created on : Mar 29, 2014, 7:39:27 PM
    Author     : aalozie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<div id="leftPanel">
    <div id="navigation">
        <div style="float: left" class="navMenu">
            <div class="navTitle">D a t a &nbsp; E n t r y</div>
            <div style="margin-bottom: 20px;">
                <s:a action="Specimen_search">Sample Registration</s:a> 
                <s:a action="Process_result">Result Processing</s:a> 
                <s:a action="Dispatch_page">Dispatch Result</s:a> 
                <s:a action="Specimen_storage">Sample Storage</s:a> 
            </div>
            <div class="navTitle">R e p o r t s</div>
            <div>
                <s:a action="Reporting_month_eid.action?formId=1">EID Laboratory Register</s:a>
                <s:a action="Reporting_month_eid.action?formId=2">EID Monthly Summary</s:a>
                <s:a action="#">EID HIV PCR Charts</s:a>
            </div>
        </div>
    </div>
</div>
