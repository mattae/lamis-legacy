<%-- 
    Document   : Patient
    Created on : Feb 8, 2012, 1:15:46 PM
    Author     : AALOZIE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>LAMIS 2.6</title>
        <link type="image/png" rel="icon" href="images/favicon.png" />
        <link type="text/css" rel="stylesheet" href="css/lamis.css" />
        <link type="text/css" rel="stylesheet" href="css/jquery-ui-1.8.18.custom.css" />
        
        <script type="text/javascript" src="js/lamis/lamis-common.js"></script>               
        <script type="text/javascript" src="js/lamis/report-common.js"></script>               
        <script type="text/javascript" src="js/lamis/dashboard-common.js"></script>               
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>       
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>       
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>       
        <script type="text/javascript" src="js/json.min.js"></script>
        <script type="text/javascript" src="js/highcharts.js"></script>               
        <script type="text/javascript" src="js/modules/exporting.js"></script>               

        <script type="text/JavaScript">

            for(i = new Date().getFullYear(); i > 1900; i--) {
            $("#reportingYearBegin").append($("<option/>").val(i).html(i));
            $("#reportingYearEnd").append($("<option/>").val(i).html(i));
             }
            $(function() {
            $('.carousel').carousel({
            interval: 5000
            });
            (function (H) {
            H.wrap(H.Chart.prototype, 'showResetZoom', function (proceed) {
            });
            }(Highcharts));

            Highcharts.theme = {

            colors: ['#FF6600', '#2a5788', '#FFCC00', '#DDDF00', '#24CBE5', '#64E572', 
            '#FF9655', '#FFF263', '#6AF9C4'],
            chart: {
            backgroundColor: {
            linearGradient: [0, 0, 500, 500],
            stops: [
            [0, 'rgb(255, 255, 255)'],
            [1, 'rgb(240, 240, 255)']
            ]
            },
            },
            title: {
            style: {
            color: '#000',
            font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
            }
            },
            subtitle: {
            style: {
            color: '#666666',
            font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
            }
            },

            legend: {
            itemStyle: {
            font: '9pt Trebuchet MS, Verdana, sans-serif',
            color: 'black'
            },
            itemHoverStyle:{
            color: 'gray'
            }   
            }
            };

            // Apply the theme
            Highcharts.setOptions(Highcharts.theme);

            });

            $(document).ready(function(){            
            generate()        

            $.ajax({
                    url: "StateId_retrieve.action",
                    dataType: "json",
                    success: function(stateMap) {
                        var options = "<option value = '0'>" + 'Select' + "</option>";
                        $.each(stateMap, function(key, value) {
                        options += "<option value = '" + key + "'>" + value + "</option>";
                        }); //end each
                        $("#stateId").html(options);                        
                   }                    
                }); //end of ajax call

            $("#stateId").change(function(event){
                $.ajax({
                    url: "LgaId_retrieve.action",
                    dataType: "json",
                    data: {stateId: $("#stateId").val()},
                    success: function(lgaMap) {
                        var options = "<option value = '0'>" + 'Select' + "</option>";
                        $.each(lgaMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }); //end each
                        $("#lgaId").html(options);                        
                    }                    
                }); //end of ajax call
            }); 

            $("#lgaId").change(function(event){
                $.ajax({
                    url: "Facility_retrieve.action",
                    dataType: "json",
                    data: {stateId: $("#stateId").val(), lgaId: $("#lgaId").val()},

                    success: function(facilityMap) {
                        var options = "<option value = '0'>" + '' + "</option>";
                        $.each(facilityMap, function(key, value) {
                            options += "<option value = '" + key + "'>" + value + "</option>";
                        }) //end each
                        $("#facilityId").html(options);                        
                    }                    
                }); //end of ajax call
            }); 

            $("#ok_button").bind("click", function() {
               generate();
               return false;
            });

        }); 

        </script> 

    </head>

    <body>
        <div id="page">           
            <jsp:include page="/WEB-INF/views/template/menu.jsp" />  
            
            <div id="mainPanel">                
                <jsp:include page="/WEB-INF/views/template/nav_visualizer.jsp" />                 
                <div id="rightPanelScroll">
                    <div id="loader"></div>
                    <div id="chart1"> 
                        <div id="container1"></div>
                    </div>
                    <div id="chart2"> 
                        <div id="container2"></div>
                    </div>
                    <div id="chart3"> 
                        <div id="container3"></div>
                    </div>
                    <div id="chart4"> 
                        <div id="container4"></div>
                    </div>                    
                    <div id="chart5"> 
                        <div id="container5"></div>
                    </div>                    
                    <div id="chart6"> 
                        <div id="container6"></div>
                    </div>                    
                    <div id="chart7"> 
                        <div id="container7"></div>
                    </div>                    
                    <div id="chart8"> 
                        <div id="container8"></div>
                    </div>                    
                    <div id="chart8"> 
                        <div id="container9"></div>
                    </div>                    
                    <div id="chart8"> 
                        <div id="container10"></div>
                    </div>                    
                    <div id="chart8"> 
                        <div id="container11"></div>
                    </div>                    
                    <div id="chart8"> 
                        <div id="container12"></div>
                    </div>                    
                    <div id="chart8"> 
                        <div id="container13"></div>
                    </div>
                     <div id="chart8"> 
                        <div id="container14"></div>
                    </div>                                     
                </div>
            </div>
        </div>
        <div id="footer">
            <jsp:include page="/WEB-INF/views/template/footer.jsp" />
        </div>
    </body>
    
    
</html>

