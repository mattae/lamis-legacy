(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(global = global || self, factory((global.fingerprint = global.fingerprint || {}, global.fingerprint.js = {})));
}(this, function (exports) { 'use strict';

	var FingerprintService = /** @class */ (function () {
	    function FingerprintService() {
	        this.url = "http://127.0.0.1:8084/LAMIS2/resources/api/fingerprint";
	    }
	    FingerprintService.prototype.getReaders = function () {
	        var readers = [];
	        return $.ajax({
	            url: this.url + "/readers",
				crossDomain: true,
				"headers": {
					"accept": "application/json",
					"Access-Control-Allow-Origin":"*"
				}
	        }).then(function (res) {
	            res.forEach(function (data) { return readers.push({ id: data.id, name: data.name }); });
	            return readers;
	        });
	    };
	    FingerprintService.prototype.enrolled = function (id) {
	        return $.ajax({
	            url: encodeURI(this.url + "/enrolled-fingers/" + id)
	        });
	    };
	    FingerprintService.prototype.identify = function (reader) {
	        return $.ajax({
	            url: encodeURI(this.url + "/identify/" + encodeURIComponent(reader)),
				crossDomain: true,
	        });
	    };
	    FingerprintService.prototype.verify = function (reader, id) {
	        return $.ajax({
	            url: encodeURI(this.url + "/verify/" + encodeURIComponent(reader) + "/" + id)
	        });
	    };
	    FingerprintService.prototype.enrol = function (reader, id, finger) {
	        return $.ajax({
	            url: encodeURI(this.url + "/enrol/" + encodeURIComponent(reader) + "/" + id + "/" + finger)
	        });
	    };
	    return FingerprintService;
	}());

	var FingerprintComponent = /** @class */ (function () {
	    function FingerprintComponent() {
	        this.service = new FingerprintService();
	        this.fingers = [];
	        this.body = $('body');
	    }
	    FingerprintComponent.prototype.enrolledFingers = function (id) {
	        return this.service.enrolled(id);
	    };
	    FingerprintComponent.prototype.enrol = function (id, fnc) {
	        var _this = this;
	        this.buildDialog();
	        $('.ZebraDialog_Title').text('Enrol Patient');
	        var form = $('.fingers').append("\n\t  <td colspan=\"3\">\n\t  \t<p style=\"font-weight: bold; font-size: .8em\">The following fingers have been enrolled for patient:</p>\n\t    <ul id=\"fingerList\" style=\" font-size: .7em\">\n\t   ");
	        this.updateEnrolledFingers(id);
	        form.append("\n\t    </ul>\n\t  </td>\n\t  ");
	        $('.fingers1').append("\n\t    <td>\n\t  \t\t<label for=\"fingers\" style=\"font-weight: bold\">Finger:</label>\n\t  \t</td>\n\t  \t<td colspan=\"2\">\n\t  \t\t<select id=\"fingers\" class=\"form-control form-control-sm\" style=\"width: 100%\">\n\t\t\t\t<option selected value=\"\">Choose..</option>\n\t\t\t\t<option value=\"LEFT_THUMB\">Left Thumb</option>\n\t\t\t\t<option value=\"LEFT_INDEX_FINGER\">Left Index</option>\n\t\t\t\t<option value=\"RIGHT_THUMB\">Right Thumb</option>\n\t\t\t\t<option value=\"RIGHT_INDEX_FINGER\">Right Index</option>\n\t  \t\t</select>\n\t  \t</td>\n\t\t");
	        $('#fingers, #readers').change(function (evt) {
	            var value = $('#fingers').val();
	            var readers = $('#readers').val();
	            if (!!value && !!readers) {
	                $('.ZebraDialog_Button_1').removeAttr('disabled');
	            }
	            else {
	                $('.ZebraDialog_Button_1').attr('disabled', 'disabled');
	            }
	        });
	        $('.ZebraDialog_Button_1').bind('click', function (evt) {
	            $('#message').text('Please put a finger on the scanner...');
	            var reader = $('#readers').val().toString();
	            var finger = $('#fingers').val().toString();
	            _this.service.enrol(reader, id, finger).then(function (res) {
	                if (!!res && !!res.name) {
	                    finger = _this.fingerValueToName(finger);
	                    $('#message').text("Finger " + finger + " for patient " + res.name + " enrolled");
	                    _this.updateEnrolledFingers(id);
	                    fnc(res);
	                }
	                else {
	                    $('#message').text('Could not enrol finger');
	                    fnc(false);
	                }
	            });
	        }).show();
	        this.disableButton('.ZebraDialog_Button_1');
	    };
	    FingerprintComponent.prototype.verify = function (patientId, fnc) {
	        var _this = this;
	        this.buildDialog();
	        $('.ZebraDialog_Title').text('Verify Patient');
	        $('.ZebraDialog_Button_2').bind('click', function (evt) {
	            $('#message').text('Please put a finger on the scanner...');
	            var reader = $('#readers').val().toString();
	            return _this.service.verify(reader, patientId).then(function (res) {
	                if (!!res && !!res.name) {
	                    $('#message').text("Patient, " + res.name + " was verified");
	                    fnc(true);
	                }
	                else {
	                    $('#message').text('Could not verify patient');
	                    fnc(false);
	                }
	            });
	        }).show();
	    };
	    FingerprintComponent.prototype.identify = function (fnc) {
	        var _this = this;
	        this.buildDialog();
	        $('.ZebraDialog_Title').text('Identify Patient');
	        $('.ZebraDialog_Button_0').bind('click', function (evt) {
	            $('#message').text('Please put a finger on the scanner...');
	            var reader = $('#readers').val().toString();
	            _this.service.identify(reader).then(function (res) {
	                if (!!res && !!res.name) {
	                    $('#message').text("Patient, " + res.name + " was identified");
	                    fnc(res);
	                }
	                else {
	                    $('#message').text('Could not identify patient');
	                    fnc(false);
	                }
	            });
	        }).show();
	    };
	    FingerprintComponent.prototype.updateEnrolledFingers = function (id) {
	        var _this = this;
	        var fingers = $('#fingerList');
	        $('#fingerList li').remove();
	        this.enrolledFingers(id).then(function (res) {
	            res.forEach(function (finger) {
	                if (!_this.fingers.includes(finger)) {
	                    _this.fingers.push(finger);
	                }
	            });
	            _this.fingers.forEach(function (f) {
	                fingers.append("<li>" + f + "</li>");
	            });
	        });
	    };
	    FingerprintComponent.prototype.updateScanners = function () {
	        this.service.getReaders().then(function (res) {
	            res.forEach(function (reader) {
	                $('#readers').append($("<option></option>").attr("value", reader.name).text(reader.name));
	            });
	        });
	    };
	    FingerprintComponent.prototype.buildDialog = function () {
	        var _this = this;
	        this.body.append("\n      <div id=\"fingerprint_dialog\">\n      \t<style>\n      \t\tZebraDialog_Buttons button, .ZebraDialog_Title {\n    \t\t\tfont-family: Roboto,sans-serif;\n    \t\t\tfont-size: 16px;\n    \t\t\tline-height: 24px;\n\t\t\t}\n\n\t\t\t.ZebraDialog_Buttons button {\n    \t\t\t display: inline-block;\n    white-space: nowrap;\n    zoom: 1;\n    *display: inline;\n    -webkit-border-radius: 6px;\n    -moz-border-radius: 6px;\n    border-radius: 6px;\n    color: #fff;\n    font-weight: 700;\n    margin-right: 5px!important;\n    min-width: 60px;\n    padding: 10px 15px;\n    text-align: center;\n    text-decoration: none;\n    text-shadow: 1px 0 2px #222;\n    _width: 60px;\n    background-color: #006dcc;\n    *background-color: #04c;\n    background-image: -moz-linear-gradient(top,#08c,#04c);\n    background-image: -webkit-gradient(linear,0 0,0 100%,from(#08c),to(#04c));\n    background-image: -webkit-linear-gradient(top,#08c,#04c);\n    background-image: -o-linear-gradient(top,#08c,#04c);\n    background-image: linear-gradient(to bottom,#08c,#04c);\n    background-repeat: repeat-x;\n    border-color: rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25);\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FF0088CC', endColorstr='#FF0044CC', GradientType=0);\n    filter: progid:DXImageTransform.Microsoft.gradient(enabled=false)\n\t\t\t}\n\t\t\t.ZebraDialog_Buttons button:hover, button:disabled {\n    background: #224467;\n    color: #fff\n}\n\t\t</style>\n        <div>\n          <table>\n          \t<tr>\n          \t<td style='width: 33% !important;'></td><td style='width: 33% !important;'></td><td style='width: 33% !important;'></td>\n\t\t\t</tr>\n            <tr>\n  \t\t      <td style=\"font-weight: bold;\">Readers:</label>\n  \t\t      <td colspan=\"2\">\n  \t\t        <select id=\"readers\" class=\"form-control form-control-sm\" style=\"width: 100%\">\n    \t\t      <option selected value=\"\">Choose...</option>\n  \t\t        </select>\n  \t\t      </td>  \n  \t\t    </tr>\n  \t\t    <tr class=\"fingers\">\n  \t\t    </tr>\n  \t\t    <tr class=\"fingers1\">\n\t\t\t</tr>\n\t\t  </table>\n\t\t</div>\n\t\t<table>\n\t\t  <tr>\n\t\t    <td>\n      \t      <p id=\"message\" style=\"color: red\"></p>\n      \t    </td>  \n      \t  </tr>\n\t    </table>\n      </div>\n        ");
	        this.updateScanners();
	        new $.Zebra_Dialog({
	            source: { inline: $('#fingerprint_dialog') },
	            title: 'Fingerprint',
	            buttons: [
	                {
	                    caption: 'Close'
	                },
	                {
	                    caption: 'Start identification',
	                    callback: function () {
	                        return false;
	                    }
	                },
	                {
	                    caption: 'Start enrollment',
	                    callback: function () {
	                        return false;
	                    }
	                },
	                {
	                    caption: 'Start verification',
	                    callback: function () {
	                        return false;
	                    }
	                }
	            ]
	        });
	        $('.ZebraDialog').removeClass('ZebraDialog_Icon');
	        $('.ZebraDialog_Button_0').replaceWith("<button class='ZebraDialog_Button_0'>Start Identification</button>");
	        $('.ZebraDialog_Button_1').replaceWith("<button class='ZebraDialog_Button_1'>Start Enrollment</button>");
	        $('.ZebraDialog_Button_2').replaceWith("<button class='ZebraDialog_Button_2'>Start Verification</button>");
	        $('#readers').change(function (evt) {
	            var value = $('#readers').val();
	            if (!!value) {
	                _this.enableButton('.ZebraDialog_Button_0');
	                _this.enableButton('.ZebraDialog_Button_1');
	                _this.enableButton('.ZebraDialog_Button_2');
	            }
	            else {
	                _this.disableButton('.ZebraDialog_Button_0');
	                _this.disableButton('.ZebraDialog_Button_1');
	                _this.disableButton('.ZebraDialog_Button_2');
	            }
	        });
	        $('.ZebraDialog_Button_3').css("background-image", "linear-gradient(to bottom,#b3c9d4,#55575d)");
	        this.disableButton('.ZebraDialog_Button_0').hide();
	        this.disableButton('.ZebraDialog_Button_1').hide();
	        this.disableButton('.ZebraDialog_Button_2').hide();
	    };
	    FingerprintComponent.prototype.disableButton = function (styleClass) {
	        return $("" + styleClass).attr('disabled', 'disabled');
	    };
	    FingerprintComponent.prototype.enableButton = function (styleClass) {
	        return $("" + styleClass).removeAttr('disabled');
	    };
	    FingerprintComponent.prototype.fingerValueToName = function (value) {
	        value = value.replace('_', ' ');
	        var finger = value.split(' ');
	        value = finger[0].charAt(0).toUpperCase() + finger[0].slice(1)
	            + ' ' + finger[1].charAt(0).toUpperCase() + finger[1].slice(1);
	        return value;
	    };
	    return FingerprintComponent;
	}());

	exports.FingerprintComponent = FingerprintComponent;
	window.FingerprintComponent = FingerprintComponent;

	Object.defineProperty(exports, '__esModule', { value: true });

}));
