/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function initialize() {
    addDatepicker("#date1");
    addDatepicker("#date2");
    addDatepicker("#date3");
    addDatepicker("#date4");

    $("#fundalHeight").mask("9?9", {placeholder:" "});
    $("#bodyWeight").mask("99?9", {placeholder:" "});
    $("#bp1").mask("9?99", {placeholder:" "});
    $("#bp2").mask("9?99", {placeholder:" "});
    
    $("#bp1").blur(function(event){
        $("#bp").val($("#bp1").val()+"/"+$("#bp2").val());
    })
    $("#bp2").blur(function(event){
        $("#bp").val($("#bp1").val()+"/"+$("#bp2").val());
    })
	
    $("#cd4").attr("disabled", true);
    $("#syphilisTestResult").attr("disabled", true);

    $("#cd4Ordered").change(function(event) {
        if ($("#cd4Ordered").val() == "Yes") {
                    $("#cd4").attr("disabled", false);
        }
        else {
            $("#cd4").attr("disabled", true);
        }		
    });
	
    $("#viralLoadCollected").change(function(event) {
        if ($("#viralLoadCollected").val() === "Yes") {
	   $("#dateSampleCollected").attr("disabled", false);
	}
        else {
            $("#dateSampleCollected").val(""); 
            $("#dateSampleCollected").attr("disabled", true);
        }		
    });
	
    $("#save_button").bind("click", function(event){
        if($("#userGroup").html() == "Data Analyst") {
            $("#lamisform").attr("action", "Error_message");
            return true;                        
        }
        else {
            if(validateForm()) {
                if(updateRecord) {
                    $("#lamisform").attr("action", "Maternalfollowup_update");                
                } 
                else {
                    $("#lamisform").attr("action", "Maternalfollowup_save");                
                }
                return true;                        
            } 
            else {
                return false;
            }
        }
    });   
				
    $("#delete_button").bind("click", function(event){
        if($("#userGroup").html() == "Data Analyst") {
            $("#lamisform").attr("action", "Error_message");
        }
        else {
            $("#lamisform").attr("action", "Maternalfollowup_delete");
        }
        return true;
    });

    $("#close_button").bind("click", function(event){
        $("#lamisform").attr("action", "Maternalfollowup_search");
        return true;
    });  	
    
    $("#timeHivDiagnosis").bind("change", function(event){ 
        //TODO: Check
        var dated = $("#date3").val();
        if(dated.length !== 0){
            var dateVisit = $("#date1").val();
            if($("#timeHivDiagnosis").val().includes("Newly")){
                if(dateVisit.length !== 0){
                    if(parseInt(compare(dateVisit, dated)) === -1) {
                        var message = "ART commencement date "+ dated +" cannot be before date newly tested positive which is "+ dateVisit;
                        $("#timeHivDiagnosis").val("");
                        alert(message);
                    }
                }
            }
        }              
    });
    
    $("#date1").bind("change", function(event){ 
        //TODO: Check
        var dated = $("#date3").val();
        if(dated.length !== 0){
            var dateVisit = $("#date1").val();
            if($("#timeHivDiagnosis").val().includes("Newly")){
                if(parseInt(compare(dateVisit, dated)) === -1) {
                    var message = "ART commencement date "+ dated +" cannot be before date newly tested positive";
                    $("#timeHivDiagnosis").val("");
                    alert(message);
                }
            }
        }              
    });
}

function addDatepicker(id) {
    $(id).mask("99/99/9999");
    $(id).datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+2",
        constrainInput: true,
        buttonImageOnly: true,
        buttonImage: "/images/calendar.gif"
    });
}

var newFollowUp = true;
function populateForm(maternalfollowupList) {
    if($.isEmptyObject(maternalfollowupList)) {
        updateRecord = false;
        resetButtons();

        $("#maternalfollowupId").val("");
        $("#date1").val("");
        $("#date2").val("");
        $("#bodyWeight").val("");
        $("#fetalPresentation").val("");
        $("#fundalHeight").val("");
        $("#bp").val("");
        $("#bp1").val("");
        $("#bp2").val("");
//        $("#date2").val("");
//        $("#date3").val("");
        $("#timeHivDiagnosis").val("");
        $("#arvRegimenCurrent").val("");
        $("#viralLoadCollected").val("");
        $("#arvRegimenPast").val("");
        $("#gestationalAge").val("");
        $("#typeOfVisit").val("");
        $("#screenPostPartum").attr("checked", false);
        $("#syphilisScreened").val("");
        $("#cd4Ordered").val("");
	$("#cd4").val("");
        $("#tbStatus").val("");
        $("#visitStatus").val("");
        $("#counselNutrition").attr("checked", false);
        $("#counselFeeding").attr("checked", false);
        $("#counselFamilyPlanning").attr("checked", false);
        $("#viralLoadCollected").val("");
        $("#familyPlanningMethod").val("");
        $("#referred").val("");
        $("#date4").val("");
        $("#partnerHivStatus").val("");
        $("#partnerNotification").val("");
        $("#fp").attr("checked", false);
        $("#art").attr("checked", false);
        $("#others").attr("checked", false);
    }
    else {
        updateRecord = true;
        initButtonsForModify();
        newFollowUp = false;

        $("#patientId").val(maternalfollowupList[0].patientId);
        $("#maternalfollowupId").val(maternalfollowupList[0].maternalfollowupId);
	$("#ancId").val(maternalfollowupList[0].ancId);
        date = maternalfollowupList[0].dateVisit;
	$("#date1").val(dateSlice(date));
        date = maternalfollowupList[0].dateSampleCollected;
	$("#date2").val(dateSlice(date));
        $("#bodyWeight").val(maternalfollowupList[0].bodyWeight);
        $("#bp").val(maternalfollowupList[0].bp);
        var bp = maternalfollowupList[0].bp;
        var bps = new Array();
        bps = bp.split("/");
        $("#bp1").val(bps[0]);
        $("#bp2").val(bps[1]);
        $("#fetalPresentation").val(maternalfollowupList[0].fetalPresentation);
        $("#fundalHeight").val(maternalfollowupList[0].fundalHeight);
        $("#tbStatus").val(maternalfollowupList[0].tbStatus);
        $("#visitStatus").val(maternalfollowupList[0].visitStatus);
        $("#typeOfVisit").val(maternalfollowupList[0].typeOfVisit);
        $("#timeHivDiagnosis").val(maternalfollowupList[0].timeHivDiagnosis);
        $("#viralLoadCollected").val(maternalfollowupList[0].viralLoadCollected);
        $("#arvRegimenPast").val(maternalfollowupList[0].arvRegimenPast);
        $("#arvRegimenCurrent").val(maternalfollowupList[0].arvRegimenCurrent);
        $("#gestationalAge").val(maternalfollowupList[0].gestationalAge);
        $("#typeOfVisit").val(maternalfollowupList[0].typeOfVisit);
        date = maternalfollowupList[0].dateArvRegimenCurrent;
        $("#date3").val(dateSlice(date));
        if (maternalfollowupList[0].screenPostPartum === "1") {
            $("#screenPostPartum").attr("checked", true);
        }
        $("#cd4Ordered").val(maternalfollowupList[0].cd4Ordered);
//	$("#cd4").val(maternalfollowupList[0].cd4);
//        if ($("#cd4").val().length !== 0) {
//            $("#cd4").attr("disabled", false);
//        }
	if (maternalfollowupList[0].counselNutrition === "1") {
            $("#counselNutrition").attr("checked", true);
        }
	if (maternalfollowupList[0].counselFeeding === "1") {
            $("#counselFeeding").attr("checked", true);
        }
	if (maternalfollowupList[0].counselFamilyPlanning === "1") {
            $("#counselFamilyPlanning").attr("checked", true);
        }

        $("#familyPlanningMethod").val(maternalfollowupList[0].familyPlanningMethod);
        $("#referred").val(maternalfollowupList[0].referred);
        date = maternalfollowupList[0].dateNextVisit;
        $("#date4").val(dateSlice(date));	
    }    

    $.ajax({
        url: "Patient_retrieve.action",
        dataType: "json",                    
        success: function(patientList) {
            // set patient id and number for which infor is to be entered
            $("#patientId").val(patientList[0].patientId);
            $("#hospitalNum").val(patientList[0].hospitalNum);
            $("#patientInfor").html(patientList[0].surname + " " + patientList[0].otherNames);
            date = patientList[0].dateStarted;
            if(date !== ""){
                $("#date3").val(dateSlice(date));
                $("#date3").attr("disabled", true);
            }else{
                $("#date3").attr("disabled", false);               
            } 
            
            dueViralLoad(patientList[0].dueViralLoad, patientList[0].viralLoadType);
            getAncLastVisit(patientList[0].patientId);
        }                    
    }); //end of ajax call 
    
    $.ajax({
        url: "Partnerinfo_retrieve.action",
        dataType: "json",                    
        success: function(partnerinformationList) {		    
            if(!$.isEmptyObject(partnerinformationList)) {
                $("#partnerinformationId").val(partnerinformationList[0].partnerinformationId);
                $("#partnerNotification").val(partnerinformationList[0].partnerNotification);
                $("#partnerHivStatus").val(partnerinformationList[0].partnerHivStatus);
                var partnerReferred = partnerinformationList[0].partnerReferred;
                if (partnerReferred.indexOf("FP") != -1) $("#fp").attr("checked", true);
                if (partnerReferred.indexOf("ART") != -1) $("#art").attr("checked", true);
                if (partnerReferred.indexOf("OTHERS") != -1) $("#others").attr("checked", true);
            }
        }                    
    }); //end of ajax call
}

function getAncLastVisit(patientId) {
    //Retrieve the last ANC visit ID for this client 
    console.log("Got Here!");
    $.ajax({
        url: "Anc_retrieve_last.action?patientId="+patientId,
        dataType: "json",                    
        success: function(ancLast) {	
            console.log(ancLast); 
            if(!$.isEmptyObject(ancLast)) {
                $("#ancId").val(ancLast[0].ancId);               
//                $("#timeHivDiagnosis").val(ancLast[0].timeHivDiagnosis);
//                $("#timeHivDiagnosis").attr("disabled", true);
                date = ancLast[0].dateArvRegimenCurrent;
//                $("#date3").val(dateSlice(date));
//                $("#date3").attr("disabled", true);
                //date = ancLast[0].dateConfirmedHiv;
//                $("#date2").val(dateSlice(date));
//                $("#date2").attr("disabled", true);
                date = ancLast[0].lmp;
                calculateGA(dateSlice(date));
            }		
        }                    
    });        
}

function calculateGA(dateVar){
    if (dateVar.length !== 0) {
        var dateString = dateVar.slice(3,5)+"/"+dateVar.slice(0,2)+"/"+dateVar.slice(6);
        var date = new Date(dateString);
        date.setMonth(date.getMonth() + 9); // add nine months 
        date.setDate(date.getDate() + 7); // add seven days
        var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var month = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
//        $("#date4").val("" + day + "/" + month + "/" + date.getFullYear());

        var second = 1000, minute = second*60, hour = minute*60, day = hour*24, week = day*7;
        var weeks = (new Date().getTime() - new Date(dateString).getTime()) / week; 
        var weekString = weeks.toString().substring(0,2);
        console.log("G.A is: "+weekString);
        if(!weekString.includes("."))
            $("#gestationalAge").val(weekString.substring(0,2));
        else
            $("#gestationalAge").val(weekString.substring(0,1));
    }
}

function getMonthDisplayName(month) {
    switch(month) {
        case "01" :return "Jan";
        case "02" :return "Feb";
        case "03" :return "Mar";
        case "04" :return "Apr";
        case "05" :return "May";
        case "06" :return "Jun";
        case "07" :return "Jul";
        case "08" :return "Aug";
        case "09" :return "Sep";
        case "10" :return "Oct";
        case "11" :return "Nov";
        case "12" :return "Dec";
        default :return undefined;
    }   
} 

function validateForm() {
    var validate = true;
    
    $("#date1").datepicker("option", "altField", "#dateVisit");    
    $("#date1").datepicker("option", "altFormat", "mm/dd/yy"); 
    $("#date2").datepicker("option", "altField", "#dateSampleCollected");    
    $("#date2").datepicker("option", "altFormat", "mm/dd/yy");
    $("#date3").datepicker("option", "altField", "#dateArvRegimenCurrent");    
    $("#date3").datepicker("option", "altFormat", "mm/dd/yy");
    $("#date4").datepicker("option", "altField", "#dateNextVisit");    
    $("#date4").datepicker("option", "altFormat", "mm/dd/yy");
    
    return validate;	
}	